#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ORIG="$(pwd)"

DEFAULT_MAKE_JOBS=$(nproc --all --ignore=2)
USING_MAKE_JOBS=${MAKE_JOBS:-$DEFAULT_MAKE_JOBS}

echo "using $USING_MAKE_JOBS parallel jobs"

cd $DIR/build

case "$1" in
    *)      rm -fr /tmp/test_causal*
            export CTEST_OUTPUT_ON_FAILURE=1
            make -j$USING_MAKE_JOBS all test ;;
esac

cd $ORIG
