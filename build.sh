#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. $DIR/set_toolchains.sh

LINUX_STRING=x86_64-linux-gnu
ANDROID_STRING=aarch64-linux-android

DEFAULT_MAKE_JOBS=$(nproc --all --ignore=2)
USING_MAKE_JOBS=${MAKE_JOBS:-$DEFAULT_MAKE_JOBS}

export PATH=${DIR}/tool/cmake/bin:$PATH

echo "using $USING_MAKE_JOBS parallel jobs"

case "$1" in
    cleandep)       cd $DIR/contrib && rm -fr src/.* src/Makefile src/config.mak build* $LINUX_STRING $ANDROID_STRING; exit 0 ;;
    cleandepandtar) cd $DIR/contrib && rm -fr src/.* src/Makefile src/config.mak tarballs/* build* $LINUX_STRING $ANDROID_STRING; exit 0 ;;
    dep)            cd $DIR/contrib && mkdir -p build_$LINUX_STRING && cd build_$LINUX_STRING &&
                    ../bootstrap && make install ;;
    depand)         export ANDROID_SDK=$DIR/tool/android
                    export ANDROID_NDK=$ANDROID_SDK/ndk/25.2.9519653
                    export ANDROID_ABI=arm64-v8a
                    ./contrib_build_android.sh ;;
    pubdep)         cd $DIR/contrib && tar cJf ../../causal-cpp-support/contrib/x86_64-linux-gnu.tar.xz x86_64-linux-gnu ;;
    pubdepand)      cd $DIR/contrib && tar cJf ../../causal-cpp-support/contrib/aarch64-linux-android.tar.xz aarch64-linux-android ;;
    pubdeptar)      cd $DIR/contrib/src && ../bootstrap && make fetch-all && cd ../tarballs && cp -afr * ../../../causal-cpp-support/contrib/tarballs/ ;;

    doc)            cd $DIR/build &&
                    make -j$USING_MAKE_JOBS doc ;;
    
    pkg)            cd $DIR/build &&
                    make -j$USING_MAKE_JOBS package ;;

    linux_gcc)      export SYSROOT=${DIR}/contrib/x86_64-linux-gnu
                    export LIBRARY_PATH=${SYSROOT}/lib
                    export LD_LIBRARY_PATH=${SYSROOT}/lib
                    export PKG_CONFIG_PATH=${SYSROOT}/lib/pkgconfig
                    #export PATH=${DIR}/tool/cmake/bin:${PATH}
                
                    rm -fr $DIR/build_linux_gcc $DIR/root_linux_gcc && mkdir -p $DIR/root_linux_gcc && mkdir -p $DIR/build_linux_gcc && cd $DIR/build_linux_gcc &&
                    cp -afr $DIR/contrib/x86_64-linux-gnu/* $DIR/root_linux_gcc/ &&
                    cmake -G "Unix Makefiles" \
                            -DCMAKE_BUILD_TYPE=Release \
                            -DCMAKE_PREFIX_PATH=${SYSROOT} \
                            -DCMAKE_C_COMPILER=$BIN_GCC   -DCMAKE_CXX_COMPILER=$BIN_GPP \
                            -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=NEVER \
                            -DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE=NEVER \
                            -DBUILD_SHARED_LIBS=ON \
                            -DBUILD_STATIC_LIBS=ON \
                            -DCMAKE_INSTALL_PREFIX=$DIR/root_linux_gcc \
                            ../ &&
                    make -j$USING_MAKE_JOBS all test install
                    ;;
    linux_clang)    export SYSROOT=${DIR}/contrib/x86_64-linux-gnu
                    export LIBRARY_PATH=${SYSROOT}/lib
                    export LD_LIBRARY_PATH=${SYSROOT}/lib
                    export PKG_CONFIG_PATH=${SYSROOT}/lib/pkgconfig
                    #export PATH=${DIR}/tool/cmake/bin:${PATH}
                    
                    rm -fr $DIR/build_linux_clang $DIR/root_linux_clang && mkdir -p $DIR/root_linux_clang && mkdir -p $DIR/build_linux_clang && cd $DIR/build_linux_clang &&
                    cp -afr $DIR/contrib/x86_64-linux-gnu/* $DIR/root_linux_clang/ &&
                    cmake -G "Unix Makefiles" \
                            -DCMAKE_BUILD_TYPE=Release \
                            -DCMAKE_PREFIX_PATH=${SYSROOT} \
                            -DCMAKE_C_COMPILER=$BIN_CLANG -DCMAKE_CXX_COMPILER=$BIN_CLANGPP \
                            -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=NEVER \
                            -DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE=NEVER \
                            -DBUILD_SHARED_LIBS=ON \
                            -DBUILD_STATIC_LIBS=ON \
                            -DCMAKE_INSTALL_PREFIX=$DIR/root_linux_clang \
                            ../ &&
                    make -j$USING_MAKE_JOBS all test install
                    ;;
    android)        export SYSROOT=${DIR}/contrib/aarch64-linux-android
                    export LIBRARY_PATH=${SYSROOT}/lib
                    export LD_LIBRARY_PATH=${SYSROOT}/lib
                    export PKG_CONFIG_PATH=${SYSROOT}/lib/pkgconfig
                    #export PATH=${DIR}/tool/cmake/bin:${PATH}

                    export JAVA_HOME=/usr/lib/jvm/default-java
                    export ANDROID_SDK=${DIR}/tool/android
                    export ANDROID_NDK=$ANDROID_SDK/ndk/25.2.9519653
                    export ANDROID_ABI=arm64-v8a
                    
                    rm -fr $DIR/build_android && mkdir -p $DIR/build_android && cd $DIR/build_android &&
                    cmake   -DCMAKE_BUILD_TYPE=Release \
                            -DQT_HOST_PATH=${SYSROOT}/../x86_64-linux-gnu \
                            -DBUILD_SHARED_LIBS=OFF \
                            -DBUILD_STATIC_LIBS=ON \
		            -DANDROID_STL=c++_static \
                            -DANDROID_ABI=$ANDROID_ABI \
                            -DANDROID_SDK=$ANDROID_SDK \
                            -DANDROID_NDK=$ANDROID_NDK \
                            -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake \
                            -DANDROID_NATIVE_API_LEVEL=25 \
                            -DANDROID_PLATFORM=android-33 \
                            -DANDROID_TOOLCHAIN=clang \
                            -DANDROID_KEYSTORE=${DIR}/tool/android/debug.keystore \
                            -DANDROID_KEY=debug \
                            -DANDROID_KEYSTORE_PASSWORD=debug123 \
                            .. &&
                    make -j$USING_MAKE_JOBS all
                    ;;

    android_verbose)    export SYSROOT=${DIR}/contrib/aarch64-linux-android
                        export LIBRARY_PATH=${SYSROOT}/lib
                        export LD_LIBRARY_PATH=${SYSROOT}/lib
                        export PKG_CONFIG_PATH=${SYSROOT}/lib/pkgconfig
                        #export PATH=${DIR}/tool/cmake/bin:${PATH}

                        export ANDROID_SDK=${DIR}/tool/android
                        export ANDROID_NDK=$ANDROID_SDK/ndk/25.2.9519653
                        export ANDROID_ABI=arm64-v8a
                        
                        rm -fr $DIR/build_android && mkdir -p $DIR/build_android && cd $DIR/build_android &&
                        cmake   -DCMAKE_BUILD_TYPE=Release \
                                -DBoost_DEBUG=ON \
                                -DQT_HOST_PATH=${SYSROOT}/../x86_64-linux-gnu \
                                -DCMAKE_VERBOSE_MAKEFILE=ON \
                                -DBUILD_SHARED_LIBS=OFF \
                                -DBUILD_STATIC_LIBS=ON \
		                -DANDROID_STL=c++_static \
                                -DANDROID_ABI=$ANDROID_ABI \
                                -DANDROID_SDK=$ANDROID_SDK \
                                -DANDROID_NDK=$ANDROID_NDK \
                                -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake \
                                -DANDROID_NATIVE_API_LEVEL=25 \
                                -DANDROID_PLATFORM=android-33 \
                                -DANDROID_TOOLCHAIN=clang \
                                -DANDROID_KEYSTORE=${DIR}/tool/android/debug.keystore \
                                -DANDROID_KEY=debug \
                                -DANDROID_KEYSTORE_PASSWORD=debug123 \
                                --trace-expand .. &&
                        make -j$USING_MAKE_JOBS all
                        ;;

    test)               export CTEST_OUTPUT_ON_FAILURE=1
    
                        rm -fr $DIR/build_gcc $DIR/root_gcc && mkdir -p $DIR/build_gcc && cd $DIR/build_gcc &&
                        cmake -G "Unix Makefiles" \
                            -DCMAKE_BUILD_TYPE=Release \
                            -DCMAKE_C_COMPILER=$BIN_GCC \
                            -DCMAKE_CXX_COMPILER=$BIN_GPP \
                            -DCMAKE_INSTALL_PREFIX=$DIR/root_gcc \
                            ../ &&
                        make -j$USING_MAKE_JOBS all test install package &&
                        
                        rm -fr $DIR/build_clang $DIR/root_clang && mkdir -p $DIR/build_clang && cd $DIR/build_clang &&
                        cmake -G "Unix Makefiles" \
                            -DCMAKE_BUILD_TYPE=Release \
                            -DCMAKE_C_COMPILER=$BIN_CLANG \
                            -DCMAKE_CXX_COMPILER=$BIN_CLANGPP \
                            -DCMAKE_INSTALL_PREFIX=$DIR/root_clang \
                            ../ &&
                        make -j$USING_MAKE_JOBS all test install package
                        ;;
    *)                  cd $DIR/build &&
                        make -j$USING_MAKE_JOBS all
                        ;;
esac