#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. $DIR/set_toolchains.sh

export PATH=${DIR}/tool/cmake/bin:$PATH

mkdir -p $DIR/build
cd $DIR/build

case "$1" in
    gcc_release)        cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON \
                          -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
                          -DCMAKE_C_COMPILER=$BIN_GCC   -DCMAKE_CXX_COMPILER=$BIN_GPP \
                          -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true ../ ;;
    gcc_debug)          cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON \
                          -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
                          -DCMAKE_C_COMPILER=$BIN_GCC   -DCMAKE_CXX_COMPILER=$BIN_GPP \
                          -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true ../ ;;
    gcc_debug_verbose)  cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
                          -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
                          -DCMAKE_C_COMPILER=$BIN_GCC   -DCMAKE_CXX_COMPILER=$BIN_GPP \
                          -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true ../ ;;
    clang_release)      cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON \
                          -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
                          -DCMAKE_C_COMPILER=$BIN_CLANG -DCMAKE_CXX_COMPILER=$BIN_CLANGPP \
                          -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true ../ ;;
    clang_debug)        cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON \
                          -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
                          -DCMAKE_C_COMPILER=$BIN_CLANG -DCMAKE_CXX_COMPILER=$BIN_CLANGPP \
                          -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true ../ ;;
    clang_debug_verbose)cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
                          -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
                          -DCMAKE_C_COMPILER=$BIN_CLANG -DCMAKE_CXX_COMPILER=$BIN_CLANGPP \
                          -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true ../ ;;
esac