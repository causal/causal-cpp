#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ -z "$1" ]
  then
    echo "No version provided"
    exit
fi

cd $DIR

if grep -q "project(causal VERSION $1)" CMakeLists.txt
then
    BRANCH="$(git rev-parse --abbrev-ref HEAD)"
    if [[ "$BRANCH" != "main" ]]; then
        echo 'You are not on main branch, releases can only be created from main';
        exit 1;
    fi

    git pull
    ./build.sh test || exit $?
    git tag -a v$1 -m "release version $1" || exit $?
    git push origin v$1 || exit $?
else
    echo "CMakeLists.txt does not contain correct project version information"
fi