#!/bin/bash

export PATH=${DIR}/tool/cmake/bin:$PATH

rm -fr build
rm -fr build_*
rm -fr CMakeFiles
rm -fr cmake_install.cmake
rm -fr CMakeCache.txt
rm -fr CTestTestfile.cmake
rm -fr CMakeLists.txt.user
rm -fr Makefile