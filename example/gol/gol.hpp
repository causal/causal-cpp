// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <sstream>
#include <fstream>

#include <boost/functional/hash.hpp>

#include <msgpack.hpp>

#include <causal/core/wirks.hpp>
#include <causal/process/thread.hpp>
#include <causal/sync/unite.hpp>
#include <causal/sync/timing.hpp>
#include <causal/sync/iterate.hpp>
#include <causal/data/memory.hpp>
#include <causal/data/msgpack.hpp>
#include <causal/data/json.hpp>
#include <causal/system/observe.hpp>
#include <causal/system/cellular/flipping.hpp>

namespace cc = causal::core;
namespace cy = causal::sync;
namespace cd = causal::data;
namespace cs = causal::system;
namespace cp = causal::process;

struct gol_id {
    long x = 0, y = 0;
    MSGPACK_DEFINE(x, y)

    operator std::string() const noexcept {
        std::stringstream ss;
        ss << "(" << this->x << "," << this->y << ")";
        return ss.str();
    }
};

bool operator<(const gol_id& lhs, const gol_id& rhs) {
    return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
}

bool operator==(const gol_id& lhs, const gol_id& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

namespace std
{
    template<> struct hash<gol_id>
    {
        std::size_t operator()(const gol_id& p) const noexcept
        {
            size_t seed = 0;
            boost::hash_combine(seed, p.x);
            boost::hash_combine(seed, p.y);

            return seed;
        }
    };
}

using gol_space = cd::memory_space<gol_id>;

using dispatcher = cp::concurrent_dispatcher<>;
//using dispatcher = cp::managed_dispatcher<>;

class gol_observation final : public cs::observation<std::pair<gol_id, bool>> {
private:
    std::function<void(const std::pair<gol_id, bool>&)> callback;

    gol_observation(std::function<void(const std::pair<gol_id, bool>&)> cb)
    : callback(cb) {}

public:
    static std::shared_ptr<gol_observation> make(std::function<void(const std::pair<gol_id, bool>&)> cb) {
        return std::shared_ptr<gol_observation>(new gol_observation(cb));
    }

    virtual void percept(const std::pair<gol_id, bool>& v) const override {
        this->callback(v);
    }

    bool operator==(const gol_observation& other) const {
        return this == &other;
    }
};

template<typename S=gol_space>
struct GolCell;

template<typename S>
using GolFlipper = cs::Flipper<3, 2, 3, GolCell<S>>;

template<typename S>
struct GolCell final : GolFlipper<S>, cs::Observable<gol_observation> {
    typedef GolFlipper<S> flipper_type;
    
    //cy::unifier_back union_start;
    cy::unifier_front union_next;

    gol_id id;

    bool new_state = false;

    MSGPACK_DEFINE(id, MSGPACK_BASE(flipper_type))

    GolCell() = default;

    GolCell(std::shared_ptr<S> s, const gol_id id, const std::shared_ptr<gol_observation> o) : id(id) {
        this->focus(o);

        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x-1, id.y-1}, s, gol_id{id.x-1, id.y-1}, o));
        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x-1, id.y  }, s, gol_id{id.x-1, id.y  }, o));
        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x-1, id.y+1}, s, gol_id{id.x-1, id.y+1}, o));

        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x  , id.y-1}, s, gol_id{id.x  , id.y-1}, o));
        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x  , id.y+1}, s, gol_id{id.x  , id.y+1}, o));

        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x+1, id.y-1}, s, gol_id{id.x+1, id.y-1}, o));
        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x+1, id.y  }, s, gol_id{id.x+1, id.y  }, o));
        this->neighbours.insert(s->template get<GolCell>(gol_id{id.x+1, id.y+1}, s, gol_id{id.x+1, id.y+1}, o));
    }

    static void Calc(cc::aspect_ptr<GolCell>& me, const typename flipper_type::facet_type& os, std::shared_ptr<cy::iterator> it) {
        auto state_change = GolCell::calc(os);
        me->new_state = state_change ? *state_change : me->state;
        
        me->project({me->id, me->new_state});
        
        cc::act_synced(it->iterate(), Apply, me, it);
    }
    
    static void Apply(cc::aspect_ptr<GolCell>& me, std::shared_ptr<cy::iterator> it) {
        if(me->state != me->new_state) {
            me->state = me->new_state;
            for(auto nit = me->neighbours.begin(); nit != me->neighbours.end(); ++nit)
                cc::act_synced(it->iterate(), Next, *nit, it);
            cc::act_synced(it->iterate(), Next, me, it);
        } else if(!me->state)
            me.dispose();
    }

    static void Measure(cc::aspect_ptr<GolCell>& me) {
        if(me.is_certain()) {
            me->project({me->id, me->state});
        }
    }

    static void Set(cc::aspect_ptr<GolCell>& me, std::shared_ptr<cy::iterator> it, const bool v) {
        me->new_state = me->state = v;

        me->project({me->id, me->state});

        cc::act_synced(it->iterate(), Trigger, me, it);
    }

    static void Toggle(cc::aspect_ptr<GolCell>& me, std::shared_ptr<cy::iterator> it) {
        me->state = !me->state;

        me->project({me->id, me->state});

        cc::act_synced(it->iterate(), Trigger, me, it);
    }

    static void Trigger(cc::aspect_ptr<GolCell>& me, std::shared_ptr<cy::iterator> it) {
        me->new_state = me->state;

        me->project({me->id, me->new_state});

        cc::act_synced(it->iterate(), Trigger_n, me, me->neighbours, it);
    }
    
    static void Trigger_n(cc::aspect_ptr<GolCell>& me, const typename flipper_type::facet_type& os, std::shared_ptr<cy::iterator> it) {
        if(me->state) {
            for(auto nit = os.begin(); nit != os.end(); ++nit)
                cc::act_synced(it->iterate(), Next, *nit, it);
            cc::act_synced(it->iterate(), Next, me, it);
        } else
            me.dispose();
    }

    static void Next(cc::aspect_ptr<GolCell>& me, std::shared_ptr<cy::iterator> it) {
        // me shall not be const since it shall get certain if uncertain
        cc::act_synced(cc::synchrons(me->union_next.make(), it->iterate()),
                            Calc, me, me->neighbours, it);
    }
};

enum gol_action_type {
    toggle,
    set,
    unset
};

struct gol_action {
    gol_id id;
    gol_action_type t;
};

class GolView;
struct GolController {
public:
    bool running = false;
    u_short delay = 0;
    size_t count = 0;

    std::vector<gol_action> actions;

    const std::shared_ptr<dispatcher> dispatch_sys;
    const std::shared_ptr<cc::branch> branch_sys;
    const std::shared_ptr<gol_space> space;

    GolView* const view;

    const std::shared_ptr<cy::iterator> iter;

    GolController(GolView* v)
    : dispatch_sys(dispatcher::make()),
      branch_sys(cc::branch::make(this->dispatch_sys)),
      space(gol_space::make()),
      view (v),
      iter(cy::iterator::make()) {}

    static void Update(cc::aspect_ptr<GolController>& ctrl, GolView* view);    
    static void Trigger(cc::aspect_ptr<GolController>& ctrl, std::shared_ptr<cy::iterator> it);
    static void Iterate(cc::aspect_ptr<GolController>& ctrl, std::shared_ptr<cy::iterator> it);
    static void Execute(GolController& ctrl, gol_action a);
    static void Copy(std::string t, cc::aspect_ptr<const GolController>& ctrl, long off_x, long off_y);
    static void Copy_cell(const GolCell<cd::msgpack_space>& c, cc::aspect_ptr<const GolController>& ctrl, long off_x, long off_y);
    static void Load(std::string t, const GolController& ctrl, long off_x, long off_y);
    static void Measure(const GolController& ctrl);
    static void Dispose(cc::aspect_ptr<GolController>& ctrl);
};

class GolView {
protected:
    mutable std::shared_mutex mtx;
    u_int size_x = 0, size_y = 0;
    long off_x = 0, off_y = 0;
    u_char* pixel_buffer = nullptr;

    void set_size(const u_int x, const u_int y) {
        std::unique_lock lk(this->mtx);
        if(this->pixel_buffer) {
            delete[] this->pixel_buffer;
            this->pixel_buffer = nullptr;
        }
        if(x > 0 && y > 0) {
            this->size_x = x;
            this->size_y = y;
            this->pixel_buffer = new u_char[x*y*4];
            for(u_long i = 0; i < x*y*4; i++)
                this->pixel_buffer[i] = 0;
        } else {
            this->size_x = 0;
            this->size_y = 0;
        }
    }

    void set_off(const long x, const long y) {
        std::unique_lock lk(this->mtx);
        this->off_x = x;
        this->off_y = y;
    }

    void use_pixels(std::function<void(u_char*)> cb) const {
        if(this->mtx.try_lock()) {
            //std::unique_lock lk(this->mtx);
            try {
                cb(this->pixel_buffer);
            } catch(...) {
                this->mtx.unlock();
                throw;
            }
            this->mtx.unlock();
        }
    }

public:
    const std::shared_ptr<gol_observation> observation;
    cc::aspect_ptr<GolController> controller;

    GolView()
    : observation(gol_observation::make([this](const gol_observation::type& v){
        std::shared_lock<std::shared_mutex> lk(this->mtx);
        if(this->pixel_buffer) {
            long x = v.first.x-this->off_x,
                   y = v.first.y-this->off_y;

            if(x >= 0 && y >= 0 && x < this->size_x && y < this->size_y) {
                auto* px = this->pixel_buffer + y*this->size_x*4 + x*4;
                px[0] = v.second ? 255 : 0;
                px[1] = v.second ? 255 : 0;
                px[2] = v.second ? 255 : 0;
                px[3] = 255;
            }
        }
      })) {}

    virtual ~GolView() {
        std::unique_lock lk(this->mtx);
        if(this->pixel_buffer)
            delete[] this->pixel_buffer;
    }

    virtual void update(cc::aspect_ptr<GolController>& ctrl) = 0;
};

void GolController::Update(cc::aspect_ptr<GolController>& ctrl, GolView* view) {
    for(auto& a : ctrl->actions) {
        auto r = ctrl->space->get<GolCell<>>(a.id, ctrl->space, a.id, ctrl->view->observation);
        switch(a.t) {
        case gol_action_type::toggle:
            if(!ctrl->running)
                ctrl->branch_sys->act(GolCell<>::Toggle, r, ctrl->iter);
            break;
        case gol_action_type::set:
            if(!ctrl->running)
                ctrl->branch_sys->act(GolCell<>::Set, r, ctrl->iter, true);
            break;
        case gol_action_type::unset:
            if(!ctrl->running)
                ctrl->branch_sys->act(GolCell<>::Set, r, ctrl->iter, false);
            break;
        }
    }
    ctrl->actions.clear();

    ctrl->view->update(ctrl);
}

void GolController::Trigger(cc::aspect_ptr<GolController>& ctrl, std::shared_ptr<cy::iterator> it) {
    ctrl->branch_sys->act_synced(it->join(), Iterate, ctrl, it);
    ctrl->running = true;
    it->next();
}

void GolController::Iterate(cc::aspect_ptr<GolController>& ctrl, std::shared_ptr<cy::iterator> it) {
    if(ctrl.is_certain()) {
        auto subrnd = it->get_round() % 3;
        if(subrnd == 0) { // only after a full round consisting of 3 subrounds
            if(ctrl->running) {
                ctrl->count++;
                cc::act_synced(it->join(), Iterate, ctrl, it);
                it->next();
            }
        } else if(subrnd == 2 && ctrl->delay > 0) {
            cc::act_synced(cc::synchrons(cy::delay_ms::make(ctrl->delay), it->join()), Iterate, ctrl, it);
            it->next();
        } else {
            cc::act_synced(it->join(), Iterate, ctrl, it);
            it->next();
        }
    } else
        it->reset();
}

void GolController::Execute(GolController& ctrl, gol_action a) {
    ctrl.actions.push_back(a);
}

void GolController::Copy(std::string t, cc::aspect_ptr<const GolController>& ctrl, long off_x, long off_y) {
    auto j = cd::json_junction::make(t);
    auto s = cd::msgpack_space::make(j);
    s->register_types<GolCell<cd::msgpack_space>>();

    cc::act(s->explode<GolCell<cd::msgpack_space>>(GolController::Copy_cell, ctrl, off_x, off_y));
}

void GolController::Copy_cell(const GolCell<cd::msgpack_space>& c, cc::aspect_ptr<const GolController>& ctrl, long off_x, long off_y) {
    gol_id id = {c.id.x+off_x, c.id.y+off_y};
    auto r = ctrl->space->get<GolCell<>>(id, ctrl->space, id, ctrl->view->observation);
    ctrl->branch_sys->act(GolCell<>::Set, r, ctrl->iter, c.state);
}

void GolController::Load(std::string t, const GolController& ctrl, long off_x, long off_y) {
    // RLE loader code adapted from https://github.com/claby2/gol
    static const std::string number_characters = "1234567890";
    static const std::string valid_rule_string_characters = "b/s123456789";

    auto isValidRuleString = [&](std::string rule_string) {
        std::transform(rule_string.begin(), rule_string.end(), rule_string.begin(),
            [](unsigned char c){ return std::tolower(c); });
        return 
            (std::count(rule_string.begin(), rule_string.end(), 'b') == 1) && 
            (std::count(rule_string.begin(), rule_string.end(), 's') == 1) && 
            (std::count(rule_string.begin(), rule_string.end(), '/') == 1) &&
            (rule_string.find_first_not_of("b/s123456789") == std::string::npos);
    };

    std::istringstream file(t);
    std::string line;
    std::string board_string;     // Composite of the lines that represent the board
    int line_number = 0;
    bool reading_number = false;  // Represents if <run_count> number is currently being recorded
    std::string current_number;   // Current <run_count> number being read
    int current_row = 0;
    int current_column = 0;
    while(std::getline(file, line)) {
        line.erase(remove(line.begin(),line.end(),' '),line.end()); // Remove whitespace
        std::transform(line.begin(), line.end(), line.begin(),
            [](unsigned char c){ return std::tolower(c); }); // Transform to lower case
        if(line[0] != '#') {
            if(line_number == 0) {
                bool reading_x = false;     // Represents if currently reading x property
                bool reading_y = false;     // Represents if currently reading y property
                bool reading_rule = false;  // Represents if currently reading rule property
                std::string x;              // Holds x property
                std::string y;              // Holds y property
                std::string rule;           // Holds rule string
                for(size_t i = 0; i < line.length(); i++) {
                    if(line[i] == 'x') {
                        reading_x = true;
                        reading_y = false;
                        reading_rule = false;
                    } else if(line[i] == 'y') {
                        reading_x = false;
                        reading_y = true;
                        reading_rule = false;
                    } else if(line[i] == 'r') {
                        reading_x = false;
                        reading_y = false;
                        reading_rule = true;
                    }
                    if(reading_x && number_characters.find(line[i]) != std::string::npos) {
                        x.push_back(line[i]);
                    } else if(reading_y && number_characters.find(line[i]) != std::string::npos) {
                        y.push_back(line[i]);
                    } else if(reading_rule && valid_rule_string_characters.find(line[i]) != std::string::npos) {
                        rule.push_back(line[i]);
                    }
                }
                if (x.empty() || y.empty()) {
                    THROW_ERROR_MSG("given file is empty");
                } else if(!isValidRuleString(rule)) {
                    THROW_ERROR_MSG("given rule string from file is not valid");
                }
            } else {
                board_string += line;
            }
            line_number++;
        }
    }

    for(size_t i = 0; i < board_string.length(); i++) {
        if(board_string[i] == '$') {
            if(reading_number) {
                // Skip multiple lines
                current_row += std::stoi(current_number);
            } else {
                current_row++;
            }
            current_column = 0;
            reading_number = false;
            current_number = "";
        } else if(number_characters.find(board_string[i]) != std::string::npos) {
            reading_number = true;
            current_number.push_back(board_string[i]);
        } else if(board_string[i] != '!') { // Exception: '!'. This character concludes the board
            bool cell_state;
            if(board_string[i] == 'b') { // Dead cell
                cell_state = false;
            } else { // Alive cell
                //  RLE readers that cannot handle more than two states 
                // should treat all letters other than b (and perhaps B) 
                // as equivalent to o. 
                cell_state = true;
            }
            if(reading_number) {
                auto cn = std::stoi(current_number);
                for(int i = current_column; i != current_column+cn; ++i) {
                    //ctrl.actions.push_back(gol_action{{i+off_x, current_column+off_y}, cell_state ? gol_action_type::set : gol_action_type::unset});
                    gol_id id = {i+off_x, current_row+off_y};
                    auto r = ctrl.space->get<GolCell<>>(id, ctrl.space, id, ctrl.view->observation);
                    ctrl.branch_sys->act(GolCell<>::Set, r, ctrl.iter, cell_state);
                }
                current_column += cn;
            } else {
                //ctrl.actions.push_back(gol_action{{current_row+off_x, current_column+off_y}, cell_state ? gol_action_type::set : gol_action_type::unset});
                gol_id id = {current_column+off_x, current_row+off_y};
                auto r = ctrl.space->get<GolCell<>>(id, ctrl.space, id, ctrl.view->observation);
                ctrl.branch_sys->act(GolCell<>::Set, r, ctrl.iter, cell_state);
                current_column++;
            }
            reading_number = false;
            current_number = "";
        } else {
            break;
        }
    }
}

void GolController::Measure(const GolController& ctrl) {
    ctrl.branch_sys->act(ctrl.space->explode(GolCell<>::Measure));
}

void GolController::Dispose(cc::aspect_ptr<GolController>& ctrl) {
    ctrl.dispose();
}