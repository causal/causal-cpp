// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Gol 1.0

ApplicationWindow
{
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Game of Life")
    readonly property bool inPortrait: window.width < window.height
    property double fps;

    signal speedChanged(real speed)
    signal playButtonClicked(bool checked)
    signal templateDropped(string name, real mouse_x, real mouxe_y)

    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                checkable: true

                icon.name: checked ? "media-playback-pause" : "media-playback-start"
                icon.source: checked ? "qrc:/images/media-playback-pause.png" : "qrc:/images/media-playback-start.png"
                onClicked: {
                    window.playButtonClicked(checked)
                }
            }
            Label {
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            Text { text: window.fps }
            ToolButton {
                checkable: true

                icon.name: "media-seek-forward"
                icon.source: "qrc:/images/media-seek-forward.png"
                onClicked: checked ? speedMenu.open() : speedMenu.close()

                Menu {
                    id: speedMenu
                    y: parent.height

                    Slider {
                        id: speedSlider

                        from: 0
                        value: 0
                        to: 500
                        stepSize: 10

                        onMoved: {
                            window.speedChanged(speedSlider.value)
                        }
                    }
                }
            }
        }
    }

    Drawer {
        id: drawer
        objectName: "drawer"
        y: header.height
        width: 200
        height: parent.height - header.height

        modal: inPortrait
        interactive: inPortrait
        position: inPortrait ? 0 : 1
        visible: !inPortrait

        ListView {
            id: templates
            objectName: "templates"
            anchors.fill: parent
            model: {}

            property string dragName: ""

            delegate: Item {
                id: item
                width: templates.width
                height: 25
                required property string modelData

                Rectangle {
                    id: rect
                    width: templates.width
                    height: 25
                    z: mouseArea.drag.active ||  mouseArea.pressed ? 2 : 1
                    property point beginDrag
                    Drag.active: mouseArea.drag.active

                    Text {
                        id: text
                        anchors.centerIn: parent
                        text: parent.parent.modelData
                    }

                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        drag.target: parent

                        onPressed: {
                            parent.beginDrag = Qt.point(parent.x, parent.y);
                            templates.dragName = text.text;
                        }
                        onReleased: {
                            parent.Drag.drop();

                            backAnimX.from = parent.x;
                            backAnimX.to = parent.beginDrag.x;
                            backAnimY.from = parent.y;
                            backAnimY.to = parent.beginDrag.y;
                            backAnim.start()
                        }

                        ParallelAnimation {
                            id: backAnim
                            SpringAnimation { id: backAnimX; target: rect; property: "x"; duration: 500; spring: 2; damping: 0.2 }
                            SpringAnimation { id: backAnimY; target: rect; property: "y"; duration: 500; spring: 2; damping: 0.2 }
                        }
                    }

                    states: [
                        State {
                            when: rect.Drag.active
                            PropertyChanges {
                                target: rect
                                opacity: 0.3
                            }
                        }
                    ]
                }
            }
        }
    }

    GolView {
        id: view
        objectName: "view"
        x: drawer.width
        width: parent.width-drawer.width
        height: parent.height

        DropArea {
            anchors.fill: parent

            onDropped: {
                window.templateDropped(templates.dragName, drag.x, drag.y)
            }
        }
    }
}
