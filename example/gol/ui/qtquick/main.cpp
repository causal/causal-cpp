// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <filesystem>
#include <iostream>

#include "main.hpp"
#include "moc_main.cpp"
#include <causal/trace.hpp>

#include <qt6/QtQuick/QQuickView>
#include <qt6/QtQml/QQmlProperty>
#include <qt6/QtCore/QDir>

namespace fs = std::filesystem;
namespace cu = causal::ui;

fs::path root_path;

QQGolView::QQGolView(QQuickItem* parent)
: QQuickPaintedItem(parent) {
    setAcceptedMouseButtons(Qt::AllButtons);
}

QQGolView::~QQGolView() {
    this->host->branch_ui->act(GolController::Dispose, this->controller);
}

void QQGolView::geometryChange(const QRectF& newGeometry, const QRectF& oldGeometry) {
    if(newGeometry != oldGeometry) {
        const int width  = newGeometry.width(),
                  height = newGeometry.height();
        this->off_x = -(width/this->pixels_per_cell)/2;
        this->off_y = -(height/this->pixels_per_cell)/2;
        if(this->host)
            this->host->branch_ui->act_synced(this->build_unit.make(), Build, this, this->controller);
    }

    this->QQuickPaintedItem::geometryChange(newGeometry, oldGeometry);
}

void QQGolView::paint(QPainter* painter) {
    auto view_x = this->size_x*this->pixels_per_cell;
    auto view_y = this->size_y*this->pixels_per_cell;
    QBrush brush(QColor("#000000"));

    painter->setBrush(brush);
    painter->setPen(Qt::NoPen);
    painter->setRenderHint(QPainter::Antialiasing);

    QSizeF itemSize = size();
    painter->drawRect(0, 0, itemSize.width(), itemSize.height());

    // protects model.buf from observation writes
    this->use_pixels([&](u_char* pb) {
        if(pb) {            
            QImage img(pb, this->size_x, this->size_y, QImage::Format_ARGB32);
            painter->drawImage(0, 0, img.scaled(view_x, view_y));
        }
    });
}

void QQGolView::wheelEvent(QWheelEvent* e) {
    u_short new_val = 0;
    auto delta = e->angleDelta().y()/120;
    if(this->pixels_per_cell > (-delta) || delta > 0)
        new_val = std::max<u_short>(1, std::min<u_short>(24, this->pixels_per_cell+delta));

    if(new_val && new_val != this->pixels_per_cell) {
        this->pixels_per_cell = new_val;
        this->host->branch_ui->act_synced(this->build_unit.make(), Build, this, this->controller);
    }
}

void QQGolView::mousePressEvent(QMouseEvent* e) {
    switch(e->button()) {
    case Qt::LeftButton:
        {
            long x = e->pos().x()/this->pixels_per_cell;
            long y = e->pos().y()/this->pixels_per_cell;
            this->host->branch_ui->act(GolController::Execute, this->controller, gol_action{{x+this->off_x,y+this->off_y}, gol_action_type::toggle});
        }
        break;
    case Qt::MiddleButton:
        this->scroll_x = e->pos().x();
        this->scroll_y = e->pos().y();
        break;
    default:
        break;
    }
}

void QQGolView::mouseReleaseEvent(QMouseEvent* e) {
    switch(e->button()) {
    case Qt::LeftButton:
        break;
    case Qt::MiddleButton: {
            this->off_x += (this->scroll_x - e->pos().x())/this->pixels_per_cell;
            this->off_y += (this->scroll_y - e->pos().y())/this->pixels_per_cell;

            this->host->branch_ui->act_synced(this->build_unit.make(), Redraw, this);
        }
        break;
    default:
        break;
    }
}

void QQGolView::on_template_dropped(QString name, double mouse_x, double mouse_y) {
    if(!name.isEmpty()) {
        fs::path p(name.toStdString());
        QFile file(":/templates/"+name);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;
        if(p.extension() == ".gol")
            this->host->branch_ui->act(GolController::Copy, QString(file.readAll()).toStdString(), this->controller, (mouse_x/this->pixels_per_cell)+this->off_x, (mouse_y/this->pixels_per_cell)+this->off_y);
        else if(p.extension() == ".rle")
            this->host->branch_ui->act(GolController::Load, QString(file.readAll()).toStdString(), this->controller, (mouse_x/this->pixels_per_cell)+this->off_x, (mouse_y/this->pixels_per_cell)+this->off_y);
    }
}

void QQGolView::Init(std::shared_ptr<GolHost> h, QQGolView* v, cc::aspect_ptr<GolController> ctrl) {
    auto p = h->engine.rootObjects().first();
    connect(p, SIGNAL(templateDropped(QString, double, double)),
            v, SLOT(on_template_dropped(QString, double, double)));

    v->host = std::move(h);
    v->controller = std::move(ctrl);

    v->host->branch_ui->act_fwd_synced(v->build_unit.make(), Build, v, v->controller);
}

void QQGolView::update(cc::aspect_ptr<GolController>& ctrl) {
    this->QQuickPaintedItem::update();

    ctrl->delay = this->host->get_delay();

    auto time = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = time-this->last_time;
    if(diff.count() >= 5) {
        this->host->set_fps(double(ctrl->count-this->last_count)/diff.count());
        this->last_count = ctrl->count;
        this->last_time = time;
    }

    if(this->host->get_play()) {
        if(!ctrl->running) {
            this->host->branch_ui->act(GolController::Trigger, ctrl, ctrl->iter);
        }
    } else
        ctrl->running = false;
}
    
void QQGolView::Build(QQGolView* ui, cc::aspect_ptr<const GolController>& ctrl) {
    const int width  = ui->width(),
              height = ui->height();

    if(width < ui->pixels_per_cell || height < ui->pixels_per_cell)
        return;

    ui->set_size(width/ui->pixels_per_cell, height/ui->pixels_per_cell);

    ui->QQuickPaintedItem::update();

    cc::act(GolController::Measure, ctrl);
}

void QQGolView::Redraw(QQGolView* ui) {
    ui->set_size(ui->size_x, ui->size_y);

    cc::act(GolController::Measure, ui->controller);
}
    
GolHost::GolHost(const QQmlApplicationEngine& e, const std::shared_ptr<cc::branch>& b_ui)
: engine(e), branch_ui(b_ui) {}

void GolHost::onPlayButtonClicked(bool c) {
    this->play = c;
}

void GolHost::onSpeedChanged(double s) {
    this->delay = 500-s;
}

void GolHost::set_fps(double fps) {
    auto p = this->engine.rootObjects().first();
    p->setProperty("fps", QVariant::fromValue(fps));
}

void GolHost::Init(std::shared_ptr<GolHost> h) {
    auto p = h->engine.rootObjects().first();
    connect(p, SIGNAL(playButtonClicked(bool)),
            h.get(), SLOT(onPlayButtonClicked(bool)));
    connect(p, SIGNAL(speedChanged(double)),
            h.get(), SLOT(onSpeedChanged(double)));

    auto d = p->findChild<QObject*>("drawer");
    auto t = d->findChild<QObject*>("templates");
    QDir directory(":/templates/");
    QStringList tl = directory.entryList(QStringList({"*.rle", "*.gol"}));

    t->setProperty("model", QVariant::fromValue(tl));

    h->view = p->findChild<QQGolView*>("view");

    cy::depender dep;
    cc::act_synced(dep.on_branch(), QQGolView::Init, h, h->view, cc::forge<GolController>(h->view));
    cc::act_synced({cy::delay_ms::make(UI_UPDATE_DELAY), dep.depend()}, Update, h);
}

void GolHost::Update(std::shared_ptr<GolHost> h) {
    if(!h->closed) {
        cc::act(GolController::Update, h->view->controller, dynamic_cast<GolView*>(h->view));
        cc::act_synced(cy::delay_ms::make(UI_UPDATE_DELAY), Update, h);
    }
}

int main(int argc, char** argv) {
    root_path = fs::path(argv[0]).parent_path();


    auto d_qt = cu::qtquick::make(argc, argv);
    auto b_qt = cc::branch::make(d_qt);

    auto h = std::make_shared<GolHost>(d_qt->engine, b_qt);

    qmlRegisterType<QQGolView>("Gol", 1, 0, "GolView");
    d_qt->load("qrc:/qml/main.qml");

    b_qt->act(GolHost::Init, h);

    return d_qt->run();
}