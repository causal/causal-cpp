// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include "../../gol.hpp"

#include <memory>
#include <chrono>
#include <vector>
#include <algorithm>
#include <fstream>

#ifndef Q_MOC_RUN
#include <causal/core/aspect.hpp>
#include <causal/core/wirks.hpp>
#include <causal/sync/util.hpp>
#include <causal/sync/timing.hpp>
#include <causal/sync/unite.hpp>
#include <causal/process/thread.hpp>
#include <causal/ui/qtquick.hpp>
#endif

#include <qt6/QtWidgets/QApplication>
#include <qt6/QtQml/QQmlApplicationEngine>
#include <qt6/QtQuick/QQuickPaintedItem>
#include <qt6/QtGui/QBrush>
#include <qt6/QtGui/QPainter>

namespace cc = causal::core;
namespace cy = causal::sync;

#define UI_UPDATE_DELAY 30

class GolHost;
class QQGolView : public QQuickPaintedItem, public GolView {
    Q_OBJECT
    QML_ELEMENT
private:
    std::shared_ptr<GolHost> host;

    cy::unifier_back build_unit;

    size_t last_count = 0;
    std::chrono::time_point<std::chrono::system_clock> last_time = std::chrono::system_clock::now();

protected:
    virtual void geometryChange(const QRectF& newGeometry, const QRectF& oldGeometry) override;
    virtual void paint(QPainter* painter) override;

    virtual void wheelEvent(QWheelEvent* e) override;
    virtual void mousePressEvent(QMouseEvent* e) override;
    virtual void mouseReleaseEvent(QMouseEvent* e) override;

private slots:
    void on_template_dropped(QString name, double mouse_x, double mouse_y);

public:
    double scroll_x = 0.0, scroll_y = 0.0;
    u_short pixels_per_cell = 8;

    QQGolView(QQuickItem* parent = 0);

    virtual ~QQGolView();

    virtual void update(cc::aspect_ptr<GolController>& ctrl) override;

    static void Init(std::shared_ptr<GolHost> h, QQGolView* v, cc::aspect_ptr<GolController> ctrl);
    static void Build(QQGolView* ui, cc::aspect_ptr<const GolController>& ctrl);
    static void Redraw(QQGolView* ui);
};

class GolHost final : public QObject {
    Q_OBJECT
private:
    QQGolView* view;

    bool closed = false;
    bool play = false;
    u_short delay = 500;

    std::vector<cc::aspect_ptr<GolController>> controller;

private slots:
    void onPlayButtonClicked(bool c);
    void onSpeedChanged(double s);

public:
    const QQmlApplicationEngine& engine;
    const std::shared_ptr<cc::branch>& branch_ui;

    GolHost(const QQmlApplicationEngine& e, const std::shared_ptr<cc::branch>& b_ui);

    bool get_play() {return this->play;}
    u_short get_delay() {return this->delay;}
    void set_fps(double fps);

    static void Init(std::shared_ptr<GolHost> h);
    static void Update(std::shared_ptr<GolHost> h);
};