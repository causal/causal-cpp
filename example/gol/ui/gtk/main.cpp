// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <filesystem>

#include "main.hpp"

namespace fs = std::filesystem;
namespace cu = causal::ui;

fs::path root_path;

GtkGolView::GtkGolView(MainWin* mw)
: GolView(), main_win(mw) {
    this->signal_button_press_event().connect(sigc::mem_fun(*this, &GtkGolView::on_button_press));
    this->signal_button_release_event().connect(sigc::mem_fun(*this, &GtkGolView::on_button_release));
    this->signal_scroll_event().connect(sigc::mem_fun(*this, &GtkGolView::on_scroll));

    this->add_events(Gdk::BUTTON_PRESS_MASK|Gdk::BUTTON_RELEASE_MASK|Gdk::SMOOTH_SCROLL_MASK|Gdk::BUTTON1_MOTION_MASK);
    this->drag_dest_set({Gtk::TargetEntry("text/plain"), Gtk::TargetEntry("move/scroll")},
                        Gtk::DestDefaults::DEST_DEFAULT_ALL,
                        Gdk::DragAction::ACTION_COPY | Gdk::DragAction::ACTION_MOVE);
}

GtkGolView::~GtkGolView() {
    this->main_win->branch_ui->act(GolController::Dispose, this->controller);
}

void GtkGolView::update(cc::aspect_ptr<GolController>& ctrl) {
    this->queue_draw();

    ctrl->delay = 500-this->main_win->headerBar.scl_speed.get_value();

    auto time = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = time-this->last_time;
    if(diff.count() >= 5) {
        this->main_win->headerBar.lbl_fps.set_text(
            std::to_string(double(ctrl->count-this->last_count)/diff.count())
        );
        this->last_count = ctrl->count;
        this->last_time = time;
    }

    if(this->main_win->headerBar.btn_play.get_active()) {
        if(!ctrl->running) {
            this->main_win->branch_ui->act(GolController::Trigger, ctrl, ctrl->iter);
        }
    } else
        ctrl->running = false;

    if(ctrl->running)
        this->main_win->headerBar.btn_play.set_stock_id(Gtk::Stock::MEDIA_PAUSE);
    else
        this->main_win->headerBar.btn_play.set_stock_id(Gtk::Stock::MEDIA_PLAY);
}

void GtkGolView::on_size_allocate(Gtk::Allocation& alloc) {
    this->Gtk::DrawingArea::on_size_allocate(alloc);
    const int width  = alloc.get_width(),
              height = alloc.get_height();
    this->off_x = -(width/this->pixels_per_cell)/2;
    this->off_y = -(height/this->pixels_per_cell)/2;
    if(this->main_win)
        this->main_win->branch_ui->act_synced(this->build_unit.make(), Build, this, this->controller);
}

bool GtkGolView::on_draw(const ::Cairo::RefPtr<::Cairo::Context>& cr) {
    auto view_x = this->size_x*this->pixels_per_cell;
    auto view_y = this->size_y*this->pixels_per_cell;

    // protects model.buf from observation writes
    this->use_pixels([&](u_char* pb) {
        if(pb) {
            auto s = ::Cairo::ImageSurface::create(pb, ::Cairo::FORMAT_ARGB32, this->size_x, this->size_y, this->size_x*4);
            //auto p = ::Cairo::SurfacePattern::create();
            //p->set_filter(::Cairo::FILTER_FAST);

            cr->set_source_rgba(0.0,0.0,0,1.0);
            cr->rectangle(0, 0, view_x, view_y);
            cr->fill();
            cr->scale(this->pixels_per_cell, this->pixels_per_cell);
            cr->set_source(s, 0, 0);
            cr->rectangle(0, 0, view_x, view_y);
            cr->fill();
            cr->translate(this->size_x, 0);
        }
    });

    return false;
}

void GtkGolView::on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& c, int x, int y, const Gtk::SelectionData& sd, guint info, guint time) {
    std::string p_string = sd.get_text();
    if(!p_string.empty()) {
        std::filesystem::path p(p_string);
        std::ifstream ifs(p_string);
        std::string t((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
        if(p.extension() == ".gol")
            this->main_win->branch_ui->act(GolController::Copy, t, this->controller, (x/this->pixels_per_cell)+this->off_x, (y/this->pixels_per_cell)+this->off_y);
        else if(p.extension() == ".rle")
            this->main_win->branch_ui->act(GolController::Load, t, this->controller, (x/this->pixels_per_cell)+this->off_x, (y/this->pixels_per_cell)+this->off_y);
    }
    
    c->drop_finish(true,time);
}
    
bool GtkGolView::on_scroll(GdkEventScroll* e) {
    u_short new_val = 0;
    auto delta = e->delta_y;
    if(this->pixels_per_cell > delta || delta < 0)
        new_val = std::max<u_short>(1, std::min<u_short>(24, this->pixels_per_cell-delta));
    
    if(new_val && new_val != this->pixels_per_cell) {
        this->pixels_per_cell = new_val;
        this->main_win->branch_ui->act_synced(this->build_unit.make(), Build, this, this->controller);
    }
    return true;
}

bool GtkGolView::on_button_press(GdkEventButton* e) {
    switch(e->button) {
    case 1: {
            long x = e->x/this->pixels_per_cell;
            long y = e->y/this->pixels_per_cell;
            this->main_win->branch_ui->act(GolController::Execute, this->controller, gol_action{{x+this->off_x,y+this->off_y}, gol_action_type::toggle});
        }
        break;
    case 2:
        this->scroll_x = e->x;
        this->scroll_y = e->y;
        break;
    default:
        break;
    }

    return true;
}

bool GtkGolView::on_button_release(GdkEventButton* e) {
    switch(e->button) {
    case 1:
        break;
    case 2: {
            this->off_x += (this->scroll_x - e->x)/this->pixels_per_cell;
            this->off_y += (this->scroll_y - e->y)/this->pixels_per_cell;

            this->main_win->branch_ui->act_synced(this->build_unit.make(), Redraw, this);
        }
        break;
    default:
        break;
    }

    return true;
}

void GtkGolView::Build(GtkGolView* ui, cc::aspect_ptr<const GolController>& ctrl) {
    Gtk::Allocation alloc = ui->get_allocation();
    const int width  = alloc.get_width(),
              height = alloc.get_height();
    if(width < ui->pixels_per_cell || height < ui->pixels_per_cell)
        return;

    ui->set_size(width/ui->pixels_per_cell, height/ui->pixels_per_cell);

    ui->queue_draw();

    cc::act(GolController::Measure, ctrl);
}

void GtkGolView::Redraw(GtkGolView* ui) {
    ui->set_size(ui->size_x, ui->size_y);

    cc::act(GolController::Measure, ui->controller);
}

GolHeaderBar::GolHeaderBar()
: btn_play(Gtk::Stock::MEDIA_PLAY), scl_speed(Gtk::BuiltinIconSize::ICON_SIZE_LARGE_TOOLBAR, 0.0, 500.0, 10.0) {
    this->set_show_close_button();
    this->pack_start(this->btn_play);
    this->pack_end(this->scl_speed);
    this->pack_end(this->lbl_fps);
    this->scl_speed.set_icons({Gtk::Stock::MEDIA_FORWARD.id, Gtk::Stock::MEDIA_FORWARD.id, Gtk::Stock::MEDIA_FORWARD.id});
}

GolTemplate::GolTemplate(const std::filesystem::path p)
: label{p.filename().c_str()}, path(p)
{
    this->signal_drag_begin().connect([](const Glib::RefPtr<Gdk::DragContext>& ctx){
        ctx->set_icon();
    });
    add(this->label);
    set_halign(Gtk::Align::ALIGN_START);
    this->label.set_size_request(200);
    show_all_children();
}


GolTemplateBar::GolTemplateBar() {
    this->drag_source_set({Gtk::TargetEntry("text/plain")},
                            Gdk::ModifierType::BUTTON1_MASK,
                            Gdk::DragAction::ACTION_COPY);
}

void GolTemplateBar::on_drag_begin(const Glib::RefPtr<Gdk::DragContext>& c) {
    int x, y;
    this->get_pointer(x, y);
    this->dragging = dynamic_cast<GolTemplate*>(this->get_row_at_y(y));
}

void GolTemplateBar::on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& c, Gtk::SelectionData& sd, guint info, guint time) {
    if(this->dragging) {
        sd.set_text(this->dragging->path.c_str());
        this->dragging = nullptr;
    }        
}

void GolTemplateBar::Init(GolTemplateBar* bar) {
    fs::directory_iterator dit(root_path/fs::path(GOL_TEMPLATE_DIR));
    for(const auto& f : dit) {
        auto p = f.path();
        if(p.extension() == ".gol" || p.extension() == ".rle") {
            bar->templates.push_back(GolTemplate(p));
            auto& l = bar->templates.back();
            bar->append(l);
            l.show();
        }
    }
}

MainWin::MainWin(const std::shared_ptr<cc::branch>& b_gtk)
: branch_ui(b_gtk), view(this) {
    this->set_default_size(640, 480);
    this->set_type_hint(Gdk::WINDOW_TYPE_HINT_DIALOG);
    this->set_titlebar(this->headerBar);
    this->add(this->pane);
    this->signal_delete_event().connect(sigc::mem_fun(*this, &MainWin::on_close));
    this->pane.add1(this->bar);
    this->pane.add2(this->view);
    this->view.controller = cc::forge<GolController>(&this->view);
    this->show_all();
}

bool MainWin::on_close(GdkEventAny* any_event) {
    this->closed = true;
    return false;
}

void MainWin::Init(MainWin* mw) {
    cc::act(GolTemplateBar::Init, &mw->bar);
    cc::act_synced(cy::delay_ms::make(UI_UPDATE_DELAY), Update, mw);
}

void MainWin::Update(MainWin* mw) {
    if(!mw->closed) {
        cc::act(GolController::Update, mw->view.controller, dynamic_cast<GolView*>(&mw->view));
        cc::act_synced(cy::delay_ms::make(UI_UPDATE_DELAY), Update, mw);
    }
}

int main(int argc, char** argv) {
    root_path = fs::path(argv[0]).parent_path();

    auto d_gtk = cu::gtk::make("org.causal.example.gol.ui.gtk");
    auto b_gtk = cc::branch::make(d_gtk);

    MainWin mw(b_gtk);
    b_gtk->act(MainWin::Init, &mw);
    return d_gtk->run(mw, argc, argv);
}
