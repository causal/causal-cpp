// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include "../../gol.hpp"

#include <memory>
#include <chrono>
#include <vector>
#include <algorithm>
#include <chrono>
#include <ctime> 

#include <causal/core/wirks.hpp>
#include <causal/sync/util.hpp>
#include <causal/sync/timing.hpp>
#include <causal/sync/unite.hpp>
#include <causal/process/thread.hpp>
#include <causal/ui/gtk.hpp>

namespace cc = causal::core;
namespace cy = causal::sync;

#define UI_UPDATE_DELAY 30

#ifndef GOL_TEMPLATE_DIR
#define GOL_TEMPLATE_DIR "share/gol/templates"
#endif

class MainWin;
class GtkGolView final : public Gtk::DrawingArea, public GolView {
private:
    MainWin* const main_win = nullptr;

    cy::unifier_back build_unit;

    size_t last_count = 0;
    std::chrono::time_point<std::chrono::system_clock> last_time = std::chrono::system_clock::now();

public:
    double scroll_x = 0.0, scroll_y = 0.0;
    u_short pixels_per_cell = 8;
    
    ::Cairo::RefPtr<Cairo::Surface> surface;

    GtkGolView(MainWin* mw);

    virtual ~GtkGolView();

    virtual void update(cc::aspect_ptr<GolController>& ctrl) override;

    virtual void on_size_allocate(Gtk::Allocation& alloc) override;
    virtual bool on_draw(const ::Cairo::RefPtr<::Cairo::Context>& cr) override;
    virtual void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& c, int x, int y, const Gtk::SelectionData& sd, guint info, guint time) override;
    
    bool on_scroll(GdkEventScroll* e);
    bool on_button_press(GdkEventButton* e);
    bool on_button_release(GdkEventButton* e);

    static void Build(GtkGolView* ui, cc::aspect_ptr<const GolController>& ctrl);
    static void Redraw(GtkGolView* ui);
};

class GolHeaderBar : public Gtk::HeaderBar {
public:
    Gtk::ToggleToolButton btn_play;
    Gtk::ScaleButton scl_speed;
    Gtk::Label lbl_fps;

    GolHeaderBar();
};

class GolTemplate : public Gtk::ListBoxRow {
private:
    Gtk::Label label;

public:
    const std::filesystem::path path;

    GolTemplate(const std::filesystem::path p);
};

class GolTemplateBar : public Gtk::ListBox {
private:
    std::vector<GolTemplate> templates;
    GolTemplate* dragging;

public:
    GolTemplateBar();

    virtual void on_drag_begin(const Glib::RefPtr<Gdk::DragContext>& c) override;
    virtual void on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& c, Gtk::SelectionData& sd, guint info, guint time) override;

    static void Init(GolTemplateBar* bar);
};

class MainWin final : public Gtk::ApplicationWindow {    
public:
    const std::shared_ptr<cc::branch> branch_ui;

    Gtk::Paned pane;
    GolTemplateBar bar;
    GolHeaderBar headerBar;
    GtkGolView view;
    std::atomic<bool> closed = false;

    MainWin(const std::shared_ptr<cc::branch>& b_gtk);

    bool on_close(GdkEventAny* any_event);

    static void Init(MainWin* mw);
    static void Update(MainWin* mw);
};