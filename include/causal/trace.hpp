// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <exception>
#include <string>
#include <sstream>
#include <iostream>

#include <boost/current_function.hpp>

#include <boost/log/trivial.hpp>

#include "utility.hpp"

namespace trace {
#ifdef WITH_STACKTRACE
    std::string stacktrace(size_t skip = 0);
#endif

#define TRACE_FUNC BOOST_CURRENT_FUNCTION

#ifndef TRACE_FILTER
    #ifdef DEBUG
        #define TRACE_FILTER 0
    #else
        #define TRACE_FILTER 1
    #endif
#endif
    // json, opendht, lmdb, msgpack

    enum level : u_char {
        TRACE = 0,      // spam me
        INFO = 1,       // everything noteworthy
        WARNING = 2,    // might be problematic
        ERROR = 3,      // problem
        FATAL = 4       // that's gonna end bad
    };
    
    void log(trace::level l, std::string msg);

    template<level L, typename S>
    class tracer : public std::stringstream {
    private:
        std::string stacktrace;

    public:
        tracer(std::string st = std::string())
        : stacktrace(st.empty() ? trace::stacktrace(true) : st) {
            if constexpr(L >= TRACE_FILTER) {
                *this << "(" << S::name << ")\t";
            }
        }

        virtual ~tracer() {
            if constexpr(L >= TRACE_FILTER) {
#ifdef WITH_STACKTRACE
                if constexpr(L >= 3)
                    *this << std::endl << this->stacktrace;
#endif
                log(L, this->str());
            }
        }
    };

#define MAKE_TRACE_SCOPE(scope_name) \
    struct scope_name final {static const std::string name;};

#define INIT_TRACE_SCOPE(scope_name, id) \
    const std::string scope_name::name = id;

#define VARGS_(_10, _9, _8, _7, _6, _5, _4, _3, _2, _1, N, ...) N 
#define VARGS(...) VARGS_(__VA_ARGS__, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)

#define CONCAT_(a, b) a##b
#define CONCAT(a, b) CONCAT_(a, b)

#define TRACE_2(scope, stacktrace) trace::tracer<trace::level::TRACE, scope>(stacktrace)
#define TRACE_1(scope) TRACE_2(scope, std::string())
#define TRACE(...) CONCAT(TRACE_, VARGS(__VA_ARGS__))(__VA_ARGS__)

#define TRACE_INFO_2(scope, stacktrace) trace::tracer<trace::level::INFO, scope>(stacktrace)
#define TRACE_INFO_1(scope) TRACE_INFO_2(scope, std::string())
#define TRACE_INFO(...) CONCAT(TRACE_INFO_, VARGS(__VA_ARGS__))(__VA_ARGS__)

#define TRACE_WARNING_2(scope, stacktrace) trace::tracer<trace::level::WARNING, scope>(stacktrace) << TRACE_FUNC << std::endl
#define TRACE_WARNING_1(scope) TRACE_WARNING_2(scope, std::string())
#define TRACE_WARNING(...) CONCAT(TRACE_WARNING_, VARGS(__VA_ARGS__))(__VA_ARGS__)

#define TRACE_ERROR_2(scope, stacktrace) trace::tracer<trace::level::ERROR, scope>(stacktrace) << TRACE_FUNC << std::endl
#define TRACE_ERROR_1(scope) TRACE_ERROR_2(scope, std::string())
#define TRACE_ERROR(...) CONCAT(TRACE_ERROR_, VARGS(__VA_ARGS__))(__VA_ARGS__)

#define TRACE_FATAL_2(scope, stacktrace) trace::tracer<trace::level::FATAL, scope>(stacktrace) << TRACE_FUNC << std::endl
#define TRACE_FATAL_1(scope) TRACE_FATAL_2(scope, std::string())
#define TRACE_FATAL(...) CONCAT(TRACE_FATAL_, VARGS(__VA_ARGS__))(__VA_ARGS__)

    /// @brief generic message carrying exception
    class exception : public std::exception {
    public:
        const std::string msg;
        const std::string stacktrace;

        exception(std::string msg);

        virtual const char* what() const noexcept override;
    };

    /// @brief generic message carrying exception causing just a warning
    class exception_warning : public exception
    {public: exception_warning(std::string msg) : trace::exception(msg) {}};

    /// @brief generic message carrying exception causing just an error
    class exception_error : public exception
    {public: exception_error(std::string msg) : trace::exception(msg) {}};

    /// @brief generic message carrying exception causing just a fatal
    class exception_fatal : public exception
    {public: exception_fatal(std::string msg) : trace::exception(msg) {}};

#define EXCEPTION_WARNING(type, msg) \
    class type : public trace::exception_warning { \
    public: \
        type() : trace::exception_warning(msg) {} \
        type(std::string m) : trace::exception_warning(std::string(msg)+"\n"+m) {} \
    };

#define EXCEPTION_ERROR(type, msg) \
    class type : public trace::exception_error { \
    public: \
        type() : trace::exception_error(msg) {} \
        type(std::string m) : trace::exception_error(std::string(msg)+"\n"+m) {} \
    };

#define EXCEPTION_FATAL(type, msg) \
    class type : public trace::exception_fatal { \
    public: \
        type() : trace::exception_fatal(msg) {} \
        type(std::string m) : trace::exception_fatal(std::string(msg)+"\n"+m) {} \
    };

#define INTERN_THROW_FATAL(scope, exc) \
    {TRACE_FATAL(scope) << exc.what(); std::abort();}
#define INTERN_THROW_ERROR(scope, exc) \
    {TRACE_ERROR(scope) << exc.what(); throw(exc);}
#define INTERN_THROW_WARNING(scope, exc) \
    {TRACE_WARNING(scope) << exc.what(); throw(exc);}

#define THROW_FATAL_MSG(msg) \
    throw trace::exception_warning(msg)
#define THROW_ERROR_MSG(msg) \
    throw trace::exception_error(msg)
#define THROW_WARNING_MSG(msg) \
    throw trace::exception_fatal(msg)

#define ASSERT(condition) \
    if(!(condition)) std::abort();

#define ASSERT_MSG(scope, condition, msg) \
    if(!(condition)) TRACE_FATAL(scope) << msg;
}

namespace causal {
    namespace core {
        MAKE_TRACE_SCOPE(scope_core)
        MAKE_TRACE_SCOPE(scope_action)
    }
    namespace sync {MAKE_TRACE_SCOPE(scope_sync)}
    namespace process {MAKE_TRACE_SCOPE(scope_process)}
    namespace data {MAKE_TRACE_SCOPE(scope_data)}
    namespace facet {MAKE_TRACE_SCOPE(scope_facet)}
    namespace ui {MAKE_TRACE_SCOPE(scope_ui)}
    namespace alien {MAKE_TRACE_SCOPE(scope_alien)}
}
// main, json, lmdb, opendht, msgpack
MAKE_TRACE_SCOPE(scope_main)