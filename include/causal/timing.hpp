// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

/* Timing template library is an algorithm timing testing library
Providing the ability to define expected complexities.
Due to unstable timing nature of non real time systems and modern flexible CPU scheduler
precise complexity timing is not exactly possible why
measurement as estimation include a CPU ramp up,
multiple measurement iterations are used and
the usage of a tolerance multiplicator is required.
*/

#pragma once

#include <cstdlib>
#include <type_traits>
#include <chrono>
#include <iostream>
#include <functional>

#include "causal/trace.hpp"

#define TIMING_ESTIMATION_ITERATIONS 25
#define TIMING_CPU_RAMP_UP_COMPLEXITY 100

namespace timing {
    namespace chr = std::chrono;

    struct complextiy {
        virtual void run() const noexcept = 0;
    };

    struct atom : complextiy {
        atom() {}

        void run() const noexcept override {
            // allocates one size_t, initializes, compares, read and writes it
            for(size_t i = 1; i == 1; i++);
        };
    };

    struct custom : complextiy {
        std::function<void()> func;

        custom(std::function<void()> f) : func(f) {}

        void run() const noexcept override {
            this->func();
        }
    };

    template<typename I1, typename I2>
    struct sum : complextiy {
        const I1 inner_1;
        const I2 inner_2;

        sum(I1&& i1, I2&& i2) : inner_1(std::forward<I1>(i1)), inner_2(std::forward<I2>(i2)) {}

        void run() const noexcept override {
            this->inner_1.run();
            this->inner_2.run();
        }
    };

    template<typename I = atom>
    struct linear : complextiy {
        const size_t depth;
        const I inner;

        linear(size_t d, I&& i = atom()) : depth(d), inner(std::forward<I>(i)) {}

        void run() const noexcept override {
            for(size_t i = 1; i <= this->depth; i++)
                this->inner.run();
        };
    };

    template<typename I = atom>
    struct pow : complextiy {
        const size_t depth;
        const size_t exponent;
        const size_t count;
        const I inner;

        static size_t calc(size_t a, size_t x) noexcept {
            if(x-- > 1)
                return a * calc(a, x);
            else
                return a;
        }

        pow(size_t d, size_t e=3, I&& i = atom()) : depth(d), exponent(e), count(calc(d, e)), inner(std::forward<I>(i)) {}

        void run() const noexcept override {
            for(size_t i = 1; i <= this->count; i++)
                this->inner.run();
        }
    };

    template<typename I = atom>
    struct log : complextiy {
        const size_t value;
        const size_t base;
        const size_t count;
        const I inner;

        static size_t calc(size_t y, size_t b) noexcept {
            if(y > b) {
                ASSERT(y % b == 0)
                y = y / b;
                return 1+calc(y, b);
            } else
                return 1;
        }

        log(size_t v, size_t b=3, I&& i = atom()) : value(v), base(b), count(calc(v, b)), inner(std::forward<I>(i)) {}

        void run() const noexcept override {
            for(size_t i = 1; i <= this->count; i++) {
                this->inner.run();
            }
        }
    };

    template<typename C1, typename C2, typename = typename std::enable_if_t<std::is_base_of_v<complextiy, C1> && std::is_base_of_v<complextiy, C2>>>
    sum<C1, C2> operator+(C1&& c1, C2&& c2) {
        return sum(std::forward<C1>(c1), std::forward<C2>(c2));
    }

    template<typename C, typename = typename std::enable_if_t<std::is_base_of_v<complextiy, C>>>
    linear<C> operator*(size_t f, C&& c) {
        return linear(f, std::forward<C>(c));
    }

    template<typename C, typename = typename std::enable_if_t<std::is_base_of_v<complextiy, C>>>
    linear<C> operator*(C&& c, size_t f) {
        return linear(f, std::forward<C>(c));
    }

    template<typename C>
    class timer {
    private:
        chr::nanoseconds cmp;

    public:
        timer(C&& c) noexcept {
            (atom()*TIMING_CPU_RAMP_UP_COMPLEXITY).run(); // CPU ramp up
            chr::time_point t_start = chr::high_resolution_clock::now();
            for(size_t i = 0; i < TIMING_ESTIMATION_ITERATIONS; i++)
                c.run();
            this->cmp = chr::high_resolution_clock::now() - t_start;
            std::cout << "estimated:\t" << this->cmp.count() / TIMING_ESTIMATION_ITERATIONS << " nanoseconds" << std::endl;
        }

        bool run(std::function<void()> f, u_short t=2) const noexcept {
            (atom()*TIMING_CPU_RAMP_UP_COMPLEXITY).run(); // CPU ramp up
            chr::time_point t_start = chr::high_resolution_clock::now();
            for(size_t i = 0; i < TIMING_ESTIMATION_ITERATIONS; i++)
                f();
            chr::nanoseconds res = chr::high_resolution_clock::now() - t_start;
            std::cout << "tolerance multiplicator:\t" << t << std::endl;
            std::cout << "consumed:\t" << res.count() / TIMING_ESTIMATION_ITERATIONS << " nanoseconds" << std::endl;
            return this->cmp*t > res;
        }
    };

    template<typename C>
    class cycler {
    private:
        uint64_t cmp;

        static uint64_t rdtsc() {
            unsigned int lo,hi;
            __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
            return ((uint64_t)hi << 32) | lo;
        }

    public:
        cycler(C&& c) noexcept {
            (atom()*TIMING_CPU_RAMP_UP_COMPLEXITY).run(); // CPU ramp up
            uint64_t t_start = rdtsc();
            for(size_t i = 0; i < TIMING_ESTIMATION_ITERATIONS; i++)
                c.run();
            this->cmp = rdtsc() - t_start;
            std::cout << "estimated:\t" << this->cmp / TIMING_ESTIMATION_ITERATIONS << " cycles" << std::endl;
        }

        bool run(std::function<void()> f, u_short t=2) const noexcept {
            (atom()*TIMING_CPU_RAMP_UP_COMPLEXITY).run(); // CPU ramp up
            uint64_t t_start = rdtsc();
            for(size_t i = 0; i < TIMING_ESTIMATION_ITERATIONS; i++)
                f();
            uint64_t res = rdtsc() - t_start;
            std::cout << "tolerance multiplicator:\t" << t << std::endl;
            std::cout << "consumed:\t" << res / TIMING_ESTIMATION_ITERATIONS << " cycles" << std::endl;
            return this->cmp*t > res;
        }
    };
}