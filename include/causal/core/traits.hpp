// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

//#include <cstdlib>
#include <type_traits>
#include <tuple>
#include <memory>

namespace causal {
    namespace core {
        template <template<typename...> typename, typename>
        struct is_instance_of : public std::false_type {};

        template <template<typename...> typename U, typename T>
        struct is_instance_of<U, U<T>> : public std::true_type {};

        template<template<typename...> typename U, typename T>
        constexpr bool is_instance_of_v = is_instance_of<U, T>::value;

        template <typename T, typename Tuple>
        struct tuple_contains_type;

        template <typename T>
        struct tuple_contains_type<T, std::tuple<>> : std::false_type {};

        template <typename T, typename U, typename... Ts>
        struct tuple_contains_type<T, std::tuple<U, Ts...>> : tuple_contains_type<T, std::tuple<Ts...>> {};

        template <typename T, typename... Ts>
        struct tuple_contains_type<T, std::tuple<T, Ts...>> : std::true_type {};

        template <typename T, typename Tuple>
        constexpr bool tuple_contains_type_v = tuple_contains_type<T, Tuple>::value;

        template<typename R, typename... Args>
        struct func_base {
            using return_type = R;
            using argument_types = std::tuple<Args...>;
            static constexpr std::size_t argument_count = sizeof...(Args);
            template<std::size_t N>
            using argument_type = typename std::tuple_element<N, argument_types>::type;
        };

        template<typename F> struct func;

        template<typename R, typename... Args, bool NonThrowing>
        struct func<R(*)(Args...) noexcept(NonThrowing)> : func_base<void, R, Args...> {
            using ptr = R(*)(Args...) noexcept(NonThrowing);
            static constexpr bool is_non_throwing = NonThrowing;
            static constexpr bool is_member = false;
        };

        template<typename T, typename R, typename... Args, bool NonThrowing>
        struct func<R(T::*)(Args...) noexcept(NonThrowing)> : func_base<T, R, Args...> {
            using ptr = R(*)(Args...) noexcept(NonThrowing);
            static constexpr bool is_non_throwing = NonThrowing;
            static constexpr bool is_member = true;
            using member_of_type = T;
            static constexpr bool is_const = false;
        };

        template<typename T, typename R, typename... Args, bool NonThrowing>
        struct func<R(T::*)(Args...) const noexcept(NonThrowing)> : func_base<T, R, Args...> {
            using ptr = R(T::*)(Args...) const noexcept(NonThrowing);
            static constexpr bool is_non_throwing = NonThrowing;
            static constexpr bool is_member = true;
            using member_of_type = T;
            static constexpr bool is_const = true;
        };

        template <size_t... Is>
        struct seq {};

        template<size_t N, size_t... Is>
        struct gen_seq : gen_seq<N - 1, N - 1, Is...> {};

        template<size_t... Is>
        struct gen_seq<0, Is...> : seq<Is...> {};
        
        template <typename T>
        struct has_model_type {
        private:
            template <typename T1>
            static typename T1::model_type test(int);
            template <typename>
            static void test(...);
        public:
            enum { value = !std::is_void<decltype(test<T>(0))>::value };
        };

        template <typename T>
        struct has_key_type {
        private:
            template <typename T1>
            static typename T1::key_type test(int);
            template <typename>
            static void test(...);
        public:
            enum { value = !std::is_void<decltype(test<T>(0))>::value };
        };

        template <typename T>
        struct has_mapped_type {
        private:
            template <typename T1>
            static typename T1::mapped_type test(int);
            template <typename>
            static void test(...);
        public:
            enum { value = !std::is_void<decltype(test<T>(0))>::value };
        };
        
        template <size_t FI, size_t LI, typename FuncT>
        inline typename std::enable_if<FI == LI, void>::type
        static_for(FuncT const& f) { }
        
        template <size_t FI, size_t LI, typename FuncT>
        inline typename std::enable_if<FI < LI, void>::type
        static_for(FuncT const& f)
        {
            if(f.template operator()<FI>())
                static_for<FI+1, LI, FuncT>(f);
        }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I == sizeof...(Tp), void>::type
        static_for_each(FuncT f, size_t i = 0) { }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I < sizeof...(Tp), void>::type
        static_for_each(FuncT f, size_t i = 0) {
            if(I >= i) {
                using T = typename std::tuple_element<I, std::tuple<Tp...>>::type;
                if(f.template operator()<I, T>())
                    static_for_each<I + 1, FuncT, Tp...>(f, i);
            } else
                static_for_each<I + 1, FuncT, Tp...>(f, i);
        }

        template<typename... Tp, typename FuncT>
        void static_for_each(FuncT f) {
            static_for_each<0, FuncT, Tp...>(f);
        }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I == sizeof...(Tp), void>::type
        static_for_each(const std::tuple<Tp...>& t, FuncT f, size_t i = 0) { }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I < sizeof...(Tp), void>::type
        static_for_each(const std::tuple<Tp...>& t, FuncT f, size_t i = 0) {
            if(I >= i) {
                using T = typename std::tuple_element<I, std::tuple<Tp...>>::type;
                if(f.template operator()<I, T>(std::get<I>(t)))
                    static_for_each<I + 1, FuncT, Tp...>(t, f, i);
            } else
                static_for_each<I + 1, FuncT, Tp...>(t, f, i);
        }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I == sizeof...(Tp), void>::type
        reverse_static_for_each(const std::tuple<Tp...>& t, FuncT f, size_t i = std::tuple_size<std::tuple<Tp...>>::value) {}

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I < sizeof...(Tp), void>::type
        reverse_static_for_each(const std::tuple<Tp...>& t, FuncT f, size_t i = std::tuple_size<std::tuple<Tp...>>::value-1) {
            if(I >= std::tuple_size<std::tuple<Tp...>>::value-i) {
                static constexpr size_t RI = std::tuple_size<std::tuple<Tp...>>::value-(I+1);
                using T = typename std::tuple_element<RI, std::tuple<Tp...>>::type;
                if(f.template operator()<RI, T>(std::get<RI>(t)))
                    reverse_static_for_each<I + 1, FuncT, Tp...>(t, f, i);
            } else
                reverse_static_for_each<I + 1, FuncT, Tp...>(t, f, i);
        }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I == sizeof...(Tp), void>::type
        static_for_each(std::tuple<Tp...>& t, FuncT f, size_t i = 0) { }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I < sizeof...(Tp), void>::type
        static_for_each(std::tuple<Tp...>& t, FuncT f, size_t i = 0) {
            if(I >= i) {
                using T = typename std::tuple_element<I, std::tuple<Tp...>>::type;
                if(f.template operator()<I, T>(std::get<I>(t)))
                    static_for_each<I + 1, FuncT, Tp...>(t, f, i);
            } else
                static_for_each<I + 1, FuncT, Tp...>(t, f, i);
        }

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I == sizeof...(Tp), void>::type
        reverse_static_for_each(std::tuple<Tp...>& t, FuncT f, size_t i = std::tuple_size<std::tuple<Tp...>>::value) {}

        template<size_t I = 0, typename FuncT, typename... Tp>
        inline typename std::enable_if<I < sizeof...(Tp), void>::type
        reverse_static_for_each(std::tuple<Tp...>& t, FuncT f, size_t i = std::tuple_size<std::tuple<Tp...>>::value-1) {
            if(I >= std::tuple_size<std::tuple<Tp...>>::value-i) {
                static constexpr size_t RI = std::tuple_size<std::tuple<Tp...>>::value-(I+1);
                using T = typename std::tuple_element<RI, std::tuple<Tp...>>::type;
                if(f.template operator()<RI, T>(std::get<RI>(t)))
                    reverse_static_for_each<I + 1, FuncT, Tp...>(t, f, i);
            } else
                reverse_static_for_each<I + 1, FuncT, Tp...>(t, f, i);
        }
        
        template <template <typename...> class Base, typename Derived>
        struct is_derived_from_template_impl
        {
            using U = typename std::remove_cv<
                                    typename std::remove_reference<Derived>::type
                                    >::type;

            template <typename... Args>
            static auto test(Base<Args...>*)
                -> typename std::integral_constant<bool
                                            , !std::is_same<U, Base<Args...>>::value>;

            static std::false_type test(void*);

            using type = decltype(test(std::declval<U*>()));
        };

        template <template <typename...> class Base, typename Derived>
        using is_derived_from_template
                        = typename is_derived_from_template_impl<Base, Derived>::type;

        template <template <typename...> class Base, typename Derived>
        constexpr bool is_derived_from_template_v = is_derived_from_template<Base, Derived>::value;
    }
}
