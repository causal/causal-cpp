// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include "causal/trace.hpp"

#include "traits.hpp"
#include "access.hpp"

#include <cassert>
#include <cstddef>

#include <atomic>
#include <mutex>
#include <thread>
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <vector>
#include <map>
#include <list>
#include <functional>
#include <algorithm>
#include <mutex>
#include <shared_mutex>

namespace causal {
    namespace core {
        /** @brief represents acting tick on environment and there fore the main building block of causal dynamics
         * @attention Nachruf: In tiefer Dankbarkeit, Betroffenheit, Demut und Reue, ihn nie persönlich kennengelernt zu haben,
         * möge Hans-Peter Dürr in Frieden und der Gewissheit, durch ihn gewirkter Inspiration, ruhen.
         * @remark a wirks describes a virtual entity representing a "happening"
         * acting an algorithm, called tick in this context, on a set of descriptive information, called either aspect or facet.
         * @remark a wirks needs to focus information from space before occupying it.
         * doing so the information of existence gets certain while properties stays uncertain.
         * there fore the focus is the transition hard uncertainty -> soft certainty/soft uncertainty.
         * @remark information is exclusive, in a non-linear system a wirks requires to occupy information from space for acting on it.
         * there fore the occupation of an entities aspect is the transition soft certainty -> hard certainty.
         */
        class wirks final {
        private:
            /// @brief tick to execute
            void* tick;

            /// @brief access helper providing abstract access to facets
            std::vector<access_*> accessors;

            /// @brief spark of wirks applying executive information on facets
            void(* const spark)(const wirks* const w);

            /** @brief gets accessor clones
             * @return vector holding pointer to clones of access helper
             */
            std::vector<access_*> get_accessor_clones() const;

            bool focused = false;

        public:
            wirks(const wirks&);
            wirks(wirks&& o);

            /** @brief constructor setting accessors and creating spark
             * @param t tick to execute
             * @param src facets
             */
            template<typename... SrcT, typename... DstT>
            wirks(void(*t)(DstT...), SrcT&&... src)
            : tick(reinterpret_cast<void*>(t)), accessors(get_access<SrcT...>(t, std::forward<SrcT>(src)...)), spark(
                [](const wirks* const w) {
                    auto t = reinterpret_cast<void(*)(DstT...)>(w->tick);
                    apply_access(t, w, w->accessors);
                }
            ) {}

            ~wirks();

            /** @brief focus on descriptive information
             * @remark point of no return, after focusing wirks cannot get modified or copied
             * @return true if wirks could focus
             */
            virtual bool focus() noexcept;

            /** @brief gets if wirks was already focused
             * @remark when wirks got focused no further modification is valid
             * @return if wirks was focused
             */
            bool is_focused() const noexcept;

            /** @brief borrow descriptive information
             * @return true if all descriptive information is successfully borrowed
             */
            virtual bool borrow() const noexcept;

            /** @brief spark it
             * @return exception if thrown any
             */
            virtual std::exception_ptr trigger() const noexcept;

            /** @brief lazy set/overwrite an accessor
             * @tparam I which argument to set/overwrite
             * @param a new accessor to set
             */
            template<size_t I>
            void set_accessor(access_* a) {
                ASSERT_MSG(scope_core, !this->focused, "wirks was already focused")
                auto old = this->accessors[I];
                ASSERT_MSG(scope_core, old->get_dst_hash() == a->get_dst_hash(), "trying to overwrite wirks facet using an incompatible accessor")
                this->accessors[I] = a;
                delete old;
            }

            /** @brief lazy set/overwrite an accessor
             * @tparam I which argument to set/overwrite
             * @param src facet
             */
            template<size_t I, typename SrcT, typename DstT>
            void set(SrcT&& src) {
                ASSERT_MSG(scope_core, !this->focused, "wirks was already focused")
                this->set_accessor<I>(new access<SrcT, DstT>(std::forward<SrcT>(src)));
            }

            /** @brief get destination type hash of accessor
             * @tparam I which accessor
             * @return size_t destination type hash
             */
            template<size_t I>
            const size_t type_hash() const noexcept {
                return this->accessors[I]->get_dst_hash();
            }
        };

        struct action;
        class action_set;

        /// @brief set of wirks
        class wirks_set : private std::vector<std::unique_ptr<wirks>> {
            friend class action_set;
        private:
            /** @brief gets clone of set
             * @return vector of wirks
             */
            std::vector<std::unique_ptr<wirks>> clone() const;

        public:
            typedef std::vector<std::unique_ptr<wirks>> container_type;

            wirks_set() = default;
            wirks_set(const wirks_set& o);
            wirks_set(wirks_set&&) = default;

            /** @brief add wirks to set
             * @param w unique pointer of wirks
             */
            void add(std::unique_ptr<wirks>&& w) noexcept;

            /** @brief apply a function to each wirks mutably
             * @param f function applying on mutable wirks
             */
            void apply(std::function<void(wirks&)> f) noexcept;

            /** @brief apply a function to each wirks immutably
             * @param f function applying on immutable wirks
             */
            void apply(std::function<void(const wirks&)> f) const noexcept;
        };

        /// @brief synchron states
        enum synchron_state {
            /// @brief depending wirks could get feasible
            possible = 0,

            /// @brief depending wirks are feasible to act
            feasible = 1,

            /// @brief depending wirks could not get feasible anymore
            impossible = 3
        };

        /// @brief base for synchronization logic for instructing dispatcher
        class synchron {
        private:
            /// @brief actual state of synchron
            std::atomic<synchron_state> state;

            /// @brief mutex to protect set of change callbacks
            std::shared_mutex callbacks_mtx;

            /// @brief set of callbacks to execute on state changes identified by a pointer
            std::map<const void*, std::function<bool(synchron&)>> callbacks;

        protected:
            synchron(synchron_state init_state = synchron_state::feasible);

            /** @brief change synchron's state
             * @remark triggers change callbacks
             * @param new_state state to transition to
             * only higher states than actual state are possible, otherwise ignored
             */
            void change(synchron_state new_state) noexcept;

        public:
            /// @brief synchron is not copyable
            synchron(const synchron&) = delete;

            /// @brief synchron is not moveable
            synchron(synchron&&) = delete;

            virtual ~synchron() = default;

            /** @brief gets actual state
             * @return actual state
             */
            inline synchron_state get_state() const noexcept {
                return this->state.load();
            }

            /** @brief registers a callback if not already registered for ptr
             * @param ptr callback is associated with
             * @param fn callback to call on state change
             * returns true if callback shall get deregistered
             */
            void register_callback(const void* const ptr, std::function<bool(synchron&)>&& fn) noexcept;

            /** @brief deregister callback for certain pointer
             * @param ptr callback is associated with
             */
            void deregister_callback(const void* const ptr) noexcept;

            /** @brief notify synchron about action being loaded */
            virtual void loaded(const action *const a) {};

            /** @brief notify synchron about action being finished */
            virtual void finished(const action *const a) {};

            /** @brief gets if synchron is still possible
             * @remark includes feasible state
             * @return true if state is possible or feasible
             */
            bool is_possible() const noexcept;

            /** @brief gets is synchron is feasible
             * @return true if state is feasible
             */
            bool is_feasible() const noexcept;

            /** @brief creates feasible empty synchron
             * @return shared pointer to feasible empty synchron
             */
            static std::shared_ptr<synchron> make();
        };

        /// @brief synchron sticking on fwd
        class fwd_synchron : public synchron {using synchron::synchron;};

        struct action;
        class branch;
        /// @brief additive collection of synchron's
        class synchrons final : public synchron {
            friend struct action;
            friend class action_set;
            friend class branch;
        private:
            const std::list<std::shared_ptr<synchron>> syncs = std::list<std::shared_ptr<synchron>>();

            std::atomic<size_t> feasible_cnt = 0;

            synchrons(const synchrons& o);

            synchrons(synchrons&& o);

            void init();

            /** @brief executed when sub synchron calls back
            * when getting a feasible count is increased
            * when feasible count reaches size of sub synchron set changes state to feasible
            * when getting impossible, immediately changes state to impossible
            * @param s sub synchron's state
            */
            void eval(synchron_state s);

        public:
            synchrons() = default;

            /// @brief constructor creating synchron collection from argument list
            template<typename... S>
            synchrons(std::shared_ptr<S>... s)
            : synchron(synchron_state::possible), syncs({std::move(s)...}) {
                this->init();
            }

            /// @brief constructor creating synchron collection from synchron set
            synchrons(std::list<std::shared_ptr<synchron>> s);
            
            ~synchrons();

            /// @brief creates a synchrons as copy of itself filtering out impossibilities
            synchrons operator*() const;

            /// @brief creates a synchrons by merging two other synchrons
            synchrons operator+(const synchrons& o) const;

            /// @brief creates a synchrons by merging two other synchrons filtering out impossibilities
            synchrons operator|(const synchrons& o) const;
            
            void loaded(const action *a) override;
            
            void finished(const action *a) override;
            
            /** @brief creates additive synchron
             * @param s synchron's
             * @return shared pointer to additive synchron collection
             */
            template<typename... S>
            static std::shared_ptr<synchrons> make(std::shared_ptr<S>... s) {
                return std::shared_ptr<synchrons>(new synchrons(std::move(s)...));
            }
            
            /** @brief creates additive synchron
             * @param s synchron set
             * @return shared pointer to additive synchron collection
             */
            static std::shared_ptr<synchrons> make(std::list<std::shared_ptr<synchron>> s);
        };

        /// @brief intention to act a wirks providing synchronization
        struct action final {
        private:
            /// @brief action running in actual thread
            static thread_local const action* executing;

            /// @brief wirks to execute
            const std::unique_ptr<wirks> act;

            /// @brief indicates if action is loaded and synchronizes attempts to load
            std::atomic<bool> loaded = false;

            /// @brief exception thrown by triggered wirks
            std::exception_ptr exception = nullptr;

        public:
            /// @brief shared pointer of context branch
            const std::shared_ptr<branch> context;

            /// @brief synchron used for synchronizing causality
            const std::unique_ptr<synchrons> sync;

            /// @brief action is not copyable
            action(const action&) = delete;

            /// @brief action is not movable
            action(action&& o) = delete;

            /** @brief constructs an action
             * @param c context branch
             * @param s synchronization of causality
             * @param w wirks to act
             */
            action(const std::shared_ptr<branch> c, synchrons&& s, std::unique_ptr<wirks>&& w);

            ~action();

            /// @brief get thrown exception
            inline std::exception_ptr get_exception() const noexcept {
                return this->exception;
            }

            /** @brief checks if current thread is executing an action
             * @return true if current thread executes an action
             */
            static bool has_current();

            /** @brief gets action running in current thread
             * @return reference to running action
             * @throw attepmt_to_imply_branch_outside_act if not inside an actions stack
             */
            static const action& current();

            /** @brief gets all synchrons from action executed in current thread
             * @return set of current actions's synchrons
             */
            static synchrons current_syncs();

            /** @brief checks if action is already loaded
             * @return true if action successfully loaded
             */
            bool is_loaded() const noexcept;

            /** @brief tries to load action if not already loaded or loading in another thread
             * @return true if successfully loaded
             */
            bool load() noexcept;

            /** @brief tries to run an action
             * @return true if executed, false if an exception was thrown within wirks
             */
            bool run() noexcept;
        };

        /// @brief set of actions
        class action_set : private std::vector<std::unique_ptr<action>> {
            friend class branch;
        public:
            typedef std::vector<std::unique_ptr<action>> container_type;

            /// @brief action set is default constructible
            action_set() = delete;

            /// @brief action set is not copyable 
            action_set(const action_set&) = delete;

            /// @brief action set is not moveable
            action_set(action_set&&) = delete;

            /** @brief constructs action set
             * @param c context branch
             * @param s synchronization of causality
             * @param ws set of wirks to copy into actions
             */
            action_set(const std::shared_ptr<branch> c, synchrons&& s, const wirks_set& ws);

            /** @brief constructs action set
             * @param c context branch
             * @param s synchronization of causality
             * @param ws set of wirks to move into actions
             */
            action_set(const std::shared_ptr<branch> c, synchrons&& s, wirks_set&& ws);
        };

        /// @brief abstract base of dispatcher queuing and running actions
        class dispatcher {
        public:
            virtual ~dispatcher() = default;

            /** @brief enqueue an action
             * @param a action to enqueue
             */
            virtual void enqueue(std::unique_ptr<action>&& a) = 0;
        };

        /// @brief context of action
        class branch final : public std::enable_shared_from_this<branch> {
            friend struct action;
        private:
            std::atomic<size_t> cnt_actions = 0;

            /// @brief mutex to protect set of change callbacks
            std::shared_mutex callbacks_mtx, exceptions_mtx;

            std::map<const void*, std::function<void(branch&, const size_t)>> callbacks;
            std::list<std::exception_ptr> exceptions;

            /** @brief constructs branch
             * @param d dispatcher to use
             */
            branch(const std::shared_ptr<dispatcher> d);

        public:
            /// @brief dispatcher used
            const std::shared_ptr<dispatcher> dispatch;
            
            static std::shared_ptr<branch> make(const std::shared_ptr<dispatcher> d);
            static std::shared_ptr<branch> current() {
                return action::current().context;
            }

            branch(branch &&);
            virtual ~branch();

            void reg(const void* id, std::function<void(branch&, const size_t)> cb);

            void dereg(const void* id);

            bool alive() const noexcept;

            const std::list<std::exception_ptr> flush_exceptions();

            /** @brief enqueue an action to dispatcher if possible
             * @param a action to enqueue
             * @return true if action could be enqueued
             */
            virtual bool enqueue(std::unique_ptr<action>&& a);

            /** @brief enqueue a set of actions to dispatcher if all are possible
             * @param as set of actions
             * @return true if actions could be enqueued
             */
            virtual bool enqueue_set(action_set&& as);

            /** @brief act wirks as unsynchronized action
             * @param w wirks to act
             */
            void act(std::unique_ptr<wirks>&& w);

            /** @brief act set of wirks as unsynchronized actions
             * @param ws set of wirks
             */
            void act(wirks_set&& ws);

            /** @brief create wirks and act it as unsynchronized action
             * @param t tick to act
             * @param src descriptive information to act tick on
             */
            template<typename... SrcT, typename... DstT>
            void act(void(* const t)(DstT...), SrcT&&... src) {
                this->enqueue(std::make_unique<action>(
                    this->shared_from_this(), synchrons{}, 
                    std::make_unique<wirks>(t, std::forward<SrcT>(src)...)));
            }

            /** @brief act wirks as synchronized action
             * @param s synchronization
             * @param w wirks to act
             * @return true if action could be enqueued
             */
            bool act_synced(synchrons&& s, std::unique_ptr<wirks>&& w);

            /** @brief act set of wirks as synchronized action
             * @param s synchronization
             * @param w wirks to act
             * @return true if action could be enqueued
             */
            bool act_synced(synchrons&& s, wirks_set&& ws);

            /** @brief create wirks and act it as synchronized action
             * @param s synchronization
             * @param t tick to act
             * @param src descriptive information to act tick on
             * @return true if action containing created wirks could be enqueued
             */
            template<typename... SrcT, typename... DstT>
            bool act_synced(synchrons&& s, void(* const t)(DstT...), SrcT&&... src) {
                return this->enqueue(std::make_unique<action>(
                    this->shared_from_this(),
                    std::move(s),
                    std::make_unique<wirks>(t, std::forward<SrcT>(src)...)));
            }

            /** @brief act wirks as forward synchronized action
             * @param w wirks to act
             * @return true if action could be enqueued
             */
            bool act_fwd(std::unique_ptr<wirks>&& w);

            /** @brief act set of wirks as forward synchronized actions
             * @param ws set of wirks
             * @return true if actions could be enqueued
             */
            bool act_fwd(wirks_set&& ws);

            /** @brief create wirks and act it as forward synchronized action
             * @param t tick to act
             * @param src descriptive information to act tick on
             * @return true if action containing created wirks could be enqueued
             */
            template<typename... SrcT, typename... DstT>
            bool act_fwd(void(* const t)(DstT...), SrcT&&... src) {
                return this->enqueue(std::make_unique<action>(
                    this->shared_from_this(),
                    *action::current_syncs(),
                    std::make_unique<wirks>(t, std::forward<SrcT>(src)...)));
            }

            /** @brief act wirks as forward synchronized action adding further synchronization
             * @param s further synchronization
             * @param w wirks to act
             * @return true if action could be enqueued
             */
            bool act_fwd_synced(synchrons&& s, std::unique_ptr<wirks>&& w);

            /** @brief act set of wirks as forward synchronized action adding further synchronization
             * @param s further synchronization
             * @param w wirks to act
             * @return true if action could be enqueued
             */
            bool act_fwd_synced(synchrons&& s, wirks_set&& ws);

            /** @brief create wirks and act it as forward synchronized action adding further synchronization
             * @param s further synchronization
             * @param t tick to act
             * @param src descriptive information to act tick on
             * @return true if action containing created wirks could be enqueued
             */
            template<typename... SrcT, typename... DstT>
            bool act_fwd_synced(synchrons&& s, void(* const t)(DstT...), SrcT&&... src) {
                return this->enqueue(std::make_unique<action>(
                    this->shared_from_this(),
                    action::current_syncs() | s, 
                    std::make_unique<wirks>(t, std::forward<SrcT>(src)...)));
            }
        };
        
        /// Exception thrown at trying to imply a branch from outside an act
        EXCEPTION_ERROR(attepmt_to_imply_branch_outside_act, "trying to imply a branch from outside an act")
        
        /** @brief forwards arguments to act of calling actions context
         * @param args forwarded arguments
         * @throw attepmt_to_imply_branch_outside_act if not inside an actions stack
         */
        template<typename... Args>
        void act(Args&&... args) {
            return action::current().context->act(std::forward<Args>(args)...);
        }
        
        /** @brief forwards arguments to synchronized act of calling actions context
         * @param args forwarded arguments
         * @return true if successfully enqueued
         * @throw attepmt_to_imply_branch_outside_act if not inside an actions stack
         */
        template<typename... Args>
        bool act_synced(synchrons&& s, Args&&... args) {
            return action::current().context
                ->act_synced(std::forward<synchrons>(s), std::forward<Args>(args)...);
        }
        
        /** @brief forwards arguments to forward synchronized act of calling actions context
         * @param args forwarded arguments
         * @return true if successfully enqueued
         * @throw attepmt_to_imply_branch_outside_act if not inside an actions stack
         */
        template<typename... Args>
        bool act_fwd(Args&&... args) {
            return action::current().context
                ->act_fwd(std::forward<Args>(args)...);
        }
        
        /** @brief forwards arguments to forward and further synchronized act of calling actions context
         * @param args forwarded arguments
         * @return true if successfully enqueued
         * @throw attepmt_to_imply_branch_outside_act if not inside an actions stack
         */
        template<typename... Args>
        bool act_fwd_synced(synchrons&& s, Args&&... args) {
            return action::current().context
                ->act_fwd_synced(std::forward<synchrons>(s), std::forward<Args>(args)...);
        }

        /** @brief allows directly unrelated actuality to be triggered by events of actuality within
         * @tparam MsgT narrows down to possible events. If empty all are allowed.
         */
        template<typename... MsgT>
        class vortex {
        protected:
            /// @brief branch to trigger wirks in
            const std::shared_ptr<branch> brnch;

            /// @brief mutex synchronizing vortex inteactions
            mutable std::shared_mutex mtx;

            /// @brief subscribed wirks to trigger when message is received
            std::unordered_map<size_t, wirks_set> wrks;

            vortex(std::shared_ptr<branch> b): brnch(std::move(b)) {
                ASSERT_MSG(scope_core, this->brnch, "trying to create vortex without valid branch")
            }

        public:
            static std::shared_ptr<vortex> make(std::shared_ptr<branch> b) {
                return std::shared_ptr<vortex>(new vortex<MsgT...>(b));
            }

            virtual ~vortex() = default;

            /** @brief subscribe wirks for getting triggered on event
             * @tparam T type of aspect
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @param t tick taking const copy of event as first argument
             * @param src further argument sources for wirks
            */
            template<typename T, typename... SrcT, typename... DstT>
            void subscribe(void(*t)(T, DstT...), SrcT&&... src)
            requires(sizeof...(MsgT) == 0 || (std::is_same<T, MsgT>::value || ...)) {
                std::unique_lock l(this->mtx);
                auto wp = new wirks(t, T{}, std::forward<SrcT>(src)...);
                this->wrks[typeid(T).hash_code()].add(std::unique_ptr<wirks>(wp));
            }

            /// @brief trigger a typed event by lvalue
            template<typename T>
            void trigger(const T& event) const
            requires(sizeof...(MsgT) == 0 || (std::is_same<T, MsgT>::value || ...)) {
                std::shared_lock l(this->mtx);
                const auto wrks_it = this->wrks.find(typeid(T).hash_code());
                if(wrks_it != this->wrks.cend()) {
                    wirks_set ws;
                    wrks_it->second.apply([&](const wirks& w){
                        auto nw = new wirks(w);
                        auto a = new access<const T&, T>(event);
                        if(a) {
                            nw->template set_accessor<0>(a);
                            ws.add(std::unique_ptr<wirks>(std::move(nw)));
                        }
                    });
                    this->brnch->act_fwd(std::move(ws));
                }
            }

            /// @brief trigger a typed event by rvalue
            template<typename T>
            void trigger(T&& event) const
            requires(sizeof...(MsgT) == 0 || (std::is_same<T, MsgT>::value || ...)) {
                std::shared_lock l(this->mtx);
                const auto wrks_it = this->wrks.find(typeid(T).hash_code());
                if(wrks_it != this->wrks.cend()) {
                    wirks_set ws;
                    wrks_it->second.apply([&](const wirks& w){
                        auto nw = new wirks(w);
                        auto a = new access<T&&, T>(std::forward<T>(event));
                        if(a) {
                            nw->template set_accessor<0>(a);
                            ws.add(std::unique_ptr<wirks>(std::move(nw)));
                        }
                    });
                    this->brnch->act_fwd(std::move(ws));
                }
            }
        };
    }
}