// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <type_traits>
#include <concepts>
#include <functional>
#include <memory>
#include <sstream>
#include <any>
#include <map>
#include <set>

#include "causal/trace.hpp"

#include "focus.hpp"
#include "borrow.hpp"
#include "traits.hpp"
#ifdef WITH_SUPPORT
#include "causal/support.hpp"
#endif

namespace causal {
    namespace core {
        template<typename ID>
        inline constexpr bool is_id_direct_serializable_v = std::is_same_v<ID, std::string> ||
            (std::is_pointer_v<ID> && std::is_same_v<std::remove_cv_t<std::remove_pointer_t<ID>>, char>) ||
            std::is_nothrow_convertible_v<ID, std::string>;

        /** @brief pass through serialization of given id
         * @pre given id needs to be of type std::string or convertible to std::string
         * @tparam ID type of id
         * @param id
         * @return passing through id
         */
        template<typename ID>
        inline std::string serialize_id(const ID& id)
        requires(is_id_direct_serializable_v<ID>) {
            return id;
        }

        /** @brief serialization of given id
         * @pre given id needs to not be of type std::string or convertible to std::string but supported by std::to_string
         * @tparam ID type of id
         * @param id
         * @return string of id
         */
        template<typename ID>
        inline std::string serialize_id(const ID& id)
        requires(!is_id_direct_serializable_v<ID>) {
            return std::to_string(id);
        }

        /** @brief interfacing helper for aspects
         * @remarks due to technical limitations of C++20 reflection an aspect is only
         * dynamically castable to its bases if implementing them via this helper
         */
        template<typename... B>
        struct interfaces : B... {
            typedef std::tuple<B...> type_tuple;

            virtual ~interfaces() = default;

            /** @brief checks at runtime if aspect implements a base of given type
             * @param t runtime information of type to check
             * @return true if aspect implements given type
             */
            static bool is_of(const std::type_info& t) {
                bool ret = false;
                static_for<0, std::tuple_size_v<std::tuple<B...>>>([&]<size_t I>() {
                    if(typeid(std::tuple_element_t<I, std::tuple<B...>>) == t) {
                        ret = true;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief dynamically casts aspect to requested type returning a reinterpret_cast able void*
             * @param t runtime information of type to return
             * @return void* which can be reinterpret_cast ed to type
             */
            void* as(const std::type_info& t) {
                void* ret = nullptr;
                static_for<0, std::tuple_size_v<std::tuple<B...>>>([&]<size_t I>() {
                    if(typeid(std::tuple_element_t<I, std::tuple<B...>>) == t) {
                        ret = dynamic_cast<std::tuple_element_t<I, std::tuple<B...>>*>(this);
                        return false;
                    }
                    return true;
                });

                return ret;
            }
        };

        namespace concepts {
            template<typename T>
            concept typed = !std::is_same_v<std::remove_cv_t<T>, void>;

            template<typename T>
            concept immutable = std::is_const_v<T>;

            template<typename T, typename N>
            concept equal_mutability = immutable<T> == immutable<N>;

            template<typename T, typename N>
            concept equal_type = std::is_same_v<std::remove_cv_t<T>, std::remove_cv_t<N>>;

            template<typename B, typename T>
            concept interfacing = equal_type<B, T> ||
                                  (is_derived_from_template_v<interfaces, typename std::remove_cv_t<T>> &&
                                   tuple_contains_type_v<std::remove_cv_t<B>, typename std::remove_cv_t<T>::type_tuple>);

            template<typename T, typename N>
            concept possibly_convertible = equal_mutability<T, N> &&
                                           (interfacing<N, T> || interfacing<T, N>);
        }

        template<typename T=void>
        class essence;

        template<typename T=void>
        using essence_ptr = std::shared_ptr<essence<T>>;

        template<typename T=void>
        using weak_essence_ptr = std::weak_ptr<essence<T>>;


        /// @brief self aware aspects can access own essence
        template<typename T>
        struct self_aware {
            friend class essence<T>;
        private:
            weak_essence_ptr<T> _ptr;

        public:
            std::shared_ptr<essence<T>> self() const {
                return this->_ptr.lock();
            }
        };

        /// @brief exception thrown on space failing to id less create aspects
        EXCEPTION_ERROR(id_less_creation_unsupported, "space does not support id less creation of aspects")

        /// @brief exception thrown on space failing to id less create aspects
        EXCEPTION_ERROR(incompatible_id, "space does not support id less creation of aspects")

        /// @brief abstract base describing spaces common interfaces
        class space_base {
        public:
            /** @brief indicates if space is managing aspect
             * @return true if space supports create/dispose/push/pull
             */
            virtual bool is_managing() const noexcept {return false;}

            /** @brief focuses an aspect in space for usage if found using given id
             * @param id identifying aspect to focus
             * @param key to use for focusing
             */
            virtual void focus(const std::any id, const void* const key) noexcept {}

            /** @brief releases an aspect if found using given id and focused in space
             * @param id identifying aspect to release
             * @param key used for focusing
             */
            virtual void release(const std::any id, const void* const key) noexcept {}

            /** @brief checks if an aspect is found using given id and focused in space
             * @param id identifying aspect to check
             * @return true when aspect is focused
             */
            virtual bool is_focused(const std::any id) const noexcept {return false;}

            /** @brief borrows an aspect from space for mutable usage if found using given id
             * @param id identifying aspect to borrow
             * @param key to use for borrowing
             * @return true when successfully borrowed
             */
            virtual bool borrow(const std::any id, const void* const key) noexcept {return true;}

            /** @brief borrows an aspect from space for immutable usage if found using given id
             * @param id identifying aspect to borrow
             * @param key to use for borrowing
             * @return true when successfully borrowed
             */
            virtual bool const_borrow(const std::any id, const void* const key) noexcept {return true;}

            /** @brief unborrows an aspect if found using given id and mutably or immutably borrowed from space
             * @param id identifying aspect to unborrow
             * @param key used for borrowing
             * @param abort indicates if transaction is to cancel or to commit
             */
            virtual void unborrow(const std::any id, const void* const key, const bool abort) noexcept {}

            /** @brief checks if an aspect is found using given id and mutably borrowed from space
             * @param id identifying aspect to check
             * @return true when aspect is mutably borrowed
             */
            virtual bool is_borrowed(const std::any id) const noexcept {return false;}

            /** @brief checks if an aspect is found using given id and immutably borrowed from space
             * @param id identifying aspect to check
             * @return true when aspect is immutably borrowed
             */
            virtual bool is_const_borrowed(const std::any id) const noexcept {return false;}

            /** @brief create an aspect's local representation
             * @param id of aspect (id's of types incompatible with space are ignored)
             * @param a pointer to untyped aspect meta object
             */
            virtual void create(const std::any id, const essence_ptr<> a) noexcept {}

            /** @brief dispose an aspect identified by given id from space
             * @param id of aspect (id's of types incompatible with space are ignored)
             */
            virtual void dispose(const std::any id) noexcept = 0;

            /** @brief destroy an aspect's local representation
             * @param id of aspect (id's of types incompatible with space are ignored)
             */
            virtual void destroy(const std::any id) noexcept {}

            /** @brief push an aspect to space
             * @param type string of essence
             * @param id of aspect (id's of types incompatible with space are ignored)
             * @param ptr void* to essence's data in memory
             */
            virtual void push(std::string type, std::any id, void* ptr) noexcept {}

            /** @brief pull an aspect from space allocating memory
             * @param type string of essence
             * @param id of aspect (id's of types incompatible with space are ignored)
             * @return void* to essence's data in memory
             */
            virtual void* const pull(std::string type, std::any id) noexcept {return nullptr;}

            /** @brief typeless clone an essence's data as aspect into space if a new id can be generated
             * @param key borrowing aspect to clone
             * @param ptr aspect to clone
             * @param id to use for identifying new aspect (empty for autogeneration if possible)
             * @throw id_less_creation_unsupported if no id could be generated if none given
             * @throw incompatible_id if id given is incompatible with space
             * @return untyped aspect pointer to clone
             */
            virtual essence_ptr<> clone(const void* const key, essence_ptr<> ptr, std::any id = std::any()) = 0;
        };

        /** @brief meta object hosting focusable and borrowable aspect
         * @remark aspect of a certain T is designed to be weakly bound to it's T and
         * can get reinterpret_cast'ed to an aspect of void or interfaced types for passing thorugh untyped or
         * to another aspect of T' implemented by T using interfaces helper
         * casts shall only happen through provided "as_" methods and
         * include runtime type checks for avoiding memory corruptions caused by faulty usage
         * @remark when reinterpret_cast'ed
         * functions contain information of original T and can work with it
         * even essence does not know it's original T anymore
         */
        template<typename T>
        class essence final : public std::enable_shared_from_this<essence<T>>,
                             private focusable, private atomic_borrowable {
            static_assert(!concepts::immutable<T>, "essence connot contain immutable data");

        private:
            /** @brief function to call when essence gets borrowed
                * @remark if std::is_copy_constructible_v<T>
                * data gets copied for restoring when transaction is aborted
                * @param key to use for borrowing
                * @return true if borrowing was successfull
                */
            std::function<bool(const void* const key)> on_borrow;

            /** @brief function to call when essence gets unborrowed
                * @remark if std::is_copy_constructible_v<T> && abort == false
                * copied data gets deleted
                * @remark if std::is_copy_constructible_v<T> && abort == true
                * actual data gets deleted and copied data moved back
                * @param key used for borrowing
                * @param abort indicates if transaction gets commited or aborted
                */
            std::function<void(const void* const key, const bool abort)> on_unborrow;

            /** @brief constructs new data either by using arguments passed to essence ctor
                * or if std::is_default_constructible_v<T> the default ctor
                * @return true if creation was successful
                */
            std::function<void()> _create_default;

            /// @brief disposes data
            std::function<void()> _dispose;

            /// @brief destroys default construction arguments and aspect from
            std::function<void()> _destroy;

            std::function<void()> _pull;
            std::function<void()> _push;
            std::function<bool()> _is_interfacing;
            std::function<bool(const std::type_info& t)> _is_of;
            std::function<void*(const std::type_info& t)> _get_data;
            std::function<bool()> _is_clonable;
            std::function<essence_ptr<>(const std::any id, const std::shared_ptr<space_base>& s)> _clone;

            /// @brief id of essence within holding space
            const std::any id;

            /// @brief untyped pointer to tuple holding creation arguments
            void* default_creation_arguments = nullptr;

            /// @brief pointer to data in heap
            void* chunk = nullptr;

            /// @brief indicates if essence is actually in a transaction
            bool in_transaction = false;

            /// @brief pointer to data backup for transaction
            void* transaction_store = nullptr;

            /// @brief runtime information of type used to initialize essence
            const std::type_info& data_type_info = typeid(T);

            /// @brief constructor of essence intializing functions for type transitions
            template<typename... Args>
            essence(const std::any i, const std::shared_ptr<space_base>& s, Args... args)
            : on_borrow([&](const void* const key){
                if(this->space->borrow(this->id, key)) {
                    if constexpr(std::is_copy_constructible_v<T>) {
                        this->in_transaction = true;
                        if(this->chunk) {
                            this->transaction_store = new T(*reinterpret_cast<T*>(this->chunk));
                        }
                    }
                    return true;
                } else
                    return false;
            }),
            on_unborrow([&](const void* const key, const bool abort){
                if constexpr(std::is_copy_constructible_v<T>) {
                    if(this->in_transaction) {
                        if(abort) {
                            if(this->chunk && !this->transaction_store)
                                this->_dispose();
                            else if(!this->chunk && this->transaction_store)
                                this->create_(*reinterpret_cast<T*>(this->transaction_store));
                            else if(this->chunk && this->transaction_store)
                                this->chunk = new T(*reinterpret_cast<T*>(this->transaction_store));
                        }
                        if(this->transaction_store) {
                            delete reinterpret_cast<T*>(this->transaction_store);
                            this->transaction_store = nullptr;
                        }
                        this->in_transaction = false;
                    }
                }
                this->space->unborrow(this->id, key, abort);
            }),
            _create_default([&](){
                if constexpr(sizeof...(Args) > 0) {
                    // TODO cannot be called if any argument is a reference
                    auto& t = *reinterpret_cast<std::tuple<std::remove_reference_t<Args>...>*>(this->default_creation_arguments);
                    this->create_from_tuple_(t, gen_seq<sizeof...(Args)>{});
                } else if constexpr(std::is_default_constructible_v<T>)
                    this->create_();
            }),
            _dispose([&](){
                delete reinterpret_cast<T*>(this->chunk);
                this->chunk = nullptr;
            }),
            _destroy([&](){
                if constexpr(sizeof...(Args) > 0)
                    delete reinterpret_cast<std::tuple<std::remove_reference_t<Args>...>*>(this->default_creation_arguments);
                
                this->space->destroy(this->id);
            }),
            _pull([&]() {
                if constexpr(std::is_move_constructible_v<T>) {
                    if(this->space->is_managing()) {
                        auto ptr = reinterpret_cast<T*>(this->space->pull(typeid(T).name(), this->id));
                        if(ptr) {
                            this->create_(std::move(*ptr));
                            delete ptr;
                        }
                    }
                }
            }),
            _push([&]() {
                if constexpr(std::is_move_constructible_v<T>) {
                    if(this->space->is_managing()) {
                        if(this->chunk)
                            this->space->push(typeid(T).name(), this->id, reinterpret_cast<T*>(this->chunk));
                        else
                            this->space->dispose(this->id);
                        this->_dispose();
                    }
                }
            }),
            _is_interfacing([&]() {
                return is_derived_from_template<interfaces, T>::value;
            }),
            _is_of([&](const std::type_info& t) {
                if(t == this->data_type_info)
                    return true;
                else {
                    if constexpr(is_derived_from_template<interfaces, T>::value)
                        return T::is_of(t);
                    else
                        return false;
                }
            }),
            _get_data([&](const std::type_info& t) -> void* {
                if(this->chunk) {
                    if(t == this->data_type_info)
                        return reinterpret_cast<T*>(this->chunk);
                    else if constexpr(is_derived_from_template<interfaces, T>::value)
                        return reinterpret_cast<T*>(this->chunk)->as(t);
                }
                
                return nullptr;
            }),
            _is_clonable([&]() {
                return std::is_copy_constructible_v<T>;
            }),
            _clone([&](const std::any id, const std::shared_ptr<space_base>& s) -> essence_ptr<> {
                if constexpr(std::is_copy_constructible_v<T>) {
                    if constexpr(sizeof...(Args) > 0) {
                        auto& t = *reinterpret_cast<std::tuple<std::remove_reference_t<Args>...>*>(this->default_creation_arguments);
                        return this->clone_from_tuple_(id, s, t, gen_seq<sizeof...(Args)>{});
                    } else
                        return this->clone_(id, s);
                } else
                    return nullptr;
            }), id(i.has_value() ? i : this), space(s) {
                if constexpr(sizeof...(Args) > 0)
                    // for lazy paramterized determination arguments are copied into tuple what limits usage of mechanism to copy constructible arguments
                    this->default_creation_arguments = new std::tuple<std::remove_reference_t<Args>...>(args...);
            }

            /** @brief creates data forwarding given arguments to constructor
             * @param args to forward to constructor of data
             */
            template<typename... Args>
            void create_(Args&&... args) {
                T* c = new T(std::forward<Args>(args)...);
                if constexpr(std::is_polymorphic_v<T>) {
                    if(self_aware<T>* sa = dynamic_cast<self_aware<T>*>(c))
                        sa->_ptr = this->weak_from_this();
                }
                this->chunk = c;
            }

            /** @brief helper creates data from a given tuple of arguments
             * @remark this is a helper function for converting argument tuple to argument sequence
             * @param args tuple of arguments forward to constructor of data
             */
            template<typename... Args, size_t... Is>
            void create_from_tuple_(std::tuple<Args...>& args, seq<Is...>) {
                this->create_(std::get<Is>(args)...);
            }

            /** @brief clones essence
             * @param id held by cloned essence
             * @param s space clones essence refers to
             * @param args arguments to use as default arguments for cloned essence
             */
            template<typename... Args>
            essence_ptr<> clone_(const std::any id, const std::shared_ptr<space_base>& s, Args&&... args) {
                auto e = essence<T>::make(id, s, std::forward<Args>(args)...);
                if(this->chunk) {
                    e->chunk = new T(*reinterpret_cast<T*>(this->chunk));
                    e->push(s.get());
                }
                return e->as_untyped();
            }

            /** @brief helper clones essence
             * @remark this is a helper function for converting argument tuple to argument sequence
             * @param id held by cloned essence
             * @param s space clones essence refers to
             * @param args tuple of arguments to use as default arguments for cloned essence
             */
            template<typename... Args, size_t... Is>
            essence_ptr<> clone_from_tuple_(const std::any id, const std::shared_ptr<space_base>& s, std::tuple<Args...>& args, seq<Is...>) {
                return this->clone_(id, s, std::get<Is>(args)...);
            }

            /** @brief gets pointer to data
             * @return pointer to data of type T
             */
            inline T* get_data() const noexcept
            requires(concepts::typed<T>) {
                return reinterpret_cast<T*>(this->_get_data(typeid(T)));
            }

        public:
            /** @brief type of data essence holds
             * @remark type could be an interface of actually held data or void if essence is untyped
             */
            typedef T data_type;

            /// @brief shared pointer to base of space holding aspects essence
            const std::shared_ptr<space_base> space = nullptr;

            inline bool is_interfacing() const noexcept {return this->_is_interfacing();}

            inline bool is_of(const std::type_info& t) const noexcept {return this->_is_of(t);}

            /// @brief essences are not copy constructible
            essence(essence<T>&) = delete;

            /// @brief essences are not move constructible
            essence(essence<T>&&) = delete;

            virtual ~essence() {
                ASSERT(!this->atomic_borrowable::is_borrowed() && !this->atomic_borrowable::is_const_borrowed())
                if(this->chunk)
                    this->_dispose();

                this->_destroy();
            }

            /** @brief gets id of essence
             * @return id of astect as std::any
             */
            inline const std::any get_id() const noexcept {
                return this->id;
            }
            
            /** @brief gets id of essence
             * @tparam expected type of id
             * @return typed id of astect
             */
            template<typename ID>
            const ID get_id_as() const {
                return std::any_cast<ID>(this->get_id());
            }
                
            /// @brief if std::is_move_constructible_v<T> pulls aspect from space
            inline void pull(const space_base* s) const noexcept {if(this->space.get() == s) this->_pull();}

            /// @brief if std::is_move_constructible_v<T> pushes aspect to space and deletes it from memory
            inline void push(const space_base* s) const noexcept {if(this->space.get() == s) this->_push();}

            /** @brief checks if essence contains certain data
             * @remark this can only be successfully done when essence is borrowed
             * @param k key identifying borrowing of essence
             * @return true if essence is borrowed and data is certain/created
             */
            inline bool is_certain(const void* const k) const noexcept {
                if(this->is_const_borrowed() || this->is_borrowing(k))
                    return this->chunk;
                else
                    return false;
            }
            
            /** @brief creates data from arguments given on ctor or using default constructor
             * @remark this can only be successfully done when essence is mutably borrowed and data uncertain
             * @param k key identifying borrowing of essence
             * @return true if data creation was successful
             */
            bool create(const void* const k) const noexcept {
                if(this->is_borrowing(k) && !this->chunk) {
                    this->_create_default();
                    return true;
                } else
                    return false;
            }

            /** @brief disposes data
             * @remark this can only be successfully done when essence is mutably borrowed
             * @param k key identifying borrowing of essence
             * @return true if data was disposed
             */
            bool dispose(const void* const k) const noexcept {
                if(this->is_borrowing(k) && this->chunk) {
                    this->_dispose();
                    return true;
                } else
                    return false;
            }

            /** @brief gets type pointer to data
             * @remark this can only be successfully done when essence is borrowed
             * @param k key identifying borrowing of essence
             * @return pointer to data
             */
            T* const get(const void* const k) const noexcept
            requires(concepts::typed<T>) {
                if(this->is_const_borrowed() || this->is_borrowing(k))
                    return this->get_data();
                else
                    return nullptr;
            }
            
            /** @brief gets type pointer to data or creates either from given arguments if type check matches
             * or from default arguments or using default constructor
             * @remark this can only be successfully done when essence is borrowed
             * @param k key identifying borrowing of essence
             * @return pointer to data
             */
            template<typename... Args>
            T* const get_or_create(const void* const k) const noexcept
            requires(concepts::typed<T>) {
                if(this->is_const_borrowed())
                    return this->get_data();
                else if(this->is_borrowing(k)) {
                    if(!this->chunk)
                        this->_create_default();

                    return this->get_data();
                } else
                    return nullptr;
            }

            /** @brief checks if essence is cloneable
             * @return true if data and there fore essence is cloneable
             */
            inline bool is_clonable() const noexcept {
                return this->_is_clonable();
            }

            /** @brief makes an essence representing aspect in a space supporting id generation
             * @param s base of space to create essence in
             * @param args default arguments for lazy data creation
             * @return essence representing uncertain aspect
             */
            template<typename... Args>
            static essence_ptr<T> make(const std::shared_ptr<space_base>& s, Args&&... args)
            requires(concepts::typed<T> && !concepts::immutable<T>) {
                std::shared_ptr<essence<T>> a(new essence<T>(std::any(), s, std::forward<Args>(args)...));
                a->space->create(a->id, a->as_untyped());
                return a;
            }

            /** @brief makes an essence representing aspect in a space using given id
             * @param id of essence representing aspect in space
             * @param s base of space to create aspect in
             * @param args default arguments for lazy aspect creation
             * @return essence representing uncertain aspect
             */
            template<typename... Args>
            static essence_ptr<T> make(const std::any id, const std::shared_ptr<space_base>& s, Args&&... args)
            requires(concepts::typed<T> && !concepts::immutable<T>) {
                std::shared_ptr<essence<T>> a(new essence<T>(id, s, std::forward<Args>(args)...));
                a->space->create(a->id, a->as_untyped());
                return a;
            }

            /** @brief makes an essence representing aspect in a space supporting id generation
             * @param s base of space to create essence in
             * @param args default arguments for aspect creation
             * @return essence representing certain aspect
             */
            template<typename... Args>
            static essence_ptr<T> make_certain(const std::shared_ptr<space_base>& s, Args&&... args)
            requires(concepts::typed<T> && !concepts::immutable<T>) {
                auto a = make(s);
                a->create_(std::forward<Args>(args)...);
                a->push(s.get());
                return a;
            }

            /** @brief makes an aspect representing aspect in a space using given id
             * @param id of aspect representing aspect in space
             * @param s base of space to create aspect in
             * @param args default arguments for data creation
             * @return aspect representing certain aspect
             */
            template<typename... Args>
            static essence_ptr<T> const make_certain(const std::any id, const std::shared_ptr<space_base>& s, Args&&... args)
            requires(concepts::typed<T> && !concepts::immutable<T>) {
                auto a = make(id, s);
                a->create_(std::forward<Args>(args)...);
                a->push(s.get());
                return a;
            }

            /** @brief clones essence if data is std::is_copy_constructible_v<T>
             * @remark this can only be successfully done when aspect is borrowed
             * @param k key identifying borrowing of aspect
             * @param id to use
             * @param s shared_ptr to space holding new aspect
             * @return pointer to new essence or nullptr
             */
            essence_ptr<T> clone(const void* const k, const std::any id, const std::shared_ptr<space_base>& s) const noexcept
            requires(concepts::typed<T>) {
                if(this->is_clonable() &&
                   (this->is_const_borrowed() || this->is_borrowing(k)) && this->is_of(typeid(T))) {
                    return this->_clone(id, s);
                }
                
                return nullptr;
            }

            /** @brief clones essence if data is std::is_copy_constructible_v<T>
             * @remark this can only be successfully done when aspect is borrowed
             * @param k key identifying borrowing of aspect
             * @param id to use
             * @param s shared_ptr to space holding new aspect
             * @return pointer to new essence or nullptr
             */
            essence_ptr<T> clone(const void* const k, const std::any id, const std::shared_ptr<space_base>& s) const noexcept
            requires(!concepts::typed<T>) {
                if(this->is_clonable() &&
                   (this->is_const_borrowed() || this->is_borrowing(k))) {
                    return this->_clone(id, s);
                }
                
                return nullptr;
            }

            /** @brief clones essence for new aspect if data is std::is_copy_constructible_v<T>
             * @remark this can only be successfully done when aspect is borrowed
             * @param k key identifying borrowing of aspect
             * @param s shared_ptr to space holding new aspect
             * @return pointer to new essence or nullptr
             */
            std::shared_ptr<essence<T>> clone(const void* const k, const std::shared_ptr<space_base>& s) const noexcept
            requires(concepts::typed<T>) {
                if(this->is_clonable() &&
                   (this->is_const_borrowed() || this->is_borrowing(k)) && this->is_of(typeid(T))) {
                    return this->_clone(std::any(), s);
                }
                
                return nullptr;
            }

            /** @brief clones essence for new aspect if data is std::is_copy_constructible_v<T>
             * @remark this can only be successfully done when aspect is borrowed
             * @param k key identifying borrowing of aspect
             * @param s shared_ptr to space holding new aspect
             * @return pointer to new essence or nullptr
             */
            std::shared_ptr<essence<T>> clone(const void* const k, const std::shared_ptr<space_base>& s) const noexcept
            requires(!concepts::typed<T>) {
                if(this->is_clonable() &&
                   (this->is_const_borrowed() || this->is_borrowing(k))) {
                    return this->_clone(std::any(), s);
                }
                
                return nullptr;
            }

            void focus(const void* const key) const noexcept override {this->space->focus(this->id, key);}
            void release(const void* const key) const noexcept override {this->space->release(this->id, key);}
            bool is_focused() const noexcept override {return this->space->is_focused(this->id);}

            bool borrow(const void* const key) const noexcept override {
                if(this->atomic_borrowable::borrow(key)) {
                    if(this->on_borrow(key))
                        return true;
                    else
                        this->atomic_borrowable::unborrow(key);
                }
                return false;
            }
            bool const_borrow(const void* const key) const noexcept override {
                if(this->atomic_borrowable::const_borrow(key)) {
                    if(this->space->const_borrow(this->id, key))
                        return true;
                    else
                        this->atomic_borrowable::unborrow(key);
                }
                return false;
            }
            void unborrow(const void* const key, const bool abort=false) const noexcept override {this->on_unborrow(key, abort); this->atomic_borrowable::unborrow(key, abort);}
            bool is_borrowed() const noexcept override {return this->space->is_borrowed(this->id) || this->atomic_borrowable::is_borrowed();}
            bool is_const_borrowed() const noexcept override {return this->space->is_const_borrowed(this->id) || this->atomic_borrowable::is_const_borrowed();}

            /** @brief casts essence to untyped essence pointer
             * @return untyped pointer to essence
             */
            inline essence_ptr<> as_untyped() noexcept {
                return std::reinterpret_pointer_cast<essence<>>(this->shared_from_this());
            }

            /** @brief casts essence to const typed pointer
             * @return const typed essence pointer if type check passed, otherwise nullptr
             */
            template<typename N>
            inline essence_ptr<N> as_typed() noexcept
            requires(!concepts::immutable<N>) {
                if(this->is_of(typeid(N)))
                    return std::reinterpret_pointer_cast<essence<N>>(this->shared_from_this());
                else
                    return nullptr;
            }
        };

        /// @brief exception thrown when trying to modify an active facet
        EXCEPTION_ERROR(facet_active, "trying to modify an active facet")

        /// @brief exception thrown when trying to use an invalid facet
        EXCEPTION_ERROR(facet_invalid, "trying to use an invalid facet")

        /// @brief abstract base for facets of aspect(s)
        class facet : public focusable, public borrowable {};
        class facet_base : public facet {
        protected:
            typedef std::set<const focusable*> focusables;
            typedef std::set<const borrowable*> borrowables;
        private:
            /// @brief set of active focuses
            mutable focusables focuses;

            /// @brief set of active borrowings
            mutable borrowables borrowings;

            mutable const void* focus_key = nullptr, * borrow_key = nullptr;

        protected:
            /** @brief checks if facet is focused or borrowed
             * @return true if focused or borrowed
             */
            bool is_active() const {
                return this->focus_key || this->borrow_key;
            }

            /** @brief ensures modification safety
             * @throw active_facet if it is not safe to modify facet
             */
            virtual void modify() const {
                if(this->is_active())
                    INTERN_THROW_ERROR(scope_core, facet_active())
            }

            /** @brief focuses childs
             * @param fs set of focusable child pointers to focus
             * @param key identifying focus
             */
            void focus_(const focusables fs, const void* const key) const noexcept {
                ASSERT_MSG(scope_core, !this->focus_key, "trying to focus already focused facet")
                this->focus_key = key;
                for(const auto& f_ptr : fs) {
                    f_ptr->focus(key);
                    this->focuses.insert(f_ptr);
                }
            }

            /** borrows childs
             * @param bs set of borrowable child pointers to borrow
             * @param key identifying borrow
             */
            bool borrow_(const borrowables bs, const void* const key) const noexcept {
                ASSERT_MSG(scope_core, !this->borrow_key, "trying to borrow already borrowed facet")
                this->borrow_key = key;
                bool ret =  true;
                for(const auto& b_ptr : bs) {
                    ret = b_ptr->borrow(key);
                    if(ret)
                        this->borrowings.insert(b_ptr);
                    else
                        break;
                }

                if(!ret)
                    this->unborrow(key, true);

                return ret;
            }
        public:
            facet_base() = default;
            facet_base(facet_base&& o)
            : focuses(std::move(o.focuses)), borrowings(std::move(o.borrowings)),
              focus_key(std::move(o.focus_key)), borrow_key(std::move(o.borrow_key)) {
                  focuses.clear();
                  borrowings.clear();
                  focus_key = nullptr;
                  borrow_key = nullptr;
            }
            facet_base(const facet_base&) {};

            /// @brief destructor unborrowing and releasing all childs
            virtual ~facet_base() {
                ASSERT_MSG(scope_core, !this->borrow_key, "trying to destroy borrowed facet")
                ASSERT_MSG(scope_core, !this->focus_key, "trying to destroy focused facet")
            }

            /// @brief converts to bool indicating if facet is ready valid
            virtual operator bool() const noexcept = 0;
            
            void release(const void* const key) const noexcept override {
                ASSERT_MSG(scope_core, this->focus_key, "trying to release an unfocused facet")
                for(auto& f : this->focuses)
                    f->release(key);

                this->focuses.clear();

                this->focus_key = nullptr;
            }

            /// @brief checks if a focus was triggered
            virtual bool is_focused() const noexcept override {return this->focus_key;}

            /// @brief checks if actually focusing on aspects
            inline bool has_focused() const noexcept {return this->is_focused() && !this->focuses.empty();}

            void unborrow(const void* const key, const bool abort=false) const noexcept override {
                ASSERT_MSG(scope_core, this->borrow_key, "trying to unborrow an unborrowed facet")
                for(auto& b : this->borrowings)
                    b->unborrow(key, abort);

                this->borrowings.clear();

                this->borrow_key = nullptr;
            }

            /// @brief checks if a borrow was triggered
            virtual bool is_borrowed() const noexcept override {return this->borrow_key;}

            /// @brief checks if actually borrowing aspects
            inline bool has_borrowed() const noexcept {return this->is_borrowed() && !this->borrowings.empty();}

            /** @brief get key used to focus facet
             * @return key used to focus
             */
            const void* get_focus_key() const {return this->focus_key;}

            /** @brief get key used to borrow facet
             * @return key used to borrow
             */
            const void* get_borrow_key() const {return this->borrow_key;}
        };
        
        /// @brief exception thrown by aspect_ptr when trying to retreive while dangling
        EXCEPTION_ERROR(aspect_ptr_dangling, "trying to retreive while dangling")
        
        /// @brief exception thrown by aspect_ptr when trying to retreive while dangling
        EXCEPTION_ERROR(aspect_ptr_uncertain, "trying to retreive by ptr while uncertain")
        
        /// @brief exception thrown by aspect_ptr when trying to retreive while unborrowed
        EXCEPTION_ERROR(aspect_ptr_unborrowed, "trying to retreive aspect_ptr while unborrowed (copy by implicit cast?)")
        
        /// @brief exception thrown by aspect_ptr when trying to determine while const borrowed
        EXCEPTION_ERROR(aspect_ptr_const_borrowed, "trying to determine aspect_ptr while const borrowed")

        /// @brief exception thrown by aspect_ptr when failing to retype
        EXCEPTION_ERROR(incompatible_aspect_ptr_cast, "trying to wrongly retype aspect_ptr failed")

        /// @brief exception thrown by aspect_ptr when cloning fails
        EXCEPTION_ERROR(cloning_aspect_ptr_failed, "trying to clone aspect_ptr failed")

        /// @brief aspect_ptr base for traits
        class aspect_ptr_base {};

        /** @brief aspect_ptr to an aspect (most simple facet)
         * @remark a aspect_ptr could be dangling
         */
        template<typename T>
        class aspect_ptr final : public facet, aspect_ptr_base {
            template<typename Q>
            friend class aspect_ptr;
        protected:
            mutable const void* focus_key = nullptr, * borrow_key = nullptr;

            /** @brief ensures modification safety
             * @throw active_facet if it is not safe to modify facet
             */
            virtual void modify() const {
                if(this->focus_key || this->borrow_key)
                    INTERN_THROW_ERROR(scope_core, facet_active())
            }

        public:
            /** @brief type of data referenced aspect holds
             * @remark type could be an interface of actually held data
             */
            typedef T aspect_type;

            static_assert(concepts::typed<T>, "aspect_ptr has to be typed");

            static constexpr bool is_immutable = concepts::immutable<T>;

            /// @brief pointer to referenced aspect
            essence_ptr<std::remove_cv_t<T>> ptr;

            aspect_ptr() = default;

            aspect_ptr(const essence_ptr<std::remove_cv_t<T>> a) : ptr(a) {}
            
            aspect_ptr(aspect_ptr<T>&& o) : ptr(std::move(o.ptr)) {}

            aspect_ptr(const aspect_ptr<T>& o) : ptr(o.ptr) {};

            virtual ~aspect_ptr() {
                ASSERT_MSG(scope_core, !this->borrow_key, "trying to destroy borrowed facet")
                ASSERT_MSG(scope_core, !this->focus_key, "trying to destroy focused facet")
            }

            /** @brief gets pointer to referenced aspect
             * @return pointer to referenced aspect or nullptr if dangling
             */
            virtual const essence_ptr<std::remove_cv_t<T>>& get_ptr() const noexcept {return this->ptr;}

            /** @brief checks if aspect_ptr is dangling and there fore not refering an aspect
             * @return true if aspect_ptr is not refering to an aspect
             */
            bool is_dangling() const noexcept {
                return !this->ptr;
            }
            
            /** @brief gets id of referenced aspect
             * @return id of aspect referenced or empty std::any if dangling
             */
            const std::any get_id() const noexcept {
                return this->ptr ? this->ptr->get_id() : std::any();
            }
            
            /** @brief gets id of referenced aspect
             * @return typed id of aspect referenced
             * @throw std::bad_any_cast if dangling or type of id mismatches
             */
            template<typename ID>
            const ID get_id_as() const {
                return std::any_cast<ID>(this->get_id());
            }        

            /** @brief checks if aspect referenced contains certain data
             * @remark this can only be successfully done when aspect_ptr is borrowed and not dangling
             * @return true if aspect_ptr is not dangling, borrowed and data is certain/created
             */
            bool is_certain() const noexcept {return this->ptr && this->ptr->is_certain(this->borrow_key);}

            /** @brief gets pointer to data held by referenced aspect
             * @return pointer to immutable data
             * @throw aspect_ptr_dangling if aspect_ptr is dangling
             * @throw aspect_ptr_unborrowed if aspect_ptr is not or only immutably borrowed
             */
            T* const get() const
            requires(concepts::immutable<T>) {
                if(!this->ptr)
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_dangling())
                if(!this->is_certain())
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_uncertain())
                if(!this->is_borrowed())
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_unborrowed())

                return this->ptr->get(this->borrow_key);
            }

            /** @brief gets pointer to data held or created by referenced aspect
             * @remark the idea is the principle of uncertainty stating that an object gets certain as soon as it is measured
             * @return pointer to mutable data
             * @throw aspect_ptr_dangling if aspect_ptr is dangling
             * @throw aspect_ptr_unborrowed if aspect_ptr is not or only immutably borrowed
             */
            T* const get() const
            requires(!concepts::immutable<T>) {
                if(!this->ptr)
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_dangling())
                if(!this->is_borrowed())
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_unborrowed())

                return this->ptr->get_or_create(this->borrow_key);
            }
            
            /** @brief creates data either from default arguments or using default constructor
             * @remark this can only be successfully done when aspect_ptr is mutably borrowed and data uncertain
             * @return true if data creation was successful
             */
            bool create() const noexcept
            requires(!concepts::immutable<T>) {
                return this->ptr && !this->is_certain() && this->ptr->create(this->borrow_key);
            }

            /** @brief disposes data
             * @remark this can only be successfully done when aspect is mutably borrowed
             * @return true if data was disposed
             */
            bool dispose() const noexcept
            requires(!concepts::immutable<T>) {
                return this->ptr && this->ptr->dispose(this->borrow_key);
            }

            /** @brief checks if aspect is cloneable
             * @return true if aspect_ptr is not dangling aspect is cloneable
             */
            bool is_clonable() const noexcept {
                return this->ptr ? this->ptr->is_clonable() : false;
            }
            
            /** @brief arrow operator to access data using r->member
             * @pre aspect_ptr needs to be typed
             * @return pointer to immutable data
             */
            T* operator->() const
            requires concepts::typed<T> {
                return this->get();
            }

            /** @brief dereference operator to access data using (*r).member
             * @return immutable data
             * @throw aspect_ptr_const_borrowed when immutably borrowed and uncertain
             */
            T& operator*() const
            requires concepts::typed<T> {
                if constexpr(is_immutable)
                    if(!this->is_certain())
                        INTERN_THROW_ERROR(scope_core, aspect_ptr_const_borrowed())
                return *this->get();
            }

            template<typename Q>
            bool operator==(const aspect_ptr<Q>& o) const noexcept {
                return this->ptr->as_untyped() == o.ptr->as_untyped();
            }

            template<typename Q>
            bool operator==(const essence_ptr<Q> o) const noexcept
            requires(!concepts::immutable<Q>) {
                return this->ptr->as_untyped() == o->as_untyped();
            }
            
            template<typename Q>
            bool operator<(const aspect_ptr<Q>& o) const noexcept {
                return this->ptr->as_untyped() < o.ptr->as_untyped();
            }

            /// @brief converts to bool indicating if aspect_ptr is dangling
            virtual operator bool() const noexcept {
                return !this->is_dangling();
            }

            /** @brief assign operator leaves previously referred aspect and relates to new one or dangles
             * @param a new aspect to refer to, dangling if nullptr
             * @return itself
             * @throw active_facet if aspect_ptr is focused or borrowed
             */
            aspect_ptr<T>& operator=(const essence_ptr<std::remove_cv_t<T>> a) {
                this->modify();
                this->ptr = a;
                return *this;
            }

            /** @brief copy assign operator leaves previously referred aspect and
             * relates to new one referred by given reference or dangles
             * @param o aspect_ptr to copy
             * @return itself
             * @throw active_facet if aspect_ptr is focused or borrowed
             */
            aspect_ptr<T>& operator=(const aspect_ptr<T>& o) {
                this->modify();
                this->ptr = o.ptr;
                return *this;
            }
            
            /// @brief resets to dangling
            void reset() {
                this->modify();
                this->ptr = nullptr;
            }

            virtual void focus(const void* const key) const noexcept override {
                ASSERT_MSG(scope_core, !this->focus_key, "trying to focus an already focused aspect_ptr")
                if(this->ptr && !this->is_focused()) {
                    this->ptr->focus(key);
                    this->focus_key = key;
                }
            }
            
            void release(const void* const key) const noexcept override {
                ASSERT_MSG(scope_core, this->focus_key, "trying to release an unfocused aspect_ptr")
                this->ptr->release(key);

                this->focus_key = nullptr;
            }

            virtual bool is_focused() const noexcept override {return this->focus_key;}

            virtual bool borrow(const void* const key) const noexcept override {
                ASSERT_MSG(scope_core, !this->borrow_key, "trying to borrow an already borrowed aspect_ptr")
                if(this->ptr && !this->is_borrowed()) {
                    if constexpr(concepts::immutable<T>) {
                        if(this->ptr->const_borrow(key)) {
                            this->borrow_key = key;
                            return true;
                        } else
                            return false;
                    } else {
                        if(this->ptr->borrow(key)) {
                            this->borrow_key = key;
                            return true;
                        } else
                            return false;
                    }
                }

                return false;
            }

            void unborrow(const void* const key, const bool abort=false) const noexcept override {
                ASSERT_MSG(scope_core, this->borrow_key, "trying to unborrow an unborrowed aspect_ptr")
                this->ptr->unborrow(key, abort);

                this->borrow_key = nullptr;
            }

            virtual bool is_borrowed() const noexcept override {return this->borrow_key;}

            /** @brief clones referred aspect into a space using id
             * @param id id to use for identifying aspect in new space
             * @param s space clones essence refers to
             * @return aspect_ptr referring cloned aspect
             * @throw aspect_ptr_dangling given aspect_ptr is dangling
             * @throw aspect_ptr_unborrowed aspect_ptr is unborrowed
             */
            aspect_ptr<std::remove_cv_t<T>> clone(const std::any id, const std::shared_ptr<space_base>& s) const {
                if(!this->ptr)
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_dangling())
                if(!this->is_borrowed())
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_unborrowed())

                auto a = s->clone(this->borrow_key, this->ptr->as_untyped(), id)->template as_typed<std::remove_cv_t<T>>();
                if(!a)
                    INTERN_THROW_ERROR(scope_core, cloning_aspect_ptr_failed())
                return aspect_ptr<std::remove_cv_t<T>>(a);
            }

            /** @brief clones referred aspect into a space supporting id generation
             * @param s space clones essence refers to
             * @return aspect_ptr referring cloned aspect
             * @throw aspect_ptr_dangling given aspect_ptr is dangling
             * @throw aspect_ptr_unborrowed aspect_ptr is unborrowed
             */
            aspect_ptr<std::remove_cv_t<T>> clone(const std::shared_ptr<space_base>& s) const {
                if(!this->ptr)
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_dangling())
                if(!this->is_borrowed())
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_unborrowed())

                auto a = s->clone(this->borrow_key, this->ptr->as_untyped())->template as_typed<std::remove_cv_t<T>>();
                if(!a)
                    INTERN_THROW_ERROR(scope_core, cloning_aspect_ptr_failed())
                return aspect_ptr<std::remove_cv_t<T>>(a);
            }

            /// @brief tries to cast aspect_ptr into a copy of new inner type
            template<typename N>
            operator aspect_ptr<N>() const
            requires(concepts::possibly_convertible<std::remove_cv_t<T>, std::remove_cv_t<N>>) {
                if(!this->ptr)
                    INTERN_THROW_ERROR(scope_core, aspect_ptr_dangling())

                if(auto a = this->ptr->template as_typed<std::remove_cv_t<N>>()) {
                    return a;
                } else
                    INTERN_THROW_ERROR(scope_core, incompatible_aspect_ptr_cast())
            }
        };

        template <typename To, typename From>
        inline aspect_ptr<To> aspect_ptr_cast(const aspect_ptr<From>& r)
        { return static_cast<aspect_ptr<To>>(r); }

        /// @brief helper focusing and borrowing existing facet
        template<typename F>
        struct opener : public focus<F>, public borrow<F> {
        private:
            F* const ptr;

        public:
            opener(F& f)
            : focus<F>(f, this), borrow<F>(f, this), ptr(&f) {}
        };

        /** @brief simple space allowing to store aspects purely in memory identified by ther memory address
         * @remark at the moment address space is a singleton using local RAM as storage, this might get extended to use different memories
         */
        class address_space final : public std::enable_shared_from_this<address_space>, public space_base {
        private:
            address_space() = default;

        public:
            /// @brief singleton getter
            static address_space& get() {
                static std::shared_ptr<address_space> i(new address_space());
                return *i;
            }

            void focus(const std::any id, const void* const key) noexcept override {}
            void release(const std::any id, const void* const key) noexcept override {}
            bool is_focused(const std::any id) const noexcept override {return false;}

            bool borrow(const std::any id, const void* const key) noexcept override {return true;}
            bool const_borrow(const std::any id, const void* const key) noexcept override {return true;}
            void unborrow(const std::any id, const void* const key, const bool abort) noexcept override {}
            bool is_borrowed(const std::any id) const noexcept override {return false;}
            bool is_const_borrowed(const std::any id) const noexcept override {return false;}

            void create(const std::any id, const essence_ptr<> a) noexcept override {}
            void dispose(const std::any id) noexcept override {}
            void destroy(const std::any id) noexcept override {}

            void push(std::string type, std::any id, void* ptr) noexcept override {}
            void* const pull(std::string type, std::any id) noexcept override {return nullptr;}

            /** @brief spawn reference to a new uncertain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param args arguments to use at lazy construction time
             * @return reference to aspect
             */
            template<typename T, typename... Args>
            inline aspect_ptr<T> spawn(Args&&... args) noexcept {
                return aspect_ptr<T>(essence<T>::make(this->shared_from_this(), std::forward<Args>(args)...));
            }

            /** @brief spawn reference to a new uncertain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param id to use for identifying aspect
             * @param args arguments to use at lazy construction time
             * @return reference to aspect
             */
            template<typename T, typename... Args>
            inline aspect_ptr<T> spawn(std::any id, Args&&... args) noexcept {
                return aspect_ptr<T>(essence<T>::make(this->shared_from_this(), std::forward<Args>(args)...));
            }

            /** @brief spawn a new uncertain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param args arguments to use at lazy construction time
             * @return pointer to aspect
             */
            template<typename T, typename... Args>
            inline essence_ptr<T> spawn_aspect(Args&&... args) noexcept {
                return essence<T>::make(this->shared_from_this(), std::forward<Args>(args)...);
            }

            /** @brief spawn a new uncertain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param id to use for identifying aspect
             * @param args arguments to use at lazy construction time
             * @return pointer to aspect
             */
            template<typename T, typename... Args>
            inline essence_ptr<T> spawn_aspect(std::any id, Args&&... args) noexcept {
                return essence<T>::make(this->shared_from_this(), std::forward<Args>(args)...);
            }

            /** @brief spawn reference to a new certain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param args arguments to use at construction time
             * @return reference to aspect
             */
            template<typename T, typename... Args>
            inline aspect_ptr<T> forge(Args&&... args) noexcept {
                return aspect_ptr<T>(essence<T>::make_certain(this->shared_from_this(), std::forward<Args>(args)...));
            }

            /** @brief spawn reference to a new certain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param id to use for identifying aspect
             * @param args arguments to use at construction time
             * @return reference to aspect
             */
            template<typename T, typename... Args>
            inline aspect_ptr<T> forge(std::any id, Args&&... args) noexcept {
                return aspect_ptr<T>(essence<T>::make_certain(this->shared_from_this(), std::forward<Args>(args)...));
            }

            /** @brief spawn a new certain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param args arguments to use at construction time
             * @return pointer to aspect
             */
            template<typename T, typename... Args>
            inline essence_ptr<T> spawn_aspect_certain(Args&&... args) noexcept {
                return essence<T>::make_certain(this->shared_from_this(), std::forward<Args>(args)...);
            }

            /** @brief spawn a new certain aspect of given type
             * @tparam type of aspect
             * @tparam argument types
             * @param id to use for identifying aspect
             * @param args arguments to use at construction time
             * @return pointer to aspect
             */
            template<typename T, typename... Args>
            inline essence_ptr<T> spawn_aspect_certain(std::any id, Args&&... args) noexcept {
                return essence<T>::make_certain(this->shared_from_this(), std::forward<Args>(args)...);
            }

            essence_ptr<> clone(const void* const key, essence_ptr<> ptr, std::any id = std::any()) override {
                return ptr->clone(key, id, this->shared_from_this());
            }
        };

        /** @brief spawn reference to new uncertain aspect in address_space
         * @tparam type of aspect
         * @tparam argument types
         * @param args arguments to use at lazy construction time
         * @return reference to aspect
         */
        template<typename T, typename... Args>
        aspect_ptr<T> spawn(Args&&... args) noexcept {
            return address_space::get().template spawn<T, Args...>(std::forward<Args>(args)...);
        }

        /** @brief spawn reference to new certain aspect in address_space
         * @tparam type of aspect
         * @tparam argument types
         * @param args arguments to use at construction time
         * @return reference to aspect
         */
        template<typename T, typename... Args>
        aspect_ptr<T> forge(Args&&... args) noexcept {
            return address_space::get().template forge<T, Args...>(std::forward<Args>(args)...);
        }

        /** @brief clone referred aspects data into address_space
         * @tparam type of aspect or interface type implemented by aspect type
         * @tparam argument types
         * @param r reference to aspect
         * @return reference to clone
         */
        template<typename T>
        aspect_ptr<T> clone(aspect_ptr<T>& r) {
            return r.clone(std::any(), address_space::get().shared_from_this());
        }
    }
}