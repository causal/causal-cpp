// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <cstddef>
#include <cassert>
#include <atomic>

#include "causal/trace.hpp"

namespace causal {
    namespace core {
        /// @brief interface describing focusable objects
        class focusable {
        public:
            /** @brief focuses object using a unique key
             * @param key identifying focus of object
             */
            virtual void focus(const void* const key) const noexcept = 0;

            /** @brief releases a focus on object
             * @param key identifying focus to release
             */
            virtual void release(const void* const key) const noexcept = 0;

            /** @brief checks if object is focused
             * @return true if object is focused
             */
            virtual bool is_focused() const noexcept = 0;
        };

        /// @brief helper focusing focusable
        template<typename F>
        struct focus {
            static_assert(std::is_base_of_v<focusable, F>, "focus requires a focusable type");
        private:
            /// @brief pointer to focusable
            F* const able;

            /// @brief key identifying focus of given focusable
            const void* const key;

        public:
            /** @brief constructor focusing focusable using given key
             * @param f focusable to focus
             * @param k key identifying focus of given focusable
             */
            focus(F& f, const void* const k)
            : able(&f), key(k ? k : this) {
                this->able->focus(this->key);
            }

            /// @brief destructor releasing focus
            virtual ~focus() {
                this->able->release(this->key);
            }
        };
    }
}