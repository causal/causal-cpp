// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <cstddef>
#include <cassert>
#include <atomic>

#include "causal/trace.hpp"

namespace causal {
    namespace core {
        /// @brief interface describing borrowable objects
        class borrowable {
        public:
            /** @brief mutably borrows object using unique key
             * @param key identifying borrow of object
             */
            virtual bool borrow(const void* const key) const noexcept = 0;

            /** @brief unborrows a borrow of object
             * @param key identifying borrow to unborrow
             * @param abort indicates if transaction is to cancel or to commit
             */
            virtual void unborrow(const void* const key, const bool abort=false) const noexcept = 0;

            /** @brief checks if object is mutably borrowed
             * @return true if object is mutably borrowed
             */
            virtual bool is_borrowed() const noexcept = 0;
        };

        /// @brief interface describing immutably borrowable objects
        class const_borrowable {
        public:
            /** @brief immutably borrows object using unique key
             * @param key identifying borrow of object
             */
            virtual bool const_borrow(const void* const key) const noexcept = 0;

            /** @brief unborrows a borrow of object
             * @param key identifying borrow to unborrow
             * @param abort indicates if transaction is to cancel or to commit
             */
            virtual void unborrow(const void* const key, const bool abort=false) const noexcept = 0;

            /** @brief checks if object is immutably borrowed
             * @return true if object is immutably borrowed
             */
            virtual bool is_const_borrowed() const noexcept = 0;
        };

        /** @brief managing borrowings using atomics
         * @remark const borrowings are not registered by key but only by count
         */
        class atomic_borrowable : public borrowable, public const_borrowable {
        private:
            /// @brief key locking borrowable
            mutable std::atomic<void const*> borrowing = nullptr;

            /// @brief count of borrowings of same key
            mutable std::atomic<size_t> borrowing_count = 0;

            /// @brief count of const borrowings
            mutable std::atomic<size_t> const_borrowing_count = 0;

        public:
            atomic_borrowable() = default;
            atomic_borrowable(atomic_borrowable&& o) noexcept
            : borrowing(o.borrowing.load()) {
                ASSERT_MSG(scope_core, o.const_borrowing_count == 0, "cannot copy  borrowable while const borrowing")
                ASSERT_MSG(scope_core, o.borrowing == nullptr, "cannot copy  borrowable while borrowing")
            }

            virtual ~atomic_borrowable() = default;

            virtual bool borrow(const void* const key) const noexcept override {
                void const* nil = nullptr;

                if(this->borrowing.compare_exchange_strong(nil, key)) {
                    if(this->const_borrowing_count == 0) {
                        this->borrowing_count++;
                        return true;
                    } else {
                        this->borrowing = nil;
                    }
                } else if(this->borrowing.load() == key) {
                    this->borrowing_count++;
                    return true;
                }
                
                return false;
            }

            virtual bool const_borrow(const void* const key) const noexcept override {
                void const* nil = nullptr;
                if(this->borrowing.load() == key) {
                    this->const_borrowing_count++;
                    return true;
                }

                if(this->borrowing.compare_exchange_strong(nil, key)) {
                    this->const_borrowing_count++;
                    this->borrowing = nil;
                    return true;
                }
                
                return false;
            }

            /*virtual bool upgrade(const void* const key) const noexcept {
                void const* nil = nullptr;
                if(this->borrowing.compare_exchange_strong(nil, key)) {
                    size_t expect = 1;
                    if(this->const_borrowing_count.compare_exchange_strong(expect, 0))
                        return true;
                    else {
                        this->borrowing = nil;
                    }
                }

                return false;
            }

            virtual bool downgrade(const void* const key) const noexcept {
                if(this->borrowing == key) {
                    this->const_borrowing_count++;
                    this->borrowing = nullptr;
                    return true;
                } else
                    return false;
            }*/

            virtual void unborrow(const void* const key, [[maybe_unused]] const bool abort=false) const noexcept override {
                if(this->const_borrowing_count > 0) {
                    this->const_borrowing_count--;
                } else if(this->borrowing == key) {
                    if(--this->borrowing_count == 0)
                        this->borrowing = nullptr;
                }
            }

            /** @brief checks if a key is mutably borrowing object
             * @remark this does not indicate if key is immutably borrowing object
             * @param key identifying borrow of object
             * @return true if key is borrowing object
             */
            virtual bool is_borrowing(const void* const key) const noexcept {
                return this->borrowing == key;
            }

            virtual bool is_borrowed() const noexcept override {
                return this->borrowing != nullptr;
            }

            virtual bool is_const_borrowed() const noexcept override {
                return this->const_borrowing_count > 0;
            }
        };
        
        /// @brief exception thrown by borrowable when failing to assign an aspect on creation
        EXCEPTION_ERROR(borrowing_failed, "unsuccessfully tried to borrow")

        /// @brief helper mutably borrowing borrowable
        template<typename B>
        struct borrow {
            static_assert(std::is_base_of_v<borrowable, B>, "borrow requires a borrowable type");
        private:
            /// @brief pointer to borrowable
            B* const able;

            /// @brief key identifying borrow of given borrowable
            const void* const key;

            /// @brief indicates if borrow transaction
            bool _abort = false;

        public:
            /** @brief constructor mutably borrowing borrowable using given key
             * @param b borrowable to borrow
             * @param k key identifying borrow of given borrowable
             */
            borrow(B& b, const void* const k)
            : able(&b), key(k ? k : this) {
                if(!this->able->borrow(this->key))
                    INTERN_THROW_ERROR(scope_core, borrowing_failed())
            }

            /// @brief destructor unborrowing borrow
            virtual ~borrow() {
                this->able->unborrow(this->key, this->_abort);
            }

            /** @brief sets borrow to abort state so
             * transaction is aborted when borrow gets destroyed
             */
            inline void abort() noexcept {this->abort = true;}
        };

        /// @brief helper immutably borrowing borrowable
        template<typename B>
        struct const_borrow {
            static_assert(std::is_base_of_v<const_borrowable, B>, "borrow requires a borrowable type");
        private:
            /// @brief pointer to borrowable
            B* const able;

            /// @brief key identifying borrow of given borrowable
            const void* const key;

        public:
            /** @brief constructor immutably borrowing borrowable using given key
             * @param b borrowable to borrow
             * @param k key identifying borrow of given borrowable
             */
            const_borrow(B& b, const void* const k)
            : able(&b), key(k ? k : this) {
                if(!this->able->const_borrow(this->key))
                    INTERN_THROW_ERROR(scope_core, borrowing_failed())
            }

            /// @brief destructor unborrowing borrow
            ~const_borrow() {
                this->able->unborrow(this->key, true);
            }
        };
    }
}