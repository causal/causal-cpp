// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <type_traits>
#include <exception>
#include <memory>
#include <vector>

#include "causal/trace.hpp"

#include "traits.hpp"
#include "aspect.hpp"

namespace causal {
    namespace facet {
        namespace cc = causal::core;
        template<typename... F> requires(std::is_base_of_v<cc::facet, F> && ...) class edges;
        template<typename F> class spreads;
        template<typename F, typename S> class scales;
        template<typename D, typename F> class routes;
    }
    namespace core {
        namespace traits {
            template<typename T> using value_type
                = std::remove_reference_t<T>;
            template<typename T> using plain_type
                = std::remove_cv_t<value_type<T>>;
        }

        namespace concepts {
            namespace type {
                namespace cf = causal::facet;

                template<typename T1, typename T2> concept equal
                    = std::is_same_v<traits::plain_type<T1>, traits::plain_type<T2>>;

                template<typename T> concept pointer
                    = std::is_pointer_v<traits::value_type<T>>;

                template<typename T> concept reference
                    = std::is_reference_v<T>;

                template<typename T> concept lvalue_reference
                    = std::is_lvalue_reference_v<T>;

                template<typename T> concept rvalue_reference
                    = std::is_rvalue_reference_v<T>;

                template<typename T> concept value
                    = !reference<T>;

                template<typename T> concept immutable
                    = std::is_const_v<traits::value_type<T>>;

                template<typename T> concept facet
                    = !type::pointer<T> &&
                      std::is_base_of_v<facet, traits::plain_type<T>>;

                template<typename T> concept aspect_ptr_facet
                    = facet<T> && (is_instance_of_v<aspect_ptr, traits::plain_type<T>> || is_derived_from_template_v<aspect_ptr, traits::plain_type<T>>);

                template<typename T> concept edges_facet
                    = facet<T> && (is_instance_of_v<cf::edges, traits::plain_type<T>> || is_derived_from_template_v<cf::edges, traits::plain_type<T>>);

                template<typename T> concept spreads_facet
                    = facet<T> && (is_instance_of_v<cf::spreads, traits::plain_type<T>> || is_derived_from_template_v<cf::spreads, traits::plain_type<T>>);

                template<typename T> concept scales_facet
                    = facet<T> && (is_instance_of_v<cf::scales, traits::plain_type<T>> || is_derived_from_template_v<cf::scales, traits::plain_type<T>>);

                template<typename T> concept routes_facet
                    = facet<T> && (is_instance_of_v<cf::routes, traits::plain_type<T>> || is_derived_from_template_v<cf::routes, traits::plain_type<T>>);

                template<typename FromT, typename ToT> concept convertible
                    = std::is_convertible_v<traits::plain_type<FromT>, traits::plain_type<ToT>>;
            }

            /// @brief copying value from lvalue and moving into destination value
            template<typename SrcT, typename DstT> concept copy_move
                = (type::lvalue_reference<SrcT> || !type::equal<SrcT, DstT>) &&
                  type::value<DstT> &&
                  type::convertible<SrcT, DstT>;

            /// @brief forwarding value from rvalue and moving into destination value
            template<typename SrcT, typename DstT> concept forward_move
                = !type::lvalue_reference<SrcT> &&
                  type::value<DstT> &&
                  type::equal<SrcT, DstT>;

            /// @brief copying a facet into destination lvalue reference
            template<typename SrcT, typename DstT> concept copy_open
                = (type::lvalue_reference<SrcT> || !type::equal<SrcT, DstT>) && type::facet<SrcT> &&
                  type::lvalue_reference<DstT> && type::facet<DstT> &&                  
                  type::convertible<SrcT, DstT>;

            /// @brief dereference a facet into destination lvalue reference
            template<typename SrcT, typename DstT> concept forward_open
                = !type::lvalue_reference<SrcT> && type::facet<SrcT> &&
                  type::lvalue_reference<DstT> && type::facet<DstT> &&
                  type::equal<SrcT, DstT>;

            /// @brief copying value from lvalue and spawning a new aspect constructed using SrcT
            template<typename SrcT, typename DstT> concept copy_spawn_open
                = (type::lvalue_reference<SrcT> || !type::equal<aspect_ptr<traits::value_type<SrcT>>, DstT>) && !type::aspect_ptr_facet<SrcT> &&
                  type::lvalue_reference<DstT> && type::aspect_ptr_facet<DstT> &&
                  type::convertible<aspect_ptr<traits::value_type<SrcT>>, DstT>;

            /// @brief copying value from lvalue and spawning a new aspect constructed using SrcT
            template<typename SrcT, typename DstT> concept forward_spawn_open
                = !type::lvalue_reference<SrcT> && !type::aspect_ptr_facet<SrcT> &&
                  type::lvalue_reference<DstT> && type::aspect_ptr_facet<DstT> &&
                  type::equal<aspect_ptr<traits::value_type<SrcT>>, DstT>;

            /// @brief copying ref facet and dereference into destination lvalue reference
            template<typename SrcT, typename DstT> concept copy_deref
                = (type::lvalue_reference<SrcT> || !type::equal<SrcT, aspect_ptr<traits::value_type<DstT>>>) && type::aspect_ptr_facet<SrcT> &&
                  type::lvalue_reference<DstT> && !type::aspect_ptr_facet<DstT> &&
                  type::convertible<SrcT, aspect_ptr<traits::value_type<DstT>>>;

            /// @brief dereference a ref facet into destination lvalue reference
            template<typename SrcT, typename DstT> concept forward_deref
                = !type::lvalue_reference<SrcT> && type::aspect_ptr_facet<SrcT> &&
                  type::lvalue_reference<DstT> && !type::aspect_ptr_facet<DstT> &&
                  type::equal<SrcT, aspect_ptr<traits::value_type<DstT>>>;
        }

        /// @brief key pointer helper type
        struct key {};

        /// @brief abstract base for accessors
        class access_ {
        protected:
            access_() = default;
            access_(const access_&) = default;

        public:
            virtual ~access_() noexcept = default;

            /** @brief gets void pointer to data using borrow key
             * @param key used to borrow
             * @return void* to data
             */
            virtual void* get(const void* const key) noexcept = 0;

            /** @brief gets if data is const
             * @return true if data is const
             */
            virtual bool is_immutable() const noexcept = 0;

            /** @brief gets hash code of destination type
             * @return hash code of destination type
             */
            virtual size_t get_dst_hash() const noexcept = 0;

            /** @brief focuses data if possible
             * @param key to use for focusing
             */
            virtual void focus(const void* const key) noexcept = 0;

            /** @brief releases data if necessary
             * @param key used for focusing
             */
            virtual void release(const void* const key) noexcept = 0;

            /** @brief borrow data if possible
             * @param key to use for borrowing
             * @return true if successfully borrowed or not borrowable
             */
            virtual bool borrow(const void* const key) noexcept = 0;

            /** @brief unborrow data if necessary
             * @param key used for borrowing
             * @param abort if transaction shall be canceled
             */
            virtual void unborrow(const void* const key, const bool abort) noexcept = 0;

            /** @brief clones accessor and if copied also data
             * @return pointer to new accessor base
             */
            virtual access_* clone() noexcept = 0;
            
            /** @brief gets data as destination pointer if type hashes match
             * @remark asserts on type hash mismatch
             * @tparam DstT type which shall be returned
             * @param key used for borrowing
             * @return data, reference or pointer depending on DstT
             */
            template<typename DstT>
            DstT get_as(const void* const key) noexcept {
                ASSERT_MSG(scope_core, typeid(DstT).hash_code() == this->get_dst_hash(), "DstT does not equal runtime type hash")

                typename traits::value_type<DstT>* const ptr = reinterpret_cast<traits::value_type<DstT>*>(this->get(key));

                if constexpr(concepts::type::lvalue_reference<DstT>)
                    return *ptr;
                else
                    return std::move(*ptr);
            }
        };

        /// @brief implementation of typed accessor
        template<typename SrcT, typename DstT>
        class access final : public access_ {
        private:
            static_assert(
                concepts::copy_move<SrcT, DstT> || concepts::forward_move<SrcT, DstT> ||
                concepts::copy_open<SrcT, DstT> || concepts::forward_open<SrcT, DstT> ||
                concepts::copy_spawn_open<SrcT, DstT> || concepts::forward_spawn_open<SrcT, DstT> ||
                concepts::copy_deref<SrcT, DstT> || concepts::forward_deref<SrcT, DstT>,
                "no capture concept was found for tick's parameter");

            static constexpr bool store_facet = concepts::copy_open<SrcT, DstT> || concepts::forward_open<SrcT, DstT> ||
                                                concepts::copy_spawn_open<SrcT, DstT> || concepts::forward_spawn_open<SrcT, DstT> ||
                                                concepts::copy_deref<SrcT, DstT> || concepts::forward_deref<SrcT, DstT>;

            static constexpr bool shall_deref = concepts::copy_deref<SrcT, DstT> || concepts::forward_deref<SrcT, DstT>;

            bool focused = false;
            bool borrowed = false;

            typedef typename std::conditional_t<concepts::copy_deref<SrcT, DstT> || concepts::forward_deref<SrcT, DstT>, aspect_ptr<traits::value_type<DstT>>, traits::plain_type<DstT>> StoreT;

            StoreT store;

            static StoreT make_store(SrcT&& s)
            requires(concepts::copy_move<SrcT, DstT>) {
                return s;
            }

            static StoreT&& make_store(SrcT&& s)
            requires(concepts::forward_move<SrcT, DstT>) {
                return std::forward<SrcT>(s);
            }

            static StoreT make_store(SrcT&& s)
            requires(concepts::copy_open<SrcT, DstT> || concepts::copy_deref<SrcT, DstT>) {
                if(!s)
                    INTERN_THROW_ERROR(scope_core, facet_invalid())
                return s;
            }

            static StoreT make_store(SrcT&& s)
            requires(concepts::copy_spawn_open<SrcT, DstT>) {
                return address_space::get().template forge<typename traits::plain_type<SrcT>>(s);
            }

            static StoreT make_store(SrcT&& s)
            requires(concepts::forward_spawn_open<SrcT, DstT>) {
                return address_space::get().template forge<typename traits::plain_type<SrcT>>(std::forward<SrcT>(s));
            }

            static StoreT&& make_store(SrcT&& s)
            requires(concepts::forward_open<SrcT, DstT> || concepts::forward_deref<SrcT, DstT>) {
                if(!s)
                    INTERN_THROW_ERROR(scope_core, facet_invalid())
                return std::forward<SrcT>(s);
            }

            access(const access& o) : store(o.store) {
                ASSERT_MSG(scope_core, !o.focused && !o.borrowed, "trying to copy access while focused or borrowed")
            }
            
        public:
            access(access&&) = default;

            access(SrcT&& s) requires(concepts::copy_move<SrcT, DstT> ||
                                      concepts::copy_open<SrcT, DstT> ||
                                      concepts::copy_spawn_open<SrcT, DstT> ||
                                      concepts::forward_spawn_open<SrcT, DstT> ||
                                      concepts::copy_deref<SrcT, DstT>)
            : store(make_store(std::forward<SrcT>(s))) {}

            access(SrcT&& s) requires(concepts::forward_move<SrcT, DstT> ||
                                      concepts::forward_open<SrcT, DstT> ||
                                      concepts::forward_deref<SrcT, DstT>)
            : store(std::forward<StoreT>(make_store(std::forward<SrcT>(s)))) {}

            virtual ~access() = default;

            virtual access_* clone() noexcept override {
                ASSERT_MSG(scope_core, !this->focused && !this->borrowed, "access cloning operation during focusing or borrowing; or memory corruption!")
                return new access(*this);
            }

            bool is_immutable() const noexcept override {
                return concepts::type::immutable<traits::value_type<DstT>>;
            }

            virtual size_t get_dst_hash() const noexcept override {
                return typeid(traits::value_type<DstT>).hash_code();
            }

            virtual void focus(const void* const key) noexcept override {
                if(!this->focused) {
                    if constexpr(store_facet) {
                        this->store.focus(key);
                        this->focused = true;
                    } else
                        this->focused = true;
                }
            }

            virtual void release(const void* const key) noexcept override {
                ASSERT_MSG(scope_core, this->focused, "access is only allowed to release when focused")
                if(this->focused) {
                    if constexpr(store_facet)
                        this->store.release(key);
                    this->focused = false;
                }
            }

            virtual bool borrow(const void* const key) noexcept override {
                ASSERT_MSG(scope_core, this->focused, "access is only allowed to borrow when focused")
                if(!this->borrowed) {
                    if constexpr(store_facet)
                        this->borrowed = this->store.borrow(key);
                    else
                        this->borrowed = true;
                }
                
                return this->borrowed;
            }

            virtual void unborrow(const void* const key, const bool abort) noexcept override {
                ASSERT_MSG(scope_core, this->focused && this->borrowed, "access is only allowed to unborrow when focused and borrowed")
                if constexpr(store_facet)
                    this->store.unborrow(key, abort);
                this->borrowed = false;
            }

            virtual void* get(const void* const key) noexcept override {
                ASSERT_MSG(scope_core, this->focused && this->borrowed, "access is only allowed when focused and borrowed; or memory corruption")
                if constexpr(shall_deref)
                    return const_cast<traits::plain_type<DstT>*>(this->store.get());
                else
                    return &this->store;
            }
        };

        /** @brief creates set of accessors for function and using given arguments
         * @tparam SrcArgs types of argument sources
         * @tparam R return type of function
         * @tparam DstArgs types of argument destinations
         * @param f function to create accessors for
         * @param src argument sources
         * @return vector of accessor bases
         */
        template<typename... SrcArgs, typename R, typename... DstArgs>
        std::vector<access_*> get_access(R(*f)(DstArgs...), SrcArgs&&... src) {
            static_assert(sizeof...(SrcArgs) == sizeof...(DstArgs), "source and destination parameter packs need to be of the same size");
            return std::vector<access_*>({new access<SrcArgs, DstArgs>(std::forward<SrcArgs>(src))...});
        }

        /// @brief sequence helper for apply_access
        template<typename R, typename... Args, size_t... Is>
        R apply_access(R(*f)(Args...), const void* const key, const std::vector<access_*>& a, seq<Is...>) {
            return f(a[Is]->get_as<Args>(key)...);
        }

        /** @brief applies a set of accessors to a function an calls it
         * @tparam R return type of function
         * @tparam Args expected argument types of given function
         * @param f function to call
         * @param key used for focusing and borrowing
         * @param a vector of accessor bases to apply
         */
        template<typename R, typename... Args>
        R apply_access(R(*f)(Args...), const void* const key, const std::vector<access_*>& a) {
            return apply_access(f, key, a, gen_seq<sizeof...(Args)>{});
        }
    }
}