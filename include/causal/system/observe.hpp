// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <vector>

#include "causal/core/wirks.hpp"

// An approach to design a "minimum interference observation" which requires observed to know observers and to actively update/project changes
namespace causal {
    namespace system {
        /// @brief base of observable allowing aspect to be observed using projection avoiding causal processing
        template<typename Otion>
        struct Observable {
        private:
            std::vector<std::shared_ptr<Otion>> observations;

        public:
            /** @brief project observing value onto observation
             * @param v value to project
             */
            void project(const typename Otion::type v) const {
                for(auto& e : this->observations)
                    e->percept(v);
            }

            /** @brief registers observable for observation
             * @param o observation to register with
             */
            void focus(const std::shared_ptr<Otion>& o) {
                auto it = std::find(this->observations.begin(), this->observations.end(), o);
                if(it == this->observations.end())
                    this->observations.push_back(o);
            }

            /** @brief deregisters observable from observation
             * @param o observation to deregister from
             */
            void leave(const std::shared_ptr<Otion>& o) {
                auto it = std::find(this->observations.begin(), this->observations.end(), o);
                if(it != this->observations.end())
                    this->observations.erase(it);
            }
        };

        /// @brief abstract base of observation
        template<typename V>
        class observation {
            template<typename Oer, typename Ole>
            friend struct Observe;

        protected:
            observation() = default;

        public:
            typedef V type;

            /** @brief percepts projected value
             * @param v value projected
             */
            virtual void percept(const V& v) const = 0;
        };
    }
}
