// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include "causal/facet/common.hpp"
#include "causal/facet/spreads.hpp"
#include <causal/data/msgpack.hpp>

namespace causal {
    namespace system {
        namespace cf = causal::facet;

        template<typename B>
        struct Oscillator;

        /// @brief bond between discretely oscillating cell automatons
        template<typename... O>
        struct Bond {
            typedef cf::ptr_edges<O...> facet_type;
            facet_type oscillators;

            /// @brief 1 / spring rate
            int64_t loose = 1;

            MSGPACK_DEFINE(loose, oscillators)

            /** @brief applying kinetics
             * @param os borrowed neighbour facet
             * @return true if anything applied
             */
            virtual bool apply(facet_type& os) {
                int64_t aspect_ptr_elongation = 0, aspect_ptr_velocity = 0;
                os.foreach([&aspect_ptr_elongation, &aspect_ptr_velocity](const auto& r, const size_t i) {
                    aspect_ptr_elongation += r->elongation;
                    aspect_ptr_velocity += r->velocity;
                    return true;
                });
                aspect_ptr_elongation /= os.size();
                aspect_ptr_velocity /= os.size();

                bool applied = false;
                os.foreach([this, &aspect_ptr_elongation, &applied](auto& r, const size_t i) {
                    if(!r->fixed && aspect_ptr_elongation != r->elongation) {
                        int64_t f = aspect_ptr_elongation - r->elongation;
                        int64_t v_ = f / (this->loose*r->inertia);
                        r->velocity += v_;
                        applied = applied || v_ != 0;
                    }
                    return true;
                });

                return applied;
            }
        };

        /// @brief discretely oscillating cell automaton
        template<typename B>
        struct Oscillator {
            typedef cf::ptr_spreads<B> facet_type;
            facet_type bonds;
            
            /// @brief inertia(mass)
            int64_t inertia = 1;

            /// @brief actual velocity
            int64_t velocity = 0;

            /// @brief actual elongation
            int64_t elongation = 0;

            /// @brief fixed oscillator cannot apply kinetics
            bool fixed = false;

            MSGPACK_DEFINE(inertia, velocity, elongation, fixed, bonds)

            /** @brief add energy to oscillator
             * @param e energy to add
             */
            virtual void push(int64_t e) {
                this->velocity += e / this->inertia;
            }

            /// @brief apply velocity to elongation
            virtual void apply() {
                this->elongation += this->velocity;
            }
        };
    }
}
