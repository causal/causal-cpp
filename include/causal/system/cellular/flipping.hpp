// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <optional>

#include <msgpack.hpp>

#include "causal/facet/common.hpp"
#include "causal/facet/spreads.hpp"
#include <causal/data/msgpack.hpp>

namespace causal {
    namespace system {
        namespace cf = causal::facet;

        /// @brief flipping cell automaton as used for game of life
        template<size_t On, size_t Lower, size_t Upper, typename F>
        struct Flipper {
            typedef cf::ptr_spreads<const F> facet_type;
            
            bool state = false;
            facet_type neighbours;

            MSGPACK_DEFINE(state, neighbours)

            /// @brief flips state
            void flip() {
                this->state = !this->state;
            }

            /** @brief sets state
             * @param state
             */
            void set(bool state) {
                this->state = state;
            }

            /** @brief calculates new state using given relations
             * @param os borrowed neighours facet
             * @return calculated state change if any
             */
            static std::optional<bool> calc(const facet_type& os) {
                size_t on = 0;
                for(auto it = os.begin(); it != os.end(); ++it) {
                    if(it->is_certain() && (**it).state)
                        on++;
                }

                if(on < Lower || on > Upper)
                    return false;
                else if(on == On)
                    return true;
                else
                    return {};
            }
        };
    }
}