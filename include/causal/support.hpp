// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>

namespace std {
    template <typename To, typename From>
    inline std::shared_ptr<To> reinterpret_pointer_cast(std::shared_ptr<From> const & ptr) noexcept
    { return std::shared_ptr<To>(ptr, reinterpret_cast<To*>(ptr.get())); }
}
