// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <mutex>
#include <condition_variable>
#include <thread>

#include "causal/core/wirks.hpp"
#include "queue.hpp"

namespace causal {
    namespace process {
        namespace cc = causal::core;

        /// @brief abstract base of dispatcher queuing and running actions multithreaded
        class threaded_dispatcher : public cc::dispatcher {
        public:
            /// @brief resume dispatcher when paused
            virtual void resume() noexcept = 0;

            /// @brief pauses dispatcher when running
            virtual void pause() noexcept = 0;

            /** @brief gets if dispatcher is busy
             * @return true if actions are actually running or enqueued
             */
            virtual bool busy() noexcept = 0;

            /// @brief blocks calling thread until dispatcher is not busy
            virtual void join() noexcept = 0;
        };

        /** @brief dispatcher distributing actions to n worker threads using a separate management thread
         * @tparam Q type of action queue to use
         */
        template<typename Q = performing_queue>
        class managed_dispatcher : public threaded_dispatcher {
            static_assert(std::is_base_of_v<action_queue, Q>, "given queue type needs to be an action_queue");
        private:
            Q queue;
            std::thread* tasker_thread;
            std::vector<std::thread*> worker_threads;
            const u_short break_us, cnt_worker;

            std::mutex mtx_tasker, mtx_joiner, mtx_mgmt;
            std::condition_variable cv_tasker, cv_joiner, cv_mgmt;

            std::atomic<bool> running = false;
            std::atomic<size_t> cnt_running = 0, cnt_working = 0, cnt_waiting = 0;

            std::vector<std::mutex> mtx_worker;
            std::vector<std::condition_variable> cv_worker;
            std::vector<std::unique_ptr<cc::action>> tasks;

            void tasker() {
                while(this->running) {
                    auto got = true;
                    while(this->running && got && this->cnt_waiting) {
                        for(u_short i = 0; i < this->cnt_worker; i++) {
                            std::unique_lock l(this->mtx_worker[i]);
                            auto& t = this->tasks[i];
                            if(!t) {
                                t = this->queue.next();
                                if(t)
                                    this->cv_worker[i].notify_one();
                                else
                                    got = false;
                            }
                        }
                    }

                    std::unique_lock l(this->mtx_tasker);
                    auto cnt_w = this->cnt_working.load();
                    this->cv_tasker.wait_for(l, std::chrono::microseconds(this->break_us), [&]{return !this->running || this->cnt_working != cnt_w;});
                }
            }

            void worker(const u_short idx) {
                while(this->running) {
                    cc::action* a = nullptr;
                    {   std::unique_lock l(this->mtx_worker[idx]);
                        a = this->tasks[idx].get();
                    }

                    if(a) {    
                        if(a->is_loaded()) {
                            a->run();
                        }

                        {   std::unique_lock l(this->mtx_worker[idx]);
                            this->tasks[idx] = nullptr;
                        }

                        if(!--this->cnt_working) {
                            this->cv_joiner.notify_all();
                        }
                    }
                    
                    if(this->running) {
                        std::unique_lock l(this->mtx_worker[idx]);
                        this->cnt_waiting++;
                        this->cv_tasker.notify_one();
                        this->cv_worker[idx].wait(l, [&]{return !this->running || this->tasks[idx];});                    
                        this->cnt_waiting--;
                    }
                }
            }
            
            managed_dispatcher(u_short break_us = 1, u_short cnt = std::max<u_int>(2, std::thread::hardware_concurrency()-2)) noexcept
            : queue([this]{
                this->cv_tasker.notify_one();
            }), break_us(break_us), cnt_worker(cnt), mtx_worker(cnt), cv_worker(cnt), tasks(cnt) {
                this->resume();
            }

        public:
            static std::shared_ptr<managed_dispatcher<Q>> make(size_t break_us = 1, size_t cnt = std::max<u_int>(2, std::thread::hardware_concurrency()-2)) noexcept {
                return std::shared_ptr<managed_dispatcher<Q>>(new managed_dispatcher<Q>(break_us, cnt));
            }

            virtual ~managed_dispatcher() {
                this->pause();
            }

            virtual void resume() noexcept override {
                auto exp = false;
                if(this->running.compare_exchange_strong(exp, true)) {
                    this->tasker_thread = new std::thread(&managed_dispatcher::tasker, this);
                    for(u_short i = 0; i < this->cnt_worker; i++) {
                        this->worker_threads.push_back(new std::thread(&managed_dispatcher::worker, this, i));
                        this->cnt_running++;
                    }
                    {   std::unique_lock l(this->mtx_mgmt);
                        this->cv_mgmt.notify_all();
                    }
                } else {
                    std::unique_lock l(this->mtx_joiner);
                    if(this->cnt_running != this->cnt_worker)
                        this->cv_mgmt.wait(l, [this]{return this->cnt_running == this->cnt_worker;});
                }
            }

            virtual void pause() noexcept override {
                bool tru = true;
                if(this->running.compare_exchange_strong(tru, false)) {
                    this->tasker_thread->join();
                    delete this->tasker_thread;

                    for(u_short i = 0; i < this->cnt_worker; i++) {
                        std::unique_lock l(this->mtx_worker[i]);
                        this->cv_worker[i].notify_one();
                    }
                    for(auto w : this->worker_threads) {
                        w->join();
                        delete w;
                        this->cnt_running--;
                    }

                    this->worker_threads.clear();

                    {   std::unique_lock l(this->mtx_mgmt);
                        this->cv_mgmt.notify_all();
                    }
                } else {
                    std::unique_lock l(this->mtx_joiner);
                    if(this->cnt_running)
                        this->cv_mgmt.wait(l, [this]{return !this->cnt_running;});
                }
            }

            void enqueue(std::unique_ptr<cc::action>&& a) override {
                this->queue.put(std::forward<std::unique_ptr<cc::action>>(a));

                std::unique_lock l(this->mtx_tasker);
                this->cnt_working++;
                this->cv_tasker.notify_one();
            }

            virtual bool busy() noexcept override {
                return this->running && this->cnt_working;
            }

            virtual void join() noexcept override {
                std::unique_lock l(this->mtx_joiner);
                if(this->busy())
                    this->cv_joiner.wait(l, [this]{return !this->busy();});
            }
        };

        /** @brief dispatcher distributing actions to n worker threads using concurrent access
         * @tparam Q type of action queue to use
         */
        template<typename Q = performing_queue>
        class concurrent_dispatcher : public threaded_dispatcher {
            static_assert(std::is_base_of_v<action_queue, Q>, "given queue type needs to be an action_queue");
        private:
            Q queue;
            std::vector<std::thread*> worker_threads;
            const size_t break_us, cnt_worker;

            std::mutex mtx_worker, mtx_joiner, mtx_mgmt;
            std::condition_variable cv_worker, cv_joiner, cv_mgmt;

            std::atomic<bool> running = false;
            std::atomic<size_t> cnt_running = 0, cnt_working = 0, cnt_waiting = 0;

            void worker() {
                while(this->running) {
                    std::unique_ptr<cc::action> a = nullptr;
                    while(this->running && (a = this->queue.next())) {
                        if(this->cnt_waiting && this->cnt_working > 1)
                            this->cv_worker.notify_one();
                        
                        if(a->is_loaded())
                            a->run();
                        a = nullptr;
                        
                        if(!--this->cnt_working) {
                            this->cv_joiner.notify_all();
                        }
                    }

                    std::unique_lock l(this->mtx_worker);
                    this->cnt_waiting++;
                    auto cnt_w = this->cnt_working.load();
                    if(this->cnt_waiting == this->cnt_worker)
                        this->cv_worker.wait_for(l, std::chrono::microseconds(this->break_us), [&]{return !this->running || this->cnt_working != cnt_w;});
                    else
                        this->cv_worker.wait(l, [&]{return !this->running || this->cnt_working != cnt_w;});
                    this->cnt_waiting--;
                }
            }

            concurrent_dispatcher(size_t break_us, size_t cnt) noexcept
            : queue([this]{
                this->cv_worker.notify_one();
            }), break_us(break_us), cnt_worker(cnt) {
                this->resume();
            }

        public:
            static std::shared_ptr<concurrent_dispatcher<Q>> make(size_t break_us = 1, size_t cnt = std::max<u_int>(2, std::thread::hardware_concurrency()-2)) noexcept {
                return std::shared_ptr<concurrent_dispatcher<Q>>(new concurrent_dispatcher<Q>(break_us, cnt));
            }

            virtual ~concurrent_dispatcher() {
                this->pause();
            }

            virtual void resume() noexcept override {
                auto exp = false;
                if(this->running.compare_exchange_strong(exp, true)) {
                    for(size_t i = 0; i < this->cnt_worker; i++) {
                        this->worker_threads.push_back(new std::thread(&concurrent_dispatcher::worker, this));
                        this->cnt_running++;
                    }
                    {   std::unique_lock l(this->mtx_mgmt);
                        this->cv_mgmt.notify_all();
                    }
                } else {
                    std::unique_lock l(this->mtx_joiner);
                    if(this->cnt_running != this->cnt_worker)
                        this->cv_mgmt.wait(l, [this]{return this->cnt_running == this->cnt_worker;});
                }
            }

            virtual void pause() noexcept override {
                bool tru = true;
                if(this->running.compare_exchange_strong(tru, false)) {
                    {   std::unique_lock l(this->mtx_worker);
                        this->cv_worker.notify_all();
                    }
                    for(auto w : this->worker_threads) {
                        w->join();
                        delete w;
                        this->cnt_running--;
                    }

                    this->worker_threads.clear();

                    {   std::unique_lock l(this->mtx_mgmt);
                        this->cv_mgmt.notify_all();
                    }
                } else {
                    std::unique_lock l(this->mtx_joiner);
                    if(this->cnt_running)
                        this->cv_mgmt.wait(l, [this]{return !this->cnt_running;});
                }
            }

            void enqueue(std::unique_ptr<cc::action>&& a) override {
                this->queue.put(std::forward<std::unique_ptr<cc::action>>(a));
                this->cnt_working++;
                
                {   std::unique_lock l(this->mtx_worker);
                    if(this->cnt_waiting == this->cnt_worker)
                        this->cv_worker.notify_one();
                }
            }

            virtual bool busy() noexcept override{
                return this->running && this->cnt_working;
            }

            virtual void join() noexcept override {
                std::unique_lock l(this->mtx_joiner);
                if(this->busy())
                    this->cv_joiner.wait(l, [this]{return !this->busy();});
            }
        };
    }
}