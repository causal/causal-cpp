// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <shared_mutex>

#include "causal/core/wirks.hpp"

namespace causal {
    namespace process {
        namespace cc = causal::core;

        /// @brief atomic borrowable queue item
        class action_queue_item final : public cc::atomic_borrowable {
        public:
            /// @brief enqueued action
            std::unique_ptr<cc::action> store;
            
            action_queue_item() = delete;
            action_queue_item(action_queue_item&& o) = delete;
            action_queue_item(const action_queue_item& o) = delete;
            virtual ~action_queue_item() = default;
            action_queue_item(std::unique_ptr<cc::action>&& a) : store(std::forward<std::unique_ptr<cc::action>>(a)) {}
        };

        /// @brief abstract base of an action queue
        class action_queue {
        protected:
            /// @brief shared mutex synchronizing queue access
            mutable std::shared_mutex mtx;

            /// @brief callback to execute when queue changed
            const std::function<void()> changed_callback;

        public:
            action_queue(std::function<void()>&& ccb) : changed_callback(std::forward<std::function<void()>>(ccb)) {}

            /** @brief puts a new action into queue
             * @param a action to enqueue
             */
            virtual void put(std::unique_ptr<cc::action>&& a) = 0;

            /** gets next feasible action from queue
             * @return next feasible action or nullptr if there is no feasible action available yet
             */
            virtual std::unique_ptr<cc::action> next() noexcept = 0;

            /** @brief gets the count of all enqueued actions
             * @return count of enqueued actions
             */
            virtual size_t size() const noexcept = 0;

            /** @brief gets if the queue is empty
             * @return true if queue is empty
             */
            virtual bool empty() const noexcept = 0;
        };

        /// @brief queue preserving order by sacrificing performance
        class preserving_queue final : public action_queue {
        private:
            std::list<std::unique_ptr<action_queue_item>> main;
            std::map<const cc::synchron*, std::vector<std::unique_ptr<action_queue_item>>> feed;

        public:
            preserving_queue(std::function<void()>&& ccb = {}) : action_queue(std::forward<std::function<void()>>(ccb)) {}

            void put(std::unique_ptr<cc::action>&& a) override {
                auto ccbi = [this](cc::synchron& s) {
                    auto it = this->feed.find(&s);
                    if(it != this->feed.end()) {
                        if(s.is_feasible() || !s.is_possible()) {
                            for(auto& a : it->second)
                                this->main.push_back(std::move(a));

                            if(this->changed_callback)
                                this->changed_callback();

                            this->feed.erase(&s);

                            return true;
                        }
                    }
                    return false;
                };

                auto ccb = [this, ccbi](cc::synchron& s) {
                    std::unique_lock l(this->mtx);
                    return ccbi(s);
                };

                std::unique_lock l(this->mtx);
                auto i = std::make_unique<action_queue_item>(std::forward<std::unique_ptr<cc::action>>(a));
                auto& s = *i->store->sync;
                s.register_callback(this, ccb);
                this->feed[&s].push_back(std::move(i));
                if(ccbi(s))
                    s.deregister_callback(this);
            }

            std::unique_ptr<cc::action> next() noexcept override {
                std::unique_lock l(this->mtx);
                for(auto it = this->main.begin(); it != this->main.end(); ++it) {
                    auto& itm = *it;
                    if(!itm->store->sync->is_possible() || itm->store->load()) {
                        auto ret = std::move(itm->store);
                        this->main.erase(it);
                        return ret;
                    }
                }

                return nullptr;
            }

            size_t size() const noexcept override {
                std::shared_lock l(this->mtx);
                return this->main.size() + this->feed.size();
            }

            bool empty() const noexcept override {
                std::shared_lock l(this->mtx);
                return this->main.empty() && this->feed.empty();
            }
        };

        /// @brief queue gaining performance by sacrificing order
        class performing_queue final : public action_queue {
        private:
            std::list<std::unique_ptr<action_queue_item>> main, side;
            std::map<const cc::synchron*, std::vector<std::unique_ptr<action_queue_item>>> feed;

        public:
            performing_queue(std::function<void()>&& ccb = {}) : action_queue(std::forward<std::function<void()>>(ccb)) {}

            void put(std::unique_ptr<cc::action>&& a) override {
                auto ccbi = [this](cc::synchron& s) {
                    auto it = this->feed.find(&s);
                    if(it != this->feed.end()) {
                        if(s.is_feasible() || !s.is_possible()) {
                            for(auto& a : it->second)
                                this->side.push_back(std::move(a));

                            if(this->changed_callback)
                                this->changed_callback();

                            this->feed.erase(&s);

                            return true;
                        }
                    }
                    return false;
                };

                auto ccb = [this, ccbi](cc::synchron& s) {
                    std::unique_lock l(this->mtx);
                    return ccbi(s);
                };

                std::unique_lock l(this->mtx);
                auto i = std::make_unique<action_queue_item>(std::forward<std::unique_ptr<cc::action>>(a));
                auto& s = *i->store->sync;
                s.register_callback(this, ccb);
                this->feed[&s].push_back(std::move(i));
                if(ccbi(s))
                    s.deregister_callback(this);
            }

            std::unique_ptr<cc::action> next() noexcept override {
                std::unique_ptr<cc::action> ret = nullptr;
                auto end = false;
                do {
                    std::unique_ptr<action_queue_item> itm = nullptr;
                    {   std::unique_lock l(this->mtx);
                        // if main queue is empty copy over side queue
                        if(this->main.empty()) {
                            this->main = std::move(this->side);
                            this->side = std::list<std::unique_ptr<action_queue_item>>();
                        }

                        // if main not empty (with or without copying over side) get first item
                        if(!this->main.empty()) {
                            itm = std::move(this->main.front());
                            this->main.pop_front();
                        }
                        
                        end = this->main.empty() && this->side.empty();
                    }

                    if(itm) {
                        if(!itm->store->sync->is_possible() || itm->store->load())
                            // return action
                            ret = std::move(itm->store);
                        else {
                            // add action to side queue after finishing recursion
                            std::unique_lock l(this->mtx);
                            this->side.push_back(std::move(itm));
                        }
                    }
                } while(!ret && !end);

                return ret;
            }

            size_t size() const noexcept override {
                std::shared_lock l(this->mtx);
                return this->main.size() + this->side.size() + this->feed.size();
            }

            bool empty() const noexcept override {
                std::shared_lock l(this->mtx);
                return this->main.empty() && this->side.empty() && this->feed.empty();
            }
        };
    }
}

