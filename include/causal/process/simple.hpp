// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <mutex>

#include "causal/core/wirks.hpp"
#include "queue.hpp"

namespace causal {
    namespace process {
        namespace cc = causal::core;

        /// @brief most simple dispatcher executing in current thread
        template<typename Q = performing_queue>
        class simple_dispatcher : public cc::dispatcher {
            static_assert(std::is_base_of_v<action_queue, Q>, "given queue type needs to be an action_queue");
        private:
            Q queue;

        protected:
            simple_dispatcher() = default;

        public:
            static std::shared_ptr<simple_dispatcher<Q>> make() {
                return std::shared_ptr<simple_dispatcher<Q>>(new simple_dispatcher<Q>());
            }

            virtual ~simple_dispatcher() = default;

            virtual void enqueue(std::unique_ptr<cc::action>&& a) override {
                this->queue.put(std::forward<std::unique_ptr<cc::action>>(a));
            }

            /** @brief do one tick
             * @return true if a tick could be executed
             * @remark return of true does not indicate, that tick didn't throw
             */
            bool do_tick() noexcept {
                if(auto a = this->queue.next()) {
                    if(a->is_loaded()) {
                        a->run();
                        return true;
                    } else {
                        a = nullptr;
                        return this->do_tick();
                    }
                } else
                    return false;
            }
        };
    }
}