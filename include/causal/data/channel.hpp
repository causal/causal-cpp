// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <mutex>
#include <memory>
#include <set>
#include <shared_mutex>
#include <functional>
#include <type_traits>
#include <unordered_map>

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/trace.hpp"

#ifndef _DATA_WRAP_SESSION_SIZE
#define _DATA_WRAP_SESSION_SIZE 64
#endif

namespace causal {
    namespace data {
        namespace cc = causal::core;

        struct NoMeta {};

        template<typename M=NoMeta>
        class source;

        /// @brief basic metadata for channels supporting message sequences
        struct MessageMeta {
            size_t id = 0;
            size_t seq = 0;
        };

        enum SessionState : u_char {
            Null = 0,
            Initilized = 1,
            Closed = 2
        };

        /// @brief basic metadata for channels supporting session state
        struct SessionMeta {
            SessionState state = SessionState::Null;
            std::string session, peer;
        };

        /// @brief basic metadata for channels supporting identified state
        struct IdMeta {
            std::string id;
            std::string sign;
        };

        /// @brief basic metadata for channels supporting identified session state
        struct IdSessionMeta : SessionMeta, IdMeta {};

        struct SessionCreated {
            std::string session, peer;
            std::shared_ptr<source<SessionMeta>> src;
        };
        struct DisposeSession {
            std::string session;
        };
        struct SessionDisposed {
            std::string session, peer;
        };

        /** @brief abstract base of channel
         * @tparam T message data type
         * @tparam M metadata type attached to messages
         */
        template<typename T, typename M=NoMeta>
        class channel {
            friend class source<M>;
        protected:
            /// @brief mutex synchronizing channel client
            mutable std::shared_mutex mtx;

            /// @brief subscribed wirks to trigger when message is received
            cc::wirks_set wirks;

            /** @brief pulls a message from channel and triggers subscriptions into given branch
             * @param d copy of message data
             * @param meta copy of message metadata
             */
            virtual void pull(const T d, const M meta) const = 0;

        public:
            channel() = default;
            virtual ~channel() = default;
        };

        /** abstract base of channel pushing to and pulling from message source
         * @tparam M type of metadata
         */
        template<typename M=NoMeta>
        class sourced_channel : public channel<std::string, M> {
            friend class source<M>;
        protected:
            virtual void push_pipe(const std::string msg) const {
                if(this->src)
                    this->src->push(msg);
            }

            sourced_channel(std::shared_ptr<source<M>> s)
            : src(std::move(s)) {
                ASSERT_MSG(scope_data, this->src, "trying to create channel without valid source")
                this->src->add_channel(*this);
            }

        public:
            const std::shared_ptr<source<M>> src;

            virtual ~sourced_channel() {
                this->src->remove_channel(*this);
            }
        };

        /** abstract base of channel source
         * @tparam M type of metadata
         */
        template<typename M>
        class source {
        protected:
            mutable std::shared_mutex mtx;
            std::set<const sourced_channel<M>*> channels;

        public:
            using metaT = M;

            virtual ~source() {
                std::unique_lock l(this->mtx);
                ASSERT_MSG(scope_data, this->channels.empty(), "destroying junction while channels are connected to it")
            }

            /** @brief register a sourced channel for source
             * @param c sourced channel to register
             * @return true if registered successful
             */
            virtual bool add_channel(const sourced_channel<M>& c) {
                std::unique_lock l(this->mtx);
                this->channels.insert(&c);
                return true;
            }

            /** @brief deregister a sourced channel from source
             * @param c sourced channel to deregister
             */
            virtual void remove_channel(const sourced_channel<M>& c) {
                std::unique_lock l(this->mtx);
                ASSERT_MSG(scope_data, this->channels.erase(&c), "channel is not registered for source")
            }
            
            /** @brief pulls a message from source to registered channels
             * @param msg copy of message data
             * @param meta copy of message metadata
             */
            void pull(const std::string msg, const M meta) const {
                std::shared_lock l(this->mtx);
                for(auto c : this->channels)
                    c->pull(msg, meta);
            }

            /** @brief pushes message to source
             * @param msg lvalue reference to message
             */
            virtual void push(const std::string& msg) const = 0;

            /** @brief pushes message to source
             * @param msg rvalue reference to message
             */
            virtual void push(std::string&& msg) const = 0;
        };

        /// @brief sourced channel transfering strings
        template<typename M=NoMeta>
        class msg_channel final : public sourced_channel<M> {
        private:
            /// @brief branch to trigger wirks in
            const std::shared_ptr<cc::branch> branch;
            
        protected:
            virtual void pull(const std::string msg, const M meta) const override {
                std::shared_lock l(this->mtx);
                cc::wirks_set ws = this->wirks;
                ws.apply([&](cc::wirks& w){
                    w.template set<0, const std::string&, const std::string>(msg);
                    if(w.type_hash<1>() == typeid(M).hash_code())
                        w.template set<1, const M&, const M>(meta);
                });
                this->branch->act_fwd(std::move(ws));
            }

            msg_channel(std::shared_ptr<source<M>> s, std::shared_ptr<cc::branch> b)
            : sourced_channel<M>(std::move(s)), branch(std::move(b)) {
                ASSERT_MSG(scope_data, this->branch, "trying to create msg channel without valid branch")
            }

        public:
            static std::shared_ptr<msg_channel<M>> make(std::shared_ptr<source<M>> s, std::shared_ptr<cc::branch> b) {
                return std::shared_ptr<msg_channel<M>>(new msg_channel<M>(s, b));
            }

            /** @brief subscribe wirks for getting triggered on received message
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @param t tick taking const copy of string message as first argument
             * @param src further argument sources for wirks
            */
            template<typename... SrcT, typename... DstT>
            void subscribe(void(*t)(const std::string, DstT...), SrcT&&... src) {
                std::unique_lock l(this->mtx);
                this->wirks.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, std::string(), std::forward<SrcT>(src)...)));
            }

            /** @brief subscribe wirks for getting triggered on received message
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @param t tick taking const copy of string message as first argument and metadata as second argument
             * @param src further argument sources for wirks
            */
            template<typename... SrcT, typename... DstT>
            void subscribe(void(*t)(const std::string, const M, DstT...), SrcT&&... src) {
                std::unique_lock l(this->mtx);
                this->wirks.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, std::string(), M{}, std::forward<SrcT>(src)...)));
            }

            /** @brief pushes message to channel's pushing pipe
             * @param msg lvalue reference to message
             */
            virtual void push(const std::string msg) const {
                this->push_pipe(msg);
            }

            /** @brief see push
             */
            virtual void operator<<(const std::string& msg) const {
                this->push(msg);
            }
        };

        /// @brief simple in process loopback source
        template<typename M=NoMeta>
        class loopback_source final : public source<M> {
        private:
            loopback_source(M meta) : meta(meta) {}

        public:
            static std::shared_ptr<loopback_source<M>> make(M meta = {}) {
                return std::shared_ptr<loopback_source<M>>(new loopback_source<M>(meta));
            }

            const M meta;

            virtual void push(const std::string& msg) const override {
                this->pull(msg, this->meta);
            }

            virtual void push(std::string&& msg) const override {
                this->pull(std::forward<std::string>(msg), this->meta);
            }
        };

        template<typename SM, typename M=NoMeta>
        class wrapping_source final : public source<M> {
            static_assert(std::is_base_of<SM, M>::value || !std::is_same<SM, M>::value, "a wrapping source can only use metadata derriving from inner source's metadata");
        protected:
            class channel final : public sourced_channel<SM> {
            protected:
                virtual void pull(const std::string d, const SM meta) const override {
                    this->_parent->pull(d, this->_parent->meta);
                }

            public:
                const wrapping_source<SM, M> *const _parent;

                channel(const wrapping_source<SM, M> *const p, std::shared_ptr<source<SM>> s) : sourced_channel<SM>(s), _parent(p) {}
            };

            channel inner;

            wrapping_source(std::shared_ptr<source<SM>> i, M m)
            : inner(this, i), meta(m) {}

        public:
            static std::shared_ptr<wrapping_source<SM, M>> make(std::shared_ptr<source<SM>> inner, M meta = {}) {
                return std::shared_ptr<wrapping_source<SM, M>>(new wrapping_source<SM, M>(inner, meta));
            }
        
            const M meta;

            virtual ~wrapping_source() = default;

            virtual void push(const std::string& msg) const override {
                TRACE(scope_data) << "wrapping_source::push(" << msg << ")";
                this->inner.src->push(msg);
            }

            virtual void push(std::string&& msg) const override {
                TRACE(scope_data) << "wrapping_source::push(" << msg << ")";
                this->inner.src->push(std::forward<std::string>(msg));
            }
        };

        template<typename... MsgT>
        class gate : public cc::vortex<SessionCreated, DisposeSession, SessionDisposed, MsgT...> {
        protected:
            gate(std::shared_ptr<cc::branch> b)
            : cc::vortex<SessionCreated, DisposeSession, SessionDisposed, MsgT...>(b) {};
            virtual ~gate() = default;
        };

        /// @brief loopback gate
        class wrap final : public gate<>, public std::enable_shared_from_this<wrap> {
        public:
            using metaT = SessionMeta;

        private:
            struct session {
                mutable std::mutex mtx;

                std::string id;
                std::shared_ptr<loopback_source<metaT>> source;
                SessionMeta meta;

                session(std::string id, std::shared_ptr<loopback_source<metaT>> source, const SessionMeta& meta)
                : id(id), source(source), meta(meta) {}
            };

            bool can_dispose = false;
            mutable std::shared_mutex serve_mtx;
            std::unordered_map<std::string, std::shared_ptr<session>> sessions;

            const std::shared_ptr<cc::branch> branch;

            std::shared_ptr<session> get_session(const std::string &uri);
            std::shared_ptr<session> create();
            void dispose(const std::string& id);

            void creating(const std::shared_ptr<session>& s);
            void disposing(const std::shared_ptr<session>& s);

            static void Dispose(DisposeSession e, const std::shared_ptr<wrap> g);

            wrap(std::shared_ptr<cc::branch> b);
        
        public:
            /**
             * @param b branch to act new sessions in
             * @param cfg httpd config
             * @param j junction to expose
             */
            static std::shared_ptr<wrap> make(std::shared_ptr<cc::branch> b);
            ~wrap() = default;

            const std::pair<std::string, std::shared_ptr<source<metaT>>> forge();
        };
    }
}