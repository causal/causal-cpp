// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <sstream>
#include <string>

#include <nlohmann/json.hpp>
#include <sys/types.h>

#include "causal/data/channel.hpp"
#include "causal/trace.hpp"
#include "msgpack.hpp"

namespace causal {
    namespace data {
        MAKE_TRACE_SCOPE(scope_json)

        /// @brief junction parsing json data
        class json_junction final : public junction {
        private:
            nlohmann::json store;

            static std::string clean(const std::string);

            json_junction() = default;
            
            json_junction(const std::string& j);

        public:
            static std::shared_ptr<json_junction> make();
            
            /** @brief constructs junction from json string
             * @param j json string containing initial data
             */
            static std::shared_ptr<json_junction> make(const std::string& j);

            /** @brief dumps a json string of actual data
             * @param pretty print json for readbility
             * @return json string
             */
            std::string dump(bool pretty = false) const;

            virtual bool add_space(const junction_space& s) override;

            void attract(junction_space& s, const std::string id) override;
            void repel(const junction_space& s, const std::string id) override;
            const std::string get(const std::string id) const override;
            void set(const std::string id, const std::string& data) override;
            void set(const std::string id, std::string&& data) override;
            void erase(const std::string id) override;
            bool merge(const junction& o, bool override = true) override;
            bool iterate(std::function<bool(const std::string&, const std::string&)>) const override;
        };

        /** @brief msgpack encoding sourced channel
         * @tparam M metadata type attached to messages
         */
        template<typename M=NoMeta>
        class json_channel final : public msgpack_channel<M> {
        protected:
            virtual void push_pipe(const std::string msg) const override {
                size_t off = 0;
                auto tn_hndl = msgpack::unpack(msg.data(), msg.size(), off);
                const auto tn = tn_hndl.get().as<std::string>();

                const auto json_data = nlohmann::json::from_msgpack(msg.substr(off));
                std::stringstream ss;
                ss << "{\"type\": \"" << tn << "\", \"data\": " << std::endl;
                ss << json_data;
                ss << "}";

                const auto json = ss.str();
                this->msgpack_channel<M>::push_pipe(json);
            }

            virtual void pull(const std::string msg, const M meta) const override{
                const auto json = nlohmann::json::parse(msg);
                const auto tn = json["type"].get<std::string>();

                std::stringstream ss;
                msgpack::pack(ss, std::string(tn));
                const auto tn_packed = ss.str();

                std::vector<u_int8_t> v(tn_packed.begin(), tn_packed.end());
                const auto v_data = nlohmann::json::to_msgpack(json["data"]);
                v.insert(v.end(), v_data.begin(), v_data.end());
                this->msgpack_channel<M>::pull(std::string(v.begin(), v.end()), meta);
            }

            json_channel(std::shared_ptr<source<M>> s, std::shared_ptr<cc::branch> b)
            : msgpack_channel<M>(std::move(s),std::move(b)) {}
        
        public:
            static std::shared_ptr<json_channel<M>> make(std::shared_ptr<source<M>> s, std::shared_ptr<cc::branch> b) {
                return std::shared_ptr<json_channel<M>>(new json_channel<M>(s, b));
            }
        };
    }
}