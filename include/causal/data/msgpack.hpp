// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <string>
#include <type_traits>
#include <typeinfo>
#include <vector>
#include <memory>
#include <set>
#include <any>
#include <map>
#include <vector>
#include <stack>
#include <iostream>
#include <sstream>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <msgpack.hpp>

#include "causal/data/lmdb.hpp"
#include "causal/trace.hpp"

#include "causal/facet/edges.hpp"
#include "causal/facet/spreads.hpp"
#include "causal/facet/scales.hpp"
#include "causal/facet/routes.hpp"
#include "crypto.hpp"
#include "junction.hpp"
#include "channel.hpp"

namespace causal {
    namespace data {
        namespace cc = causal::core;

        MAKE_TRACE_SCOPE(scope_msgpack)
        
        /// @brief msgpack encoding junction space
        class msgpack_space final : public std::enable_shared_from_this<msgpack_space>, public junction_space {
        private:
            static constexpr const char* TYPE_POSTFIX = "_type";
            std::atomic<size_t> deserializing_cnt = 0;

            struct type_register {
                std::function<cc::essence_ptr<>(const std::string& id)> make;
                std::function<void(const std::string& id, const void* const)> serialize;
                std::function<void* const(const std::string& id)> deserialize;
            };
            std::map<std::string, type_register> type_registry;

            std::string get_type_name(const std::string& id) noexcept {
                auto& data = this->junc->get(id+TYPE_POSTFIX);
                return data.empty() ? std::string() : msgpack::unpack(data.data(), data.size()).get().as<std::string>();
            }
            
            template<bool certain, typename T, typename... Args>
            cc::aspect_ptr<T> get_(const std::string& id, Args&&... args) {
                cc::aspect_ptr<T> r;
                std::unique_lock l(this->mtx);
                auto it = this->aspects.find(id);
                if(it != this->aspects.end() && !it->second.ptr.expired()) {
                    auto& m = it->second;
                    auto a = m.ptr.lock();
                    r = a->template as_typed<std::remove_cv_t<T>>();
                } else {
                    auto type_name = this->get_type_name(id);
                    if(!type_name.empty()) {
                        auto it = this->type_registry.find(type_name);
                        if(it != this->type_registry.end())
                            r = it->second.make(id)->template as_typed<std::remove_cv_t<T>>();
                        else if constexpr(!std::is_abstract_v<T>) {
                            if constexpr(certain) {
                                r = cc::essence<std::remove_cv_t<T>>::make_certain(id, this->shared_from_this(), std::forward<Args>(args)...);
                            } else {
                                r = cc::essence<std::remove_cv_t<T>>::make(id, this->shared_from_this(), std::forward<Args>(args)...);
                            }
                        }
                    } else if constexpr(!std::is_abstract_v<T>) {
                        if constexpr(certain) {
                            r = cc::essence<std::remove_cv_t<T>>::make_certain(id, this->shared_from_this(), std::forward<Args>(args)...);
                        } else {
                            r = cc::essence<std::remove_cv_t<T>>::make(id, this->shared_from_this(), std::forward<Args>(args)...);
                        }
                    }
                }
                return r;
            }

            template<typename T>
            T* deserialize_(const std::string& id) noexcept {
                auto& data = this->junc->get(id);
                T* ret = nullptr;
                if(!data.empty()) {
                    auto hndl = msgpack::unpack(data.data(), data.size());
                    if(this->deserializing_cnt == 0)
                        this->active = this;
                    this->deserializing_cnt++;
                    ret = new T(hndl.get().as<T>());
                    this->deserializing_cnt--;
                    if(this->deserializing_cnt == 0)
                        this->active = nullptr;
                }
                return ret;
            }

            template<typename T>
            void serialize_(const std::string& id, const T* const data) noexcept {
                std::stringstream ss, ss_type;
                msgpack::pack(ss, *data);
                this->junc->set(id, ss.str());
                msgpack::pack(ss_type, typeid(T).name());
                this->junc->set(id+TYPE_POSTFIX, ss_type.str());
            }

            msgpack_space(const std::shared_ptr<junction> j);
            
        public:
            static constexpr bool is_serializing = true;
            static thread_local msgpack_space* active;

            static std::shared_ptr<msgpack_space> make(const std::shared_ptr<junction> j);

            /** @brief register a pack of type used from space
             * @tparam tuple type listing types used
             * @remark it is possible to register multiple type packs
             */
            template<typename Ts>
            void register_type_pack() {
                cc::static_for<0, std::tuple_size_v<Ts>>([&]<size_t I>() {
                    typedef std::tuple_element_t<I, Ts> T;
                    std::string type_name = typeid(T).name();
                    if(!this->type_registry.contains(type_name)) {
                        this->type_registry[type_name] = {[&](const std::string& id) {
                            return cc::essence<std::remove_cv_t<T>>::make(id, this->shared_from_this())->as_untyped();
                        }, [&](const std::string& id, const void* const ptr) {
                            this->serialize_<T>(id, reinterpret_cast<const T* const>(ptr));
                        }, [&](const std::string& id) {
                            return this->deserialize_<T>(id);
                        }};
                    }
                    return true;
                });

            }

            /** @brief register types used from space
             * @tparam type list used
             * @remark it is possible to register multiple sets of types
             */
            template<typename... Ts>
            void register_types() {
                this->register_type_pack<std::tuple<Ts...>>();
            }
            
            bool is_managing() const noexcept override {return true;}

            void focus(const std::any id, const void* const key) noexcept override;
            void release(const std::any id, const void* const key) noexcept override;
            bool is_focused(const std::any id) const noexcept override;

            bool borrow(const std::any id, const void* const key) noexcept override;
            bool const_borrow(const std::any id, const void* const key) noexcept override;
            void unborrow(const std::any id, const void* const key, const bool abort) noexcept override;
            bool is_borrowed(const std::any id) const noexcept override;
            bool is_const_borrowed(const std::any id) const noexcept override;

            void dispose(const std::any id) noexcept override;

            void push(std::string type, std::any id, void* ptr) noexcept override;
            void* const pull(std::string type, std::any id) noexcept override;

            void attracted(const std::string id) noexcept override;

            /** @brief spawns new aspect first degree certain
             * @tparam T type of aspect
             * @tparam args argument types
             * @param args arguments to use at lazy construction time
             * @return reference to aspect
             * @remark an UUID is generated
             */
            template<typename T, typename... Args>
            cc::aspect_ptr<T> spawn(Args&&... args) {
                return this->get_<false, T>(boost::uuids::to_string(boost::uuids::random_generator()()), std::forward<Args>(args)...);
            }

            /** @brief forges new aspect second degree certain 
             * @tparam T type of aspect
             * @tparam args argument types
             * @param args arguments to use at construction time
             * @return reference to aspect
             * @remark an UUID is generated
             */
            template<typename T, typename... Args>
            cc::aspect_ptr<T> forge(Args&&... args) {
                return this->get_<true, T>(boost::uuids::to_string(boost::uuids::random_generator()()), std::forward<Args>(args)...);
            }

            /** @brief takes certain identified second degree certain aspect
             * @tparam T type of aspect
             * @tparam Args argument types
             * @tparam ID string convertible type
             * @param id of aspect
             * @param args arguments to use at lazy construction time
             * @return reference to aspect
             * @remark this is a spawning operation if aspect is not existing
             */
            template<typename T, typename ID, typename... Args>
            cc::aspect_ptr<T> take(const ID id, Args&&... args) {
                return this->get_<true, T>(cc::serialize_id(id), std::forward<Args>(args)...);
            }

            /** @brief gets certain identified first degree certain aspect
             * @tparam T type of aspect or interface type implemented by aspect type
             * @tparam Args argument types
             * @tparam ID string convertible type
             * @param id of aspect
             * @param args arguments to use at lazy construction time
             * @return reference to aspect or dangling
             * @remark this is no spawning operation and requires junction to return aspect type
             */
            template<typename T, typename ID, typename... Args>
            cc::aspect_ptr<T> get(const ID id, Args&&... args) {
                return this->get_<false, T>(cc::serialize_id(id), std::forward<Args>(args)...);
            }

            /** @brief clone referred aspects data into space
             * @tparam T type of aspect or interface type implemented by aspect type
             * @param r reference to aspect
             * @return reference to clone
             * @remark an UUID is generated
             */
            template<typename T>
            inline cc::aspect_ptr<T> clone(cc::aspect_ptr<T>& r) requires(!std::is_same_v<T, void>) {
                return r.clone(boost::uuids::to_string(boost::uuids::random_generator()()), this->shared_from_this());
            }

            /** @brief clone referred aspects data into space
             * @tparam T type of aspect or interface type implemented by aspect type
             * @tparam ID string convertible type
             * @param id to use for clone
             * @param r reference to aspect
             * @return reference to clone
             */
            template<typename T, typename ID>
            inline cc::aspect_ptr<T> clone(const ID id, cc::aspect_ptr<T>& r) {
                return r.clone(cc::serialize_id(id), this->shared_from_this());
            }

            cc::essence_ptr<> clone(const void* const key, cc::essence_ptr<> ptr, std::any id = std::any()) override;

            /** @brief creates a set of wirks applying a tick to each aspect in space
             * @tparam T type of aspect or interface type implemented by aspect type
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to take a T& or cc::aspect_ptr<T>
             * @param t tick to act on aspects taking reference to face
             * @param src tick argument sources
             * @return set of wirks
             */
            template<typename T, typename... SrcT, typename... DstT>
            cc::wirks_set explode(void(*t)(DstT...), SrcT&&... src) {
                cc::wirks_set ws;
                this->active = this;
                this->junc->iterate([&](const std::string& sid, const std::string& data) {
                    auto hndl = msgpack::unpack(data.data(), data.size());
                    try { // try it deserialize data for estimating if type matches (sadly msgpack offers no func to just check)
                        hndl.get().as<T>();
                    } catch(msgpack::type_error&) {
                        return true;
                    }
                    
                    ws.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, this->get<T>(sid), src...)));
                    return true;
                });
                this->active = nullptr;
                return ws;
            }

            /** @brief creates a set of wirks applying a tick to each aspect of given type in space
             * @tparam T type of aspect or interface type implemented by aspect type
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to take a T& or cc::aspect_ptr<T>
             * @param t tick to act on aspects taking reference to face
             * @param src tick argument sources
             * @return set of wirks
             */
            template<typename T, typename... SrcT, typename... DstT>
            cc::wirks_set explode(void(*t)(const std::string, DstT...), SrcT&&... src) {
                cc::wirks_set ws;
                this->junc->iterate([&](const std::string& sid, const std::string& data) {
                    auto hndl = msgpack::unpack(data.data(), data.size());
                    try { // try it deserialize data for estimating if type matches (sadly msgpack offers no func to just check)
                        hndl.get().as<T>();
                    } catch(msgpack::type_error&) {
                        return true;
                    }
                        
                    ws.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, sid, this->get<T>(sid), src...)));
                    return true;
                });
                return ws;
            }
        };

        /** @brief msgpack encoding sourced channel
         * @tparam S source_channel derivate
         */
        template<typename M=NoMeta>
        class msgpack_channel : public sourced_channel<M> {
        private:
            std::map<size_t, std::function<cc::access_*(const std::string&)>> makers;

            /// @brief branch to trigger wirks in
            const std::shared_ptr<cc::branch> branch;

        protected:
            virtual void push_pipe(const std::string msg) const override {
                sourced_channel<M>::push_pipe(msg);
            }

            virtual void pull(const std::string msg, const M meta) const override {
                std::shared_lock l(this->mtx);
                cc::wirks_set ws;
                this->wirks.apply([&](const cc::wirks& w){
                    const size_t th = w.type_hash<0>();
                    auto nw = new cc::wirks(w);
                    auto a = this->makers.at(th)(msg);
                    if(a) {
                        nw->template set_accessor<0>(a);
                        if(nw->type_hash<1>() == typeid(M).hash_code())
                            nw->template set<1, const M&, const M>(meta);
                        ws.add(std::unique_ptr<cc::wirks>(std::move(nw)));
                    }
                });
                this->branch->act_fwd(std::move(ws));
            }

            msgpack_channel(std::shared_ptr<source<M>> s, std::shared_ptr<cc::branch> b)
            : sourced_channel<M>(std::move(s)), branch(std::move(b)) {
                ASSERT_MSG(scope_data, this->branch, "trying to create msgpack channel without valid branch")
            }

        public:
            static std::shared_ptr<msgpack_channel<M>> make(std::shared_ptr<source<M>> s, std::shared_ptr<cc::branch> b) {
                return std::shared_ptr<msgpack_channel<M>>(new msgpack_channel<M>(s, b));
            }

            /** @brief subscribe wirks for getting triggered on received message
             * @tparam T type of aspect
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @param t tick taking const copy of string message as first argument
             * @param src further argument sources for wirks
            */
            template<typename T, typename... SrcT, typename... DstT>
            void subscribe(void(*t)(const T, DstT...), SrcT&&... src) {
                std::unique_lock l(this->mtx);
                auto wp = new cc::wirks(t, T{}, std::forward<SrcT>(src)...);
                const size_t th = typeid(T).hash_code();
                if(this->makers.find(th) == this->makers.end()) {
                    this->makers[th] = [](const std::string& msg) -> cc::access_* {
                        size_t off = 0;
                        auto tn_hndl = msgpack::unpack(msg.data(), msg.size(), off);
                        if(tn_hndl.get().as<std::string>() == typeid(T).name()) {
                            auto msg_hndl = msgpack::unpack(msg.data(), msg.size(), off);
                            return new cc::access<T&&, const T>(msg_hndl.get().as<T>());
                        } else
                            return nullptr;
                    };
                }
                this->wirks.add(std::unique_ptr<cc::wirks>(wp));
            }

            /** @brief subscribe wirks for getting triggered on received message
             * @tparam T type of aspect
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @param t tick taking const copy of string message as first argument
             * @param src further argument sources for wirks
            */
            template<typename T, typename... SrcT, typename... DstT>
            void subscribe(void(*t)(const T, const M, DstT...), SrcT&&... src) {
                std::unique_lock l(this->mtx);
                auto wp = new cc::wirks(t, T{}, M{}, std::forward<SrcT>(src)...);
                const size_t th = typeid(T).hash_code();
                if(this->makers.find(th) == this->makers.end()) {
                    this->makers[th] = [](const std::string& msg) -> cc::access_* {
                        size_t off = 0;
                        auto tn_hndl = msgpack::unpack(msg.data(), msg.size(), off);
                        if(tn_hndl.get().as<std::string>() == typeid(T).name()) {
                            auto msg_hndl = msgpack::unpack(msg.data(), msg.size(), off);
                            return new cc::access<T&&, const T>(msg_hndl.get().as<T>());
                        } else
                            return nullptr;
                    };
                }
                this->wirks.add(std::unique_ptr<cc::wirks>(wp));
            }

            /** @brief pushes message through encoding pipe channel's source
             * @tparam T type of message
             * @param d lvalue reference to message
             */
            template<typename T> requires(std::is_copy_constructible_v<T>)
            void push(const T& d) const {
                this->template push(T(d));
            }

            /** @brief pushes message through encoding pipe channel's source
             * @tparam T type of message
             * @param d rvalue reference to message
             */
            template<typename T>
            void push(T&& d) const {
                auto tn = typeid(T).name();
                std::stringstream ss;
                msgpack::pack(ss, std::string(tn));
                msgpack::pack(ss, std::forward<T>(d));
                this->push_pipe(ss.str());
            }
        };
    }
}

namespace msgpack {
    namespace cc = causal::core;
    namespace cd = causal::data;
    namespace cf = causal::facet;

    MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
        namespace adaptor {
            template<typename T>
            struct convert<cc::aspect_ptr<T>> {
                msgpack::object const& operator()(msgpack::object const& o, cc::aspect_ptr<T>& r) const {
                    auto id = o.as<std::string>();
                    if(!id.empty())
                        r = cd::msgpack_space::active->get<T>(id);
                    return o;
                }
            };

            template<typename T>
            struct pack<cc::aspect_ptr<T>> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, cc::aspect_ptr<T> const& r) const {
                    if(r)
                        p.pack(r.template get_id_as<std::string>());
                    else
                        p.pack(std::string());
                    return p;
                }
            };

            template<typename O>
            struct convert<cf::spreads<O>> {
                msgpack::object const& operator()(msgpack::object const& o, cf::spreads<O>& facet) const {
                    for(auto& e : o.via.array)
                        facet.insert(e.as<O>());
                    return o;
                }
            };

            template<typename O>
            struct pack<cf::spreads<O>> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, cf::spreads<O> const& facet) const {
                    p.pack_array(facet.size());
                    for(auto& f : facet)
                        p.pack(f);
                    return p;
                }
            };

            template<typename O, typename S>
            struct convert<cf::scales<O, S>> {
                msgpack::object const& operator()(msgpack::object const& o, cf::scales<O, S>& facet) const {
                    for(auto& e : o.via.array) {
                        auto p = e.as<std::pair<O, S>>();
                        facet.insert(p.first, p.second);
                    }
                    return o;
                }
            };

            template<typename O, typename S>
            struct pack<cf::scales<O, S>> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, cf::scales<O, S> const& facet) const {
                    p.pack_array(facet.size());
                    for(auto& f : facet)
                        p.pack(f);
                    return p;
                }
            };

            template<typename D, typename O>
            struct convert<cf::routes<D, O>> {
                msgpack::object const& operator()(msgpack::object const& o, cf::routes<D, O>& facet) const {
                    for(auto& e : o.via.array) {
                        auto p = e.as<std::pair<D, O>>();
                        facet.insert(p.first, p.second);
                    }
                    return o;
                }
            };

            template<typename D, typename O>
            struct pack<cf::routes<D, O>> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, cf::routes<D, O> const& facet) const {
                    p.pack_array(facet.size());
                    for(auto& f : facet)
                        p.pack(f);
                    return p;
                }
            };

            template<typename... O>
            struct convert<cf::edges<O...>> {
                msgpack::object const& operator()(msgpack::object const& o, cf::edges<O...>& facet) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != sizeof...(O)) throw msgpack::type_error();
                    cc::static_for<0, sizeof...(O)>([&]<size_t I>() {
                        facet.template set<I>(o.via.array.ptr[I].as<typename std::tuple_element_t<I, typename cf::edges<O...>::container_type>>());
                        return true;
                    });

                    return o;
                }
            };

            template<typename... O>
            struct pack<cf::edges<O...>> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, cf::edges<O...> const& facet) const {
                    p.pack_array(sizeof...(O));
                    facet.foreach([&p](const auto& f, const size_t i) {
                        p.pack(f);
                        return true;
                    });
                    return p;
                }
            };

            template<>
            struct convert<cd::Key> {
                msgpack::object const& operator()(msgpack::object const& o, cd::Key& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 3) throw msgpack::type_error();
                    v = {
                        o.via.array.ptr[0].as<cd::KeyType>(),
                        o.via.array.ptr[1].as<u_short>(),
                        o.via.array.ptr[2].as<std::string>()
                    };
                    return o;
                }
            };

            template<>
            struct pack<cd::Key> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, const cd::Key& v) const {
                    p.pack_array(3);
                    p.pack(v.type);
                    p.pack(v.length);
                    p.pack(v.data);
                    return p;
                }
            };

            template<>
            struct convert<cd::CertificateId> {
                msgpack::object const& operator()(msgpack::object const& o, cd::CertificateId& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 2) throw msgpack::type_error();
                    v = {
                        o.via.array.ptr[0].as<cd::IdentityId>(),
                        o.via.array.ptr[1].as<std::string>()
                    };
                    return o;
                }
            };

            template<>
            struct pack<cd::CertificateId> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, const cd::CertificateId& v) const {
                    p.pack_array(2);
                    p.pack(static_cast<const cd::IdentityId&>(v));
                    p.pack(v.name);
                    return p;
                }
            };

            template<>
            struct convert<cd::Certificate> {
                msgpack::object const& operator()(msgpack::object const& o, cd::Certificate& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 3) throw msgpack::type_error();
                    v = {
                        o.via.array.ptr[0].as<cd::CertificateId>(),
                        o.via.array.ptr[1].as<std::string>(),
                        o.via.array.ptr[2].as<std::string>()
                    };
                    return o;
                }
            };

            template<>
            struct pack<cd::Certificate> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, const cd::Certificate& v) const {
                    p.pack_array(3);
                    p.pack(static_cast<const cd::CertificateId&>(v));
                    p.pack(v.data);
                    p.pack(v.issuer_name);
                    return p;
                }
            };

            template<>
            struct convert<cd::IdentityId> {
                msgpack::object const& operator()(msgpack::object const& o, cd::IdentityId& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 2) throw msgpack::type_error();
                    v = {
                        o.via.array.ptr[0].as<cd::IdentityType>(),
                        o.via.array.ptr[1].as<std::string>()
                    };
                    return o;
                }
            };

            template<>
            struct pack<cd::IdentityId> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, const cd::IdentityId& v) const {
                    p.pack_array(2);
                    p.pack(v.type);
                    p.pack(v.id);
                    return p;
                }
            };

            template<>
            struct convert<cd::PublicIdentity> {
                msgpack::object const& operator()(msgpack::object const& o, cd::PublicIdentity& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 2) throw msgpack::type_error();
                    v = {
                        o.via.array.ptr[0].as<cd::IdentityId>(),
                        o.via.array.ptr[1].as<std::string>()
                    };
                    return o;
                }
            };

            template<>
            struct pack<cd::PublicIdentity> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, const cd::PublicIdentity& v) const {
                    p.pack_array(2);
                    p.pack(static_cast<const cd::IdentityId&>(v));
                    p.pack(v.pkey);
                    return p;
                }
            };

            template<>
            struct convert<cd::Identity> {
                msgpack::object const& operator()(msgpack::object const& o, cd::Identity& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 3) throw msgpack::type_error();
                    v = {
                        o.via.array.ptr[0].as<cd::PublicIdentity>(),
                        o.via.array.ptr[1].as<std::string>(),
                        o.via.array.ptr[2].as<cd::Certificate>()
                    };
                    return o;
                }
            };

            template<>
            struct pack<cd::Identity> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, const cd::Identity& v) const {
                    p.pack_array(3);
                    p.pack(static_cast<const cd::PublicIdentity&>(v));
                    p.pack(v.data);
                    p.pack(v.certificate);
                    return p;
                }
            };
        }
    }
}
MSGPACK_ADD_ENUM(causal::data::KeyType)
MSGPACK_ADD_ENUM(causal::data::IdentityType)
