// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/data/msgpack.hpp"
#include "channel.hpp"
#include "junction.hpp"
#include "json.hpp"
#include <cstddef>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <shared_mutex>
#include <string>
#include <sys/types.h>
#include <memory.h>
#include <vector>

#ifndef _DATA_HTTPD_SESSION_SIZE
#define _DATA_HTTPD_SESSION_SIZE 64
#endif

#ifndef _DATA_HTTPD_MAX_UPLOAD
#define _DATA_HTTPD_MAX_UPLOAD 16777216
#endif

namespace causal {
    namespace data {
        namespace cc = causal::core;

        MAKE_TRACE_SCOPE(scope_httpd)

        class httpd_source;

        struct httpd_config {
            std::optional<std::pair<std::string, std::string>> certificate;
            std::string address = "0.0.0.0";
            u_short port = 0;

            httpd_config(std::string addr, u_short port, const std::optional<std::pair<std::string, std::string>> cert = std::optional<std::pair<std::string, std::string>>());
            httpd_config(std::pair<std::string, std::string> cert);
            httpd_config();
        };

        struct httpd_result {
            const std::string data;
            const u_short code = 200;
            const std::map<std::string, std::string> headers = {{"Content-Type", "application/json"}};
        };

        /// @brief gate fed by httpd service
        class httpd_gate final : public gate<>, public std::enable_shared_from_this<httpd_gate> {
            friend class httpd_source;
        public:
            using metaT = SessionMeta;

        private:
            struct session {
                mutable std::mutex mtx;

                std::string id;
                std::shared_ptr<httpd_source> source;
                SessionMeta meta;
                std::queue<std::string> queue;

                session(std::string id, std::shared_ptr<httpd_source> source, SessionMeta&& meta)
                : id(id), source(source), meta(std::forward<SessionMeta>(meta)) {}
            };

            const httpd_config config;
            bool can_dispose = false;
            
            void* daemon = nullptr; // MHD_Daemon *

            std::vector<std::string> blacklist;

            mutable std::shared_mutex serve_mtx, blacklist_mtx;
            std::unordered_map<std::string, std::shared_ptr<session>> sessions;

            const std::shared_ptr<cc::branch> branch;
            std::shared_ptr<const junction> exposed;

            void listen();

            std::shared_ptr<session> get_session(const std::string &uri);
            std::shared_ptr<session> create();
            void push(const std::string& id, const std::string& data) const;
            void dispose(const std::string& id);

            void creating(const std::shared_ptr<session>& s);
            void disposing(const std::shared_ptr<session>& s);

            static void Dispose(DisposeSession e, const std::shared_ptr<httpd_gate> g);

            httpd_gate(std::shared_ptr<cc::branch> b, httpd_config cfg, std::shared_ptr<const junction> j);

        public:
            /**
             * @param b branch to act new sessions in
             * @param cfg httpd config
             * @param j junction to expose
             */
            static std::shared_ptr<httpd_gate> make(std::shared_ptr<cc::branch> b,
                       httpd_config cfg, std::shared_ptr<const junction> j = nullptr);

            ~httpd_gate();

            void update_blacklist(std::vector<std::string> list);
            const std::vector<std::string> get_blacklist() const;

            const httpd_result handle(const std::string uri, const std::string method, const std::string upload);
        };
        
        /// @brief httpd source
        class httpd_source final : public source<SessionMeta> {
            friend class httpd_gate;
        private:
            const std::string session;
            httpd_gate &gate;
            
            httpd_source(const std::string session, httpd_gate &gate);

        public:
            ~httpd_source() = default;

            virtual void push(const std::string& msg) const override;
            virtual void push(std::string&& msg) const override;

            /// @brief closes underlying httpd session
            void dispose();
        };
    }
}