// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <mutex>
#include <shared_mutex>
#include <map>
#include <set>
#include <future>
#include <chrono>

#include <opendht.h>
#include <opendht/peer_discovery.h>

#include "causal/trace.hpp"

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "crypto.hpp"
#include "channel.hpp"
#include "msgpack.hpp"

namespace causal {
    namespace data {
        MAKE_TRACE_SCOPE(scope_opendht)

        /// @brief OpenDHT backed SHA crypto hasher
        class opendht_sha_hasher final : public hasher {
        public:
            std::string hash(const std::string& data, u_short hash_length=DEFAULT_SHA_HASH_LENGTH) const override;
        };

        /// @brief OpenDHT backed crypto provider
        struct opendht_crypto_provider final : crypto_provider {
        private:
            mutable std::shared_mutex mtx;
            std::map<IdentityId, dht::crypto::Identity> identities;
            std::map<IdentityId, std::shared_ptr<dht::crypto::PublicKey>> publics;
            std::map<CertificateId, std::shared_ptr<dht::crypto::Certificate>> certificates;

            dht::crypto::Identity add_internal(const Identity& id);
            dht::crypto::PublicKey add_internal(const PublicIdentity& id);
            dht::crypto::Certificate add_internal(const Certificate& crt);
            Identity get_internal(const dht::crypto::Identity& dht_id) const;
            PublicIdentity get_internal(const dht::crypto::PublicKey& dht_pk) const;
            Certificate get_internal(const dht::crypto::Certificate& dht_crt) const;

        public:
            opendht_crypto_provider() = default;
            opendht_crypto_provider(const opendht_crypto_provider& o);
            opendht_crypto_provider(opendht_crypto_provider&& o);

            const hasher& get_hasher() const override;
            IdentityId add(const Identity id) override;
            IdentityId add(const PublicIdentity id) override;
            CertificateId add(const Certificate crt) override;
            size_t add(const std::map<std::string, Identity> ids) override;
            size_t add(const std::map<std::string, PublicIdentity> pks) override;
            size_t add(const std::map<std::string, Certificate> crts) override;
            void remove(const IdentityId id) override;
            void remove(const CertificateId id) override;
            bool has(const IdentityId id) const override;
            bool has_public(const IdentityId id) const override;
            bool has(const CertificateId id) const override;
            Identity get(const IdentityId id) const override;
            PublicIdentity get_public(const IdentityId id) const override;
            Certificate get(const CertificateId id) const override;
            std::map<std::string, Identity> get_identities() const override;
            std::map<std::string, PublicIdentity> get_publics() const override;
            std::map<std::string, Certificate> get_certificates() const override;
            IdentityId create_rsa(const std::string name, const IdentityId ca_id = {}, const u_short key_length=DEFAULT_RSA_KEY_LENGTH, const bool is_ca = false) override;
            std::string sign(const IdentityId id, const std::string& data) const override;
            bool check(const IdentityId id, const std::string& data, const std::string& signature) const override;
            bool check(const CertificateId id, const std::string& data, const std::string& signature) const override;
            std::string encrypt(const IdentityId id, const std::string& data) const override;
            std::string encrypt(const CertificateId id, const std::string& data) const override;
            std::string decrypt(const IdentityId id, const std::string& encrypted_data) const override;
        };

        /// @brief OpenDHT backed AES crypto coder
        class opendht_aes_cipher final : public cipher {
        private:
            const dht::Blob key;

        public:
            const u_short key_length;

            opendht_aes_cipher(const std::string& password, const u_short key_length = 256);

            const hasher& get_hasher() const override;
            const std::string get_token() const override;
            std::string encrypt(const std::string& data) const override;
            std::string decrypt(const std::string& encrypted_data) const override;
        };
        
        /// @brief exception thrown when opendht node timeouts on listen
        class opendht_source_listen_timeout : public trace::exception {
            public: opendht_source_listen_timeout(dht::InfoHash key) : trace::exception("opendht node timeouted when trying to listen to " + key.toString()) {}
        };

        class opendht_source;

        /// @brief OpenDHT node wrapper
        class opendht_node final : public dht::DhtRunner {
        private:
            struct service {
                dht::InfoHash id;
                in_port_t port;

                MSGPACK_DEFINE(id, port)
            };

            std::mutex mtx;
            std::set<const opendht_source*> sources;
            dht::PeerDiscovery discovery;

            opendht_node(const dht::crypto::Identity id = dht::crypto::generateIdentity(),
                         const std::string bootstrap = {}, const std::string socket = "4658",
                         const in_port_t port = 4658,
                         const std::string service_name = "Causal_DHT",
                         const in_port_t dport = 4558,
                         const std::chrono::milliseconds timeout = std::chrono::seconds(120));

            opendht_node(const dht::crypto::Identity id,
                         const dht::SockAddr addr,
                         const in_port_t port = 4658,
                         const std::string service_name = "Causal_DHT",
                         const in_port_t dport = 4558,
                         const std::chrono::milliseconds timeout = std::chrono::seconds(120));

        public:
            const dht::crypto::Identity id;
            const std::chrono::milliseconds timeout;
            const std::string service_name;

            static std::shared_ptr<opendht_node> make(const dht::crypto::Identity id = dht::crypto::generateIdentity(),
                         const std::string bootstrap = {}, const std::string socket = "4658",
                         const in_port_t port = 4658,
                         const std::string service_name = "Causal_DHT",
                         const in_port_t dport = 4558,
                         const std::chrono::milliseconds timeout = std::chrono::seconds(120));

            static std::shared_ptr<opendht_node> make(const dht::crypto::Identity id,
                         const dht::SockAddr addr,
                         const in_port_t port = 4658,
                         const std::string service_name = "Causal_DHT",
                         const in_port_t dport = 4558,
                         const std::chrono::milliseconds timeout = std::chrono::seconds(120));

            ~opendht_node();

            /// @brief register a source
            void add_source(const opendht_source& s);

            /// @brief deregister a source
            void remove_source(const opendht_source& s);
        };

        /// @brief msgpack serializable OpenDHT serializer
        struct opendht_error_msg {
            dht::InfoHash to_id;
            dht::InfoHash key;
            std::string err;

            MSGPACK_DEFINE(to_id, key, err)
        };

        /// @brief OpenDHT backed source
        class opendht_source final : public source<IdentifiedMessageMeta> {
            friend class opendht_node;
        private:
            dht::InfoHash to_id;
            dht::InfoHash key;

            std::future<size_t> listen_token;

            void listen();

            opendht_source(const std::shared_ptr<opendht_node> node, const dht::InfoHash k, const dht::InfoHash to);

        public:
            static std::shared_ptr<opendht_source> make(const std::shared_ptr<opendht_node> node,
                const dht::InfoHash k = dht::InfoHash::get("UNIVERSE"),
                const dht::InfoHash to = {});

            const std::shared_ptr<opendht_node> node;

            ~opendht_source();

            virtual void push(const std::string& msg) const override;
            virtual void push(std::string&& msg) const override;
        };
    }
}

namespace msgpack {
    namespace cd = causal::data;

    MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
        namespace adaptor {
            template<>
            struct convert<cd::opendht_crypto_provider> {
                msgpack::object const& operator()(msgpack::object const& o, cd::opendht_crypto_provider& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 3) throw msgpack::type_error();
                    v.add(o.via.array.ptr[0].as<std::map<std::string, cd::Identity>>());
                    v.add(o.via.array.ptr[1].as<std::map<std::string, cd::PublicIdentity>>());
                    v.add(o.via.array.ptr[2].as<std::map<std::string, cd::Certificate>>());

                    return o;
                }
            };

            template<>
            struct pack<cd::opendht_crypto_provider> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& p, const cd::opendht_crypto_provider& v) const {
                    p.pack_array(3);
                    p.pack(v.get_identities());
                    p.pack(v.get_publics());
                    p.pack(v.get_certificates());

                    return p;
                }
            };
        }
    }
}