// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <any>
#include <set>
#include <map>
#include <memory>
#include <functional>
#include <unordered_map>
#include <mutex>
#include <shared_mutex>

#include "causal/core/aspect.hpp"

namespace causal {
    namespace data {
        namespace cc = causal::core;

        class junction;

        /// @brief abstract base of space using a junction
        class junction_space : public cc::space_base {
        protected:
            mutable std::recursive_mutex mtx;

            /// @brief meta object holding locally available aspect
            struct aspect_meta {
                /// @brief weak pointer of aspect
                cc::weak_essence_ptr<> ptr;

                /// @brief indicates if aspect is attracted from junction and available for borrowing
                bool attracted = false;

                /// @brief set holding all keys focusing aspect
                std::set<const void*> focus_keys;

                /// @brief keys borrowing aspect
                const void* borrow_key = nullptr;

                /// @brief set holding all keys const borrowing aspect
                std::set<const void*> const_borrow_keys;
            };

            /// @brief unordered map holding meta objects of locally available aspects
            std::unordered_map<std::string, aspect_meta> aspects;
            
            junction_space(const std::shared_ptr<junction> j);
        
        public:
            /// @brief junction used by space
            const std::shared_ptr<junction> junc;

            virtual ~junction_space();

            /** @brief create meta object for aspect
             * @param id of aspect
             * @param a shared pointer to aspect
             */
            virtual void create(const std::any id, const cc::essence_ptr<> a) noexcept override;

            /** @brief remove meta object for aspect
             * @param id of aspect
             */
            virtual void destroy(const std::any id) noexcept override;

            /** @brief called by junction when aspect is attracted
             * @param id of aspect
             */
            virtual void attracted(const std::string id) noexcept = 0;
        };

        class junction {
        protected:
            mutable std::shared_mutex mtx;

            /// @brief set of spaces using this junction
            std::set<const junction_space*> spaces;

        public:
            virtual ~junction();

            /** @brief register a space
             * @param s reference of space
             */
            virtual bool add_space(const junction_space& s);

            /** @brief deregister a space
             * @param s reference of space
             */
            virtual bool remove_space(const junction_space& s);

            /** @brief attracts data from whereever it might be
             * @param s space attracting data
             * @param id of data to attract
             * @remark calls junction_space::attracted for notifying space that aspect can be borrowed locally
             */
            virtual void attract(junction_space& s, const std::string id) = 0;

            /** @brief repels previously attracted data
             * @remark usually happens when last focus is released
             * @param s space attracting data
             * @param id of data to attract
             */
            virtual void repel(const junction_space& s, const std::string id) = 0;

            /** @brief gets data if attracted and available
             * @param id of data
             * @return data
             */
            virtual const std::string get(const std::string id) const = 0;

            /** @brief inserts or overwrites data copying it
             * @param id of data
             * @param data as const reference
             */
            virtual void set(const std::string id, const std::string& data) = 0;

            /** @brief inserts or overwrites data by moving it
             * @param id of data
             * @param data as rvalue reference
             */
            virtual void set(const std::string id, std::string&& data) = 0;
            
            /** @brief erases the data identified by id
             * @param id of data
             * @throws std::out_of_range if data not found
             */
            virtual void erase(const std::string id) = 0;

            /** @brief merges content of another junction into itself
             * @param o reference to other junction
             * @param override already existing data
             * @return true if merged successfully
             */
            virtual bool merge(const junction& o, bool override = true) = 0;

            /** @brief iterates all ids and passes them together with their data to a callback
             * @param f function to call for each item
             * @return true if iterated successfully
             */
            virtual bool iterate(std::function<bool(const std::string&, const std::string&)> f) const = 0;
        };

        /// @brief simple in memory junction using an unordered map
        class map_junction final : public junction {
        private:
            std::unordered_map<std::string, std::string> store;

            map_junction() = default;

        public:
            static std::shared_ptr<map_junction> make();

            void attract(junction_space& s, const std::string id) override;
            void repel(const junction_space& s, const std::string id) override;
            const std::string get(const std::string id) const override;
            void set(const std::string id, const std::string& data) override;
            void set(const std::string id, std::string&& data) override;
            void erase(const std::string id) override;
            bool merge(const junction& o, bool override = true) override;
            bool iterate(std::function<bool(const std::string&, const std::string&)>) const override;
        };
    }
}
