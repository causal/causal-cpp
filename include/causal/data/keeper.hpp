// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <msgpack/adaptor/define_decl.hpp>
#include <mutex>
#include <memory>
#include <set>
#include <shared_mutex>
#include <functional>
#include <type_traits>
#include <unordered_map>

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/data/channel.hpp"
#include "causal/data/crypto.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/trace.hpp"

#ifndef _DATA_WRAP_SESSION_SIZE
#define _DATA_WRAP_SESSION_SIZE 64
#endif

namespace causal {
    namespace data {
        namespace cc = causal::core;

        struct IdentifyRequest {
            std::string session;
            std::string id;
            std::string phrase;

            MSGPACK_DEFINE_MAP(session, id, phrase)
        };
        struct AnonRequest {
            std::string session;

            MSGPACK_DEFINE_MAP(session)
        };
        struct IdentifyResponse {
            std::string session;
            std::string id;
            std::string sign;
            std::string error;

            MSGPACK_DEFINE_MAP(session, id, sign, error)
        };

        struct LeaveRequest {
            std::string session;
            std::string id;

            MSGPACK_DEFINE_MAP(session, id)
        };

        struct LeaveResponse {
            std::string session;
            std::string id;

            MSGPACK_DEFINE_MAP(session, id)
        };

        struct Identified {
            std::string session, peer;
            std::string id;
            std::string sign;
            std::shared_ptr<source<IdSessionMeta>> src;
        };
        struct Left {
            std::string session, peer;
            std::string id;
        };

        template<typename channelT, typename gateT, typename... MsgT>
        class keeper : public cc::vortex<Identified, Left, MsgT...> {
        private:
            struct Session {
                SessionCreated event;
                std::shared_ptr<channelT> channel;
                std::shared_ptr<wrapping_source<SessionMeta, IdSessionMeta>> source;
            };
        protected:
            using sessionsT = std::unordered_map<std::string, Session>;

            std::set<std::shared_ptr<gateT>> gate;

            cc::aspect_ptr<const bool> dismissed = cc::forge<bool>(false);
            cc::aspect_ptr<const sessionsT> sessions = cc::spawn<sessionsT>();

            keeper(std::shared_ptr<cc::branch> b)
            : cc::vortex<Identified, Left, MsgT...>(b) {};

            virtual void connect(const std::shared_ptr<gateT>& g) = 0;

            static void Dismiss(bool &dismissed) {
                dismissed = true;
            }

        public:
            virtual ~keeper() {
                this->brnch->act(Dismiss, this->dismissed);
            };

            void attach(std::shared_ptr<gateT> g) {
                const auto ret = this->gate.insert(g);

                if(ret.second)
                    this->connect(*ret.first);
            }
        };

        template<typename channelT, typename gateT, typename spaceT>
        class bouncer final : public keeper<channelT, gateT>, public std::enable_shared_from_this<bouncer<channelT, gateT, spaceT>> {
        public:
            using ptrT = std::shared_ptr<const bouncer<channelT, gateT, spaceT>>;
            using keeperT = keeper<channelT, gateT>;
        private:
            using _ptrT = std::shared_ptr<bouncer<channelT, gateT, spaceT>>;

            std::shared_ptr<spaceT> space;

            static void Reg(const bool &dismissed, const std::string id, const std::string phrase, std::string &key) {
                if(dismissed) {
                    TRACE_WARNING(scope_data) << "Bouncer cannot register '" << id << "' because bouncer is dismissed";
                    return;
                }

                // key is the hash from concatenation of id and phrase to avoid hash compare attacks
                key = gnutls_derrive_key_sha(id+phrase, 256);
                TRACE_INFO(scope_data) << "Bouncer succeffuly registered '" << id << "'";
            }

            static void Identify(ptrT b, const bool &dismissed, typename keeperT::sessionsT &sessions, const IdentifyRequest req, const SessionMeta meta, cc::aspect_ptr<std::string> &key) {
                TRACE(scope_data) << "tick bouncer::Identify";
                if(dismissed) {
                    TRACE_WARNING(scope_data) << "Bouncer cannot identify because bouncer is dismissed";
                    return;
                }

                const auto kkk = *key;
                if(key && *key == gnutls_derrive_key_sha(req.id+req.phrase, 256)) {
                    const IdSessionMeta id_meta{meta, {.id=req.id, .sign=req.id}};
                    sessions[meta.session].source = wrapping_source<SessionMeta, IdSessionMeta>::make(
                        sessions[meta.session].event.src,
                        id_meta
                    );
                    b->trigger(Identified{.session=meta.session, .peer=meta.peer, .id=req.id, .sign=req.id, .src=std::dynamic_pointer_cast<source<IdSessionMeta>>(sessions[meta.session].source)});
                    sessions[meta.session].channel->push(IdentifyResponse{.session = meta.session, .id = req.id, .sign = id_meta.sign});
                    TRACE_INFO(scope_data) << "Bouncer succeffuly identified '" << req.id << "'";
                } else {
                    TRACE_INFO(scope_data) << "Bouncer cannot identify '" << req.id << "'";
                }
                TRACE(scope_data) << "ticked bouncer::Identify";
            }

            static void SourceOnIdentifyRequest(const IdentifyRequest e, const SessionMeta meta, ptrT b, const bool &dismissed) {
                TRACE(scope_data) << "tick bouncer::SourceOnIdentifyRequest";
                if(dismissed)
                    TRACE_WARNING(scope_data) << "Bouncer cannot prepare to identify because bouncer is dismissed";
                else
                    cc::act_fwd(Identify, b, b->dismissed, b->sessions, e, meta, b->space->template get<std::string>(e.id));
                TRACE(scope_data) << "ticked bouncer::SourceOnIdentifyRequest";
            }

            static void SourceOnLeaveRequest(const LeaveRequest e, const SessionMeta meta, ptrT b, const bool &dismissed, typename keeperT::sessionsT &sessions) {
                TRACE(scope_data) << "tick bouncer::SourceOnLeaveRequest";
                sessions[meta.session].source = nullptr;
                b->trigger(Left{.session=meta.session, .peer=meta.peer, .id=e.id});
                sessions[meta.session].channel->push(LeaveResponse{.session = meta.session, .id = e.id,});
                TRACE(scope_data) << "ticked bouncer::SourceOnLeaveRequest";
            }

            static void GateSessionCreated(const SessionCreated e, _ptrT b, const bool &dismissed, typename keeperT::sessionsT &sessions) {
                TRACE(scope_data) << "tick bouncer::GateSessionCreated";
                if(dismissed) {
                    TRACE_WARNING(scope_data) << "Bouncer cannot accept session because bouncer is dismissed";
                    return;
                }

                sessions[e.session] = {.event=e, .channel=channelT::make(e.src, cc::action::current().context)};
                sessions[e.session].channel->template subscribe<IdentifyRequest>(SourceOnIdentifyRequest, b, b->dismissed);
                sessions[e.session].channel->template subscribe<LeaveRequest>(SourceOnLeaveRequest, b, b->dismissed, b->sessions);
                TRACE(scope_data) << "ticked bouncer::GateSessionCreated";
            }

            static void GateOnSessionDisposed(const SessionDisposed s, ptrT b, typename keeperT::sessionsT &sessions) {
                TRACE(scope_data) << "tick bouncer::GateOnSessionDisposed";
                sessions.erase(s.session);
                TRACE(scope_data) << "ticked bouncer::GateOnSessionDisposed";
            }

            bouncer(std::shared_ptr<cc::branch> b, std::shared_ptr<spaceT> s)
            : keeper<channelT, gateT>(b), space(s) {
                ASSERT_MSG(cc::scope_core, s != nullptr, "Bouncer requires a set space for storing and recalling registrations")
            }

        protected:
            virtual void connect(const std::shared_ptr<gateT>& g) override {
                g->template subscribe<SessionCreated>(GateSessionCreated, this->shared_from_this(), this->dismissed, this->sessions);
                g->template subscribe<SessionDisposed>(GateOnSessionDisposed, this->shared_from_this(), this->sessions);
            }

        public:
            static std::shared_ptr<bouncer<channelT, gateT, spaceT>> make(std::shared_ptr<cc::branch> b, std::shared_ptr<spaceT> s) {
                return std::shared_ptr<bouncer<channelT, gateT, spaceT>>(new bouncer<channelT, gateT, spaceT>(b, s));
            }

            void reg(const std::string id, const std::string phrase) const {
                this->brnch->act(Reg, this->dismissed, id, phrase, this->space->template take<std::string>(id));
            }

            void dereg(const std::string id) const {
                this->space->dispose(id);
                TRACE_INFO(scope_data) << "Bouncer deregistered '" << id << "'";
            }
        };
    }
}