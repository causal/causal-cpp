// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <sstream>
#include <string>

#include <sys/types.h>

#include "causal/data/junction.hpp"
#include "causal/data/channel.hpp"
#include "causal/trace.hpp"
#include "msgpack.hpp"

namespace causal {
    namespace data {
        MAKE_TRACE_SCOPE(scope_fs)

        /// @brief exception thrown on creation of fs_junction without valid root
        EXCEPTION_ERROR(fs_root_not_valid, "filesystem root not existing")

        /// @brief exception thrown on creation of fs_junction without valid root
        EXCEPTION_ERROR(fs_id_not_valid, "given id is either not valid or taken by e.g. a directory")

        /** @brief junction getting binary data from fs
         * @warning this junction does not support file system level locking yet
         */
        class fs_junction final : public junction {
        private:
            fs_junction(const std::string root);

        public:
            /** @brief constructs junction from root path string
             * @param root base directory of junction
             */
            static std::shared_ptr<fs_junction> make(const std::string root);

            const std::string root;

            virtual bool add_space(const junction_space& s) override;

            void attract(junction_space& s, const std::string id) override;
            void repel(const junction_space& s, const std::string id) override;
            const std::string get(const std::string id) const override;
            void set(const std::string id, const std::string& data) override;
            void set(const std::string id, std::string&& data) override;
            void erase(const std::string id) override;
            bool merge(const junction& o, bool override = true) override;
            bool iterate(std::function<bool(const std::string&, const std::string&)>) const override;
        };
    }
}