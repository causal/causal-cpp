// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <map>
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <mutex>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "causal/trace.hpp"

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/data/junction.hpp"
#include "causal/data/channel.hpp"

namespace causal {
    namespace data {
        namespace cc = causal::core;

        /** @brief junction getting binary data from map
         */
        class memory_junction final : public junction, std::unordered_map<std::string, std::string> {
        private:
            memory_junction() = default;

        public:
            /** @brief constructs in-memory junction
             */
            static std::shared_ptr<memory_junction> make();

            virtual bool add_space(const junction_space& s) override;

            void attract(junction_space& s, const std::string id) override;
            void repel(const junction_space& s, const std::string id) override;
            const std::string get(const std::string id) const override;
            void set(const std::string id, const std::string& data) override;
            void set(const std::string id, std::string&& data) override;
            void erase(const std::string id) override;
            bool merge(const junction& o, bool override = true) override;
            bool iterate(std::function<bool(const std::string&, const std::string&)>) const override;
        };

        /** @brief simple in-memory space
         * @tparam ID identifier type of aspects
         */
        template<typename ID=std::string>
        class memory_space final : public std::enable_shared_from_this<memory_space<ID>>, public cc::space_base {
        private:
            mutable std::recursive_mutex mtx;

            std::unordered_map<ID, cc::essence_ptr<>> aspects;

            template<typename GID> requires(std::is_same_v<ID, std::string>)
            static inline ID get_id(const GID& gid) {
                return cc::serialize_id(gid);
            }

            template<typename GID> requires(!std::is_same_v<ID, std::string>)
            static inline ID get_id(const GID& gid) {
                return gid;
            }

            template<bool certain, typename T, typename... Args, typename GID=ID>
            cc::aspect_ptr<T> get_(const GID gid, Args&&... args) {
                ID id = get_id(gid);
                cc::aspect_ptr<T> r;
                std::unique_lock l(this->mtx);
                auto it = this->aspects.find(id);
                if(it != this->aspects.end()) {
                    r = it->second->template as_typed<std::remove_cv_t<T>>();
                }
                
                if(!r) {
                    if constexpr(certain)
                        r = cc::essence<std::remove_cv_t<T>>::make_certain(id, this->shared_from_this(), std::forward<Args>(args)...);
                    else
                        r = cc::essence<std::remove_cv_t<T>>::make(id, this->shared_from_this(), std::forward<Args>(args)...);

                    this->aspects.insert({id, r.get_ptr()->as_untyped()});
                }

                return r;
            }

            memory_space() = default;

        public:
            static std::shared_ptr<memory_space<ID>> make() {
                return std::shared_ptr<memory_space<ID>>(new memory_space<ID>());
            }

            ~memory_space() = default;
            
            void create(const std::any id, const cc::essence_ptr<> a) noexcept override {
                if(id.type() == typeid(ID)) {
                    std::unique_lock l(this->mtx);
                    this->aspects[std::any_cast<ID>(id)] = a;
                }
            }

            void dispose(const std::any id) noexcept override {
                if(id.type() == typeid(ID)) {
                    std::unique_lock l(this->mtx);
                    auto it = this->aspects.find(std::any_cast<ID>(id));
                    if(it != this->aspects.end()) {
                        this->aspects.erase(it);
                    }
                }
            }

            template<typename T, typename... Args>
            cc::aspect_ptr<T> spawn(Args&&... args) {
                if constexpr(std::is_same_v<ID, std::string>)
                    return this->get_<false, T>(boost::uuids::to_string(boost::uuids::random_generator()()), std::forward<Args>(args)...);
                else
                    INTERN_THROW_ERROR(scope_data, cc::id_less_creation_unsupported())
            }

            template<typename T, typename... Args>
            cc::aspect_ptr<T> forge(Args&&... args) {
                if constexpr(std::is_same_v<ID, std::string>)
                    return this->get_<true, T>(boost::uuids::to_string(boost::uuids::random_generator()()), std::forward<Args>(args)...);
                else
                    INTERN_THROW_ERROR(scope_data, cc::id_less_creation_unsupported())
            }

            template<typename T, typename... Args, typename GID=ID>
            cc::aspect_ptr<T> take(const GID gid, Args&&... args) {
                return this->get_<true, T>(gid, std::forward<Args>(args)...);
            }

            template<typename T, typename... Args, typename GID=ID>
            cc::aspect_ptr<T> get(const GID gid) const {
                ID id = get_id(gid);
                cc::aspect_ptr<T> r;
                std::unique_lock l(this->mtx);
                auto it = this->aspects.find(id);
                if(it != this->aspects.end()) {
                    r = it->second->template as_typed<std::remove_cv_t<T>>();
                }

                return r;
            }

            template<typename T, typename... Args, typename GID=ID>
            cc::aspect_ptr<T> get(const GID gid, Args&&... args) {
                return this->get_<false, T>(gid, std::forward<Args>(args)...);
            }

            /** @brief clone referred aspects data into space
             * @tparam T type of aspect or interface type implemented by aspect type
             * @param r reference to aspect
             * @return reference to clone
             * @remark an UUID is generated
             */
            template<typename T, typename GID=ID>
            cc::aspect_ptr<T> clone(cc::aspect_ptr<T>& r)
            requires(!std::is_same_v<T, void> && std::is_same_v<GID, std::string>) {
                return r.clone(boost::uuids::to_string(boost::uuids::random_generator()()), this->shared_from_this());
            }

            /** @brief clone referred aspects data into space
             * @tparam T type of aspect or interface type implemented by aspect type
             * @tparam GID string convertible type
             * @param gid to use for clone
             * @param r reference to aspect
             * @return reference to clone
             */
            template<typename T, typename GID=ID>
            cc::aspect_ptr<T> clone(const GID gid, cc::aspect_ptr<T>& r) {
                return r.clone(get_id(gid), this->shared_from_this());
            }

            cc::essence_ptr<> clone(const void* const key, cc::essence_ptr<> ptr, std::any id = std::any()) override {
                if(id.has_value()) {
                    if(id.type() == typeid(ID))
                        return ptr->clone(key, id, this->shared_from_this());
                    else
                        INTERN_THROW_ERROR(scope_data, cc::incompatible_id())
                    
                } else {
                    if constexpr(std::is_same_v<ID, std::string>)
                        return ptr->clone(key, boost::uuids::to_string(boost::uuids::random_generator()()), this->shared_from_this());
                    else
                        INTERN_THROW_ERROR(scope_data, cc::id_less_creation_unsupported())
                }
            }

            template<typename T, typename... SrcT, typename... DstT>
            cc::wirks_set explode(void(*t)(cc::aspect_ptr<T>&, DstT...), SrcT&&... src) const {
                std::unique_lock l(this->mtx);
                cc::wirks_set ws;
                for(auto& e : this->aspects) {
                    if(auto r = this->get<T>(e.first))
                        ws.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, r, src...)));
                }
                return ws;
            }

            template<typename T, typename... SrcT, typename... DstT>
            cc::wirks_set explode(void(*t)(const ID, cc::aspect_ptr<T>&, DstT...), SrcT&&... src) const {
                std::unique_lock l(this->mtx);
                cc::wirks_set ws;
                for(auto& e : this->aspects) {
                    if(auto r = this->get<T>(e.first))
                        ws.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, e.first, r, src...)));
                }
                return ws;
            }
        };

        /** @brief simple memory channel
         * @tparam T message data type
         */
        template<typename T>
        class memory_channel : public channel<T> {
            static_assert(std::is_copy_constructible_v<T>, "memory channel needs to copy data");
        protected:
            virtual void pull(const T& d) const override {
                std::shared_lock l(this->mtx);
                cc::wirks_set ws = this->wirks;
                ws.apply([&](cc::wirks& w){
                    w.template set<0, const std::string, std::string&&>(T(d));
                });
                this->branch->act_fwd(std::move(ws));
            }

            memory_channel(const std::shared_ptr<cc::branch> b);

        public:
            static std::shared_ptr<memory_channel> make(std::shared_ptr<cc::branch> b) {
                return std::shared_ptr<memory_channel>(new memory_channel(b));
            }

            virtual void push(const T& msg) const override {
                this->pull(msg);
            }

            /** @brief subscribe wirks for getting triggered on received message
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @param t tick taking const copy of string message as first argument
             * @param src further argument sources for wirks
            */
            template<typename... SrcT, typename... DstT>
            void subscribe(void(*t)(const T, DstT...), SrcT&&... src) {
                std::unique_lock l(this->mtx);
                this->wirks.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, std::string(), std::forward<SrcT>(src)...)));
            }            
        };
    }
}
