// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <map>

#include <gnutls/crypto.h>
#include <gnutls/gnutls.h>

#include "causal/trace.hpp"

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "channel.hpp"

#define DEFAULT_RSA_KEY_LENGTH 4096
#define MIN_RSA_KEY_LENGTH 256

#define DEFAULT_SHA_HASH_LENGTH 512
#define MIN_SHA_HASH_LENGTH 160

namespace causal {
    namespace data {
        /// @brief listing supported symmetric key types
        enum class KeyType {
            AES
        };

        /// @brief holding symmetric key information
        struct Key {
            /// @brief type of key
            KeyType type;

            /// @brief length of key
            u_short length;

            /// @brief binary data of key
            std::string data;
        };

        /// @brief listing supported asymmetric  identity types
        enum class IdentityType {
            x509
        };

        /// @brief holding asymmetric identity identification information
        struct IdentityId {
            /// @brief type of identity
            IdentityType type;

            /// @brief id of identity
            std::string id;

            bool operator==(const IdentityId& o) const {
                return this->type == o.type && this->id == o.id;
            }

            bool operator<(const IdentityId& o) const {
                return this->type == o.type ? this->id < o.id : this->type < o.type;
            }

            operator bool() const {
                return !this->id.empty();
            }
        };

        /// @brief holding asymmetric certificate identification information
        struct CertificateId : IdentityId {
            /// @brief name of related identity
            std::string name;
        };

        /// @brief holding asymmetric certificate information
        struct Certificate : CertificateId {
            /// @brief binary data of certificate
            std::string data;

            /// @brief name of issuer certificate
            std::string issuer_name;
        };

        /// @brief holding asymmetric identities public information
        struct PublicIdentity : IdentityId {
            /// @brief binary data of public key
            std::string pkey;
        };

        /// @brief holding asymmetric identities private information
        struct Identity : PublicIdentity {
            /// @brief binary data of private key
            std::string data;

            /// @brief certificate information
            Certificate certificate;
        };

        EXCEPTION_ERROR(gnutls_error, "GnuTLS error")

        class gnutls_init final {
        private:
            // Private constructor to prevent instantiation from outside
            gnutls_init();

            // Singleton instance
            static gnutls_init* instance;

            // Flag for thread-safe initialization
            static std::once_flag initFlag;

            // Method to initialize the library if not already initialized
            static void _init();
            
        public:
            ~gnutls_init();

            // Delete copy constructor and assignment operator to prevent copying
            gnutls_init(const gnutls_init&) = delete;
            void operator=(const gnutls_init&) = delete;

            // Public method to access the singleton instance
            static void init();
        };

        std::string gnutls_derrive_key_sha(const std::string& password, u_short key_length);

        EXCEPTION_ERROR(sha_hash_lenght_invalid, "hash_length should be larger than or equal to 256")

        /// @brief abstract base of hasher
        class hasher {
        public:
            /** @brief generate an SHA hash from given data
             * @param data to generate hash from
             * @param hash_length requested SHA length
             * @return SHA hash of data
             */
            virtual std::string hash(const std::string& data, u_short hash_length=DEFAULT_SHA_HASH_LENGTH) const = 0;
        };

        /// @brief GnuTLS backed SHA crypto hasher
        class gnutls_sha_hasher final : public hasher {
        public:
            gnutls_sha_hasher();

            std::string hash(const std::string& data, u_short hash_length=DEFAULT_SHA_HASH_LENGTH) const override;
        };

        /// @brief abstract base of crypto provider
        struct crypto_provider {
        public:
            /** @brief get hasher using providers backend
             * @return hasher to use
             */
            virtual const hasher& get_hasher() const = 0;

            /** @brief add an asymmetric private identity
             * @param id identity to add
             * @return identification of given identity
             */
            virtual IdentityId add(const Identity id) = 0;

            /** @brief add an asymmetric public identity
             * @param id identity to add
             * @return identification of given identity
             */
            virtual IdentityId add(const PublicIdentity id) = 0;

            /** @brief add an asymmetric public certificate
             * @param crt certificate to add
             * @return identification of given certificate
             */
            virtual CertificateId add(const Certificate crt) = 0;

            /** @brief add multiple asymmetric private identities
             * @param ids map containing identification identity pairs
             * @return amount of added identities
             */
            virtual size_t add(const std::map<std::string, Identity> ids) = 0;

            /** @brief add multiple asymmetric public identities
             * @param pks map containing identification identity pairs
             * @return amount of added identities
             */
            virtual size_t add(const std::map<std::string, PublicIdentity> pks) = 0;

            /** @brief add multiple asymmetric public certificates
             * @param crts map containing identification certificate pairs
             * @return amount of added identities
             */
            virtual size_t add(const std::map<std::string, Certificate> crts) = 0;

            /** @brief remove an asymmetric identity
             * @remark removes public and private identity
             */
            virtual void remove(const IdentityId id) = 0;

            /** @brief remove an asymmetric certificate
             * @remark removes public and private identity
             */
            virtual void remove(const CertificateId id) = 0;

            /** @brief checks if an asymmetric private identity is known
             * @return true if private identity is known
             */
            virtual bool has(const IdentityId id) const = 0;

            /** @brief checks if an asymmetric public identity is known
             * @return true if public identity is known
             */
            virtual bool has_public(const IdentityId id) const = 0;

            /** @brief checks if an asymmetric public certificate is known
             * @return true if public certificate is known
             */
            virtual bool has(const CertificateId id) const = 0;

            /** @brief gets private identity
             * @param id identification of identity
             * @return private identity
             * @throw std::out_of_range if private identity is not known
             */
            virtual Identity get(const IdentityId id) const = 0;

            /** @brief gets public identity
             * @param id identification of identity
             * @return public identity
             * @throw std::out_of_range if identity is not known
             */
            virtual PublicIdentity get_public(const IdentityId id) const = 0;

            /** @brief gets public certificate
             * @param id identification of certificate
             * @return public certificate
             * @throw std::out_of_range if certificate is not known
             */
            virtual Certificate get(const CertificateId id) const = 0;

            /** @brief gets all known private identities
             * @return map containing identification identity pairs
             */
            virtual std::map<std::string, Identity> get_identities() const = 0;

            /** @brief gets all known public identities
             * @remark without those derrivable from known private identities
             * @return map containing identification identity pairs
             */
            virtual std::map<std::string, PublicIdentity> get_publics() const = 0;

            /** @brief gets all known public certificates
             * @remark without those derrivable from known private identities
             * @return map containing identification certificate pairs
             */
            virtual std::map<std::string, Certificate> get_certificates() const = 0;

            /** @brief generates new rsa private identity
             * @param name of certificate to generate
             * @param ca_id identification of authorities identity for signing generated key
             * @remark authority needs to be a known private identity
             * @attention certificate requests are not supported yet
             * @param key_length length of RSA key to generate
             * @param is_ca will be used as certificate authority
             * @return identification of private identity
             */
            virtual IdentityId create_rsa(const std::string name, const IdentityId ca_id = {}, const u_short key_length=DEFAULT_RSA_KEY_LENGTH, const bool is_ca = false) = 0;

            /** @brief generate signature using a known private identity
             * @param id identification of private identity to use for signing
             * @param data to sign
             * @return signature of data generated using private identity
             */
            virtual std::string sign(const IdentityId id, const std::string& data) const = 0;

            /** @brief check signature of data using known identity
             * @param id identification of known identity
             * @param data to check
             * @param signature to verify
             * @return true if signature could be verified
             * @throw std::out_of_range if identity is not known
             */
            virtual bool check(const IdentityId id, const std::string& data, const std::string& signature) const = 0;

            /** @brief check signature of data using known certificate
             * @param id identification of known certificate
             * @param data to check
             * @param signature to verify
             * @return true if signature could be verified
             * @throw std::out_of_range if certificate is not known
             */
            virtual bool check(const CertificateId id, const std::string& data, const std::string& signature) const = 0;

            /** @brief encrypt data using known identity
             * @param id identification of known identity
             * @param data to encrypt
             * @return encrypted data
             * @throw std::out_of_range if identity is not known
             */
            virtual std::string encrypt(const IdentityId id, const std::string& data) const = 0;

            /** @brief encrypt data using known certificate
             * @param id identification of known certificate
             * @param data to encrypt
             * @return encrypted data
             * @throw std::out_of_range if certificate is not known
             */
            virtual std::string encrypt(const CertificateId id, const std::string& data) const = 0;

            /** @brief decrypts encrypted data using known private identity
             * @param id identification of known private identity
             * @param encrypted_data to decrypt
             * @return decrypted data
             * @throw std::out_of_range if private identity is not known
             */
            virtual std::string decrypt(const IdentityId id, const std::string& encrypted_data) const = 0;
        };

        /// @brief abstract base of symmetric coder
        class cipher {
        public:
            virtual ~cipher() = default;

            /** @brief get hasher using coders backend
             * @return hasher to use
             */
            virtual const hasher& get_hasher() const = 0;

            /** @brief gets hash of key in use
             * @return hash of key in use
             */
            virtual const std::string get_token() const = 0;

            /** @brief encrypt data
             * @param data to encrypt
             * @return encrypted data
             */
            virtual std::string encrypt(const std::string& data) const = 0;

            /** @brief decrypt data
             * @param encrypted_data to decrypt
             * @return decrypted data
             * @throw if decryption failed
             */
            virtual std::string decrypt(const std::string& encrypted_data) const = 0;
        };

        /// @brief GnuTLS backed AES crypto coder
        class gnutls_aes_cipher final : public cipher {
        private:
            const std::string key, iv;
            const gnutls_cipher_algorithm_t algo;

        public:
            const u_short key_length;

            gnutls_aes_cipher(const std::string& password, const u_short key_length = 256);

            const hasher& get_hasher() const override;
            const std::string get_token() const override;
            std::string encrypt(const std::string& data) const override;
            std::string decrypt(const std::string& encrypted_data) const override;
        };

        /// @brief basic metadata for channels supporting identification
        struct IdentifiedMessageMeta : MessageMeta {
            PublicIdentity owner_id;
        };
    }
}