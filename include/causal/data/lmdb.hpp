// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <string>
#include <map>

#include <lmdb.h>
#include <type_traits>

#include "junction.hpp"
#include "crypto.hpp"

namespace causal {
    namespace data {
        EXCEPTION_ERROR(lmdb_driver_bug_detected, "lmdb driver bug detected")
        EXCEPTION_ERROR(lmdb_env_creation_failed, "lmdb environment creation failed")
        EXCEPTION_ERROR(lmdb_env_panic, "lmdb environment panic")
        EXCEPTION_ERROR(lmdb_out_of_memory, "lmdb environment out of memory")
        EXCEPTION_ERROR(lmdb_version_mismatch, "lmdb driver and database version mismatch")
        EXCEPTION_ERROR(lmdb_db_corrupt, "lmdb database is corrupt")
        EXCEPTION_ERROR(lmdb_db_dir_not_found, "lmdb database directory not found")
        EXCEPTION_ERROR(lmdb_db_access_failed, "lmdb database access failed")
        EXCEPTION_ERROR(lmdb_db_locked, "lmdb database is already locked")
        EXCEPTION_ERROR(lmdb_db_full, "lmdb database is full")
        EXCEPTION_ERROR(lmdb_overcrowded, "lmdb environment opened too many databases")
        EXCEPTION_ERROR(lmdb_transaction_failed, "lmdb transaction failed")
        EXCEPTION_ERROR(lmdb_transaction_full, "lmdb transaction has too many dirty pages")
        EXCEPTION_ERROR(lmdb_get_failed, "lmdb get operation failed")
        EXCEPTION_ERROR(lmdb_put_failed, "lmdb put operation failed")
        EXCEPTION_ERROR(lmdb_del_failed, "lmdb del operation failed")
        EXCEPTION_ERROR(lmdb_crypto_token_mismatch, "lmdb crypto token mismatch")

        MAKE_TRACE_SCOPE(scope_lmdb)

        /** @brief junction storing data in a LMDB
         * @warning this junction relies on a database which can only used by one thread
         */
        class lmdb_junction final : public junction {
        private:
            const std::unique_ptr<const cipher> ciph;

            MDB_dbi db;
            MDB_env* env = nullptr;
            
            void initialize(const std::string path, const std::string name, const mode_t create_mode);
            void open(const std::string name, const size_t map_size);
            void check_crypto_token();
            
            MDB_txn* begin_txn(bool ro = false) const;
            void commit_txn(MDB_txn* txn) const;
            void abort_txn(MDB_txn* txn) const;
            void reset_txn(MDB_txn* txn) const;

            MDB_val get_hashed_id(const std::string& id, std::string& buf) const;
            MDB_val get_crypt_data(const std::string& data, std::string& buf) const;
            std::string get_decrypt_data(const MDB_val val) const;

            const std::string get_val(const std::string& id, MDB_txn* txn) const;
            bool set_val(const std::string& id, const std::string& data, MDB_txn* txn, bool override = true);
            void erase_val(const std::string& id, MDB_txn* txn);

            lmdb_junction(const std::string path, const std::string name, const mode_t create_mode, const size_t map_size);

            template<typename cipherT>
            lmdb_junction(cipherT&& ciph, const std::string path, const std::string name,
                          const mode_t create_mode, const size_t map_size)
            requires(std::is_base_of<cipher, cipherT>::value)
            : ciph(std::make_unique<cipherT>(std::forward<cipherT>(ciph))) {
                this->initialize(path, name, create_mode);
                this->open(name, map_size);
                this->check_crypto_token();
            }

        public:
            /** @brief constructs junction for given LMDB
             * @param path to LMDB file
             * @param name of database, empty for using unnamed database
             * @param create_mode filemode to use when creating new LMDB file
             * @param map_size to use for mapping data
             * @remark changing map size requires to reload database file and is not supported yet
             */
            static std::shared_ptr<lmdb_junction> make(const std::string path, const std::string name = std::string(), const mode_t create_mode = 0600, const size_t map_size = 10485760);

            /** @brief constructs encrypting junction for given LMDB
             * @param ciph cipher to use for symmetric encryption
             * @param path to LMDB file
             * @param name of database, empty for using unnamed database
             * @param create_mode filemode to use when creating new LMDB file
             * @param map_size to use for mapping data
             * @remark changing map size requires to reload database file and is not supported yet
             */
            template<typename cipherT>
            static std::shared_ptr<lmdb_junction> make(cipherT&& ciph, const std::string path, const std::string name = std::string(),
                          const mode_t create_mode = 0600, const size_t map_size = 10485760)
            requires(std::is_base_of<cipher, cipherT>::value) {
                return std::shared_ptr<lmdb_junction>(new lmdb_junction(std::forward<cipherT>(ciph), path, name, create_mode, map_size));
            }

            ~lmdb_junction();

            void attract(junction_space& s, const std::string id) override;
            void repel(const junction_space& s, const std::string id) override;
            const std::string get(const std::string id) const override;
            void set(const std::string id, const std::string& data) override;
            void set(const std::string id, std::string&& data) override;
            void erase(const std::string id) override;
            bool merge(const junction& o, bool override = true) override;
            bool iterate(std::function<bool(const std::string&, const std::string&)>) const override;
        };
    }
}