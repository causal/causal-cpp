// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once
#include <boost/beast/core/buffers_to_string.hpp>
#include <cstdint>
#include <string>
#include <memory>
#include <algorithm>
#include <iostream>
#include <map>

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

namespace std {
    struct string_literal {
        template<size_t N>
        constexpr string_literal(const char (&str)[N]) {
            static_assert(N < 11, "scopes longer than 10 chars are not supported");
            std::copy_n(str, N, value);
        }
        
        char value[11];
    };

    class base64 {
    public:
        static std::string encode(const std::string &data) {
            static constexpr char sEncodingTable[] = {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

            size_t in_len = data.size();
            size_t out_len = 4 * ((in_len + 2) / 3);
            std::string out(out_len, '\0');
            size_t i;
            char *p = const_cast<char *>(out.c_str());

            for (i = 0; in_len > 2 && i < in_len - 2; i += 3) {
                *p++ = sEncodingTable[(data[i] >> 2) & 0x3F];
                *p++ = sEncodingTable[((data[i] & 0x3) << 4) |
                                        ((int)(data[i + 1] & 0xF0) >> 4)];
                *p++ = sEncodingTable[((data[i + 1] & 0xF) << 2) |
                                        ((int)(data[i + 2] & 0xC0) >> 6)];
                *p++ = sEncodingTable[data[i + 2] & 0x3F];
            }
            if (i < in_len) {
                *p++ = sEncodingTable[(data[i] >> 2) & 0x3F];
                if (i == (in_len - 1)) {
                    *p++ = sEncodingTable[((data[i] & 0x3) << 4)];
                    *p++ = '=';
                } else {
                    *p++ = sEncodingTable[((data[i] & 0x3) << 4) |
                                        ((int)(data[i + 1] & 0xF0) >> 4)];
                    *p++ = sEncodingTable[((data[i + 1] & 0xF) << 2)];
                }
                *p++ = '=';
            }

            return out;
        }

        static std::string decode(const std::string &input) {
            static constexpr unsigned char kDecodingTable[] = {
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63, 52, 53, 54, 55, 56, 57,
                58, 59, 60, 61, 64, 64, 64, 64, 64, 64, 64, 0,  1,  2,  3,  4,  5,  6,
                7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                25, 64, 64, 64, 64, 64, 64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
                37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
                64, 64, 64, 64};

            size_t in_len = input.size();
            if (in_len % 4 != 0)
                return "Input data size is not a multiple of 4";

            size_t out_len = in_len / 4 * 3;
            if (in_len >= 1 && input[in_len - 1] == '=')
                out_len--;
            if (in_len >= 2 && input[in_len - 2] == '=')
                out_len--;

            std::string out('\0', out_len);

            for (size_t i = 0, j = 0; i < in_len;) {
                uint32_t a = input[i] == '='
                    ? 0 & i++
                    : kDecodingTable[static_cast<int>(input[i++])];
                uint32_t b = input[i] == '='
                    ? 0 & i++
                    : kDecodingTable[static_cast<int>(input[i++])];
                uint32_t c = input[i] == '='
                    ? 0 & i++
                    : kDecodingTable[static_cast<int>(input[i++])];
                uint32_t d = input[i] == '='
                    ? 0 & i++
                    : kDecodingTable[static_cast<int>(input[i++])];

                uint32_t triple =
                    (a << 3 * 6) + (b << 2 * 6) + (c << 1 * 6) + (d << 0 * 6);

                if (j < out_len)
                    out[j++] = (triple >> 2 * 8) & 0xFF;
                if (j < out_len)
                    out[j++] = (triple >> 1 * 8) & 0xFF;
                if (j < out_len)
                    out[j++] = (triple >> 0 * 8) & 0xFF;
            }

            return out;
        }
    };
}

namespace utility {
    namespace beast = boost::beast;
    namespace http = beast::http;
    namespace net = boost::asio;
    using tcp = net::ip::tcp;

    class http_client {
    public:
        explicit http_client() : resolver_(ioc_), stream_(net::make_strand(ioc_)) {}

        struct response {
            const u_int status_code;
            const std::map<std::string, std::string> headers;
            const std::string data;
        };

        response get(const std::string& url) {
            return performRequest(url, http::verb::get);
        }

        response post(const std::string& url, const std::string& data = "") {
            return performRequest(url, http::verb::post, data);
        }

        response put(const std::string& url, const std::string& data = "") {
            return performRequest(url, http::verb::put, data);
        }

        response del(const std::string& url) {
            return performRequest(url, http::verb::delete_);
        }

    private:
        net::io_context ioc_;
        tcp::resolver resolver_;
        beast::tcp_stream stream_;

        response performRequest(const std::string& url, http::verb method, const std::string& data = "") {
            auto [proto, host, port, path] = parseUrl(url);
            return performRequest(proto, host, port, path, method, data);
        }

        response performRequest(const std::string& proto, const std::string& host, const std::string& port, const std::string& path, http::verb method, const std::string& data = "") {
            // Resolve the host
            auto const results = resolver_.resolve(host.c_str(), port.c_str());

            // Establish a connection to the server
            net::connect(stream_.socket(), results.begin(), results.end());

            // Create the HTTP request
            http::request<http::string_body> req{method, path.c_str(), 11};
            req.set(http::field::host, host);
            req.set(http::field::content_type, "application/json");
            if (!data.empty()) {
                req.body() = data;
                req.prepare_payload();
            }

            // Send the request
            http::write(stream_, req);

            // Read the response
            beast::flat_buffer buffer;
            http::response<http::dynamic_body> res;
            http::read(stream_, buffer, res);

            // Close the connection
            stream_.socket().shutdown(tcp::socket::shutdown_both);

            std::map<std::string, std::string> headers;
            for (const auto& h : res.base())
                headers[h.name_string().to_string()] = h.value().to_string();
            
            return {res.result_int(), std::move(headers), beast::buffers_to_string(res.body().cdata())};
        }

        static std::tuple<std::string, std::string, std::string, std::string> parseUrl(const std::string& url) {
            size_t protoPos = url.find("://");
            if (protoPos == std::string::npos) throw std::runtime_error("Invalid URL: Missing proto");

            std::string proto = url.substr(0, protoPos);
            if (proto != "http" && proto != "https") throw std::runtime_error("Unsupported protocol");

            size_t hostStart = protoPos + 3;
            size_t pathStart = url.find('/', hostStart);

            std::string host = url.substr(hostStart, pathStart - hostStart);
            std::string port = (proto == "http" ? "80" : "443");
            if (host.find(':') != std::string::npos) {
                size_t portPos = host.rfind(':');
                port = host.substr(portPos + 1);
                host = host.substr(0, portPos);
            }

            std::string path = pathStart == std::string::npos ? "/" : url.substr(pathStart);

            return {proto, host, port, path};
        }
    };
}
