// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <thread>

#include <qt6/QtCore/QObject>
#include <qt6/QtGui/QGuiApplication>
#include <qt6/QtQml/QQmlApplicationEngine>
#include <qt6/QtCore/QTimer>

#undef foreach

#include "causal/core/wirks.hpp"
#include "causal/process/simple.hpp"

namespace causal {
    namespace ui {
        namespace cp = causal::process;

        /// @brief QtQuick dispatcher
        class qtquick : public cp::simple_dispatcher<> {
        private:
            std::thread::id ui_thread;

            QTimer tickTimer;

            qtquick(int& argc, char** argv);

        public:
            QGuiApplication app;
            QQmlApplicationEngine engine;
            
            /** @brief constructs QtQuick dispatchers
             * @param argc mains argument count
             * @param argv mains arguments
             */
            static std::shared_ptr<qtquick> make(int& argc, char** argv);

            virtual ~qtquick() = default;

            /// @brief checks if called in ui thread
            bool is_ui_thread();

            /** @brief loads QtQuick URI
             * @param uri of qml file to load
             */
            void load(std::string uri);

            /// @brief run QtQuick dispatcher returning application exit code
            int run();

            /// @brief quits QtQuick dispatcher
            void quit();
        };
    }
}