// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <gtkmm.h>
#include <memory>
#include "causal/core/wirks.hpp"
#include "causal/process/simple.hpp"

namespace causal {
    namespace ui {
        namespace cp = causal::process;

        /// @brief GTK3 dispatcher
        class gtk : public cp::simple_dispatcher<> {
        private:
            Glib::RefPtr<Gtk::Application> app;
            std::thread::id ui_thread;

            bool on_tick();

            gtk(std::string domain);

        public:
            /** @brief constructs GTK dispatcher
             * @param domain of GTK application
             */
            static std::shared_ptr<gtk> make(std::string domain);

            /** @brief plugging window to gtk dispatcher
             * @param win window to plug
             */
            template<typename W>
            void plug(W& win) {
                this->app->add_window(win);
                win->plug(this);
                win->show();
            }

            /** @brief unplugging window from gtk dispatcher
             * @param win window to plug
             */
            template<typename W>
            void unplug(W& win) {
                win->hide();
                win->unplug();
                this->app->remove_window(win);
            }

            /// @brief checks if called in ui thread
            bool is_ui_thread();

            /// @brief quits gtk dispatcher
            void quit();

            /** @brief runs gtk dispatcher
             * @param w main window
             * @param argc mains argument count
             * @param argv mains arguments
             */
            int run(Gtk::Window& w, int& argc, char** argv);
        };
    }
}
