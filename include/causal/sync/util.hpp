// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <set>

#include "causal/core/wirks.hpp"

namespace causal {
    namespace sync {
        namespace cc = causal::core;

        class dependant;
        template<typename ScopeT=cc::synchron> class dependency_base;
        typedef dependency_base<> dependency;
        typedef dependency_base<cc::fwd_synchron> branch_dependency;

        /// @brief exception thrown when dependant got already feasible
        EXCEPTION_ERROR(dependant_already_feasible, "dependant already feasible")

        /** @brief dependency synchron creator
         * @remark dependant has to be acted before dependencies otherwise
         * it might get feasible before all dependencies are acted and
         * therefore depender throws on adding dependency
         */
        struct depender final {
            friend class dependant;
            friend dependency;
            friend branch_dependency;
        private:
            std::shared_ptr<dependant> dep;

        public:
            depender(depender&& o);
            depender(const depender& o);
            depender();

            depender& operator=(depender&&);
            depender& operator=(const depender& o);

            /** @brief gets dependant synchron
             * @return dependant synchron
             */
            std::shared_ptr<dependant> depend() const;

            /** @brief creates new dependency synchron
             * @return dependency synchron
             * @throw dependant_already_feasible if dependant state already changed to feasible or higher
             */
            std::shared_ptr<dependency> on() const;

            /** @brief creates new branch dependency synchron
             * @return dependency synchron
             * @throw dependant_already_feasible if dependant state already changed to feasible or higher
             */
            std::shared_ptr<branch_dependency> on_branch() const;
        };

        /// @brief dependant of dependencies
        class dependant final : public cc::synchron {
            friend struct depender;
            friend dependency;
            friend branch_dependency;
        private:
            std::mutex mtx;
            std::set<const void*> dependencies;

            dependant();

        public:
            dependant(const dependant&) = delete;
            dependant(dependant&&) = delete;

            void loaded(const cc::action *a) override;
        };

        /** @brief dependency of dependants
         *
         */
        template<typename ScopeT>
        class dependency_base : public ScopeT {
            friend struct depender;
        private:
            std::shared_ptr<dependant> dep;

        public:

            dependency_base(const depender& o)
            : ScopeT(cc::synchron_state::feasible),
              dep(o.dep) {
                std::unique_lock l(this->dep->mtx);
                this->dep->dependencies.insert(this);
            }
            dependency_base() = delete;
            dependency_base(const dependency_base&) = delete;
            dependency_base(dependency_base&&) = delete;

            virtual ~dependency_base() {
                if(this->dep) {
                    std::unique_lock l(this->dep->mtx);
                    this->dep->dependencies.erase(this);
                    if(this->dep->dependencies.empty())
                    this->dep->change(cc::synchron_state::feasible);
                }
            }

            virtual void loaded(const cc::action *a) override {
                if constexpr(std::is_same_v<cc::synchron, ScopeT>)
                {
                    this->change(cc::synchron_state::impossible);
                }
            }
        };

        class anchor final : public cc::fwd_synchron {
        public:
            void dispose();
        };

        /// @brief binds acts to lifetime of object
        struct ground final {
        private:
            std::list<std::weak_ptr<anchor>> anchors;

        public:
            ~ground();

            std::shared_ptr<anchor> drop();
        };

        /// @brief binds acts to lifetime of object (self synchronizing)
        struct sync_ground final {
        private:
            std::mutex mtx;
            std::list<std::weak_ptr<anchor>> anchors;

        public:
            ~sync_ground();

            std::shared_ptr<anchor> drop();
        };
        
        /// @brief wait for a branch to die
        class await : public cc::synchron {
        private:
            std::weak_ptr<cc::branch> branch;
            std::atomic<bool> done = false;

            await(std::shared_ptr<cc::branch> b);

        public:
            virtual ~await();
            
            void loaded(const cc::action *const a) override;

            static std::shared_ptr<await> make(std::shared_ptr<cc::branch> b);
        };

        /// @brief catch exceptions
        class catcher : public cc::synchron {
        private:
            std::weak_ptr<cc::branch> branch;
            const std::unique_ptr<cc::wirks> act;

            catcher(std::shared_ptr<cc::branch> b, std::unique_ptr<cc::wirks> wp);

        public:
            void finished(const cc::action *const a) override;

            template<typename... SrcT, typename... DstT>
            static std::shared_ptr<catcher> make(std::shared_ptr<cc::branch> b, void(*t)(const std::exception_ptr, DstT...), SrcT&&... src) {
                auto wp = std::unique_ptr<cc::wirks>(new cc::wirks(t, std::exception_ptr{}, std::forward<SrcT>(src)...));
                return std::shared_ptr<catcher>(new catcher(b, std::move(wp)));
            }
        };
    }
}