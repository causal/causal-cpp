// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <mutex>
#include <memory>
#include <atomic>
#include <type_traits>

#include "causal/core/wirks.hpp"

namespace causal {
    namespace sync {
        namespace cc = causal::core;

        /// @brief synchron merging multiple wirks
        template<typename U>
        class united final : public cc::synchron {
            friend U;
        private:
            inline cc::synchron_state set(std::shared_ptr<std::atomic<bool>> a) const {
                bool exp = false;
                return a->compare_exchange_strong(exp, true) ?
                       cc::synchron_state::feasible :
                       cc::synchron_state::impossible;
            }
            
            std::shared_ptr<std::atomic<bool>> active;

            united(const U& o)
            : synchron(this->set(o.active)),
              active(o.active)
            {}

        public:
            united() = delete;
            united(const united&) = delete;
            united(united&&) = delete;

            ~united() {
                if(this->get_state() == cc::synchron_state::feasible)
                    *this->active = false;
            }
        };

        /// @brief merge synchron creator using first wirks enqueued, disposing the rest
        struct unifier_front final {
            friend class united<unifier_front>;
        private:
            std::shared_ptr<std::atomic<bool>> active;

        public:
            unifier_front(unifier_front&&) = delete;
            unifier_front(const unifier_front& o);
            unifier_front();

            unifier_front& operator=(unifier_front&&) = delete;
            unifier_front& operator=(const unifier_front& o);

            std::shared_ptr<united<unifier_front>> make() const;
        };

        /** @brief merge synchron creator using last wirks enqueued, disposing the rest
         * @attention unifying back has limitations why it shall be avoided
         * in case of last being disposed execution is swallowed
         * in case last is currently executing, the new united in worst case also will get executed
         */
        struct unifier_back final {
            friend class united<unifier_back>;
        private:
            struct common {
                std::mutex mtx;
                united<unifier_back>* last = nullptr;
            };

            std::shared_ptr<common> share;

        public:
            unifier_back(unifier_back&&) = delete;
            unifier_back(const unifier_back& o);
            unifier_back();

            unifier_back& operator=(unifier_back&&) = delete;
            unifier_back& operator=(const unifier_back& o);

            std::shared_ptr<united<unifier_back>> make() const;
        };

        template<>
        class united<unifier_back> final : public cc::synchron {
            friend struct unifier_back;
        private:
            std::shared_ptr<unifier_back::common> share;

            united(const unifier_back& o)
            : synchron(cc::synchron_state::feasible),
              share(o.share) {
                std::unique_lock l(this->share->mtx);
                if(this->share->last)
                    this->share->last->change(cc::synchron_state::impossible);
                this->share->last = this;
            }

        public:
            united() = delete;
            united(const united&) = delete;
            united(united&&) = delete;

            ~united() {
                if(this->share) {
                    std::unique_lock l(this->share->mtx);
                    if(this->share->last == this)
                        this->share->last = nullptr;
                }
            }
        };
    }
}