// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "causal/core/wirks.hpp"

namespace causal {
    namespace sync {
        namespace cc = causal::core;
        
        /// @brief synchron delaying wirks for at least given amount of miliseconds
        class delay_ms : public cc::synchron {
        private:
            const std::chrono::time_point<std::chrono::high_resolution_clock> until;

            std::thread thr;

            bool done = false, destroy = false;
            std::mutex mtx_cv;
            std::condition_variable cv_wait, cv_destroy;

            delay_ms(u_long ms);

            void waiter();
            
        public:
            static std::shared_ptr<delay_ms> make(u_long ms);
            
            delay_ms() = delete;
            delay_ms(const delay_ms&) = delete;
            delay_ms(delay_ms&&) = delete;

            virtual ~delay_ms();
            
            void loaded(const cc::action *a) override;
        };

        /// @brief synchron disposing wirks after given amount of miliseconds
        class expire_ms : public cc::synchron {
        private:
            const std::chrono::time_point<std::chrono::high_resolution_clock> until;

            std::thread thr;

            bool done = false, destroy = false;
            std::mutex mtx_cv;
            std::condition_variable cv_wait, cv_destroy;

            expire_ms(u_long ms);

            void waiter();
            
        public:
            static std::shared_ptr<expire_ms> make(u_long ms);

            expire_ms() = delete;
            expire_ms(const expire_ms&) = delete;
            expire_ms(expire_ms&&) = delete;

            virtual ~expire_ms();
            
            void loaded(const cc::action *a) override;
        };
    }
}