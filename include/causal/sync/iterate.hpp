// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <memory>
#include <atomic>
#include <boost/smart_ptr/atomic_shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>
#include <type_traits>
#include <chrono>
#include <shared_mutex>

#include "causal/trace.hpp"

#include "causal/core/wirks.hpp"
#include "causal/sync/util.hpp"

namespace causal {
    namespace sync {
        namespace cc = causal::core;

        /** @brief synchron of wirks to call after an iteration is done
         * @remark usually used for triggering next iteration and doing cleanups
         */
        class joined final : public cc::synchron {
            friend struct iterator;
            friend class iterated;
        private:
            joined();
        public:
            joined(const joined&) = delete;
            joined(joined&&) = delete;
        };

        /// @brief synchron of an iteration
        class iterated final : public cc::synchron {
            friend struct iterator;
        private:
            iterated();
        public:
            iterated(const iterated&) = delete;
            iterated(iterated&&) = delete;
        };

        /// @brief iteration synchron creator
        struct iterator final {
            friend class iterated;
            friend class joined;
        private:
            struct common {
                mutable std::shared_mutex mtx;
                bool locked = false;
                size_t round = 1;
                depender dep;
                std::weak_ptr<iterated> act_iterated;
                std::weak_ptr<joined> act_joined;
                std::shared_ptr<iterated> next_iterated = std::shared_ptr<iterated>(new iterated());
                std::shared_ptr<joined> next_joined = std::shared_ptr<joined>(new joined());
            };

            boost::atomic_shared_ptr<common> share;

            void invalidate();

        public:
            static std::shared_ptr<iterator> make();

            iterator();
            iterator(iterator&&) = delete;
            iterator(const iterator& o) = delete;

            virtual ~iterator();

            iterator& operator=(iterator&&) = delete;
            iterator& operator=(const iterator& o) = delete;

            /** @brief act a wirks in next iteration
             * @return synchron defining iteration and dependencies
             */
            std::shared_ptr<cc::synchrons> iterate() const;

            /** @brief act wirks when next iteration is done
             * @return synchron defining dependencies and joining
             */
            std::shared_ptr<cc::synchrons> join() const;

            /// @brief locks iterator in current iteration
            void lock();

            /// @brief unlocks iterator
            void unlock();

            /** @brief gets if iterator is locked
             * @return true if iterator is locked
             */
            bool is_locked() const;

            /** @brief triggers next iteration if current iteration is done and iterator is unlocked
             * @return true if successfully triggered next iteration
             */
            bool next() const;

            /** @brief gets the iteration count
             * @return number of current iteration round
             */
            size_t get_round() const;

            /// @brief resets iterator disposing enqueued wirks
            void reset();
        };
    }
}