// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <tuple>
#include <type_traits>
#include <functional>
#include <map>

#include "causal/trace.hpp"

#include "causal/core/traits.hpp"
#include "causal/core/focus.hpp"
#include "causal/core/borrow.hpp"
#include "causal/core/wirks.hpp"

#include "causal/facet/common.hpp"

namespace causal {
    namespace facet {
        namespace cc = causal::core;
        
        /// @brief edges coupling multiple facet types
        template<typename... F> requires(std::is_base_of_v<cc::facet, F> && ...)
        class edges : private std::tuple<F...>, public cc::facet_base {
            template<typename... OF> requires(std::is_base_of_v<cc::facet, OF> && ...)
            friend class edges;
        public:
            typedef std::tuple<F...> container_type;

            edges() = default;
            edges(const edges& o) noexcept : container_type(o) {}

            /** @brief constructs edges from facets
             * @param fs facets
             */
            edges(const F... fs) : container_type(fs...) {}

            virtual ~edges() = default;

            /** @brief execute a function for each element of edges immutably
             * @tparam Fn function of type bool(const auto&, size_t)
             * @remark aborts loop when a call returns false
             * @param fn_do function to call for each element
             * @return true if all calls returned true
             */
            template<typename Fn>
            bool foreach(Fn fn_do) const {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(const IF& f) {
                    if(!fn_do(f, I)) {
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief execute a function for each element of edges immutably
             * @tparam Fn function of type bool(const auto&, size_t) executed for each element
             * @tparam UFn function of type void(const auto&, size_t) executed backwards
             * for each finished element when one Fn returns false
             * @remark aborts loop when a call returns false and
             * triggers fn_undo for all previous elements in a backward order
             * @param fn_do function to call for each element
             * @param fn_undo undo function to call for each element to revert
             * @return true if all calls of fn_do returned true
             */
            template<typename Fn, typename UFn>
            bool foreach(Fn fn_do, UFn fn_undo) const {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(const IF& f) {
                    if(!fn_do(f, I)) {
                        cc::reverse_static_for_each(*this, [&]<size_t IU, typename IFU>(const IFU& fu) {
                            fn_undo(fu, IU);
                            return true;
                        }, I);
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief execute a function for each element of edges mutably
             * @tparam Fn function of type bool(auto&, size_t)
             * @remark aborts loop when a call returns false
             * @param fn_do function to call for each element
             * @return true if all calls returned true
             */
            template<typename Fn>
            bool foreach(Fn fn_do) {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(IF& f) {
                    if(!fn_do(f, I)) {
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief execute a function for each element of edges mutably
             * @tparam Fn function of type bool(const auto&, size_t) executed for each element
             * @tparam UFn function of type void(const auto&, size_t) executed backwards
             * for each finished element when one Fn returns false
             * @remark aborts loop when a call returns false and
             * triggers fn_undo for all previous elements in a backward order
             * @param fn_do function to call for each element
             * @param fn_undo undo function to call for each element to revert
             * @return true if all calls of fn_do returned true
             */
            template<typename Fn, typename UFn>
            bool foreach(Fn fn_do, UFn fn_undo) {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(IF& f) {
                    if(!fn_do(f, I)) {
                        cc::reverse_static_for_each(*this, [&]<size_t IU, typename IFU>(IFU& fu) {
                            fn_undo(fu, IU);
                            return true;
                        }, I);
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief execute a function template for each element of edges immutably
             * @remark clang support >= 14
             * @tparam Fn function template of type bool<size_t I, typename R>(const R&)
             * @remark aborts loop when a call returns false
             * @param fn_do function template to call for each element
             * @return true if all calls returned true
             */
            template<typename Fn>
            bool foreach_typed(Fn fn_do) const {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(const IF& f) {
                    if(!fn_do.template operator()<I, IF>(f)) {
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief execute a function template for each element of edges immutably
             * @remark clang support >= 14
             * @tparam Fn function template of type bool<size_t I, typename R>(const R&) executed for each element
             * @tparam UFn function template of type void<size_t I, typename R>(const R&) executed backwards
             * for each finished element when one Fn returns false
             * @remark aborts loop when a call returns false and
             * triggers fn_undo for all previous elements in a backward order
             * @param fn_do function template to call for each element
             * @param fn_undo undo function template to call for each element to revert
             * @return true if all calls of fn_do returned true
             */
            template<typename Fn, typename UFn>
            bool foreach_typed(Fn fn_do, UFn fn_undo) const {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(const IF& f) {
                    if(!fn_do.template operator()<I, IF>(f)) {
                        cc::reverse_static_for_each(*this, [&]<size_t IU, typename IFU>(const IFU& fu) {
                            fn_undo.template operator()<IU, IFU>(fu);
                            return true;
                        }, I);
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief execute a function template for each element of edges mutably
             * @remark clang support >= 14
             * @tparam Fn function template of type bool<size_t I, typename R>(const R&)
             * @remark aborts loop when a call returns false
             * @param fn_do function template to call for each element
             * @return true if all calls returned true
             */
            template<typename Fn>
            bool foreach_typed(Fn fn_do) {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(IF& f) {
                    if(!fn_do.template operator()<I, IF>(f)) {
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief execute a function template for each element of edges mutably
             * @remark clang support >= 14
             * @tparam Fn function template of type bool<size_t I, typename R>(const R&) executed for each element
             * @tparam UFn function template of type void<size_t I, typename R>(const R&) executed backwards
             * for each finished element when one Fn returns false
             * @remark aborts loop when a call returns false and
             * triggers fn_undo for all previous elements in a backward order
             * @param fn_do function template to call for each element
             * @param fn_undo undo function template to call for each element to revert
             * @return true if all calls of fn_do returned true
             */
            template<typename Fn, typename UFn>
            bool foreach_typed(Fn fn_do, UFn fn_undo) {
                bool ret = true;
                cc::static_for_each(*this, [&]<size_t I, typename IF>(IF& f) {
                    if(!fn_do.template operator()<I, IF>(f)) {
                        cc::reverse_static_for_each(*this, [&]<size_t IU, typename IFU>(IFU& fu) {
                            fn_undo.template operator()<IU, IFU>(fu);
                            return true;
                        }, I);
                        ret = false;
                        return false;
                    }
                    return true;
                });

                return ret;
            }

            /** @brief gets size of edges
             * @return size of edges
             */
            constexpr size_t size() const noexcept {
                return std::tuple_size<container_type>::value;
            }

            edges& operator=(const container_type& o) {
                this->modify();
                this->container_type::operator=(o);
                return *this;
            }

            edges& operator=(const edges& o) {
                this->modify();
                this->container_type::operator=(o);
                return *this;
            }

            virtual operator bool() const noexcept override {return true;}

            virtual void focus(const void* const key) const noexcept override {
                if(!this->is_focused()) {
                    focusables fs;
                    this->foreach(
                        [&fs](const auto& f, const size_t i) {fs.insert(&f); return true;}
                    );
                    this->focus_(fs, key);
                }
            }

            virtual bool borrow(const void* const key) const noexcept override {
                if(!this->is_borrowed()) {
                    borrowables bs;
                    this->foreach(
                        [&bs](const auto& f, const size_t i) {bs.insert(&f); return true;}
                    );
                    return this->borrow_(bs, key);
                } else
                    return false;
            }

            /** @brief sets facet as element
             * @tparam I index of edges element
             * @param f facet to set
             */
            template<size_t I>
            void set(const typename std::tuple_element_t<I, container_type> f) {
                this->modify();
                std::get<I>(*this) = f;
            }

            /** @brief gets reference of facet element immutably
             * @tparam I index of edges element
             * @return reference of elements facet
             */
            template<size_t I>
            const typename std::tuple_element_t<I, container_type>& get() const {
                return std::get<I>(*this);
            }

            /** @brief expands edges as first argument of one tick as one wirks
             * @tparam SrcT arguments to add after edges
             * @tparam DstT arguments of tick
             * @param t tick of resulting wirks
             * @param src argument sources to forward as 2-n argument of tick
             * @return unique pointer of wirks to use for building an action
             */
            template<typename... SrcT, typename... DstT>
            std::unique_ptr<cc::wirks> expand(void(*t)(DstT...), SrcT&&... src) const {
                return std::unique_ptr<cc::wirks>(new cc::wirks(t, *this, std::forward<SrcT>(src)...));
            }

            /// @brief tries to cast facet into a copy of new inner type
            template<typename... NF>
            operator edges<NF...>() const
            requires(std::is_convertible_v<F, NF> && ...) {
                edges<NF...> e;

                cc::static_for_each(*this, [&]<size_t I, typename IF>(const IF& f) {
                    std::get<I>(e) = f;
                    return true;
                });
                
                return e;
            }
        };

        template<typename... T>
        using ptr_edges = edges<cc::aspect_ptr<T>...>;
    }
}
