// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <type_traits>
#include <functional>
#include <map>

#include "causal/trace.hpp"

#include "causal/core/traits.hpp"
#include "causal/core/focus.hpp"
#include "causal/core/borrow.hpp"

namespace causal {
    namespace facet {        
        /// @brief exception thrown by connect when trying to connect connected
        EXCEPTION_ERROR(facet_already_connected, "trying to connect already connected facet")
        
        /// @brief exception thrown by disconnect when trying to disconnect unconnected
        EXCEPTION_ERROR(facet_not_connected, "trying to disconnect unconnected facet")
    }
}
