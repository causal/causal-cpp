// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#pragma once

#include <type_traits>
#include <functional>
#include <map>

#include "causal/trace.hpp"

#include "causal/core/traits.hpp"
#include "causal/core/focus.hpp"
#include "causal/core/borrow.hpp"
#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"

#include "causal/facet/common.hpp"

namespace causal {
    namespace facet {
        namespace cc = causal::core;

        /// @brief coupling multiple facets of one type with scale information for each
        template<typename F, typename S>
        class scales : private std::map<F, S>, public cc::facet_base {
        public:
            typedef std::map<F, S> container_type;
            typedef std::set<F> key_container_type;

            using typename container_type::key_type;
            using typename container_type::value_type;
            using typename container_type::key_compare;
            using typename container_type::value_compare;
            using typename container_type::allocator_type;
            using typename container_type::reference;
            using typename container_type::const_reference;
            using typename container_type::pointer;
            using typename container_type::const_pointer;
            using typename container_type::const_iterator;
            using typename container_type::const_reverse_iterator;
            using typename container_type::difference_type;
            using typename container_type::size_type;
            using container_type::contains;
            using container_type::empty;
            using container_type::size;
            using container_type::max_size;
            using container_type::key_comp;
            using container_type::value_comp;
            using container_type::find;
            using container_type::count;
            using container_type::lower_bound;
            using container_type::upper_bound;
            using container_type::equal_range;
            using container_type::get_allocator;

            scales() = default;
            scales(scales&& o) = default;
            scales(const scales& o) = default;

            /** @brief constructs scales from container
             * @param fs facets
             */
            scales(container_type fs) : container_type(fs) {}

            /** @brief constructs routes from facet and scale pairs
             * @param init initializer list of facet and scale pairs
             */
            scales(std::initializer_list<value_type> init) : container_type(init) {}

            virtual ~scales() = default;

            container_type get_container() const {return this;}

            const_iterator begin() const noexcept {return this->container_type::begin();}
            const_iterator end() const noexcept {return this->container_type::end();}

            const_reverse_iterator rbegin() const noexcept {return this->container_type::rbegin();}
            const_reverse_iterator rend() const noexcept {return this->container_type::rend();}

            const_iterator cbegin() const noexcept {return this->container_type::cbegin();}
            const_iterator cend() const noexcept {return this->container_type::cend();}
            const_reverse_iterator crbegin() const noexcept {return this->container_type::crbegin();}
            const_reverse_iterator crend() const noexcept {return this->container_type::crend();}

            scales& operator=(const container_type& o) {
                this->modify();
                this->container_type::operator=(o);
                return *this;
            }

            scales& operator=(const scales& o) {
                this->modify();
                this->container_type::operator=(o);
                return *this;
            }
            
            std::pair<const_iterator,bool> insert(const F f, S s) {
                this->modify();
                if(!f)
                    INTERN_THROW_ERROR(scope_facet, cc::facet_invalid())
                return this->container_type::insert({f, s});
            }
            
            void erase(const_iterator position) {
                this->modify();
                this->container_type::erase(position);
            }

            size_type erase(const F f) {
                this->modify();
                return this->container_type::erase(f);
            }
            
            S& at(const F f) {
                this->modify();
                return this->container_type::at(f);
            }

            const S& at(const F f) const {
                return this->container_type::at(f);
            }

            S& operator[](const F f) {
                this->modify();
                return this->container_type::operator[](f);
            }

            void swap(container_type& o) {
                this->modify();
                this->container_type::swap(o);
            }

            void clear() {
                this->modify();
                this->container_type::clear();
            }

            /** @brief execute a function for each element of scales immutably
             * @remark aborts loop when a call returns false and
             * triggers fn_undo for all previous elements in a backward order
             * @param fn_do function to call for each element
             * @param fn_undo undo function to call for each element to revert
             * @return true if all calls of fn_do returned true
             */
            bool foreach(std::function<bool(const F&, const S&)> fn_do, std::function<void(const F&, const S&)> fn_undo = {}) const {
                for(auto it = this->container_type::cbegin(); it != this->container_type::cend(); ++it)
                    if(!fn_do(it->first, it->second)) {
                        if(fn_undo && it != this->container_type::cbegin())
                            for (auto bit = std::make_reverse_iterator(it); bit != this->container_type::crend(); ++bit)
                                fn_undo(bit->first, bit->second);
                        return false;
                    }
                return true;
            }

            /** @brief execute a function for each element of scales with mutable scale
             * @remark aborts loop when a call returns false and
             * triggers fn_undo for all previous elements in a backward order
             * @param fn_do function to call for each element
             * @param fn_undo undo function to call for each element to revert
             * @return true if all calls of fn_do returned true
             */
            bool foreach(std::function<bool(const F&, S&)> fn_do, std::function<void(const F&, S&)> fn_undo = {}) {
                for(auto it = this->container_type::cbegin(); it != this->container_type::cend(); ++it)
                    if(!fn_do(it->first, it->second)) {
                        if(fn_undo && it != this->container_type::cbegin())
                            for (auto bit = std::make_reverse_iterator(it); bit != this->container_type::crend(); ++bit)
                                fn_undo(bit->first, bit->second);
                        return false;
                    }
                return true;
            }

            virtual operator bool() const noexcept override {return true;}

            virtual void focus(const void* const key) const noexcept override {
                if(!this->is_focused()) {
                    focusables fs;
                    this->foreach(
                        [&fs](const F& f, const S&) {fs.insert(&f); return true;}
                    );
                    this->focus_(fs, key);
                }
            }

            virtual bool borrow(const void* const key) const noexcept override {
                if(!this->is_borrowed()) {
                    borrowables bs;
                    this->foreach(
                        [&bs](const F& f, const S&) {bs.insert(&f); return true;}
                    );
                    return this->borrow_(bs, key);
                } else
                    return false;
            }

            /** @brief inserts given facet and creates a wirks
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type F
             * @param t tick to act on facet given
             * @param f facet to insert and to act tick on
             * @param scale to insert
             * @param src tick argument sources
            */
            template<typename... SrcT, typename... DstT>
            std::unique_ptr<cc::wirks> connect(void(*t)(DstT...), const F f, S scale, SrcT&&... src) {
                this->modify();
                if(!f)
                    INTERN_THROW_ERROR(scope_facet, cc::facet_invalid())

                if(this->container_type::insert({f, scale}).second)
                    return std::unique_ptr<cc::wirks>(new cc::wirks(t, f, scale, std::forward<SrcT>(src)...));
                else
                    INTERN_THROW_ERROR(scope_facet, facet_already_connected())
            }

            /** @brief inserts given facets and creates a wirks
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type scales<F, S>
             * @param t tick to act on facets given
             * @remark only facets which truly could be inserted will be part of wirks
             * @param fs facets and scales to insert and to act tick on
             * @param src tick argument sources
            */
            template<typename... SrcT, typename... DstT>
            std::unique_ptr<cc::wirks> connect_expand(void(*t)(DstT...), container_type fs, SrcT&&... src) {
                this->modify();
                scales os;
                for(auto& f : fs) {
                    if(f.first && this->container_type::insert({f.first, f.second}).second)
                        os.container_type::insert({f.first, f.second});
                }
                return std::unique_ptr<cc::wirks>(new cc::wirks(t, os, std::forward<SrcT>(src)...));
            }

            /** @brief inserts given facets and creates a set of wirks
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type F
             * @param t tick to act on facets given
             * @remark only facets which truly could be inserted will be part of wirks
             * @param fs facets and scales to insert and to act tick on
             * @param src tick argument sources
            */
            template<typename... SrcT, typename... DstT>
            cc::wirks_set connect_explode(void(*t)(DstT...), container_type fs, SrcT&&... src) {
                this->modify();
                cc::wirks_set ws;
                for(auto& f : fs) {
                    if(f.first && this->container_type::insert({f.first, f.second}).second)
                        ws.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, f.first, f.second, std::forward<SrcT>(src)...)));
                }
                return ws;
            }

            /** @brief removes given facet and creates a wirks
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type F
             * @param t tick to act on facet given
             * @param f facet to remove and to act tick on
             * @param src tick argument sources
            */
            template<typename... SrcT, typename... DstT>
            std::unique_ptr<cc::wirks> disconnect(void(*t)(DstT...), const F f, SrcT&&... src) {
                this->modify();
                auto s = this->at(f);
                if(this->container_type::erase(f))
                    return std::unique_ptr<cc::wirks>(new cc::wirks(t, f, s, std::forward<SrcT>(src)...));
                else
                    INTERN_THROW_ERROR(scope_facet, facet_not_connected())
            }

            /** @brief removes given facets and creates a wirks
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type scales<F, S>
             * @param t tick to act on facets given
             * @remark only facets which truly could be removed will be part of wirks
             * @param fs facets to remove and to act tick on
             * @param src tick argument sources
            */
            template<typename... SrcT, typename... DstT>
            std::unique_ptr<cc::wirks> disconnect_expand(void(*t)(DstT...), std::set<F> fs, SrcT&&... src) {
                this->modify();
                scales os;
                for(auto& f : fs) {
                    auto s = this->at(f);
                    if(this->container_type::erase(f))
                        os.container_type::insert({f, s});
                }
                return std::unique_ptr<cc::wirks>(new cc::wirks(t, os, std::forward<SrcT>(src)...));
            }

            /** @brief removes given facets and creates a set of wirks
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type F
             * @param t tick to act on facets given
             * @remark only facets which truly could be removed will be part of wirks
             * @param fs facets to remove and to act tick on
             * @param src tick argument sources
            */
            template<typename... SrcT, typename... DstT>
            cc::wirks_set disconnect_explode(void(*t)(DstT...), std::set<F> fs, SrcT&&... src) {
                this->modify();
                cc::wirks_set ws;
                for(auto& f : fs) {
                    auto s = this->at(f);
                    if(this->container_type::erase(f))
                        ws.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, f, s, std::forward<SrcT>(src)...)));
                }
                return ws;
            }

            /** @brief creates a wirks applying a tick to all facets
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type scales<F, S>
             * @param t tick to act on facets
             * @param src tick argument sources
             */
            template<typename... SrcT, typename... DstT>
            std::unique_ptr<cc::wirks> expand(void(*t)(DstT...), SrcT&&... src) const {
                return std::unique_ptr<cc::wirks>(new cc::wirks(t, *this, std::forward<SrcT>(src)...));
            }

            /** @brief creates a set of wirks applying a tick to each facet
             * @tparam SrcT argument source types
             * @tparam DstT argument types of given tick
             * @remark first DstT needs to be of type F
             * @param t tick to act on facets
             * @param src tick argument sources
             */
            template<typename... SrcT, typename... DstT>
            cc::wirks_set explode(void(*t)(DstT...), SrcT&&... src) const {
                cc::wirks_set ws;
                for(auto& f : *this)
                    ws.add(std::unique_ptr<cc::wirks>(new cc::wirks(t, F(f.first), f.second, src...)));
                return ws;
            }

            /// @brief tries to cast facet into a copy of new inner type
            template<typename NF, typename NS>
            operator scales<NF, NS>() const
            requires(std::is_convertible_v<F, NF> && std::is_convertible_v<S, NS>) {
                scales<NF, NS> s;

                for(auto f : *this)
                    s.insert(static_cast<NF>(f.first), static_cast<NS>(f.second));
                
                return s;
            }
        };

        template<typename T, typename S>
        using ptr_scales = scales<cc::aspect_ptr<T>, S>;
    }
}