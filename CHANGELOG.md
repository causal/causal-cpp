# Changelog
## 0.3.1
* 0000000 implemented transactional borrow support on aspects
* 0000004 implemented lmdb junction (locking)
* 0000018 implemented support for database level symmetric encryption into lmdb_junction
* 0000005 implemented channel/source concept
* 0000008 implemented msgpack_channel
* 0000009 implemented opendht_source
* 0000014 implement shared_ptr/weak_ptr support
* 0000015 implement concept for crypto aspect
* 0000016 implement opendht_crypto aspect
* 0000000 added support to access for copying unmanaged references into new memory aspect
* 0000021 implement merge concept for junctions
* 0000024 implement explode to junction_spaces and memory_space
* 0000000 strictly tighten compilation rules
* 0000000 supporting clang 13 build

## 0.3.2
* 0000000 refactored focusing concept
* 0000025 regulate managed_dispatcher using condition variables

## 0.4.0
* 0000000 refined get/take concept on spaces
* 0000000 refined branch pointer handling
* 0000000 refined space pointer handling
* 0000000 separate gol example from test
* 0000000 fix iterator logic, there can only be one joiner
* 0000000 removing type from space id's which now needs to implement operator<<
* 0000026 revive gtk gol example
* 0000000 introduced memory_channel concept
* 0000000 renamings related to branch and intentions(now synchrons)
* 0000028 performance improvements
* 0000023 qt quick integration

## 0.4.1
* 0000000 performance improvements
* 0000000 bug fixes

## 0.5.0
* 0000000 removed requirement of operator<< for space id's but require given id to be convertible to real it which might be string
* 0000000 dispatcher supporting different action_queue types
* 0000000 readded order preserving queue
* 0000031 added change callback for syncs and queues

## 0.5.1
* 0000031 refactored synchron's to be pure atomic state machines
* 0000031 pooling synchrons

## 0.7.0
* 0000036 implemented support for abstract interfaces for aspect's
* 0000042 added cloning to aspect and spaces
* 0000045 introduced ref storing ref instead of aspect* for facets
* 0000000 removed ref constness implications (const focus- and borrowables are mutable)
* 0000046 implemented automatic ID creation for spaces
* 0000000 moved edges facet to own header
* 0000048 implemented facet base
* 0000048 made facets editable while being active
* 0000049 generalized edges, spreads, scales and routes to contain other facets rather than only refs
* 0000049 replaced relatable with shared_ptr
* 0000043 replaced own dependency building system with jami's contrib system
* 0000043 added full android dependency build

## 0.8.0
* 0000055 refactored ref and removed const_borrow; constness is determined by inner type constness of ref
* 0000054 refactored access to utilize clear concepts
* 0000033 refined build for building gol qtquick example successfully for android-28
* 0000033 ported qtquick GoL example to android
* 0000060 remove memory_chunk and replace with unmanaged pointer
* 0000061 remove aspect::essence and rename aspect to essence

## 0.9
* 0000000 cleaned up facet namings and added initializer_list cotr to routes and scales
* 0000074 ground-anchor synchron  tangling to hosting object's lifetime
* 0000075 implemented tracing
* 0000078 refactored wirks concept to avoid accumulation and deep recursion
* 0000080 replaced backtrace support and replaced with -rdynamic linker flag so boost::stacktrace_basic can directly resolve addresses
* 0000084 added dependency build for bullet physics engine
* 0000079 added dependency build for CGAL geometry library
* 0000090 added dependency build for OpenCV library
* 0000081 refactored dependency synchrons
* 0000085 added TRACE scopes

## 0.10
* 0000000 slightly fixed wirks documentation
* 0000093 implemented amap2dot converter
* 0000094 implemented amap-designer

## 0.11
* removed utilities (amap2dot converter, amap-designer)
* #000009 Catching Synchron
* #000011 JSON Channel
* #000010 Filesystem Junction
* #000012 Memory Junction
* #000006 added Web Application Server
* #000015 Replace OpenDHT based crypto implementation for core functions with a GnuTLS based
