// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include <chrono>
#include <memory>

#include "causal/core/wirks.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"

#include "causal/system/observe.hpp"

namespace cc = causal::core;
namespace cp = causal::process;
namespace cs = causal::system;

#define GOAL 10u

class observation final : public cs::observation<size_t> {
public:
    const std::weak_ptr<cc::branch> target;
    cc::essence_ptr<> observer;

    template<typename T>
    observation(std::weak_ptr<cc::branch>&& b, const cc::aspect_ptr<T> oable)
    : target(std::forward<std::weak_ptr<cc::branch>>(b)),
      observer(oable.get_ptr()->as_untyped()) {}

    void percept(const size_t& v) const override;
    bool operator==(const observation& other) const;
};

class Observable : public cs::Observable<observation> {
public:
    size_t val = 0;

    static void Trigger(Observable& ole) {
        ole.project(ole.val);
    }

    static void Increment(Observable& ole, const size_t& val) {
        if(ole.val < GOAL && val >= ole.val) {
            ole.val++;
            ole.project(ole.val);
        }
    }
};

void observation::percept(const size_t& v) const {
    auto b = this->target.lock();
    if(b)
        b->act_fwd(Observable::Increment, cc::aspect_ptr<Observable>(this->observer->as_typed<Observable>()), cc::forge<size_t>(v));
}

bool observation::operator==(const observation& other) const {
    return this == &other;
}

TEST(causal_system_observe, simple) {
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);

    auto oale_1 = cc::spawn<Observable>();
    auto oale_2 = cc::spawn<Observable>();
    auto otion_1 = std::make_shared<observation>(b, oale_2);
    auto otion_2 = std::make_shared<observation>(b, oale_1);
    {   cc::opener o1(oale_1);
        cc::opener o2(oale_2);
        
        (*oale_1).focus(otion_1);
        (*oale_2).focus(otion_2);
        b->act_fwd(Observable::Trigger, oale_1);
    }

    for(size_t i = 0; i <= GOAL; i++) {
        EXPECT_TRUE(d->do_tick());
        EXPECT_TRUE(d->do_tick());
    }
    EXPECT_FALSE(d->do_tick());


    {   cc::opener o1(oale_1);
        cc::opener o2(oale_2);

        EXPECT_EQ(oale_1->val, GOAL);
        EXPECT_EQ(oale_2->val, GOAL);
    }
}

TEST(causal_system_observe, many) {
    auto d1 = cp::managed_dispatcher<>::make();
    auto d2 = cp::managed_dispatcher<>::make();
    auto b1 = cc::branch::make(d1);
    auto b2 = cc::branch::make(d2);

    auto oale_1 = cc::spawn<Observable>();
    auto oale_2 = cc::spawn<Observable>();
    {   cc::opener o1(oale_1);
        cc::opener o2(oale_2);
        
        (*oale_1).focus(std::make_shared<observation>(b2, oale_2));
        (*oale_2).focus(std::make_shared<observation>(b1, oale_1));
        b1->act_fwd(Observable::Trigger, oale_1);
    }

    while(d1->busy() || d2->busy())
        std::this_thread::sleep_for(std::chrono::milliseconds(5));

    {   cc::opener o1(oale_1);
        cc::opener o2(oale_2);

        EXPECT_EQ(oale_1->val, GOAL);
        EXPECT_EQ(oale_2->val, GOAL);
    }
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}