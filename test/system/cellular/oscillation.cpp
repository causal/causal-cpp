// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <vector>
#include <cmath>
#include <algorithm>
#include <queue>
#include <map>
#include <iostream>
#include <iomanip>
#include <memory>

#include <gtest/gtest.h>

#include "causal/core/wirks.hpp"
#include "causal/sync/unite.hpp"
#include "causal/system/cellular/oscillation.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"

namespace cc = causal::core;
namespace cy = causal::sync;
namespace cf = causal::facet;
namespace cp = causal::process;
namespace cs = causal::system;

struct Pendulum;

#define WAIT_FOR_HITS 11
/* due to disrete distribution for low values at differing amplitude might change wavelength slightly.
since an exact match is done, skip lower values at test */
#define START_AT 11
#define DEPTH 30
#define MAX_TICKS 2500
//#define VERBOSE
//#define SERIES

struct Spring final : cs::Bond<Pendulum, Pendulum> {
    cy::unifier_front unifier;

    u_int64_t apply_cnt = 0;
    u_int64_t hit_cnt = 0;
    u_int64_t eq_cnt = 0;
    u_int64_t max_cnt_1 = 0;
    u_int64_t max_cnt_2 = 0;
    int64_t ref, prev;
    bool up = false;

    static void Pre_apply(cc::aspect_ptr<const Spring>& s);
    static void Apply(cf::ptr_edges<Pendulum, Pendulum>& ps, cc::aspect_ptr<Spring>& s);
};

struct Pendulum final : cs::Oscillator<Spring> {
    std::string name;
    
    static void Apply(Pendulum& p);
};

void Spring::Pre_apply(cc::aspect_ptr<const Spring>& s) {
    cc::act_synced(s->unifier.make(), s->oscillators.expand(Apply, s));
}

void Spring::Apply(cf::ptr_edges<Pendulum, Pendulum>& ps, cc::aspect_ptr<Spring>& s) {
    if(s->apply_cnt == 0)
        s->ref = s->prev = ps.get<0>()->velocity;
    
    s->apply(ps);

#ifdef SERIES
    std::cout << s.ref << "\t"
              << ps.get<0>().velocity << "\t"
              << ps.get<1>().velocity << "\t"
              << ps.get<0>().elongation << "\t"
              << ps.get<1>().elongation << "\t"
              << std::endl;
#endif
    
    if(ps.get<0>()->velocity == ps.get<1>()->velocity)
        s->eq_cnt++;

    if(s->apply_cnt > 0 && ps.get<0>()->velocity == s->ref)
        s->max_cnt_1++;

    if(s->apply_cnt > 0 && ps.get<1>()->velocity == s->ref)
        s->max_cnt_2++;

    if(s->up && ps.get<0>()->velocity < s->prev) {
        s->hit_cnt++;
        s->up = false;
    }

    if(!s->up && ps.get<0>()->velocity > s->prev) {
        s->up = true;
    }

    if(s->hit_cnt < WAIT_FOR_HITS) {
        s->apply_cnt++;
        s->prev = ps.get<0>()->velocity;
        cc::act(Pendulum::Apply, ps.get<0>());
        cc::act(Pendulum::Apply, ps.get<1>());
    }
}

void Pendulum::Apply(Pendulum& p) {
    p.apply();
    cc::act(p.bonds.explode(Spring::Pre_apply));
}

TEST(causal_system_oscillator, osciallation) {
    std::queue<u_int64_t> values;

    std::map<u_int64_t, u_int64_t> pairs;

#ifdef VERBOSE
    std::cout << std::left << std::setw(12) << std::setfill(' ') << "Arg"
              << std::left << std::setw(12) << std::setfill(' ') << "Equals"
              << std::left << std::setw(12) << std::setfill(' ') << "Hits"
              << std::left << std::setw(12) << std::setfill(' ') << "MaxCnt1"
              << std::left << std::setw(12) << std::setfill(' ') << "MaxCnt2"
              << std::left << std::setw(12) << std::setfill(' ') << "ApplyCnt"
              << std::left << std::setw(12) << std::setfill(' ') << "WaveLength"
              << std::left << std::setw(12) << std::setfill(' ') << "WaveRest"
              << std::left << std::setw(12) << std::setfill(' ') << "TickCnt"
              << std::endl;
#endif

    for(u_int64_t value = START_AT; value <= START_AT+DEPTH; value++) {
        auto d = cp::simple_dispatcher<>::make();
        auto b = cc::branch::make(d);

        auto rp1_1 = cc::spawn<Pendulum>(),
             rp1_2 = cc::spawn<Pendulum>(),
             rp2_1 = cc::spawn<Pendulum>(),
             rp2_2 = cc::spawn<Pendulum>();
        auto rs_1 = cc::spawn<Spring>(),
             rs_2 = cc::spawn<Spring>();

        size_t Cnt_1 = 0, Cnt_2 = 0;

        {
            {   cc::opener op1(rp1_1), op2(rp1_2); cc::opener os(rs_1);
                (*rp1_1).name = "Osc1";
                (*rp1_2).name = "Osc2";
                (*rp1_1).velocity = pow(value, 3)*2;
                (*rp1_1).elongation = (pow(value, 3)*2)/value;
                (*rp1_1).bonds.insert(rs_1);
                (*rp1_2).bonds.insert(rs_1);
                (*rs_1).loose = value;
                (*rs_1).oscillators = cf::edges(rp1_1, rp1_2);
            }

            b->act(Spring::Pre_apply, rs_1);

            bool done = false;
            for(Cnt_1 = 0; Cnt_1 < MAX_TICKS; Cnt_1++) {
                if(!d->do_tick()) {
                    done = true;
                    break;
                }
            }

            EXPECT_TRUE(done);
        }

        {
            {   cc::opener op1(rp2_1), op2(rp2_2); cc::opener os(rs_2);
                (*rp2_1).name = "Osc1";
                (*rp2_2).name = "Osc2";
                (*rp2_1).velocity = pow(value, 6)*2;
                (*rp2_1).elongation = (pow(value, 6)*2)/value;
                (*rp2_1).bonds.insert(rs_2);
                (*rp2_2).bonds.insert(rs_2);
                (*rs_2).loose = value;
                (*rs_2).oscillators = cf::edges(rp2_1, rp2_2);
            }

            b->act(Spring::Pre_apply, rs_2);

            bool done = false;
            for(Cnt_2 = 0; Cnt_2 < MAX_TICKS; Cnt_2++) {
                if(!d->do_tick()) {
                    done = true;
                    break;
                }
            }

            EXPECT_TRUE(done);
        }
        
        {   cc::opener os_1(rs_1), os_2(rs_2);

            // check that differing amplitude didn't change wavelength
            EXPECT_EQ(rs_1->apply_cnt/WAIT_FOR_HITS, rs_2->apply_cnt/WAIT_FOR_HITS);

#ifdef VERBOSE
            std::cout << std::left << std::setw(12) << std::setfill(' ') << value
                      << std::left << std::setw(12) << std::setfill(' ') << rs_1->eq_cnt
                      << std::left << std::setw(12) << std::setfill(' ') << rs_1->hit_cnt
                      << std::left << std::setw(12) << std::setfill(' ') << rs_1->max_cnt_1
                      << std::left << std::setw(12) << std::setfill(' ') << rs_1->max_cnt_2
                      << std::left << std::setw(12) << std::setfill(' ') << rs_1->apply_cnt
                      << std::left << std::setw(12) << std::setfill(' ') << rs_1->apply_cnt/WAIT_FOR_HITS
                      << std::left << std::setw(12) << std::setfill(' ') << rs_1->apply_cnt%WAIT_FOR_HITS
                      << std::left << std::setw(12) << std::setfill(' ') << Cnt_1
                      << std::endl;
            std::cout << std::left << std::setw(12) << std::setfill(' ') << value
                      << std::left << std::setw(12) << std::setfill(' ') << rs_2->eq_cnt
                      << std::left << std::setw(12) << std::setfill(' ') << rs_2->hit_cnt
                      << std::left << std::setw(12) << std::setfill(' ') << rs_2->max_cnt_1
                      << std::left << std::setw(12) << std::setfill(' ') << rs_2->max_cnt_2
                      << std::left << std::setw(12) << std::setfill(' ') << rs_2->apply_cnt
                      << std::left << std::setw(12) << std::setfill(' ') << rs_2->apply_cnt/WAIT_FOR_HITS
                      << std::left << std::setw(12) << std::setfill(' ') << rs_2->apply_cnt%WAIT_FOR_HITS
                      << std::left << std::setw(12) << std::setfill(' ') << Cnt_2
                      << std::endl;
#endif
        }

    }
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
