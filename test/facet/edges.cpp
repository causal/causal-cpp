// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include <memory>

#include "causal/core/wirks.hpp"
#include "causal/facet/edges.hpp"
#include "causal/facet/spreads.hpp"
#include "causal/process/simple.hpp"

namespace cc = causal::core;
namespace cf = causal::facet;
namespace cp = causal::process;

std::atomic<size_t> ran = false;

class test_aspect_1; class test_aspect_2;

class test_edge : public cf::ptr_edges<test_aspect_1, test_aspect_2> {
    using edges::edges;
};

class test_aspect_1 : public cf::ptr_spreads<test_edge> {
public:
    int x = 0;

    test_aspect_1() = default;
    test_aspect_1(int v) : x(v) {}
};

class test_aspect_2 : public cf::ptr_spreads<test_edge> {
public:
    int y = 0;

    test_aspect_2() = default;
    test_aspect_2(int v) : y(v) {}
};

TEST(causal_facet_edges, ctor_focus_borrow_access) {
    cc::key key;
    cf::ptr_edges<test_aspect_1, test_aspect_2> e(
        cc::forge<test_aspect_1>(1),
        cc::forge<test_aspect_2>(2)
    );

    e.focus(&key);
    EXPECT_TRUE(e.borrow(&key));
    EXPECT_EQ(e.get<0>()->x, 1);
    EXPECT_EQ(e.get<1>()->y, 2);
    e.unborrow(&key);
    e.release(&key);
}

TEST(causal_facet_edges, ctor_focus_borrow_access_fail) {
    cc::key key;
    auto some_r = cc::forge<test_aspect_2>(2);
    cf::ptr_edges<test_aspect_1, test_aspect_2> e(
        cc::forge<test_aspect_1>(1),
        some_r
    );

    {   cc::opener o(some_r);

        e.focus(&key);
        EXPECT_FALSE(e.borrow(&key));
        EXPECT_THROW(*e.get<0>(), cc::aspect_ptr_unborrowed);
        EXPECT_THROW(*e.get<1>(), cc::aspect_ptr_unborrowed);
        e.release(&key);
    }
}

TEST(causal_facet_edges, ctor_focus_const_borrow_access) {
    cc::key key;
    auto some_r = cc::aspect_ptr_cast<const test_aspect_2>(cc::forge<test_aspect_2>(2));
    cf::ptr_edges<const test_aspect_1, const test_aspect_2> e(
        cc::forge<test_aspect_1>(1),
        some_r
    );

    {   cc::opener o(some_r);

        e.focus(&key);
        EXPECT_TRUE(e.borrow(&key));
        EXPECT_EQ(e.get<0>()->x, 1);
        EXPECT_EQ(e.get<1>()->y, 2);
        e.unborrow(&key);
        e.release(&key);
    }
}

void Expand(cf::ptr_edges<test_aspect_1, test_aspect_2>& f, test_edge& e) {
    ran += f.get<0>()->x;
    ran += f.get<1>()->y;
}

TEST(causal_facet_edges, expand) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {
        auto r = cc::forge<test_edge>(
                     cc::forge<test_aspect_1>(1),
                     cc::forge<test_aspect_2>(2)
                 ); cc::opener o(r);
        b->act_fwd(r->expand(Expand, r));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 3u);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
