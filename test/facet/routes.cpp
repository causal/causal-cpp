// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include <memory>

#include "causal/core/wirks.hpp"
#include "causal/facet/routes.hpp"
#include "causal/process/simple.hpp"

namespace cc = causal::core;
namespace cf = causal::facet;
namespace cp = causal::process;

std::atomic<size_t> ran = false;

struct test_aspect : cf::ptr_routes<int, test_aspect> {
    int x = 0;

    test_aspect() = default;
    test_aspect(int v) : x(v) {}
};

TEST(causal_facet_routes, ctor_focus_borrow_access) {
    cc::key key;
    cf::ptr_routes<int, test_aspect> s({
        {1, cc::forge<test_aspect>(1)},
        {2, cc::forge<test_aspect>(2)},
        {3, cc::forge<test_aspect>(3)},
        {4, cc::forge<test_aspect>(4)},
        {5, cc::forge<test_aspect>(5)}
    });

    s.focus(&key);
    EXPECT_TRUE(s.borrow(&key));
    int sum = 0;
    for(auto& f : s)
        sum += f.second->x;
    EXPECT_EQ(sum, 15);
    s.unborrow(&key);
    s.release(&key);
}

TEST(causal_facet_routes, ctor_focus_borrow_access_fail) {
    cc::key key;
    auto some_r = cc::forge<test_aspect>(3);
    cf::ptr_routes<int, test_aspect> s({
        {1, cc::forge<test_aspect>(1)},
        {2, cc::forge<test_aspect>(2)},
        {3, some_r},
        {4, cc::forge<test_aspect>(4)},
        {5, cc::forge<test_aspect>(5)}
    });

    {   cc::opener o(some_r);

        s.focus(&key);
        EXPECT_FALSE(s.borrow(&key));
        for(auto& f : s)
            EXPECT_THROW(*f.second, cc::aspect_ptr_unborrowed);
        s.release(&key);
    }
}

TEST(causal_facet_routes, ctor_focus_const_borrow_access) {
    cc::key key;
    auto some_r = cc::aspect_ptr_cast<const test_aspect>(cc::forge<test_aspect>(3));
    cf::ptr_routes<int, const test_aspect> s({
        {1, cc::forge<test_aspect>(1)},
        {2, cc::forge<test_aspect>(2)},
        {3, some_r},
        {4, cc::forge<test_aspect>(4)},
        {5, cc::forge<test_aspect>(5)}
    });

    {   cc::opener o(some_r);

        s.focus(&key);
        EXPECT_TRUE(s.borrow(&key));
        int sum = 0;
        for(const auto& f : s)
            sum += f.second->x;
        EXPECT_EQ(sum, 15);
        s.unborrow(&key);
        s.release(&key);
    }
}

void tick(const int d, test_aspect& o, test_aspect& a) {
    ran += o.x*a.x*d;
}

void Expand(cf::ptr_routes<int, test_aspect>& fs, test_aspect& a) {
    for(auto& f : fs)
        ran += f.second->x*a.x*f.first;
}

TEST(causal_facet_routes, connect) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2));

        b->act_fwd((*r1).connect(tick, 2, r2, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());

    EXPECT_EQ(ran, 4u);
}

TEST(causal_facet_routes, connect_expand) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2));
        cc::aspect_ptr r3(cc::forge<test_aspect>(3));
        b->act_fwd((*r1).connect_expand(Expand, {{2,r2}, {3,r3}}, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_routes, connect_explode) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2));
        cc::aspect_ptr r3(cc::forge<test_aspect>(3));
        b->act_fwd((*r1).connect_explode(tick, {{2,r2}, {3,r3}}, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_routes, disconnect) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   auto r1 = cc::forge<test_aspect>(1); cc::opener o1(r1);
        auto r2 = cc::forge<test_aspect>(2);
        (*r1).insert(2, r2);
        b->act_fwd((*r1).disconnect(tick, 2, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());

    EXPECT_EQ(ran, 4u);
}

TEST(causal_facet_routes, disconnect_expand) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2));
        cc::aspect_ptr r3(cc::forge<test_aspect>(3));
        (*r1).insert(2, r2);
        (*r1).insert(3, r3);
        b->act_fwd((*r1).disconnect_expand(Expand, {2, 3}, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_routes, disconnect_explode) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2));
        cc::aspect_ptr r3(cc::forge<test_aspect>(3));
        (*r1).insert(2, r2);
        (*r1).insert(3, r3);
        b->act_fwd((*r1).disconnect_explode(tick, {2, 3}, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_routes, expand) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2));
        cc::aspect_ptr r3(cc::forge<test_aspect>(3));
        (*r1).insert(2, r2);
        (*r1).insert(3, r3);
        b->act_fwd((*r1).expand(Expand, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_routes, explode) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2));
        cc::aspect_ptr r3(cc::forge<test_aspect>(3));
        (*r1).insert(2, r2);
        (*r1).insert(3, r3);
        b->act_fwd((*r1).explode(tick, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}