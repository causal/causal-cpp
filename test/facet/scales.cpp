// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include <memory>

#include "causal/core/wirks.hpp"
#include "causal/facet/scales.hpp"
#include "causal/process/simple.hpp"

namespace cc = causal::core;
namespace cf = causal::facet;
namespace cp = causal::process;

std::atomic<size_t> ran = false;

struct test_aspect : cf::ptr_scales<test_aspect, int> {
    int x = 0;

    test_aspect() = default;
    test_aspect(int v) : x(v) {}
};

TEST(causal_facet_scales, ctor_focus_borrow_access) {
    cc::key key;
    cf::ptr_scales<test_aspect, int> s({
        {cc::forge<test_aspect>(1), 1},
        {cc::forge<test_aspect>(2), 2},
        {cc::forge<test_aspect>(3), 3},
        {cc::forge<test_aspect>(4), 4},
        {cc::forge<test_aspect>(5), 5}
    });

    s.focus(&key);
    EXPECT_TRUE(s.borrow(&key));
    int sum = 0;
    for(auto& f : s)
        sum += f.first->x;
    EXPECT_EQ(sum, 15);
    s.unborrow(&key);
    s.release(&key);
}

TEST(causal_facet_scales, ctor_focus_borrow_access_fail) {
    cc::key key;
    auto some_r = cc::forge<test_aspect>(3);
    cf::ptr_scales<test_aspect, int> s({
        {cc::forge<test_aspect>(1), 1},
        {cc::forge<test_aspect>(2), 2},
        {some_r, 3},
        {cc::forge<test_aspect>(4), 4},
        {cc::forge<test_aspect>(5), 5}
    });

    {   cc::opener o(some_r);

        s.focus(&key);
        EXPECT_FALSE(s.borrow(&key));
        for(auto& f : s)
            EXPECT_THROW(*f.first, cc::aspect_ptr_unborrowed);
        s.release(&key);
    }
}

TEST(causal_facet_scales, ctor_focus_const_borrow_access) {
    cc::key key;
    auto some_r = cc::aspect_ptr_cast<const test_aspect>(cc::forge<test_aspect>(3));
    cf::ptr_scales<const test_aspect, int> s({
        {cc::forge<test_aspect>(1), 1},
        {cc::forge<test_aspect>(2), 2},
        {some_r, 3},
        {cc::forge<test_aspect>(4), 4},
        {cc::forge<test_aspect>(5), 5}
    });

    {   cc::opener o(some_r);

        s.focus(&key);
        EXPECT_TRUE(s.borrow(&key));
        int sum = 0;
        for(auto& f : s)
            sum += f.first->x;
        EXPECT_EQ(sum, 15);
        s.unborrow(&key);
        s.release(&key);
    }
}

void tick(test_aspect& o, const int s, test_aspect& a) {
    ran += o.x*a.x*s;
}

void Expand(cf::ptr_scales<test_aspect, int>& fs, test_aspect& a) {
    for(auto& f : fs)
        ran += f.first->x*a.x*f.second;
}

TEST(causal_facet_scales, connect) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);                   
        b->act_fwd((*r1).connect(tick, r2, 2, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());

    EXPECT_EQ(ran, 4u);
}

TEST(causal_facet_scales, connect_expand) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);
        cc::aspect_ptr r3(cc::forge<test_aspect>(3)); cc::opener o3(r3);
        b->act_fwd((*r1).connect_expand(Expand, {{r2,2}, {r3,3}}, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_scales, connect_explode) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);
        cc::aspect_ptr r3(cc::forge<test_aspect>(3)); cc::opener o3(r3);
        b->act_fwd((*r1).connect_explode(tick, {{r2,2}, {r3,3}}, r1));   
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_scales, disconnect) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);
        r1->insert(r2, 2);
        b->act_fwd((*r1).disconnect(tick, r2, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());

    EXPECT_EQ(ran, 4u);
}

TEST(causal_facet_scales, disconnect_expand) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);
        cc::aspect_ptr r3(cc::forge<test_aspect>(3)); cc::opener o3(r3);
        r1->insert(r2, 2);
        r1->insert(r3, 3);
        b->act_fwd((*r1).disconnect_expand(Expand, {r2, r3}, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_scales, disconnect_explode) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);
        cc::aspect_ptr r3(cc::forge<test_aspect>(3)); cc::opener o3(r3);
        r1->insert(r2, 2);
        r1->insert(r3, 3);
        b->act_fwd((*r1).disconnect_explode(tick, {r2, r3}, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_scales, expand) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);
        cc::aspect_ptr r3(cc::forge<test_aspect>(3)); cc::opener o3(r3);
        r1->insert(r2, 2);
        r1->insert(r3, 3);
        b->act_fwd((*r1).expand(Expand, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

TEST(causal_facet_scales, explode) {
    ran = 0;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<test_aspect>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<test_aspect>(2)); cc::opener o2(r2);
        cc::aspect_ptr r3(cc::forge<test_aspect>(3)); cc::opener o3(r3);
        r1->insert(r2, 2);
        r1->insert(r3, 3);
        b->act_fwd((*r1).explode(tick, r1));
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());
    
    EXPECT_EQ(ran, 13u);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}