// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <memory>

#include <gtest/gtest.h>

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"
#include "causal/data/channel.hpp"

namespace cc = causal::core;
namespace cp = causal::process;
namespace cd = causal::data;

void Subscribe1(const std::string msg, size_t& hits) {
    if(!msg.empty())
        hits++;
}
void Subscribe2(const std::string msg, size_t& hits) {
    if(!msg.empty())
        hits++;
}
TEST(causal_data_channel, msg_channel) {
    auto d = cp::managed_dispatcher<>::make();
    auto b = cc::branch::make(d);

    auto r1 = cc::forge<size_t>(0);
    auto r2 = cc::forge<size_t>(1);

    auto s = cd::loopback_source<>::make();
    const auto c = cd::msg_channel<>::make(s, b);

    c->subscribe(Subscribe1, r1);
    c->subscribe(Subscribe2, r2);

    c->push(std::string());

    auto chk = [&](const size_t v1, const size_t v2) {
        cc::opener o1(r1), o2(r2);
        EXPECT_EQ(*r1,v1);
        EXPECT_EQ(*r2,v2);
    };

    d->join();

    chk(0,1);

    c->push("foo");
    c->push("bar");

    d->join();

    chk(2,3);
}

TEST(causal_data_channel, wrap) {
    struct Context {
        std::string session;
        std::shared_ptr<cd::source<cd::SessionMeta>> src;
        
        static void Create(const cd::SessionCreated e, Context &ctx) {
            TRACE(cd::scope_data) << "tick Session_created '" << e.session << "'";
            ctx.session = e.session;
            ctx.src = e.src;
            TRACE(cd::scope_data) << "ticked Session_created '" << e.session << "'";
        }
        static void Dispose(const cd::SessionDisposed e, const Context &ctx) {
            TRACE(cd::scope_data) << "tick Session_closed '" << e.session << "'";
            EXPECT_EQ(ctx.session, e.session);
            TRACE(cd::scope_data) << "ticked Session_closed '" << e.session << "'";
        };
    };

    const auto ctx = cc::spawn<Context>();

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    const auto w = cd::wrap::make(b);
    w->subscribe<cd::SessionCreated>(Context::Create, ctx);
    w->subscribe<cd::SessionDisposed>(Context::Dispose, ctx);

    // initializing session
    const auto s = w->forge();

    EXPECT_TRUE(d->do_tick());

    {   cc::opener o(ctx);
        EXPECT_EQ(ctx->session, s.first);
        EXPECT_EQ(ctx->src, s.second);
    }

    // TODO expand for triggering data as in keeper bouncer test

    w->trigger(cd::DisposeSession{.session=s.first});

    EXPECT_TRUE(d->do_tick());
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
