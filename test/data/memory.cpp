// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <memory>
#include <sstream>

#include <boost/functional/hash.hpp>

#include <gtest/gtest.h>

#include <string>

#include "causal/core/aspect.hpp"
#include "causal/data/memory.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/process/simple.hpp"

using namespace std::string_literals;
namespace cc = causal::core;
namespace cf = causal::facet;
namespace cd = causal::data;
namespace cp = causal::process;

struct test_other_1 {
    int x = 0;

    test_other_1() = default;
    test_other_1(int v) : x(v) {}

    MSGPACK_DEFINE_MAP(x)
};

struct test_other_2 {
    std::string x;
    
    test_other_2() = default;
    test_other_2(std::string v) : x(v) {}

    MSGPACK_DEFINE_MAP(x)
};

struct test_facing {
    std::string x;
    
    test_facing() = default;
    test_facing(std::string v) : x(v) {}

    cf::ptr_edges<test_facing, test_other_1, test_other_2> edges;
    cf::ptr_spreads<test_facing> spreads;
    cf::ptr_spreads<test_other_1> spreads_1;
    cf::ptr_spreads<test_other_2> spreads_2;
    cf::ptr_scales<test_facing, std::string> scales;
    cf::ptr_scales<test_other_1, std::string> scales_1;
    cf::ptr_scales<test_other_2, int> scales_2;
    cf::ptr_routes<std::string, test_facing> routes;
    cf::ptr_routes<std::string, test_other_1> routes_1;
    cf::ptr_routes<int, test_other_2> routes_2;

    MSGPACK_DEFINE_MAP(x, edges, spreads, spreads_1, spreads_2, scales, scales_1, scales_2, routes, routes_1, routes_2)
};

TEST(causal_data_fs, msgpack_space_merge_override) {
    auto j1 = cd::memory_junction::make();
    auto j2 = cd::memory_junction::make();
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<int, test_other_1, test_other_2>();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<int, test_other_1, test_other_2>();
    {   auto r1_1 = s1->get<int>("1"); cc::opener o1_1(r1_1);
        auto r1_2 = s2->get<int>("1"); cc::opener o1_2(r1_2);
        auto r2_1 = s1->get<test_other_1>("2"); cc::opener o2_1(r2_1);
        auto r3_1 = s1->get<test_other_2>("3"); cc::opener o3_1(r3_1);
        *r1_1 = 1;
        *r1_2 = 2;
        *r2_1 = test_other_1{2};
        *r3_1 = test_other_2{"3"};
    }

    j2->merge(*j1);

    {   auto r1_2 = s2->get<int>("1"); cc::opener o1(r1_2);
        auto r2_2 = s2->get<test_other_1>("2"); cc::opener o2(r2_2);
        auto r3_2 = s2->get<test_other_2>("3"); cc::opener o3(r3_2);
        EXPECT_EQ(*r1_2, 1);
        EXPECT_EQ(r2_2->x, 2);
        EXPECT_EQ(r3_2->x, "3");
    }
}

TEST(causal_data_fs, msgpack_space_merge_nonoverride) {
    auto j1 = cd::memory_junction::make();
    auto j2 = cd::memory_junction::make();
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<int, test_other_1, test_other_2>();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<int, test_other_1, test_other_2>();
    {   auto r1_1 = s1->get<int>("1"); cc::opener o1_1(r1_1);
        auto r1_2 = s2->get<int>("1"); cc::opener o1_2(r1_2);
        auto r2_1 = s1->get<test_other_1>("2"); cc::opener o2_1(r2_1);
        auto r3_1 = s1->get<test_other_2>("3"); cc::opener o3_1(r3_1);
        *r1_1 = 1;
        *r1_2 = 2;
        *r2_1 = test_other_1{2};
        *r3_1 = test_other_2{"3"};
    }

    j2->merge(*j1, false);

    {   auto r1_2 = s2->get<int>("1"); cc::opener o1(r1_2);
        auto r2_2 = s2->get<test_other_1>("2"); cc::opener o2(r2_2);
        auto r3_2 = s2->get<test_other_2>("3"); cc::opener o3(r3_2);
        EXPECT_EQ(*r1_2, 2);
        EXPECT_EQ(r2_2->x, 2);
        EXPECT_EQ(r3_2->x, "3");
    }
}

void round_trip_set(cd::msgpack_space& s) {
    s.register_types<test_facing, test_other_1, test_other_2>();

    auto r_other_1_1 = s.get<test_other_1>("o_1_1", 1);
    auto r_other_1_2 = s.get<test_other_1>("o_1_2", 2);
    auto r_other_2_1 = s.get<test_other_2>("o_2_1", std::string("foo"));
    auto r_other_2_2 = s.get<test_other_2>("o_2_2", std::string("bar"));
    auto r_facing_1 = s.get<test_facing>("f_1", std::string("_foo"));
    auto r_facing_2 = s.get<test_facing>("f_2", std::string("_bar"));

    {   cc::opener o_o_1_1(r_other_1_1);
        cc::opener o_o_1_2(r_other_1_2);
        cc::opener o_o_2_1(r_other_2_1);
        cc::opener o_o_2_2(r_other_2_2);
        cc::opener o_f_1(r_facing_1);
        cc::opener o_f_2(r_facing_2);

        r_other_1_1.create();
        r_other_1_2.create();

        r_other_2_1.create();
        r_other_2_2.create();

        r_facing_1.create();
        r_facing_2.create();

        r_facing_1->edges.set<0>(r_facing_1);
        r_facing_1->edges.set<1>(r_other_1_1);
        r_facing_1->edges.set<2>(r_other_2_1);

        r_facing_1->spreads.insert(r_facing_1);
        r_facing_1->spreads.insert(r_facing_2);

        r_facing_1->spreads_1.insert(r_other_1_1);
        r_facing_1->spreads_1.insert(r_other_1_2);

        r_facing_1->spreads_2.insert(r_other_2_1);
        r_facing_1->spreads_2.insert(r_other_2_2);

        r_facing_1->scales.insert(r_facing_1, "1");
        r_facing_1->scales.insert(r_facing_2, "2");

        r_facing_1->scales_1.insert(r_other_1_1, "3");
        r_facing_1->scales_1.insert(r_other_1_2, "4");

        r_facing_1->scales_2.insert(r_other_2_1, 1);
        r_facing_1->scales_2.insert(r_other_2_2, 2);

        r_facing_1->routes.insert("5", r_facing_1);
        r_facing_1->routes.insert("6", r_facing_2);

        r_facing_1->routes_1.insert("7", r_other_1_1);
        r_facing_1->routes_1.insert("8", r_other_1_2);

        r_facing_1->routes_2.insert(3, r_other_2_1);
        r_facing_1->routes_2.insert(4, r_other_2_2);
    }
}

void round_trip_check(cd::msgpack_space& s) {
    s.register_types<test_facing, test_other_1, test_other_2>();
    
    auto r_other_1_1 = s.get<test_other_1>("o_1_1");
    auto r_other_1_2 = s.get<test_other_1>("o_1_2");
    auto r_other_2_1 = s.get<test_other_2>("o_2_1");
    auto r_other_2_2 = s.get<test_other_2>("o_2_2");
    auto r_facing_1 = s.get<test_facing>("f_1");
    auto r_facing_2 = s.get<test_facing>("f_2");

    {   cc::opener o_o_1_1(r_other_1_1);
        cc::opener o_o_1_2(r_other_1_2);
        cc::opener o_o_2_1(r_other_2_1);
        cc::opener o_o_2_2(r_other_2_2);
        cc::opener o_f_1(r_facing_1);
        cc::opener o_f_2(r_facing_2);

        EXPECT_EQ(r_other_1_1->x, 1);
        EXPECT_EQ(r_other_1_2->x, 2);

        EXPECT_EQ(r_other_2_1->x, "foo");
        EXPECT_EQ(r_other_2_2->x, "bar");

        EXPECT_EQ(r_facing_1->x, "_foo");
        EXPECT_EQ(r_facing_2->x, "_bar");

        EXPECT_EQ(r_facing_1->edges.get<0>(), r_facing_1);
        EXPECT_EQ(r_facing_1->edges.get<1>(), r_other_1_1);
        EXPECT_EQ(r_facing_1->edges.get<2>(), r_other_2_1);

        EXPECT_TRUE(r_facing_1->spreads.contains(r_facing_1));
        EXPECT_TRUE(r_facing_1->spreads.contains(r_facing_2));

        EXPECT_TRUE(r_facing_1->spreads_1.contains(r_other_1_1));
        EXPECT_TRUE(r_facing_1->spreads_1.contains(r_other_1_2));

        EXPECT_TRUE(r_facing_1->spreads_2.contains(r_other_2_1));
        EXPECT_TRUE(r_facing_1->spreads_2.contains(r_other_2_2));

        EXPECT_EQ(r_facing_1->scales[r_facing_1], "1");
        EXPECT_EQ(r_facing_1->scales[r_facing_2], "2");

        EXPECT_EQ(r_facing_1->scales_1[r_other_1_1], "3");
        EXPECT_EQ(r_facing_1->scales_1[r_other_1_2], "4");

        EXPECT_EQ(r_facing_1->scales_2[r_other_2_1], 1);
        EXPECT_EQ(r_facing_1->scales_2[r_other_2_2], 2);

        EXPECT_EQ(r_facing_1->routes["5"s], r_facing_1);
        EXPECT_EQ(r_facing_1->routes["6"s], r_facing_2);

        EXPECT_EQ(r_facing_1->routes_1["7"s], r_other_1_1);
        EXPECT_EQ(r_facing_1->routes_1["8"s], r_other_1_2);

        EXPECT_EQ(r_facing_1->routes_2[3], r_other_2_1);
        EXPECT_EQ(r_facing_1->routes_2[4], r_other_2_2);
    }
}

struct foo_id {
    short x = 0, y = 0;
};

bool operator<(const foo_id& lhs, const foo_id& rhs) {
    return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
}

bool operator==(const foo_id& lhs, const foo_id& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

namespace std
{
    template<> struct hash<foo_id>
    {
        std::size_t operator()(const foo_id& p) const noexcept
        {
            size_t seed = 0;
            boost::hash_combine(seed, p.x);
            boost::hash_combine(seed, p.y);

            return seed;
        }
    };
}

struct test_base {
    virtual ~test_base() = default;
    virtual int get() const = 0;
    virtual void set(int v) = 0;
};

struct test_derived1 : cc::interfaces<test_base> {
    int x;
    int get() const override {
        return this->x + 2;
    }

    void set(int v) override {
        this->x = v;
    }

    test_derived1(int v) : x(v) {}
};

struct test_derived2 : cc::interfaces<test_base> {
    const int x;
    int get() const override {
        return this->x + 3;
    }

    test_derived2(int v) : x(v) {}
};

TEST(causal_data_memory, memory_space) {
    auto s = cd::memory_space<foo_id>::make();
    
    cc::essence_ptr<> pin1, pin2, pin3;
    {   auto r1 = s->template get<std::string>({1,2}, std::string("1")); cc::opener o1(r1);
        auto r2 = s->template get<size_t>({0,1}, 2); cc::opener o2(r2);
        auto r3 = s->template get<long>({-1,-2}, -3); cc::opener o3(r3);
        EXPECT_TRUE(!r1.is_certain());
        EXPECT_TRUE(!r2.is_certain());
        EXPECT_TRUE(!r3.is_certain());

        pin1 = r1.get_ptr()->as_untyped();
        pin2 = r2.get_ptr()->as_untyped();
        pin3 = r3.get_ptr()->as_untyped();
    }

    {   auto r1 = s->template get<std::string>({1,2}); cc::opener o1(r1);
        auto r2 = s->template get<size_t>({0,1}); cc::opener o2(r2);
        auto r3 = s->template get<long>({-1,-2}); cc::opener o3(r3);

        EXPECT_TRUE(!r1.is_certain());
        EXPECT_TRUE(!r2.is_certain());
        EXPECT_TRUE(!r3.is_certain());

        EXPECT_EQ(*r1, "1");
        EXPECT_EQ(*r2, 2u);
        EXPECT_EQ(*r3, -3);
    }
}

TEST(causal_data_memory, aspect_clone_uncertain) {
    auto s1 = cd::memory_space<foo_id>::make();
    auto s2 = cd::memory_space<foo_id>::make();
    cc::aspect_ptr<test_base> r = s1->template get<test_derived1>({1,1}, 5);

    EXPECT_THROW(s2->clone({1,2}, r), cc::aspect_ptr_unborrowed);
    cc::opener o1(r);
    EXPECT_FALSE(r.is_certain());

    {   EXPECT_TRUE(r.is_clonable());
        auto t = s2->clone({1,2}, r);
        auto c = s2->template get<test_base>({1,2});
        cc::opener o2(c);
        EXPECT_FALSE(c.is_certain());

        EXPECT_EQ(r->get(), 7);
        EXPECT_EQ(c->get(), 7);
        EXPECT_NE(&(*r), &(*c));
        r.dispose();
    }

    {   EXPECT_THROW(cc::aspect_ptr_cast<test_derived2>(s2->clone({1,3}, r)), cc::incompatible_aspect_ptr_cast);
        auto c = cc::aspect_ptr_cast<test_derived1>(s2->clone({1,3}, r));
        cc::opener o2(c);
        EXPECT_FALSE(c.is_certain());

        EXPECT_EQ(c->x, 5);
    }
}

TEST(causal_data_memory, aspect_clone_certain) {
    auto s1 = cd::memory_space<foo_id>::make();
    auto s2 = cd::memory_space<foo_id>::make();
    cc::aspect_ptr<test_base> r = s1->template take<test_derived1>({1,1}, 5);

    cc::opener o1(r);
    EXPECT_TRUE(r.is_certain());
    EXPECT_EQ(r->get(), 7);
    r->set(7);

    {   EXPECT_TRUE(r.is_clonable());
        auto t = s2->clone({1,2}, r);
        auto c = s2->template get<test_base>({1,2});
        cc::opener o2(c);
        EXPECT_TRUE(c.is_certain());

        EXPECT_EQ(r->get(), 9);
        EXPECT_EQ(c->get(), 9);
        EXPECT_NE(&(*r), &(*c));
    }

    {   EXPECT_THROW(cc::aspect_ptr_cast<test_derived2>(s2->clone({1,3}, r)), cc::incompatible_aspect_ptr_cast);
        auto c = cc::aspect_ptr_cast<test_derived1>(s2->clone({1,3}, r));
        cc::opener o2(c);
        EXPECT_TRUE(c.is_certain());

        EXPECT_EQ(c->x, 7);
    }
}

void Test_explode_1([[maybe_unused]]const std::string id, [[maybe_unused]]cc::aspect_ptr<size_t>& v, size_t& cnt) {
    cnt++;
}
void Test_explode_2([[maybe_unused]]const std::string id, [[maybe_unused]]cc::aspect_ptr<std::string>& v, size_t& cnt) {
    cnt++;
}
TEST(causal_data_msgpack_space, explode) {
    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    auto s = cd::memory_space<>::make();

    auto r_other_1_1 = s->take<size_t>("o_1_1");
    auto r_other_1_2 = s->take<size_t>("o_1_2");
    auto r_other_1_3 = s->take<size_t>("o_1_3");
    auto r_other_2_1 = s->take<std::string>("o_2_1");
    auto r_other_2_2 = s->take<std::string>("o_2_2");

    auto r_cnt_1 = cc::spawn<size_t>(0);
    auto r_cnt_2 = cc::spawn<size_t>(0);

    b->act_fwd(s->explode(Test_explode_1, r_cnt_1));
    b->act_fwd(s->explode(Test_explode_2, r_cnt_2));

    for(size_t i = 0; i < 5; i++)
        EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());

    {   cc::opener o_cnt_1(r_cnt_1);
        cc::opener o_cnt_2(r_cnt_2);

        EXPECT_EQ(*r_cnt_1, 3u);
        EXPECT_EQ(*r_cnt_2, 2u);
    }
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
