// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <chrono>
#include <cstdlib>
#include <gtest/gtest.h>
#include <memory>
#include <string>
#include <filesystem>
#include <thread>

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/data/channel.hpp"
#include "causal/data/fs.hpp"
#include "causal/data/httpd.hpp"
#include "causal/data/json.hpp"
#include "causal/data/memory.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"
#include "causal/data/keeper.hpp"
#include "causal/trace.hpp"

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "causal/utility.hpp"

#include <nlohmann/json.hpp>
#include <boost/asio.hpp>
#include <boost/process.hpp>

namespace cc = causal::core;
namespace cd = causal::data;
namespace cp = causal::process;
namespace fs = std::filesystem;

TEST(causal_data_httpd, httpd_gate_expose_junction) {
    const std::string index("<html><head><title>INDEX</title></head><body>index</body><html>");
    const std::string binary({-128,0,127,0,-128});

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    const auto j = cd::memory_junction::make();
    const auto g = cd::httpd_gate::make(b, cd::httpd_config("127.0.0.1", 50080), j);

    j->set("index.html", index);
    j->set("data/binary", binary);

    const std::string url_base("http://127.0.0.1:50080");
    utility::http_client cli;
    
    const auto res1 = cli.get(url_base + "/index.html");
    EXPECT_EQ(res1.status_code, 200);
    EXPECT_TRUE(res1.headers.find("Content-Type") != res1.headers.end());
    EXPECT_EQ(res1.headers.at("Content-Type"), "text/html");
    EXPECT_EQ(res1.data, index);

    const auto res_login_req = cli.get(url_base + "/");
    EXPECT_EQ(res_login_req.status_code, 200);
    EXPECT_TRUE(res_login_req.headers.find("Content-Type") != res_login_req.headers.end());
    EXPECT_EQ(res_login_req.headers.at("Content-Type"), "text/html");
    EXPECT_EQ(res_login_req.data, index);

    const auto res_login_resp = cli.get(url_base + "/data/binary");
    EXPECT_EQ(res_login_resp.status_code, 200);
    EXPECT_TRUE(res_login_resp.headers.find("Content-Type") != res_login_resp.headers.end());
    EXPECT_EQ(res_login_resp.headers.at("Content-Type"), "application/octet-stream");
    EXPECT_EQ(res_login_resp.data, binary);

    const auto res4 = cli.get(url_base + "/foo");
    EXPECT_EQ(res4.status_code, 404);
    EXPECT_EQ(res4.data, "");
}

TEST(causal_data_httpd, httpd_gate_manage_sess) {
    struct Context {
        std::string session;
        
        static void Create(cd::SessionCreated e, Context &ctx) {
            TRACE(cd::scope_httpd) << "tick Session_created '" << e.session << "'";
            ctx.session = e.session;
            TRACE(cd::scope_httpd) << "ticked Session_created '" << e.session << "'";
        }
        static void Dispose(cd::SessionDisposed e, const Context &ctx) {
            TRACE(cd::scope_httpd) << "tick Session_closed '" << e.session << "'";
            EXPECT_EQ(ctx.session, e.session);
            TRACE(cd::scope_httpd) << "ticked Session_closed '" << e.session << "'";
        };
    };

    const auto ctx = cc::spawn<Context>();

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    const auto g = cd::httpd_gate::make(b, cd::httpd_config("127.0.0.1", 50080));
    g->subscribe<cd::SessionCreated>(Context::Create, ctx);
    g->subscribe<cd::SessionDisposed>(Context::Dispose, ctx);

    const std::string url_base("http://127.0.0.1:50080");
    utility::http_client cli;

    // initializing session
    const auto res1 = cli.get(url_base + "/_");
    EXPECT_EQ(res1.status_code, 200);
    EXPECT_TRUE(res1.headers.find("Content-Type") != res1.headers.end());
    EXPECT_EQ(res1.headers.at("Content-Type"), "application/json");
    const auto session = nlohmann::json::parse(res1.data)["session"].get<std::string>();

    EXPECT_TRUE(d->do_tick());

    {   cc::opener o(ctx);
        EXPECT_EQ(ctx->session, session);
    }

    const auto res_login_req = cli.del(url_base + "/_"+session);
    EXPECT_EQ(res_login_req.status_code, 200);
    EXPECT_TRUE(res_login_req.headers.find("Content-Type") != res_login_req.headers.end());
    EXPECT_EQ(res_login_req.headers.at("Content-Type"), "application/json");

    EXPECT_TRUE(d->do_tick());
}

namespace bp = boost::process;
bool run_test() {
    bp::child c(
        "../test/data/httpd/webticks/test.sh causal_test",
        bp::std_out > stdout,
        bp::std_err > stderr
    );

    c.join();

    return c.exit_code() == EXIT_SUCCESS;
}

struct TestRequest {
    std::string msg;

    MSGPACK_DEFINE_MAP(msg)
};

struct TestResponse {
    std::string msg;

    MSGPACK_DEFINE_MAP(msg)
};

TEST(causal_data_httpd, bouncer_login) {
    if(!fs::exists("../test/data/httpd/webticks/test.sh"))
        return;

    using httpd_bouncer = cd::bouncer<cd::json_channel<cd::SessionMeta>, cd::httpd_gate, cd::memory_space<std::string>>;
    struct Context {
        std::string session;
        std::string id;
        std::string sign;
        bool left = false;
        std::shared_ptr<cd::source<cd::IdSessionMeta>> src;
        std::shared_ptr<cd::json_channel<cd::IdSessionMeta>> chan;

        static void OnTestRequest(const TestRequest e, Context &ctx) {
            TRACE(cd::scope_data) << "tick OnTestRequest '" << e.msg << "'";
            ctx.chan->push(TestResponse{.msg = e.msg});
        }
        
        static void OnIdentified(const cd::Identified e, cc::aspect_ptr<Context> &ctx) {
            TRACE(cd::scope_data) << "tick OnIdentified '" << e.id << "' with '" << e.sign << "' for '" << e.session << "' on '" << e.peer << "'";
            ctx->session = e.session;
            ctx->src = e.src;
            ctx->chan = cd::json_channel<cd::IdSessionMeta>::make(ctx->src, cc::branch::current());
            ctx->chan->subscribe(OnTestRequest, ctx);
            TRACE(cd::scope_data) << "ticked OnIdentified '" << e.session << "'";
        }
        static void OnLeft(const cd::Left e, const Context &ctx) {
            TRACE(cd::scope_data) << "tick OnLeft '" << e.id << "' from '" << e.session << "' on '" << e.peer << "'";
            EXPECT_EQ(ctx.session, e.session);
            TRACE(cd::scope_data) << "ticked OnLeft '" << e.session << "'";
        };
    };

    const auto ctx = cc::spawn<Context>();
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    const auto wt_j = cd::fs_junction::make("../test/data/httpd/webticks");
    const auto g = cd::httpd_gate::make(b, cd::httpd_config("127.0.0.1", 58000), wt_j);
    const auto bspc = causal::data::memory_space<>::make();
    const auto k = httpd_bouncer::make(b, bspc);
    k->subscribe<cd::Identified>(Context::OnIdentified, ctx);
    k->subscribe<cd::Left>(Context::OnLeft, ctx);    k->subscribe<cd::Left>(Context::OnLeft, ctx);
    k->reg("testuser", "testpassword");
    TRACE(cd::scope_httpd) << typeid(TestRequest).name();
    k->attach(g);

    EXPECT_TRUE(run_test());

    d->join();
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}