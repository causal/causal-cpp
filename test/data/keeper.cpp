// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <cstddef>
#include <memory>

#include <gtest/gtest.h>

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/data/keeper.hpp"
#include "causal/data/memory.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/process/simple.hpp"
#include "causal/data/channel.hpp"
#include "causal/trace.hpp"

namespace cc = causal::core;
namespace cp = causal::process;
namespace cd = causal::data;

TEST(causal_data_keeper, bouncer_login) {
    using wrap_bouncer = cd::bouncer<cd::msgpack_channel<cd::SessionMeta>, cd::wrap, cd::memory_space<std::string>>;
    struct Context {
        std::string session;
        std::string id;
        std::string sign;
        bool left = false;
        std::shared_ptr<cd::source<cd::IdSessionMeta>> src;

        static void OnIdentifyResponse(const cd::IdentifyResponse r, Context &ctx) {
            TRACE(cd::scope_data) << "tick OnIdentifyResponse '" << r.id << "' with '" << r.sign << "' for '" << r.session << "'";
            EXPECT_EQ(ctx.session, r.session);
            ctx.id = r.id; ctx.sign = r.sign;
            TRACE(cd::scope_data) << "ticked OnIdentifyResponse '" << r.session << "'";
        }

        static void OnLeaveResponse(const cd::LeaveResponse r, Context &ctx) {
            TRACE(cd::scope_data) << "tick OnLeaveResponse '" << r.id << "' for '" << r.session << "'";
            EXPECT_EQ(ctx.session, r.session);
            EXPECT_EQ(ctx.id, r.id);
            ctx.left = true;
            TRACE(cd::scope_data) << "ticked OnLeaveResponse '" << r.session << "'";
        }
        
        static void OnIdentified(const cd::Identified e, Context &ctx) {
            TRACE(cd::scope_data) << "tick OnIdentified '" << e.id << "' with '" << e.sign << "' for '" << e.session << "' on '" << e.peer << "'";
            ctx.session = e.session;
            ctx.src = e.src;
            TRACE(cd::scope_data) << "ticked OnIdentified '" << e.session << "'";
        }
        static void OnLeft(const cd::Left e, const Context &ctx) {
            TRACE(cd::scope_data) << "tick OnLeft '" << e.id << "' from '" << e.session << "' on '" << e.peer << "'";
            EXPECT_EQ(ctx.session, e.session);
            TRACE(cd::scope_data) << "ticked OnLeft '" << e.session << "'";
        };
    };

    const auto ctx = cc::spawn<Context>();
    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    const auto g = cd::wrap::make(b);
    const auto bspc = causal::data::memory_space<>::make();
    const auto k = wrap_bouncer::make(b, bspc);
    k->subscribe<cd::Identified>(Context::OnIdentified, ctx);
    k->subscribe<cd::Left>(Context::OnLeft, ctx);
    k->reg("testuser", "testpassword");

    k->attach(g);

    // initializing session
    const auto s = g->forge();

    while(d->do_tick()) {};

    const auto c = cd::msgpack_channel<cd::SessionMeta>::make(s.second, b);
    c->subscribe<cd::IdentifyResponse>(Context::OnIdentifyResponse, ctx);
    c->subscribe<cd::LeaveResponse>(Context::OnLeaveResponse, ctx);

    c->push(cd::IdentifyRequest{.session=s.first, .id="testuser", .phrase="testpassword"});

    while(d->do_tick()) {};

    {   cc::opener o(ctx);
        EXPECT_EQ(ctx->id, "testuser");
        EXPECT_FALSE(ctx->sign.empty());
    }

    c->push(cd::LeaveRequest{.session=s.first, .id="testuser"});

    while(d->do_tick()) {};

    {   cc::opener o(ctx);
        EXPECT_TRUE(ctx->left);
    }

    k->dereg("testuser");

    g->trigger(cd::DisposeSession{.session=s.first});

    while(d->do_tick()) {};
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}