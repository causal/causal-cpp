// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <memory>

#include <gtest/gtest.h>

//#define VERBOSE

#include <map>
#include <string>
#ifdef VERBOSE
#include <iostream>
#endif

#include "causal/data/junction.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/facet/common.hpp"
#include "causal/facet/spreads.hpp"
#include "causal/facet/scales.hpp"
#include "causal/facet/routes.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"

using namespace std::string_literals;
namespace cc = causal::core;
namespace cp = causal::process;
namespace cf = causal::facet;
namespace cd = causal::data;

struct test_base {
    virtual ~test_base() = default;
    virtual std::string get() const = 0;
    virtual void set_x(std::string v) = 0;
};

struct test_other_1 : cc::interfaces<test_base> {
    int x = 0;
    
    test_other_1() = default;
    test_other_1(int v) : x(v) {}

    virtual std::string get() const override {return std::to_string(this->x);}
    virtual void set_x(std::string v) override { this->x = stoi(v); }

    MSGPACK_DEFINE_MAP(x)
};

struct test_other_2 : cc::interfaces<test_base> {
    std::string x;
    
    test_other_2() = default;
    test_other_2(std::string v) : x(v) {}

    virtual std::string get() const override {return this->x;}
    virtual void set_x(std::string v) override { this->x = v; }

    MSGPACK_DEFINE_MAP(x)
};

struct test_facing {
    std::string x;
    
    test_facing() = default;
    test_facing(std::string v) : x(v) {}

    cf::ptr_edges<test_facing, test_other_1, test_other_2, test_base, test_base> edges;
    cf::ptr_spreads<test_facing> spreads;
    cf::ptr_spreads<test_other_1> spreads_1;
    cf::ptr_spreads<test_other_2> spreads_2;
    cf::ptr_spreads<test_base> spreads_3;
    cf::ptr_scales<test_facing, std::string> scales;
    cf::ptr_scales<test_other_1, std::string> scales_1;
    cf::ptr_scales<test_other_2, int> scales_2;
    cf::ptr_scales<test_base, std::string> scales_3;
    cf::ptr_routes<std::string, test_facing> routes;
    cf::ptr_routes<std::string, test_other_1> routes_1;
    cf::ptr_routes<int, test_other_2> routes_2;
    cf::ptr_routes<std::string, test_base> routes_3;

    MSGPACK_DEFINE_MAP(x, edges, spreads, spreads_1, spreads_2, spreads_3, scales, scales_1, scales_2, scales_3, routes, routes_1, routes_2, routes_3)
};

TEST(causal_data_msgpack_space, merge_override) {
    auto j1 = cd::map_junction::make();
    auto j2 = cd::map_junction::make();
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<int, test_other_1, test_other_2>();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<int, test_other_1, test_other_2>();
    {   auto r1_1 = s1->get<int>(1); cc::opener o1_1(r1_1);
        auto r1_2 = s2->get<int>(1); cc::opener o1_2(r1_2);
        auto r2_1 = s1->get<test_other_1>(2); cc::opener o2_1(r2_1);
        auto r3_1 = s1->get<test_other_2>(3); cc::opener o3_1(r3_1);
        *r1_1 = 1;
        *r1_2 = 2;
        *r2_1 = test_other_1{2};
        *r3_1 = test_other_2{"3"};
    }

    j2->merge(*j1);

    {   auto r1_2 = s2->get<int>(1); cc::opener o1(r1_2);
        auto r2_2 = s2->get<test_other_1>(2); cc::opener o2(r2_2);
        auto r3_2 = s2->get<test_other_2>(3); cc::opener o3(r3_2);
        EXPECT_EQ(*r1_2, 1);
        EXPECT_EQ(r2_2->x, 2);
        EXPECT_EQ(r3_2->x, "3");
    }

    {   auto r4_2 = s2->get<test_base>(2); cc::opener o4(r4_2);
        auto r5_2 = s2->get<test_base>(3); cc::opener o5(r5_2);
        EXPECT_EQ(r4_2->get(), "2");
        EXPECT_EQ(r5_2->get(), "3");
    }
}

TEST(causal_data_msgpack_space, merge_nonoverride) {
    auto j1 = cd::map_junction::make();
    auto j2 = cd::map_junction::make();
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<int, test_other_1, test_other_2>();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<int, test_other_1, test_other_2>();
    {   auto r1_1 = s1->get<int>(1); cc::opener o1_1(r1_1);
        auto r1_2 = s2->get<int>(1); cc::opener o1_2(r1_2);
        auto r2_1 = s1->get<test_other_1>(2); cc::opener o2_1(r2_1);
        auto r3_1 = s1->get<test_other_2>(3); cc::opener o3_1(r3_1);
        *r1_1 = 1;
        *r1_2 = 2;
        *r2_1 = test_other_1{2};
        *r3_1 = test_other_2{"3"};
    }

    j2->merge(*j1, false);

    {   auto r1_2 = s2->get<int>(1); cc::opener o1(r1_2);
        auto r2_2 = s2->get<test_other_1>(2); cc::opener o2(r2_2);
        auto r3_2 = s2->get<test_other_2>(3); cc::opener o3(r3_2);
        EXPECT_EQ(*r1_2, 2);
        EXPECT_EQ(r2_2->x, 2);
        EXPECT_EQ(r3_2->x, "3");
    }
}

TEST(causal_data_msgpack_space, basic) {
    auto j = cd::map_junction::make();
    auto s = cd::msgpack_space::make(j);

    EXPECT_NO_THROW(s->get<int>("1"));

    EXPECT_NO_THROW(s->get<test_other_1>(2));

    EXPECT_EQ(s->get<test_other_2>(3u).get_id_as<std::string>(), "3");

    EXPECT_NO_THROW(s->get<test_other_1>(3u));

    auto bound = s->get<test_other_2>(3u);

    EXPECT_FALSE(s->get<test_other_1>(3u));
}

TEST(causal_data_msgpack_space, aspect_clone_uncertain) {
    auto j1 = cd::map_junction::make();
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<test_other_1, test_other_2>();
    auto j2 = cd::map_junction::make();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<test_other_1, test_other_2>();
    cc::aspect_ptr<test_base> r = s1->template get<test_other_1>(1, 5);

    EXPECT_THROW(s2->clone(2, r), cc::aspect_ptr_unborrowed);
    cc::opener o1(r);
    EXPECT_FALSE(r.is_certain());

    {   EXPECT_TRUE(r.is_clonable());
        auto c = s2->clone(2, r);
        cc::opener o2(c);
        EXPECT_FALSE(c.is_certain());

        EXPECT_EQ(r->get(), "5");
        EXPECT_EQ(c->get(), "5");
        EXPECT_NE(&(*r), &(*c));
        r.dispose();
    }

    {   EXPECT_THROW(cc::aspect_ptr_cast<test_other_2>(s2->clone(3, r)), cc::incompatible_aspect_ptr_cast);
        auto c = cc::aspect_ptr_cast<test_other_1>(s2->clone(3, r));
        cc::opener o2(c);
        EXPECT_FALSE(c.is_certain());

        EXPECT_EQ(c->x, 5);
    }
}

TEST(causal_data_msgpack_space, aspect_clone_certain) {
    auto j1 = cd::map_junction::make();
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<test_other_1, test_other_2>();
    auto j2 = cd::map_junction::make();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<test_other_1, test_other_2>();
    cc::aspect_ptr<test_base> r = s1->template take<test_other_1>(1, 5);

    cc::opener o1(r);
    EXPECT_TRUE(r.is_certain());
    EXPECT_EQ(r->get(), "5");
    r->set_x("7");

    {   EXPECT_TRUE(r.is_clonable());
        s2->clone(2, r);
        auto c = s2->template get<test_base>(2);
        cc::opener o2(c);
        EXPECT_TRUE(c.is_certain());

        EXPECT_EQ(r->get(), "7");
        EXPECT_EQ(c->get(), "7");
        EXPECT_NE(&(*r), &(*c));
    }

    {   EXPECT_THROW(cc::aspect_ptr_cast<test_other_2>(s2->clone(3, r)), cc::incompatible_aspect_ptr_cast);
        auto c = cc::aspect_ptr_cast<test_other_1>(s2->clone(3, r));
        cc::opener o2(c);
        EXPECT_TRUE(c.is_certain());

        EXPECT_EQ(c->x, 7);
    }
}

void round_trip_set(cd::msgpack_space& s) {
    s.register_types<test_facing, test_other_1, test_other_2>();

    auto r_other_1_1 = s.get<test_other_1>("o_1_1", 1);
    auto r_other_1_2 = s.get<test_other_1>("o_1_2", 2);
    auto r_other_1_3 = s.get<test_other_1>("o_1_3", 3);
    auto r_other_2_1 = s.get<test_other_2>("o_2_1", std::string("foo"));
    auto r_other_2_2 = s.get<test_other_2>("o_2_2", std::string("bar"));
    auto r_other_2_3 = s.get<test_other_2>("o_2_3", std::string("baz"));
    auto r_facing_1 = s.get<test_facing>("f_1", std::string("_foo"));
    auto r_facing_2 = s.get<test_facing>("f_2", std::string("_bar"));

    {   cc::opener o_o_1_1(r_other_1_1);
        cc::opener o_o_1_2(r_other_1_2);
        cc::opener o_o_1_3(r_other_1_3);
        cc::opener o_o_2_1(r_other_2_1);
        cc::opener o_o_2_2(r_other_2_2);
        cc::opener o_o_2_3(r_other_2_3);
        cc::opener o_f_1(r_facing_1);
        cc::opener o_f_2(r_facing_2);

        r_other_1_1.create();
        r_other_1_2.create();
        r_other_1_3.create();

        r_other_2_1.create();
        r_other_2_2.create();
        r_other_2_3.create();

        r_facing_1.create();
        r_facing_2.create();

        r_facing_1->edges.set<0>(r_facing_1);
        r_facing_1->edges.set<1>(r_other_1_1);
        r_facing_1->edges.set<2>(r_other_2_1);
        r_facing_1->edges.set<3>(r_other_1_1);
        r_facing_1->edges.set<4>(r_other_2_1);

        r_facing_1->spreads.insert(r_facing_1);
        r_facing_1->spreads.insert(r_facing_2);

        r_facing_1->spreads_1.insert(r_other_1_1);
        r_facing_1->spreads_1.insert(r_other_1_2);

        r_facing_1->spreads_2.insert(r_other_2_1);
        r_facing_1->spreads_2.insert(r_other_2_2);

        r_facing_1->spreads_3.insert(r_other_1_1);
        r_facing_1->spreads_3.insert(r_other_1_2);
        r_facing_1->spreads_3.insert(r_other_2_1);
        r_facing_1->spreads_3.insert(r_other_2_2);

        r_facing_1->scales.insert(r_facing_1, "1");
        r_facing_1->scales.insert(r_facing_2, "2");

        r_facing_1->scales_1.insert(r_other_1_1, "3");
        r_facing_1->scales_1.insert(r_other_1_2, "4");

        r_facing_1->scales_2.insert(r_other_2_1, 1);
        r_facing_1->scales_2.insert(r_other_2_2, 2);

        r_facing_1->scales_3.insert(r_other_1_1, "3");
        r_facing_1->scales_3.insert(r_other_1_2, "4");
        r_facing_1->scales_3.insert(r_other_2_1, "1");
        r_facing_1->scales_3.insert(r_other_2_2, "2");

        r_facing_1->routes.insert("5", r_facing_1);
        r_facing_1->routes.insert("6", r_facing_2);

        r_facing_1->routes_1.insert("7", r_other_1_1);
        r_facing_1->routes_1.insert("8", r_other_1_2);

        r_facing_1->routes_2.insert(3, r_other_2_1);
        r_facing_1->routes_2.insert(4, r_other_2_2);

        r_facing_1->routes_3.insert("7", r_other_1_1);
        r_facing_1->routes_3.insert("8", r_other_1_2);
        r_facing_1->routes_3.insert("3", r_other_2_1);
        r_facing_1->routes_3.insert("4", r_other_2_2);
    }
}

void round_trip_check(cd::msgpack_space& s) {
    auto r_other_1_1 = s.get<test_other_1>("o_1_1");
    auto r_other_1_2 = s.get<test_other_1>("o_1_2");
    auto r_other_1_3 = s.get<test_base>("o_1_3");
    auto r_other_2_1 = s.get<test_other_2>("o_2_1");
    auto r_other_2_2 = s.get<test_other_2>("o_2_2");
    auto r_other_2_3 = s.get<test_base>("o_2_3");
    auto r_facing_1 = s.get<test_facing>("f_1");
    auto r_facing_2 = s.get<test_facing>("f_2");

    {   cc::opener o_o_1_1(r_other_1_1);
        cc::opener o_o_1_2(r_other_1_2);
        cc::opener o_o_2_1(r_other_2_1);
        cc::opener o_o_2_2(r_other_2_2);
        cc::opener o_f_1(r_facing_1);
        cc::opener o_f_2(r_facing_2);

        EXPECT_EQ(r_other_1_1->x, 1);
        EXPECT_EQ(r_other_1_2->x, 2);

        EXPECT_EQ(r_other_2_1->x, "foo");
        EXPECT_EQ(r_other_2_2->x, "bar");

        EXPECT_EQ(r_facing_1->x, "_foo");
        EXPECT_EQ(r_facing_2->x, "_bar");

        EXPECT_EQ(r_facing_1->edges.get<0>(), r_facing_1);
        EXPECT_EQ(r_facing_1->edges.get<1>(), r_other_1_1);
        EXPECT_EQ(r_facing_1->edges.get<2>(), r_other_2_1);
        EXPECT_EQ(r_facing_1->edges.get<3>(), r_other_1_1);
        EXPECT_EQ(r_facing_1->edges.get<4>(), r_other_2_1);

        EXPECT_TRUE(r_facing_1->spreads.contains(r_facing_1));
        EXPECT_TRUE(r_facing_1->spreads.contains(r_facing_2));

        EXPECT_TRUE(r_facing_1->spreads_1.contains(r_other_1_1));
        EXPECT_TRUE(r_facing_1->spreads_1.contains(r_other_1_2));

        EXPECT_TRUE(r_facing_1->spreads_2.contains(r_other_2_1));
        EXPECT_TRUE(r_facing_1->spreads_2.contains(r_other_2_2));

        EXPECT_TRUE(r_facing_1->spreads_3.contains(r_other_1_1));
        EXPECT_TRUE(r_facing_1->spreads_3.contains(r_other_1_2));
        EXPECT_TRUE(r_facing_1->spreads_3.contains(r_other_2_1));
        EXPECT_TRUE(r_facing_1->spreads_3.contains(r_other_2_2));

        EXPECT_EQ(r_facing_1->scales[r_facing_1], "1");
        EXPECT_EQ(r_facing_1->scales[r_facing_2], "2");

        EXPECT_EQ(r_facing_1->scales_1[r_other_1_1], "3");
        EXPECT_EQ(r_facing_1->scales_1[r_other_1_2], "4");

        EXPECT_EQ(r_facing_1->scales_2[r_other_2_1], 1);
        EXPECT_EQ(r_facing_1->scales_2[r_other_2_2], 2);

        EXPECT_EQ(r_facing_1->scales_3[r_other_1_1], "3");
        EXPECT_EQ(r_facing_1->scales_3[r_other_1_2], "4");
        EXPECT_EQ(r_facing_1->scales_3[r_other_2_1], "1");
        EXPECT_EQ(r_facing_1->scales_3[r_other_2_2], "2");
        
        EXPECT_EQ(r_facing_1->routes["5"s], r_facing_1);
        EXPECT_EQ(r_facing_1->routes["6"s], r_facing_2);

        EXPECT_EQ(r_facing_1->routes_1["7"s], r_other_1_1);
        EXPECT_EQ(r_facing_1->routes_1["8"s], r_other_1_2);

        EXPECT_EQ(r_facing_1->routes_2[3], r_other_2_1);
        EXPECT_EQ(r_facing_1->routes_2[4], r_other_2_2);

        EXPECT_EQ(r_facing_1->routes_3["7"s], r_other_1_1);
        EXPECT_EQ(r_facing_1->routes_3["8"s], r_other_1_2);
        EXPECT_EQ(r_facing_1->routes_3["3"s], r_other_2_1);
        EXPECT_EQ(r_facing_1->routes_3["4"s], r_other_2_2);
    }

    {   cc::opener o_o_1_3(r_other_1_3);
        cc::opener o_o_2_3(r_other_2_3);

        EXPECT_EQ(r_other_1_3->get(), "3");
        EXPECT_EQ(r_other_2_3->get(), "baz");
    }
}

TEST(causal_data_msgpack_space, round_trip) {
    auto j = cd::map_junction::make();
    auto s = cd::msgpack_space::make(j);

    round_trip_set(*s);
    round_trip_check(*s);
}

void Test_explode_1([[maybe_unused]]const std::string id, [[maybe_unused]]test_other_1& v, size_t& cnt) {
    cnt++;
}
void Test_explode_2([[maybe_unused]]const std::string id, [[maybe_unused]]test_other_2& v, size_t& cnt) {
    cnt++;
}
TEST(causal_data_msgpack_space, explode) {
    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    auto j = cd::map_junction::make();
    auto s = cd::msgpack_space::make(j);
    s->register_types<test_other_1, test_other_2>();

    auto r_other_1_1 = s->take<test_other_1>("o_1_1");
    auto r_other_1_2 = s->take<test_other_1>("o_1_2");
    auto r_other_1_3 = s->take<test_other_1>("o_1_3");
    auto r_other_2_1 = s->take<test_other_2>(1);
    auto r_other_2_2 = s->take<test_other_2>(2);

    auto r_cnt_1 = cc::spawn<size_t>(0);
    auto r_cnt_2 = cc::spawn<size_t>(0);

    b->act_fwd(s->explode<test_other_1>(Test_explode_1, r_cnt_1));
    b->act_fwd(s->explode<test_other_2>(Test_explode_2, r_cnt_2));

    for(size_t i = 0; i < 5; i++)
        EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());

    {   cc::opener o_cnt_1(r_cnt_1);
        cc::opener o_cnt_2(r_cnt_2);

        EXPECT_EQ(*r_cnt_1, 3);
        EXPECT_EQ(*r_cnt_2, 2);
    }
}

void Subscribe1(const std::string msg, size_t& hits) {
    if(!msg.empty())
        hits++;
}
void Subscribe2(const size_t msg, size_t& hits) {
    if(msg > 0)
        hits++;
}
TEST(causal_data_msgpack_channel, channel) {
    auto d = cp::managed_dispatcher<>::make();
    auto b = cc::branch::make(d);

    auto r1 = cc::forge<size_t>(0);
    auto r2 = cc::forge<size_t>(1);

    auto s = cd::loopback_source<>::make();
    const auto c = cd::msgpack_channel<>::make(s, b);

    c->subscribe<std::string>(Subscribe1, r1);
    c->subscribe<size_t>(Subscribe2, r2);

    c->push(std::string());
    c->push(size_t(0));

    auto chk = [&](const size_t v1, const size_t v2) {
        cc::opener o1(r1), o2(r2);
        EXPECT_EQ(*r1,v1);
        EXPECT_EQ(*r2,v2);
    };

    d->join();

    chk(0,1);

    c->push(std::string("foo"));
    c->push(size_t(2));
    c->push(std::string("bar"));
    c->push(size_t(3));

    d->join();

    chk(2,3);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
