// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <memory>

#include <gtest/gtest.h>

#include <thread>
#include <chrono>
#include <functional>
#include <sstream>

#include <msgpack.hpp>
#include <opendht.h>

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/process/thread.hpp"
#include "causal/data/channel.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/data/opendht.hpp"

namespace cc = causal::core;
namespace cp = causal::process;
namespace cd = causal::data;

char DHT_SERVICE_NAME[] = "TEST";

bool wait_for(std::function<bool()> condition, std::chrono::milliseconds timeout) {
    auto now = std::chrono::system_clock::now();
    while(std::chrono::system_clock::now() < (now + timeout) && !condition())
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    return condition();
}

std::atomic<size_t> call_cnt;

void Msg_subscribe1(const std::string msg, const cd::IdentifiedMessageMeta meta, size_t& hits) {
    call_cnt++;
    if(!msg.empty())
        hits++;
}
void Msg_subscribe2(const std::string msg, const cd::IdentifiedMessageMeta meta, size_t& hits) {
    call_cnt++;
    if(!msg.empty())
        hits++;
}

TEST(causal_data_opendht_source, msg_channel) {
    call_cnt = 0;
    auto d = cp::managed_dispatcher<>::make();
    auto b = cc::branch::make(d);

    auto r1 = cc::forge<size_t>(0);
    auto r2 = cc::forge<size_t>(1);

    auto chk = [&](const size_t v1, const size_t v2) {
        cc::opener o1(r1), o2(r2);
        EXPECT_EQ(*r1,v1);
        EXPECT_EQ(*r2,v2);
    };

    auto n1 = cd::opendht_node::make();
    auto n2 = cd::opendht_node::make(dht::crypto::generateIdentity(),
                                                 n1->getBound(), 4659, "Causal_DHT", 4559);
    auto s1 = cd::opendht_source::make(n1);
    auto s2 = cd::opendht_source::make(n2);
    const auto c1 = cd::msg_channel<cd::IdentifiedMessageMeta>::make(s1, b);
    const auto c2 = cd::msg_channel<cd::IdentifiedMessageMeta>::make(s2, b);

    EXPECT_TRUE(wait_for(
        [&]{return !n1->exportNodes().empty() && !n2->exportNodes().empty();},
        std::chrono::seconds(30)
    ));

    c1->subscribe(Msg_subscribe1, r1);
    c1->subscribe(Msg_subscribe2, r2);

    c2->subscribe(Msg_subscribe1, r1);
    c2->subscribe(Msg_subscribe2, r2);

    c1->push(std::string());

    EXPECT_TRUE(wait_for(
        [&]{return call_cnt == 4;},
        std::chrono::seconds(30)
    ));

    d->join();

    chk(0,1);

    c2->push("foo");
    c2->push("bar");

    EXPECT_TRUE(wait_for(
        [&]{return call_cnt == 12;},
        std::chrono::seconds(30)
    ));

    d->join();

    chk(4,5);
}

TEST(causal_data_opendht_crypto, crypto_crate_manage_serialize) {
    std::map<std::string, cd::Identity> ids1, ids2;
    std::map<std::string, cd::PublicIdentity> pks1, pks2;
    std::map<std::string, cd::Certificate> crts1, crts2;
    cd::IdentityId first_id, ca_id;
    cd::CertificateId ca_crt_id;
    std::string pkg1, pkg2;
    {   cd::opendht_crypto_provider c1, c2;
        ca_id = c1.create_rsa("ca", {}, 4096, true);
        EXPECT_EQ(ca_id.type, cd::IdentityType::x509);
        EXPECT_FALSE(ca_id.id.empty());

        first_id = c1.create_rsa("first", ca_id);
        EXPECT_EQ(first_id.type, cd::IdentityType::x509);
        EXPECT_FALSE(first_id.id.empty());

        auto ca_pk_id = c2.add(static_cast<cd::PublicIdentity>(c1.get(ca_id)));
        EXPECT_EQ(ca_pk_id, ca_id);

        ca_crt_id = c2.add(c1.get(ca_id).certificate);
        EXPECT_EQ(ca_crt_id.type, cd::IdentityType::x509);
        EXPECT_FALSE(ca_crt_id.id.empty());

        auto first = c1.get(first_id);
        EXPECT_EQ(static_cast<cd::IdentityId>(first), first_id);
        EXPECT_EQ(first.certificate.type, cd::IdentityType::x509);
        EXPECT_EQ(first.certificate.id, first_id.id);
        EXPECT_EQ(first.certificate.issuer_name, "ca");

        c2.create_rsa("second");

        ids1 = c1.get_identities();
        ids2 = c2.get_identities();
        pks1 = c1.get_publics();
        pks2 = c2.get_publics();
        crts1 = c1.get_certificates();
        crts2 = c2.get_certificates();

        std::stringstream ss1, ss2;
        msgpack::pack(ss1, c1);
        pkg1 = ss1.str();
        msgpack::pack(ss2, c2);
        pkg2 = ss2.str();
    }

    EXPECT_EQ(ids1.size(), 2);
    EXPECT_EQ(ids2.size(), 1);
    EXPECT_EQ(pks1.size(), 0);
    EXPECT_EQ(pks2.size(), 1);
    EXPECT_EQ(crts1.size(), 0);
    EXPECT_EQ(crts2.size(), 1);

    {   cd::opendht_crypto_provider c1, c2;
        c1.add(ids1); c1.add(pks1); c1.add(crts1);
        auto first = c1.get(first_id);
        EXPECT_EQ(static_cast<cd::IdentityId>(first), first_id);
        EXPECT_EQ(first.certificate.type, cd::IdentityType::x509);
        EXPECT_EQ(first.certificate.id, first_id.id);
        EXPECT_EQ(first.certificate.issuer_name, "ca");

        c1.add(ids2); c2.add(pks2); c2.add(crts2);
        auto ca_crt = c2.get(ca_crt_id);
        EXPECT_EQ(ca_crt.type, cd::IdentityType::x509);
        EXPECT_FALSE(ca_crt.id.empty());
        EXPECT_EQ(ca_crt.name, "ca");

        auto ca_pk = c2.get_public(ca_id);
        EXPECT_EQ(static_cast<cd::PublicIdentity>(c1.get(ca_id)), ca_pk);
        EXPECT_EQ(static_cast<cd::IdentityId>(ca_pk), ca_id);
    }

    {   auto hndl1 = msgpack::unpack(pkg1.data(), pkg1.size());
        auto hndl2 = msgpack::unpack(pkg2.data(), pkg2.size());
        cd::opendht_crypto_provider c1 = hndl1.get().as<cd::opendht_crypto_provider>(),
                          c2 = hndl2.get().as<cd::opendht_crypto_provider>();
        auto first = c1.get(first_id);
        EXPECT_EQ(static_cast<cd::IdentityId>(first), first_id);
        EXPECT_EQ(first.certificate.type, cd::IdentityType::x509);
        EXPECT_EQ(first.certificate.id, first_id.id);
        EXPECT_EQ(first.certificate.issuer_name, "ca");

        auto ca_crt = c2.get(ca_crt_id);
        EXPECT_EQ(ca_crt.type, cd::IdentityType::x509);
        EXPECT_FALSE(ca_crt.id.empty());
        EXPECT_EQ(ca_crt.name, "ca");

        auto ca_pk = c2.get_public(ca_id);
        EXPECT_EQ(static_cast<cd::PublicIdentity>(c1.get(ca_id)), ca_pk);
        EXPECT_EQ(static_cast<cd::IdentityId>(ca_pk), ca_id);
    }
}

TEST(causal_data_opendht_crypto, crypto_sign_check) {
    cd::opendht_crypto_provider c1, c2;
    auto first = c1.create_rsa("first");
    auto second = c2.create_rsa("second");
    auto second_crt = c1.add(c2.get(second).certificate);
    auto second_pk = c1.add(static_cast<cd::PublicIdentity>(c2.get(second)));
    auto first_crt = c2.add(c1.get(first).certificate);
    auto first_pk = c2.add(static_cast<cd::PublicIdentity>(c1.get(first)));
    std::string msg1("message1"),
                msg2("message2");
    
    auto sign1 = c1.sign(first, msg1);
    auto sign2 = c2.sign(second, msg2);
    EXPECT_TRUE(c2.check(first_crt, msg1, sign1));
    EXPECT_TRUE(c2.check(first_pk, msg1, sign1));
    EXPECT_FALSE(c1.check(second_crt, msg1, sign1));
    EXPECT_FALSE(c1.check(second_pk, msg1, sign1));
    EXPECT_TRUE(c1.check(second_crt, msg2, sign2));
    EXPECT_TRUE(c1.check(second_pk, msg2, sign2));
    EXPECT_FALSE(c2.check(first_crt, msg2, sign2));
    EXPECT_FALSE(c2.check(first_pk, msg2, sign2));
}

struct test_struct {
    std::string x;
    int y;

    bool operator==(const test_struct& o) const {
        return this->x == o.x && this->y == o.y;
    }

    MSGPACK_DEFINE(x, y)
};

TEST(causal_data_opendht_crypto, crypto_encrypt_decrypt) {
    cd::opendht_crypto_provider c1, c2;
    auto key = c1.create_rsa("key");
    auto crt = c2.add(c1.get(key).certificate);
    auto pk = c2.add(static_cast<cd::PublicIdentity>(c1.get(key)));
    std::string msg("message");
    auto encrypted_msg_crt = c2.encrypt(crt, msg);
    auto encrypted_msg_pk = c2.encrypt(crt, msg);
    EXPECT_NE(msg, encrypted_msg_crt);
    EXPECT_NE(msg, encrypted_msg_pk);
    auto decrypted_msg_crt = c1.decrypt(key, encrypted_msg_crt);
    auto decrypted_msg_pk = c1.decrypt(key, encrypted_msg_pk);
    EXPECT_EQ(msg, decrypted_msg_crt);
    EXPECT_EQ(msg, decrypted_msg_pk);
}

TEST(causal_data_opendht_crypto, crypto_encrypt_decrypt_msgpack) {
    test_struct ts = {"foo", -10};

    std::stringstream ss;
    msgpack::pack(ss, ts);

    cd::opendht_crypto_provider c1, c2;
    auto key = c1.create_rsa("key");
    auto crt = c2.add(c1.get(key).certificate);
    auto pk = c2.add(static_cast<cd::PublicIdentity>(c1.get(key)));
    std::string msg(ss.str());
    auto encrypted_msg = c2.encrypt(crt, msg);
    EXPECT_NE(msg, encrypted_msg);
    auto decrypted_msg = c1.decrypt(key, encrypted_msg);
    EXPECT_EQ(msg, decrypted_msg);

    auto hndl = msgpack::unpack(decrypted_msg.data(), decrypted_msg.size());
    auto tst = hndl.get().as<test_struct>();
    EXPECT_EQ(tst, ts);
}

TEST(causal_data_opendht_crypter, crypto_encrypt_decrypt) {
    std::string msg("message"),
                pwd("password1");
    cd::opendht_aes_cipher c1(pwd), c2(pwd), c3("password2");
    auto encrypted_msg1 = c1.encrypt(msg);
    auto encrypted_msg2 = c2.encrypt(msg);
    EXPECT_NE(msg, encrypted_msg1);
    EXPECT_NE(msg, encrypted_msg2);
    EXPECT_NE(encrypted_msg1, encrypted_msg2);
    auto decrypted_msg1 = c1.decrypt(encrypted_msg2);
    auto decrypted_msg2 = c2.decrypt(encrypted_msg1);
    EXPECT_EQ(msg, decrypted_msg1);
    EXPECT_EQ(msg, decrypted_msg2);
    EXPECT_THROW(c3.decrypt(encrypted_msg1), dht::crypto::DecryptError);
}

TEST(causal_data_opendht_crypter, crypto_encrypt_decrypt_msgpack) {
    test_struct ts = {"foo", -10};

    std::stringstream ss;
    msgpack::pack(ss, ts);

    std::string msg(ss.str()),
                pwd("password");
    cd::opendht_aes_cipher c1(pwd), c2(pwd);
    auto encrypted_msg1 = c1.encrypt(msg);
    auto encrypted_msg2 = c2.encrypt(msg);
    EXPECT_NE(msg, encrypted_msg1);
    EXPECT_NE(msg, encrypted_msg2);
    EXPECT_NE(encrypted_msg1, encrypted_msg2);
    auto decrypted_msg1 = c1.decrypt(encrypted_msg2);
    auto decrypted_msg2 = c2.decrypt(encrypted_msg1);
    EXPECT_EQ(msg, decrypted_msg1);
    EXPECT_EQ(msg, decrypted_msg2);

    auto hndl1 = msgpack::unpack(decrypted_msg1.data(), decrypted_msg1.size());
    auto hndl2 = msgpack::unpack(decrypted_msg2.data(), decrypted_msg2.size());
    auto ts1 = hndl1.get().as<test_struct>();
    auto ts2 = hndl2.get().as<test_struct>();
    EXPECT_EQ(ts1, ts2);
    EXPECT_EQ(ts1, ts);
}

TEST(causal_data_opendht_crypter, crypto_hash) {
    std::string msg("message"),
                pwd1("password1"),
                pwd2("password2");
    cd::opendht_aes_cipher c1(pwd1), c2(pwd2);
    auto hashed_msg1 = c1.get_hasher().hash(msg);
    auto hashed_msg2 = c2.get_hasher().hash(msg);
    EXPECT_NE(msg, hashed_msg1);
    EXPECT_NE(msg, hashed_msg2);
    EXPECT_EQ(hashed_msg1, hashed_msg2);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
