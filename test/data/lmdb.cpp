// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

//#define VERBOSE

#include <map>
#include <string>
#include <memory>
#include <stdio.h>

#include "causal/data/junction.hpp"
#include "causal/data/crypto.hpp"
#include "causal/data/msgpack.hpp"
#include "causal/data/lmdb.hpp"
#include "causal/facet/common.hpp"
#include "causal/facet/spreads.hpp"
#include "causal/facet/scales.hpp"
#include "causal/facet/routes.hpp"

using namespace std::string_literals;
namespace cc = causal::core;
namespace cf = causal::facet;
namespace cd = causal::data;

struct test_other_1 {
    int x = 0;

    test_other_1() = default;
    test_other_1(int v) : x(v) {}

    MSGPACK_DEFINE_MAP(x)
};

struct test_other_2 {
    std::string x;
    
    test_other_2() = default;
    test_other_2(std::string v) : x(v) {}

    MSGPACK_DEFINE_MAP(x)
};

struct test_facing {
    std::string x;
    
    test_facing() = default;
    test_facing(std::string v) : x(v) {}

    cf::ptr_edges<test_facing, test_other_1, test_other_2> edges;
    cf::ptr_spreads<test_facing> spreads;
    cf::ptr_spreads<test_other_1> spreads_1;
    cf::ptr_spreads<test_other_2> spreads_2;
    cf::ptr_scales<test_facing, std::string> scales;
    cf::ptr_scales<test_other_1, std::string> scales_1;
    cf::ptr_scales<test_other_2, int> scales_2;
    cf::ptr_routes<std::string, test_facing> routes;
    cf::ptr_routes<std::string, test_other_1> routes_1;
    cf::ptr_routes<int, test_other_2> routes_2;

    MSGPACK_DEFINE(x, edges, spreads, spreads_1, spreads_2, scales, scales_1, scales_2, routes, routes_1, routes_2)
};

TEST(causal_data_lmdb_junction, msgpack_space_merge_override) {
    remove("/tmp/testlmdb1");
    remove("/tmp/testlmdb2");
    
    auto j1 = cd::lmdb_junction::make("/tmp/testlmdb1", "test1");
    auto j2 = cd::lmdb_junction::make("/tmp/testlmdb2", "test1");
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<int, test_other_1, test_other_2>();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<int, test_other_1, test_other_2>();
    {   auto r1_1 = s1->get<int>("1"); cc::opener o1_1(r1_1);
        auto r1_2 = s2->get<int>("1"); cc::opener o1_2(r1_2);
        auto r2_1 = s1->get<test_other_1>("2"); cc::opener o2_1(r2_1);
        auto r3_1 = s1->get<test_other_2>("3"); cc::opener o3_1(r3_1);
        *r1_1 = 1;
        *r1_2 = 2;
        *r2_1 = test_other_1{2};
        *r3_1 = test_other_2{"3"};
    }

    j2->merge(*j1);

    {   auto r1_2 = s2->get<int>("1"); cc::opener o1(r1_2);
        auto r2_2 = s2->get<test_other_1>("2"); cc::opener o2(r2_2);
        auto r3_2 = s2->get<test_other_2>("3"); cc::opener o3(r3_2);
        EXPECT_EQ(*r1_2, 1);
        EXPECT_EQ(r2_2->x, 2);
        EXPECT_EQ(r3_2->x, "3");
    }
}

TEST(causal_data_lmdb_junction, msgpack_space_merge_nonoverride) {
    remove("/tmp/testlmdb1");
    remove("/tmp/testlmdb2");
    
    auto j1 = cd::lmdb_junction::make("/tmp/testlmdb1", "test1");
    auto j2 = cd::lmdb_junction::make("/tmp/testlmdb2", "test1");
    auto s1 = cd::msgpack_space::make(j1);
    s1->register_types<int, test_other_1, test_other_2>();
    auto s2 = cd::msgpack_space::make(j2);
    s2->register_types<int, test_other_1, test_other_2>();
    {   auto r1_1 = s1->get<int>("1"); cc::opener o1_1(r1_1);
        auto r1_2 = s2->get<int>("1"); cc::opener o1_2(r1_2);
        auto r2_1 = s1->get<test_other_1>("2"); cc::opener o2_1(r2_1);
        auto r3_1 = s1->get<test_other_2>("3"); cc::opener o3_1(r3_1);
        *r1_1 = 1;
        *r1_2 = 2;
        *r2_1 = test_other_1{2};
        *r3_1 = test_other_2{"3"};
    }

    j2->merge(*j1, false);

    {   auto r1_2 = s2->get<int>("1"); cc::opener o1(r1_2);
        auto r2_2 = s2->get<test_other_1>("2"); cc::opener o2(r2_2);
        auto r3_2 = s2->get<test_other_2>("3"); cc::opener o3(r3_2);
        EXPECT_EQ(*r1_2, 2);
        EXPECT_EQ(r2_2->x, 2);
        EXPECT_EQ(r3_2->x, "3");
    }
}

void round_trip_set(cd::msgpack_space& s, std::string custom) {
    s.register_types<std::string, test_facing, test_other_1, test_other_2>();

    auto r_custom = s.get<std::string>("custom");
    auto r_other_1_1 = s.get<test_other_1>("o_1_1", 1);
    auto r_other_1_2 = s.get<test_other_1>("o_1_2", 2);
    auto r_other_2_1 = s.get<test_other_2>("o_2_1", std::string("foo"));
    auto r_other_2_2 = s.get<test_other_2>("o_2_2", std::string("bar"));
    auto r_facing_1 = s.get<test_facing>("f_1", std::string("_foo"));
    auto r_facing_2 = s.get<test_facing>("f_2", std::string("_bar"));

    {   cc::opener o_custom(r_custom);
        cc::opener o_o_1_1(r_other_1_1);
        cc::opener o_o_1_2(r_other_1_2);
        cc::opener o_o_2_1(r_other_2_1);
        cc::opener o_o_2_2(r_other_2_2);
        cc::opener o_f_1(r_facing_1);
        cc::opener o_f_2(r_facing_2);

        *r_custom = custom;

        r_other_1_1.create();
        r_other_1_2.create();

        r_other_2_1.create();
        r_other_2_2.create();

        r_facing_1.create();
        r_facing_2.create();

        r_facing_1->edges.set<0>(r_facing_1);
        r_facing_1->edges.set<1>(r_other_1_1);
        r_facing_1->edges.set<2>(r_other_2_1);

        r_facing_1->spreads.insert(r_facing_1);
        r_facing_1->spreads.insert(r_facing_2);

        r_facing_1->spreads_1.insert(r_other_1_1);
        r_facing_1->spreads_1.insert(r_other_1_2);

        r_facing_1->spreads_2.insert(r_other_2_1);
        r_facing_1->spreads_2.insert(r_other_2_2);

        r_facing_1->scales.insert(r_facing_1, "1");
        r_facing_1->scales.insert(r_facing_2, "2");

        r_facing_1->scales_1.insert(r_other_1_1, "3");
        r_facing_1->scales_1.insert(r_other_1_2, "4");

        r_facing_1->scales_2.insert(r_other_2_1, 1);
        r_facing_1->scales_2.insert(r_other_2_2, 2);

        r_facing_1->routes.insert("5", r_facing_1);
        r_facing_1->routes.insert("6", r_facing_2);

        r_facing_1->routes_1.insert("7", r_other_1_1);
        r_facing_1->routes_1.insert("8", r_other_1_2);

        r_facing_1->routes_2.insert(3, r_other_2_1);
        r_facing_1->routes_2.insert(4, r_other_2_2);
    }
}

void round_trip_check(cd::msgpack_space& s, std::string custom) {
    s.register_types<std::string, test_facing, test_other_1, test_other_2>();

    auto r_custom = s.get<std::string>("custom");
    auto r_other_1_1 = s.get<test_other_1>("o_1_1");
    auto r_other_1_2 = s.get<test_other_1>("o_1_2");
    auto r_other_2_1 = s.get<test_other_2>("o_2_1");
    auto r_other_2_2 = s.get<test_other_2>("o_2_2");
    auto r_facing_1 = s.get<test_facing>("f_1");
    auto r_facing_2 = s.get<test_facing>("f_2");

    {   cc::opener o_custom(r_custom);
        cc::opener o_o_1_1(r_other_1_1);
        cc::opener o_o_1_2(r_other_1_2);
        cc::opener o_o_2_1(r_other_2_1);
        cc::opener o_o_2_2(r_other_2_2);
        cc::opener o_f_1(r_facing_1);
        cc::opener o_f_2(r_facing_2);

        EXPECT_EQ(*r_custom, custom);

        EXPECT_EQ(r_other_1_1->x, 1);
        EXPECT_EQ(r_other_1_2->x, 2);

        EXPECT_EQ(r_other_2_1->x, "foo");
        EXPECT_EQ(r_other_2_2->x, "bar");

        EXPECT_EQ(r_facing_1->x, "_foo");
        EXPECT_EQ(r_facing_2->x, "_bar");

        EXPECT_EQ(r_facing_1->edges.get<0>(), r_facing_1);
        EXPECT_EQ(r_facing_1->edges.get<1>(), r_other_1_1);
        EXPECT_EQ(r_facing_1->edges.get<2>(), r_other_2_1);

        EXPECT_TRUE(r_facing_1->spreads.contains(r_facing_1));
        EXPECT_TRUE(r_facing_1->spreads.contains(r_facing_2));

        EXPECT_TRUE(r_facing_1->spreads_1.contains(r_other_1_1));
        EXPECT_TRUE(r_facing_1->spreads_1.contains(r_other_1_2));

        EXPECT_TRUE(r_facing_1->spreads_2.contains(r_other_2_1));
        EXPECT_TRUE(r_facing_1->spreads_2.contains(r_other_2_2));

        EXPECT_EQ(r_facing_1->scales[r_facing_1], "1");
        EXPECT_EQ(r_facing_1->scales[r_facing_2], "2");

        EXPECT_EQ(r_facing_1->scales_1[r_other_1_1], "3");
        EXPECT_EQ(r_facing_1->scales_1[r_other_1_2], "4");

        EXPECT_EQ(r_facing_1->scales_2[r_other_2_1], 1);
        EXPECT_EQ(r_facing_1->scales_2[r_other_2_2], 2);

        EXPECT_EQ(r_facing_1->routes["5"s], r_facing_1);
        EXPECT_EQ(r_facing_1->routes["6"s], r_facing_2);

        EXPECT_EQ(r_facing_1->routes_1["7"s], r_other_1_1);
        EXPECT_EQ(r_facing_1->routes_1["8"s], r_other_1_2);

        EXPECT_EQ(r_facing_1->routes_2[3], r_other_2_1);
        EXPECT_EQ(r_facing_1->routes_2[4], r_other_2_2);
    }
}

TEST(causal_data_lmdb_junction, round_trip) {
    remove("/tmp/testlmdb1");
    {   auto j1 = cd::lmdb_junction::make("/tmp/testlmdb1", "test1");
        auto s1 = cd::msgpack_space::make(j1);
        s1->register_types<test_other_1, test_other_2>();
        
        auto j2 = cd::lmdb_junction::make("/tmp/testlmdb1", "test2");
        auto s2 = cd::msgpack_space::make(j2);
        s2->register_types<test_other_1, test_other_2>();

        round_trip_set(*s1, "foo");
        round_trip_set(*s2, "bar");
        
        round_trip_check(*s1, "foo");
        round_trip_check(*s2, "bar");
    }
    
    {   auto j1 = cd::lmdb_junction::make("/tmp/testlmdb1", "test1");
        auto s1 = cd::msgpack_space::make(j1);
        s1->register_types<test_other_1, test_other_2>();
        
        auto j2 = cd::lmdb_junction::make("/tmp/testlmdb1", "test2");
        auto s2 = cd::msgpack_space::make(j2);
        s2->register_types<test_other_1, test_other_2>();
        
        round_trip_check(*s1, "foo");
        round_trip_check(*s2, "bar");
    }
}

TEST(causal_data_lmdb_junction, crypted_round_trip) {
    std::string pwd1("password1"), pwd2("password2");
    remove("/tmp/testlmdb_crypted");
    {    auto j1 = cd::lmdb_junction::make(cd::gnutls_aes_cipher(pwd1), "/tmp/testlmdb_crypted", "test1");
        auto s1 = cd::msgpack_space::make(j1);
        s1->register_types<test_other_1, test_other_2>();
        
        auto j2 = cd::lmdb_junction::make(cd::gnutls_aes_cipher(pwd1), "/tmp/testlmdb_crypted", "test2");
        auto s2 = cd::msgpack_space::make(j2);
        s2->register_types<test_other_1, test_other_2>();

        round_trip_set(*s1, "foo");
        round_trip_set(*s2, "bar");
        
        round_trip_check(*s1, "foo");
        round_trip_check(*s2, "bar");
    }

    {   auto j1 = cd::lmdb_junction::make(cd::gnutls_aes_cipher(pwd1), "/tmp/testlmdb_crypted", "test1");
        auto s1 = cd::msgpack_space::make(j1);
        s1->register_types<test_other_1, test_other_2>();
        
        auto j2 = cd::lmdb_junction::make(cd::gnutls_aes_cipher(pwd1), "/tmp/testlmdb_crypted", "test2");
        auto s2 = cd::msgpack_space::make(j2);
        s2->register_types<test_other_1, test_other_2>();
        
        round_trip_check(*s1, "foo");
        round_trip_check(*s2, "bar");
    }

    {
        EXPECT_THROW(cd::lmdb_junction::make(cd::gnutls_aes_cipher(pwd2), "/tmp/testlmdb_crypted", "test1"), cd::lmdb_crypto_token_mismatch);
        EXPECT_THROW(cd::lmdb_junction::make(cd::gnutls_aes_cipher(pwd2), "/tmp/testlmdb_crypted", "test2"), cd::lmdb_crypto_token_mismatch);
    }

    {
        EXPECT_THROW(cd::lmdb_junction::make("/tmp/testlmdb_crypted", "test1"), cd::lmdb_crypto_token_mismatch);
        EXPECT_THROW(cd::lmdb_junction::make("/tmp/testlmdb_crypted", "test2"), cd::lmdb_crypto_token_mismatch);
    }

    {   remove("/tmp/testlmdb_noncrypted");
        cd::lmdb_junction::make("/tmp/testlmdb_noncrypted");
        
        EXPECT_THROW(cd::lmdb_junction::make(cd::gnutls_aes_cipher(pwd1), "/tmp/testlmdb_noncrypted"), cd::lmdb_crypto_token_mismatch);
    }
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
