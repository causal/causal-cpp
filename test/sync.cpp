// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <exception>
#include <gtest/gtest.h>

#include <memory>

#include "causal/sync/util.hpp"
#include "causal/sync/timing.hpp"
#include "causal/sync/unite.hpp"
#include "causal/sync/iterate.hpp"
#include "causal/process/simple.hpp"
#include "causal/trace.hpp"

namespace cc = causal::core;
namespace cy = causal::sync;
namespace cp = causal::process;

void Depend_dependant(const size_t& i1, const size_t& i2, const size_t expectation, bool& ran) {
    EXPECT_FALSE(ran);
    ran = true;
    EXPECT_EQ(i1, expectation);
    EXPECT_EQ(i2, expectation);
}

void Depend_dependency(cc::aspect_ptr<size_t>& i) {
    if(++(*i) < 50u)
        cc::act_fwd(Depend_dependency, i);
}

TEST(causal_sync, depend_on) {
    auto d = cp::simple_dispatcher<cp::preserving_queue>::make();
    auto r1 = cc::spawn<size_t>(0);
    auto r2 = cc::spawn<size_t>(0);
    auto r3 = cc::spawn<bool>(false);
    auto b = cc::branch::make(d);

    {   cy::depender dep;
        b->act_synced(dep.on(), Depend_dependency, r1);
        b->act_synced(dep.on(), Depend_dependency, r2);
        // dependency runs, enqueues anew and only afterwards synchron is fulfilled and dependant moved to queue feed
        // there fore we expect 2u
        b->act_synced(dep.depend(), Depend_dependant, r1, r2, 2u, r3);
    }

    for(size_t i = 1; i <= 100; i++)
        EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());

    {   cc::opener o(r3);
        EXPECT_TRUE(*r3);
    }
}

TEST(causal_sync, depend_on_branch) {
    auto d = cp::simple_dispatcher<cp::preserving_queue>::make();
    auto r1 = cc::spawn<size_t>(0);
    auto r2 = cc::spawn<size_t>(0);
    auto r3 = cc::spawn<bool>(false);
    auto b = cc::branch::make(d);

    {   cy::depender dep;
        b->act_synced(dep.on_branch(), Depend_dependency, r1);
        b->act_synced(dep.on_branch(), Depend_dependency, r2);
        b->act_synced(dep.depend(), Depend_dependant, r1, r2, 50u, r3);
    }

    for(size_t i = 1; i <= 100; i++)
        EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());

    {   cc::opener o(r3);
        EXPECT_TRUE(*r3);
    }
}

void branch_loop(cc::aspect_ptr<u_long>& cnt) {
    (*cnt)++;
    cc::act_fwd(branch_loop, cnt);
}

TEST(causal_core_wirks, ground_anchor) {
    auto cnt = cc::forge<u_long>(0);

    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);

    {   cy::ground g;
        b->act_synced(g.drop(), branch_loop, cnt);

        for(u_long i = 0; i < 10; i++)
            EXPECT_TRUE(d->do_tick());

        {   cc::opener o_cnt(cnt);
            EXPECT_EQ(*cnt, 10);
        }
    }

    EXPECT_FALSE(d->do_tick());
}

void Timing(size_t& i) {
    i = 1;
}

TEST(causal_sync, timing_delay_ms) {
    constexpr size_t duration = 10, wait = 20;
    auto d = cp::simple_dispatcher<>::make();
    cc::aspect_ptr<size_t> r(cc::forge<size_t>(0));
    auto b = cc::branch::make(d);

    {
        b->act_synced(cy::delay_ms::make(duration), Timing, r);
    }

    EXPECT_FALSE(d->do_tick());
    {   cc::opener o(r);
        EXPECT_EQ(*r, 0u);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(wait));

    EXPECT_TRUE(d->do_tick());
    {   cc::opener o(r);
        EXPECT_EQ(*r, 1u);
    }
}

TEST(causal_sync, timing_expire_ms) {
    constexpr size_t duration = 100, wait = 120;
    auto d = cp::simple_dispatcher<>::make();
    cc::aspect_ptr<size_t> r(cc::forge<size_t>(0));
    auto b = cc::branch::make(d);

    {   cy::depender dep;
        b->act_synced(cy::expire_ms::make(duration), Timing, r);
    }
    
    std::this_thread::sleep_for(std::chrono::milliseconds(wait));

    EXPECT_FALSE(d->do_tick());
    {   cc::opener o(r);
        EXPECT_EQ(*r, 0u);
    }
}

void Unite(size_t& i, const size_t v) {
    EXPECT_EQ(i, 0u);
    i = v;
}

TEST(causal_sync, unifier_front) {
    auto d = cp::simple_dispatcher<>::make();
    cc::aspect_ptr<size_t> r(cc::spawn<size_t>());
    auto b = cc::branch::make(d);
    cy::unifier_front u;
    for(size_t i = 1; i <= 5; ++i) {
        {   cc::opener o(r);
            *r = 0;
        }

        for(size_t j = 1; j <= i; ++j) {
            b->act_synced(u.make(), Unite, r, j);
        }
        
        while(d->do_tick()){}

        {   cc::opener o(r);
            EXPECT_EQ(*r, 1u);
        }
    }
}

TEST(causal_sync, unifier_back) {
    auto d = cp::simple_dispatcher<>::make();
    cc::aspect_ptr<size_t> r(cc::spawn<size_t>());
    auto b = cc::branch::make(d);
    cy::unifier_back u;
    for(size_t i = 1; i <= 5; ++i) {
        {   cc::opener o(r);
            *r = 0;
        }

        for(size_t j = 1; j <= i; ++j) {
            b->act_synced(u.make(), Unite, r, j);
        }
        
        while(d->do_tick()){}

        {   cc::opener o(r);
            EXPECT_EQ(*r, i);
        }
    }
}

void Iterate(cc::aspect_ptr<size_t>& i, const size_t v, std::shared_ptr<cy::iterator> it) {
    *i += v;

    if(it->get_round() < 5)
        act_synced(it->iterate(), Iterate, i, v, it);
}

void Iterate_join(cc::aspect_ptr<const size_t>& i, const size_t e, std::shared_ptr<cy::iterator> it) {
    EXPECT_EQ(*i, e);

    if(it->get_round() < 5) {
        act_synced(it->join(), Iterate_join, i, e+15, it);
        EXPECT_TRUE(it->next());
    }
}

TEST(causal_sync, iterate) {
    auto d = cp::simple_dispatcher<>::make();
    auto r = cc::forge<size_t>(0);
    auto b = cc::branch::make(d);
    auto it = cy::iterator::make();
    size_t e = 0;
    for(size_t j = 1; j <= 5; ++j) {
        e += j;
        b->act_synced(it->iterate(), Iterate, r, j, it);
    }
    
    b->act_synced(it->join(), Iterate_join, r, e, it);

    EXPECT_TRUE(it->next());

    while(d->do_tick()){}

    {   cc::opener o(r);
        EXPECT_EQ(*r, 75u);
    }
}

void Noexcept(size_t &i) {
    EXPECT_EQ(i++, 1u);
}

class test_exception : public trace::exception_warning {
public:
    const size_t i;
    test_exception(size_t i) : exception_warning("test_exception"), i(i) {}
};

void Except(size_t &i) {
    EXPECT_EQ(i++, 1u);
    throw test_exception(i);
}

void Catch(const std::exception_ptr e, size_t &i) noexcept {
    // throw did a rollback why i again == 1
    EXPECT_EQ(i++, 1u);
    try {
        if(e) std::rethrow_exception(e);
    } catch(const test_exception& e) {
        EXPECT_EQ(e.i, 2u);
    } catch(...) {
        EXPECT_TRUE(false);
    }
}

TEST(causal_sync, catcher_except) {
    auto d = cp::simple_dispatcher<>::make();
    auto r = cc::forge<size_t>(1);
    auto b = cc::branch::make(d);
    auto c = cy::catcher::make(b, Catch, r);

    b->act_synced(c, Except, r);

    while(d->do_tick()){}

    {   cc::opener o(r);
        EXPECT_EQ(*r, 2u);
    }
}

TEST(causal_sync, catcher_noexcept) {
    auto d = cp::simple_dispatcher<>::make();
    auto r = cc::forge<size_t>(1);
    auto b = cc::branch::make(d);
    auto c = cy::catcher::make(b, Catch, r);

    b->act_synced(c, Noexcept, r);

    while(d->do_tick()){}

    {   cc::opener o(r);
        EXPECT_EQ(*r, 2u);
    }
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
