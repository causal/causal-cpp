// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include "timing.hpp"

TEST(notes_algorithm_timing_measurement, discrete_pow) {
    std::function<size_t(size_t,size_t)> pow = [&pow](size_t a, size_t x) {
        if(x-- > 1)
            return a * pow(a, x);
        else
            return a;
    };

    EXPECT_EQ(1, pow(1,1));
    EXPECT_EQ(4, pow(2,2));
    EXPECT_EQ(8, pow(2,3));
    EXPECT_EQ(65536, pow(2,16));
    EXPECT_EQ(9, pow(3,2));
    EXPECT_EQ(27, pow(3,3));
}

TEST(notes_algorithm_timing_measurement, discrete_log) {
    std::function<size_t(size_t,size_t)> log = [&log](size_t y, size_t b) -> size_t {
        if(y > b) {
            EXPECT_TRUE(y % b == 0);
            y = y / b;
            return 1+log(y, b);
        } else
            return 1;
    };

    EXPECT_EQ(1, log(1,1));
    EXPECT_EQ(2, log(4,2));
    EXPECT_EQ(3, log(8,2));
    EXPECT_EQ(16, log(65536,2));
    EXPECT_EQ(2, log(9,3));
    EXPECT_EQ(3, log(27,3));
}

TEST(notes_algorithm_timing_measurement, atom) {
    bool done = false;
    for(size_t i = 1; i <= 1; i++)
        done = true;
    EXPECT_TRUE(done);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}