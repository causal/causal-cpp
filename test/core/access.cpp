// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include "causal/core/access.hpp"

namespace cc = causal::core;

thread_local bool ran = false;

struct test_struct {
    virtual std::string get() const = 0;
};

struct test_struct_derrived : cc::interfaces<test_struct> {
    std::string s;
    virtual std::string get() const override {return "|"+this->s;}

    test_struct_derrived(std::string s) : s(s) {}
};

void test_access_open(const void* const k, const std::vector<cc::access_*>& av) {
    for(auto& a : av) {
        a->focus(k);
        EXPECT_TRUE(a->borrow(k));
    }
}

void test_access_close(const void* const k, const std::vector<cc::access_*>& av) {
    for(auto& a : av) {
        a->unborrow(k, false);
        a->release(k);
        delete a;
    }
}

void x_move_values(size_t i, const size_t ci, int ic, const int cic,
                   std::string s, const std::string cs) {
    ran = true;
    EXPECT_EQ(i, 5);
    EXPECT_EQ(ci, 5);
    EXPECT_EQ(ic, 5);
    EXPECT_EQ(cic, 5);
    EXPECT_EQ(s, "test");
    EXPECT_EQ(cs, "test");
}
TEST(causal_core_access, copy_move) {
    cc::key key;
    ran = false;
    size_t i = 5;
    std::string s = "test";

    auto av = cc::get_access(x_move_values, i, i, i, i, s, s);
    test_access_open(&key, av);
    cc::apply_access(x_move_values, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}
TEST(causal_core_access, forward_move) {
    cc::key key;
    ran = false;

    auto av = cc::get_access(x_move_values, size_t(5), size_t(5), size_t(5), size_t(5), std::string("test"), std::string("test"));
    test_access_open(&key, av);
    cc::apply_access(x_move_values, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}
TEST(causal_core_access, forward_move_free) {
    cc::key key;
    ran = false;

    auto av = cc::get_access(x_move_values, 5, 5, 5, 5, "test", "test");
    test_access_open(&key, av);
    cc::apply_access(x_move_values, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}

void x_move_pointer(size_t* i, const size_t* ci,
                    std::string* s, const std::string* cs) {
    ran = true;
    EXPECT_EQ(*i, 5);
    EXPECT_EQ(*ci, 5);
    EXPECT_EQ(*s, "test");
    EXPECT_EQ(*cs, "test");
}
TEST(causal_core_access, copy_move_ptr) {
    cc::key key;
    ran = false;
    size_t i = 5;
    std::string s = "test";

    auto av = cc::get_access(x_move_pointer, &i, &i, &s, &s);
    test_access_open(&key, av);
    cc::apply_access(x_move_pointer, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}

void x_open(cc::aspect_ptr<int>& i, cc::aspect_ptr<const int>& ci,
            cc::aspect_ptr<test_struct_derrived>& s, cc::aspect_ptr<test_struct>& sb,
            cc::aspect_ptr<const test_struct_derrived>& cs, cc::aspect_ptr<const test_struct>& csb,
            cc::aspect_ptr<test_struct_derrived>& sc, cc::aspect_ptr<test_struct>& sbc) {
    ran = true;
    EXPECT_EQ(*i, 1);
    EXPECT_EQ(*ci, 1);
    EXPECT_EQ(s->s, "foo");
    EXPECT_EQ(sb->get(), "|foo");
    EXPECT_EQ(cs->s, "foo");
    EXPECT_EQ(csb->get(), "|foo");
    EXPECT_EQ(sc->s, "foo");
    EXPECT_EQ(sbc->get(), "|foo");

    EXPECT_TRUE(i.get_ptr()->is_borrowed());
    EXPECT_TRUE(ci.get_ptr()->is_const_borrowed());
    EXPECT_TRUE(s.get_ptr()->is_borrowed());
    EXPECT_TRUE(sb.get_ptr()->is_borrowed());
    EXPECT_TRUE(cs.get_ptr()->is_const_borrowed());
    EXPECT_TRUE(csb.get_ptr()->is_const_borrowed());
    EXPECT_TRUE(sc.get_ptr()->is_borrowed());
    EXPECT_TRUE(sbc.get_ptr()->is_borrowed());
}
TEST(causal_core_access, copy_open) {
    cc::key key;
    ran = false;
    auto i = cc::forge<int>(1);
    auto s = cc::forge<test_struct_derrived>("foo");
    auto sb = cc::aspect_ptr_cast<test_struct>(s);
    auto av = cc::get_access(x_open, i, i, s, sb, s, sb, sb, s);

    test_access_open(&key, av);
    cc::apply_access(x_open, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}
TEST(causal_core_access, forward_open) {
    cc::key key;
    ran = false;
    auto i = cc::forge<int>(1);
    auto s = cc::forge<test_struct_derrived>("foo");
    auto sb = cc::aspect_ptr_cast<test_struct>(s);
    auto av = cc::get_access(x_open, cc::aspect_ptr_cast<int>(i), cc::aspect_ptr_cast<const int>(i),
                                     cc::aspect_ptr_cast<test_struct_derrived>(s), cc::aspect_ptr_cast<test_struct>(s),
                                     cc::aspect_ptr_cast<const test_struct_derrived>(s), cc::aspect_ptr_cast<const test_struct>(s),
                                     cc::aspect_ptr_cast<test_struct>(s), cc::aspect_ptr_cast<test_struct_derrived>(s));

    test_access_open(&key, av);
    cc::apply_access(x_open, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}
TEST(causal_core_access, copy_spawn_open) {
    cc::key key;
    ran = false;
    int i = 1;
    test_struct_derrived s("foo");
    auto av = cc::get_access(x_open, i, i, s, s, s, s, s, s);

    test_access_open(&key, av);
    cc::apply_access(x_open, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}
TEST(causal_core_access, forward_spawn_open) {
    cc::key key;
    ran = false;
    auto av = cc::get_access(x_open, 1, 1,
                                     test_struct_derrived("foo"), test_struct_derrived("foo"),
                                     test_struct_derrived("foo"), test_struct_derrived("foo"),
                                     test_struct_derrived("foo"), test_struct_derrived("foo"));

    test_access_open(&key, av);
    cc::apply_access(x_open, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}

void x_deref(int& i, const int& ci,
             test_struct_derrived& s, test_struct& sb,
             const test_struct_derrived& cs, const test_struct& csb,
             test_struct_derrived& sc, test_struct& sbc) {
    ran = true;
    EXPECT_EQ(i, 1);
    EXPECT_EQ(ci, 1);
    EXPECT_EQ(s.s, "foo");
    EXPECT_EQ(sb.get(), "|foo");
    EXPECT_EQ(cs.s, "foo");
    EXPECT_EQ(csb.get(), "|foo");
    EXPECT_EQ(sc.s, "foo");
    EXPECT_EQ(sbc.get(), "|foo");
}
TEST(causal_core_access, copy_deref) {
    cc::key key;
    ran = false;
    auto i = cc::forge<int>(1);
    auto s = cc::forge<test_struct_derrived>("foo");
    auto sb = cc::aspect_ptr_cast<test_struct>(s);
    auto av = cc::get_access(x_deref, i, i, s, sb, s, sb, sb, s);

    test_access_open(&key, av);
    cc::apply_access(x_deref, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}
TEST(causal_core_access, forward_deref) {
    cc::key key;
    ran = false;
    auto i = cc::forge<int>(1);
    auto s = cc::forge<test_struct_derrived>("foo");
    auto sb = cc::aspect_ptr_cast<test_struct>(s);
    auto av = cc::get_access(x_deref, cc::aspect_ptr_cast<int>(i), cc::aspect_ptr_cast<const int>(i),
                                      cc::aspect_ptr_cast<test_struct_derrived>(s), cc::aspect_ptr_cast<test_struct>(s),
                                      cc::aspect_ptr_cast<const test_struct_derrived>(s), cc::aspect_ptr_cast<const test_struct>(s),
                                      cc::aspect_ptr_cast<test_struct>(s), cc::aspect_ptr_cast<test_struct_derrived>(s));

    test_access_open(&key, av);
    cc::apply_access(x_deref, &key, av);
    test_access_close(&key, av);

    EXPECT_TRUE(ran);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}