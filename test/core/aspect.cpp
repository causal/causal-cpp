// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include <functional>
#include <iostream>

#include "causal/core/access.hpp"
#include "causal/core/aspect.hpp"

namespace cc = causal::core;

size_t mock_focus_cnt, mock_release_cnt,
       mock_is_focused_cnt,
       mock_borrow_cnt, mock_const_borrow_cnt, mock_unborrow_cnt,
       mock_is_borrowed_cnt, mock_is_const_borrowed_cnt,
       mock_create, mock_dispose, mock_destroy;
class mock_space final : public std::enable_shared_from_this<mock_space>, public cc::space_base {
public:
    const cc::key* const key;

    void focus(const std::any id, const void* const key) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_focus_cnt++;
    }
    void release(const std::any id, const void* const key) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_release_cnt++;
    }
    bool is_focused(const std::any id) const noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_is_focused_cnt++;
        return true;
    }

    bool borrow(const std::any id, const void* const key) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_borrow_cnt++;
        return true;
    }
    bool const_borrow(const std::any id, const void* const key) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_const_borrow_cnt++;
        return true;
    }
    void unborrow(const std::any id, const void* const key, [[maybe_unused]]const bool abort) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_unborrow_cnt++;
    }
    bool is_borrowed(const std::any id) const noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_is_borrowed_cnt++;
        return true;
    }
    bool is_const_borrowed(const std::any id) const noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_is_const_borrowed_cnt++;
        return true;
    }

    void create(const std::any id, [[maybe_unused]]const cc::essence_ptr<> a) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        mock_create++;
    };
    
    virtual void dispose(const std::any id) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        mock_dispose++;
    };

    void destroy(const std::any id) noexcept override {
        EXPECT_EQ(std::any_cast<std::string>(id), "1");
        EXPECT_EQ(this->key, key);
        mock_destroy++;
    };

    cc::essence_ptr<int> spawn() noexcept {
        return cc::essence<int>::make(std::string("1"), this->shared_from_this());
    }

    cc::essence_ptr<> clone(const void* const key, cc::essence_ptr<> ptr, std::any id = std::any()) override {
        return nullptr;
    }

    mock_space(const cc::key& k) : key(&k) {}
};

TEST(causal_core_aspect, space) {
    mock_focus_cnt = 0; mock_release_cnt = 0; mock_is_focused_cnt = 0;
    mock_borrow_cnt = 0; mock_const_borrow_cnt = 0; mock_unborrow_cnt = 0;
    mock_is_borrowed_cnt = 0; mock_is_const_borrowed_cnt = 0;

    cc::key key;
    auto s = std::make_shared<mock_space>(key);
    auto a = s->spawn();

    a->focus(&key);
    EXPECT_TRUE(a->borrow(&key));
    EXPECT_TRUE(a->is_focused());
    EXPECT_TRUE(a->is_borrowed());
    a->unborrow(&key);
    a->release(&key);

    a->focus(&key);
    EXPECT_TRUE(a->const_borrow(&key));
    EXPECT_TRUE(a->is_focused());
    EXPECT_TRUE(a->is_const_borrowed());
    a->unborrow(&key);
    a->release(&key);

    EXPECT_EQ(mock_focus_cnt, 2u);
    EXPECT_EQ(mock_release_cnt, 2u);
    EXPECT_EQ(mock_is_focused_cnt, 2u);
    EXPECT_EQ(mock_borrow_cnt, 1u);
    EXPECT_EQ(mock_const_borrow_cnt, 1u);
    EXPECT_EQ(mock_unborrow_cnt, 2u);
    EXPECT_EQ(mock_is_borrowed_cnt, 1u);
    EXPECT_EQ(mock_is_const_borrowed_cnt, 1u);
}

struct test_base {
    virtual ~test_base() = default;
    virtual int get() const = 0;
    virtual void set(int v) = 0;
};

struct test_derived1 : cc::interfaces<test_base> {
    int x;
    int get() const override {
        return this->x + 2;
    }

    void set(int v) override {
        this->x = v;
    }

    test_derived1(int v) : x(v) {}
};

struct test_derived2 : cc::interfaces<test_base> {
    const int x;
    int get() const override {
        return this->x + 3;
    }

    test_derived2(int v) : x(v) {}
};

TEST(causal_core_aspect, aspect_get_untyped_get_typed) {
    cc::aspect_ptr r(cc::spawn<int>()); cc::opener o(r);
    cc::aspect_ptr rd(cc::spawn<test_derived1>(5)); cc::opener od(rd);

    cc::essence_ptr<> a_untyped = r.get_ptr()->as_untyped();
    cc::essence_ptr<> ad_untyped = rd.get_ptr()->as_untyped();
    EXPECT_EQ(reinterpret_cast<const void*>(r.get_ptr().get()), reinterpret_cast<const void*>(a_untyped.get()));
    EXPECT_EQ(reinterpret_cast<const void*>(rd.get_ptr().get()), reinterpret_cast<const void*>(ad_untyped.get()));

    cc::essence_ptr<int> a_retyped = a_untyped->as_typed<int>();
    std::shared_ptr<const cc::essence<test_base>> adb_retyped = ad_untyped->as_typed<test_base>();
    std::shared_ptr<const cc::essence<test_derived1>> add_retyped = ad_untyped->as_typed<test_derived1>();
    EXPECT_EQ(reinterpret_cast<const void*>(r.get_ptr().get()), reinterpret_cast<const void*>(a_retyped.get()));
    EXPECT_EQ(reinterpret_cast<const void*>(rd.get_ptr().get()), reinterpret_cast<const void*>(adb_retyped.get()));
    EXPECT_EQ(reinterpret_cast<const void*>(rd.get_ptr().get()), reinterpret_cast<const void*>(add_retyped.get()));

    EXPECT_FALSE(a_untyped->as_typed<double>());
    EXPECT_FALSE(ad_untyped->as_typed<test_derived2>());
}

TEST(causal_core_aspect, aspect_create_get_dispose) {
    cc::essence_ptr<> a;

    {   auto r = cc::spawn<test_derived1>(1); cc::opener o(r);
        EXPECT_FALSE(r.is_certain());
        EXPECT_TRUE(r.create());
        EXPECT_TRUE(r.is_certain());
        EXPECT_EQ(r->x, 1);
        EXPECT_TRUE(r.dispose());
        EXPECT_FALSE(r.is_certain());
        r->x = 0xFFFF0000;
        EXPECT_TRUE(r.is_certain());
        a = r.get_ptr()->as_untyped();
        EXPECT_EQ(r.get_id_as<causal::core::essence<test_derived1>*>(), r.get_ptr().get());
    }

    {   cc::aspect_ptr<test_base> r(a->as_typed<test_base>());
        cc::opener o(r);
        EXPECT_EQ(r->get(), (int)0xFFFF0002);
    }

    {   cc::aspect_ptr<test_derived1> r(a->as_typed<test_derived1>());
        cc::opener o(r);
        EXPECT_EQ(r->x, (int)0xFFFF0000);
    }

    {   cc::aspect_ptr<test_base> r(a->as_typed<test_base>());
        cc::opener o(r);
        EXPECT_TRUE(r.dispose());
        EXPECT_FALSE(r.is_certain());
    }
}

TEST(causal_core_aspect, aspect_get_or_create_dispose) {
    auto r = cc::spawn<int>(5);

    {   cc::opener o(r);
        EXPECT_FALSE(r.is_certain());
        EXPECT_EQ(*r, 5);
        *r = 0xFFFF0000;
    }

    {   auto a = r.get_ptr();
        cc::opener o(r);
        EXPECT_EQ(*a->get(&o), (int)0xFFFF0000);
    }

    {   auto a = r.get_ptr();
        cc::opener o(r);
        EXPECT_TRUE(a->dispose(&o));
        EXPECT_EQ(a->get(&o), nullptr);
    }
}

TEST(causal_core_aspect, aspect_clone_uncertain) {
    cc::aspect_ptr<test_base> r = cc::spawn<test_derived1>(5);

    EXPECT_THROW(cc::clone(r), cc::aspect_ptr_unborrowed);
    cc::opener o1(r);
    EXPECT_FALSE(r.is_certain());

    {   EXPECT_TRUE(r.is_clonable());
        auto c = cc::clone(r);
        cc::opener o2(c);
        EXPECT_FALSE(c.is_certain());

        EXPECT_EQ(r->get(), 7);
        EXPECT_EQ(c->get(), 7);
        EXPECT_NE(&(*r), &(*c));
        r.dispose();
    }

    {   EXPECT_THROW(cc::aspect_ptr_cast<test_derived2>(cc::clone(r)), cc::incompatible_aspect_ptr_cast);
        auto c = cc::aspect_ptr_cast<test_derived1>(cc::clone(r));
        EXPECT_TRUE(c);
        cc::opener o2(c);
        EXPECT_FALSE(c.is_certain());

        EXPECT_EQ(c->x, 5);
    }
}

TEST(causal_core_aspect, aspect_clone_certain) {
    cc::aspect_ptr<test_base> r = cc::forge<test_derived1>(5);

    cc::opener o1(r);
    EXPECT_TRUE(r.is_certain());
    EXPECT_EQ(r->get(), 7);
    r->set(7);

    {   EXPECT_TRUE(r.is_clonable());
        auto c = cc::clone(r);
        cc::opener o2(c);
        EXPECT_TRUE(c.is_certain());

        EXPECT_EQ(r->get(), 9);
        EXPECT_EQ(c->get(), 9);
        EXPECT_NE(&(*r), &(*c));
    }

    {   EXPECT_THROW(cc::aspect_ptr_cast<test_derived2>(cc::clone(r)), cc::incompatible_aspect_ptr_cast);
        auto c = cc::aspect_ptr_cast<test_derived1>(cc::clone(r));
        EXPECT_TRUE(c);
        cc::opener o2(c);
        EXPECT_TRUE(c.is_certain());

        EXPECT_EQ(c->x, 7);
    }
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
