// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <gtest/gtest.h>

#include <causal/core/borrow.hpp>

namespace cc = causal::core;

TEST(causal_core_borrow, atomic_borrowable_borrow) {
    cc::atomic_borrowable b;
    const void* const key1 = reinterpret_cast<void*>(1u),
              * const key2 = reinterpret_cast<void*>(2u);
    EXPECT_TRUE(b.borrow(key1));
    EXPECT_TRUE(b.is_borrowed());
    EXPECT_FALSE(b.is_const_borrowed());
    EXPECT_FALSE(b.borrow(key2));
    EXPECT_FALSE(b.const_borrow(key2));
    EXPECT_FALSE(b.is_const_borrowed());
    b.unborrow(key2);
    EXPECT_TRUE(b.is_borrowed());
    b.unborrow(key1);
    EXPECT_FALSE(b.is_borrowed());
}

TEST(causal_core_borrow, atomic_borrowable_const_borrow) {
    cc::atomic_borrowable b;
    const void* const key1 = reinterpret_cast<void*>(1u),
              * const key2 = reinterpret_cast<void*>(2u),
              * const key3 = reinterpret_cast<void*>(3u);
    EXPECT_TRUE(b.const_borrow(key1));
    EXPECT_TRUE(b.is_const_borrowed());
    EXPECT_FALSE(b.is_borrowed());
    EXPECT_TRUE(b.const_borrow(key2));
    EXPECT_TRUE(b.is_const_borrowed());
    EXPECT_FALSE(b.borrow(key3));
    EXPECT_FALSE(b.is_borrowed());
    // atomic borrowable does not support const borrowing key tracking
    b.unborrow(key1);
    EXPECT_TRUE(b.is_const_borrowed());
    b.unborrow(key2);
    EXPECT_FALSE(b.is_const_borrowed());
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}