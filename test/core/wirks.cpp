// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <vector>
#include <ctime>
#include <memory>

#include <gtest/gtest.h>

#include "causal/core/aspect.hpp"
#include "causal/core/wirks.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"

namespace cc = causal::core;
namespace cp = causal::process;

std::atomic<size_t> ran = false;

void test_func(cc::aspect_ptr<int>& ir1, std::string* ir2, std::string* ir3, cc::aspect_ptr<const std::string>& ir4) {
    ran++;
    EXPECT_TRUE(ir1.get_ptr()->is_borrowed());
    EXPECT_TRUE(ir4.get_ptr()->is_const_borrowed());
    EXPECT_EQ(*ir1, 1);
    EXPECT_EQ(*ir2, "foo");
    EXPECT_EQ(*ir4, "bar");
}
TEST(causal_core_wirks, wirks) {
    ran = false;
    std::string c1 = "foo";
    cc::wirks* w;
    {   cc::aspect_ptr r1(cc::forge<int>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<std::string>("bar")); cc::opener o2(r2);
        w = new cc::wirks(test_func, r1, &c1, r2.get(), r2);
        EXPECT_TRUE(w->focus());
        EXPECT_FALSE(w->borrow());
    }

    EXPECT_TRUE(w->borrow());
    EXPECT_TRUE(!w->trigger());

    delete w;

    EXPECT_TRUE(ran);
}

TEST(causal_core_wirks, act) {
    ran = false;
    std::string c1 = "foo";
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    std::unique_ptr<cc::action> a;
    {   cc::aspect_ptr r1(cc::forge<int>(1)); cc::opener o1(r1);
        cc::aspect_ptr r2(cc::forge<std::string>("bar")); cc::opener o2(r2);
        a = std::make_unique<cc::action>(b, cc::synchron::make(), std::make_unique<cc::wirks>(test_func, r1, &c1, r2.get(), r2));
        EXPECT_FALSE(a->load());
    }

    EXPECT_TRUE(a->load());
    EXPECT_TRUE(a->run());

    a = nullptr;

    EXPECT_TRUE(ran);
}

void branch_test_func5(cc::aspect_ptr<int>& ir1, const std::string* ir2/*, uintptr_t ir3*/) {
    ran++;
    EXPECT_EQ(*ir1, 1);
    EXPECT_EQ(*ir2, "foo");
    EXPECT_TRUE(ir1.get_ptr()->is_borrowed());
    /* TODO 78 EXPECT_TRUE(cc::action::current()->sync);
    EXPECT_FALSE(std::dynamic_pointer_cast<cc::synchrons>(cc::action::current()->sync));
    EXPECT_NE(reinterpret_cast<uintptr_t>(cc::action::current()->sync.get()), ir3);*/
}
void branch_test_func4(cc::aspect_ptr<int>& ir1, const std::string* ir2/*, uintptr_t ir3*/) {
    ran++;
    EXPECT_EQ(*ir1, 1);
    EXPECT_EQ(*ir2, "foo");
    EXPECT_TRUE(ir1.get_ptr()->is_borrowed());
    /* TODO 78 EXPECT_TRUE(cc::action::current()->sync);
    auto fi = std::dynamic_pointer_cast<cc::synchrons>(cc::action::current()->sync);
    EXPECT_TRUE(fi);*/
    //EXPECT_EQ(reinterpret_cast<uintptr_t>(fi->syncs[0]), ir3);
    cc::act_synced({cc::synchron::make()}, branch_test_func5, ir1, ir2/*, ir3*/);
}
void branch_test_func3(cc::aspect_ptr<int>& ir1, const std::string* ir2/*, uintptr_t ir3*/) {
    ran++;
    EXPECT_EQ(*ir1, 1);
    EXPECT_EQ(*ir2, "foo");
    EXPECT_TRUE(ir1.get_ptr()->is_borrowed());
    /* TODO 78 EXPECT_TRUE(cc::action::current()->sync);
    EXPECT_FALSE(std::dynamic_pointer_cast<cc::synchrons>(cc::action::current()->sync));
    EXPECT_NE(reinterpret_cast<uintptr_t>(cc::action::current()->sync.get()), ir3);*/
    cc::act_fwd_synced({cc::synchron::make()}, branch_test_func4, ir1, ir2/*, reinterpret_cast<uintptr_t>(cc::action::current()->sync.get())*/);
}
void branch_test_func2(cc::aspect_ptr<int>& ir1, const std::string* ir2/*, uintptr_t ir3*/) {
    ran++;
    EXPECT_EQ(*ir1, 1);
    EXPECT_EQ(*ir2, "foo");
    EXPECT_TRUE(ir1.get_ptr()->is_borrowed());
    // TODO 78 EXPECT_EQ(reinterpret_cast<uintptr_t>(cc::action::current()->sync.get()), ir3);
    cc::act(branch_test_func3, ir1, ir2/*, ir3*/);
}
void branch_test_func1(cc::aspect_ptr<int>& ir1, const std::string* ir2) {
    ran++;
    EXPECT_EQ(*ir1, 1);
    EXPECT_EQ(*ir2, "foo");
    EXPECT_TRUE(ir1.get_ptr()->is_borrowed());
    /* TODO 78 EXPECT_TRUE(cc::action::current()->sync);
    EXPECT_FALSE(std::dynamic_pointer_cast<cc::synchrons>(cc::action::current()->sync));*/
    cc::act_fwd(branch_test_func2, ir1, ir2/*, reinterpret_cast<uintptr_t>(cc::action::current()->sync.get())*/);
}
TEST(causal_core_wirks, simple_dispatcher) {
    ran = 0;
    const std::string c1 = "foo";
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<int>(1)); cc::opener o1(r1);
        b->act(branch_test_func1, *r1, &c1);
        b->act_synced({cc::synchron::make()}, branch_test_func1, *r1, &c1);
    }

    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_TRUE(d->do_tick());
    EXPECT_FALSE(d->do_tick());

    EXPECT_EQ(ran, 10u);
}

TEST(causal_core_wirks, managed_dispatcher) {
    ran = 0;
    const std::string c1 = "foo";
    auto d = cp::managed_dispatcher<>::make(1, 1);
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<int>(1)); cc::opener o1(r1);
        b->act(branch_test_func1, *r1, &c1);
        b->act_synced({cc::synchron::make()}, branch_test_func1, *r1, &c1);
    }
    
    d->join();

    EXPECT_EQ(ran, 10u);
}

TEST(causal_core_wirks, concurrent_dispatcher) {
    ran = 0;
    const std::string c1 = "foo";
    auto d = cp::concurrent_dispatcher<>::make(1, 1);
    auto b = cc::branch::make(d);
    {   cc::aspect_ptr r1(cc::forge<int>(1)); cc::opener o1(r1);
        b->act(branch_test_func1, *r1, &c1);
        b->act_synced({cc::synchron::make()}, branch_test_func1, *r1, &c1);
    }
    
    d->join();

    EXPECT_EQ(ran, 10u);
}

void Test_vortex(std::string event, cc::aspect_ptr<std::string> &tmp) {
    *tmp = event;
}

TEST(causal_core_wirks, vortex) {
    auto d = cp::concurrent_dispatcher<>::make(1, 1);
    auto b = cc::branch::make(d);
    auto v = cc::vortex<>::make(b);
    const auto tmp = cc::spawn<std::string>();
    v->subscribe<std::string>(Test_vortex, tmp);
    v->trigger(std::string("foo"));

    d->join();

    {   cc::opener o(tmp);
        EXPECT_EQ(*tmp, "foo");
    }
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}