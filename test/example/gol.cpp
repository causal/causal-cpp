// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

//#define VERBOSE
//#define WRITE_FIG

#include <atomic>
#include <set>
#include <map>
#include <functional>
#include <mutex>
#include <memory>
#include <iostream>
#include <fstream>
#ifdef VERBOSE
#include <bits/stdc++.h>
#endif

#include <gtest/gtest.h>

#include "causal/core/wirks.hpp"
#include "causal/sync/unite.hpp"
#include "causal/sync/iterate.hpp"
#include "causal/data/memory.hpp"
#include "causal/data/json.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"
#include "causal/system/observe.hpp"

#include "causal/system/cellular/flipping.hpp"
#include "../../example/gol/gol.hpp"

namespace cc = causal::core;
namespace cy = causal::sync;
namespace cp = causal::process;

#ifdef VERBOSE
std::mutex active_mtx;
std::set<gol_id> active_set;

void print() {
    std::unique_lock l(active_mtx);
    if(!active_set.empty()) {
        int min_x = INT_MIN, max_x = INT_MAX, min_y = INT_MIN, max_y = INT_MAX;
        for(auto i : active_set) {
            if(min_x == INT_MIN || i.x < min_x)
                min_x = i.x;
            if(max_x == INT_MAX || i.x > max_x)
                max_x = i.x;
            if(min_y == INT_MIN || i.y < min_y)
                min_y = i.y;
            if(max_y == INT_MAX || i.y > max_y)
                max_y = i.y;            
        }
        for(int y = min_y; y <= max_y; y++) {
            for(int x = min_x; x <= max_x; x++)
                std::cout << (active_set.find({x,y}) != active_set.end() ? "X" : "o") << " ";
            std::cout << std::endl;
        }
        std::cout << std::endl;
        std::cout.flush();
        active_set.clear();
    }
}
#endif

void Drown(std::shared_ptr<cy::iterator> it, const size_t r_cnt) {
    auto rnd = it->get_round();
    if(rnd < r_cnt*3)
        cc::act_synced(it->iterate(), Drown, it, r_cnt);
    else
        it->lock();

#ifdef VERBOSE
    if(rnd % 3 == 0)
        print();
#endif
}

void Round(std::shared_ptr<cy::iterator> it) {
    if(it->is_locked())
        it->reset();
    else {
        cc::act_synced(it->join(), Round, it);
        EXPECT_TRUE(it->next());
    }
}

template<typename S>
void fill_space(std::shared_ptr<S>& s, const std::set<gol_id>& set,
                const std::shared_ptr<gol_observation>& o = {},
                std::function<void(const gol_id&, cc::aspect_ptr<GolCell<S>>&)> f = {}) {
    for(auto& id : set) {
        auto r = s->template get<GolCell<S>>(id, s, id, o);
        {   cc::opener o(r);
            r->state = true;
            if(f)
                f(id, r);
        }
    }
}

#ifdef WRITE_FIG
void write_fig(const std::string name, const std::set<gol_id>& set){
    auto j = cd::json_junction::make();
    auto s = cd::msgpack_space::make(j);
    s->register_types<GolCell<cd::msgpack_space>>();
    fill_space(s, set);
#ifdef VERBOSE
    std::cout << "|" << name << "|" << std::endl << j->dump(true) << std::endl;
#endif
    std::ofstream fs;
    fs.open(name+".gol");
    fs << j->dump(true);
    fs.close();
}
#endif

void gol_test(std::shared_ptr<cc::branch>& b, std::function<void()> join, const std::set<gol_id>& set, const std::map<gol_id, bool>& expect, const size_t round_cnt) {
    auto obs = gol_observation::make([](const gol_observation::type& v){
#ifdef VERBOSE
        if(v.second) {
            std::unique_lock l(active_mtx);
            active_set.insert(v.first);
        }
#endif
    });

    auto s = gol_space::make();

    std::map<gol_id, cc::aspect_ptr<GolCell<>>> refs;

    fill_space<gol_space>(s, set, obs, [&](const gol_id& id, cc::aspect_ptr<GolCell<>>& r){
        refs.insert({id, r});
    });

    {   auto it = cy::iterator::make();
        b->act_synced(it->iterate(), Drown, it, round_cnt+1);
        b->act_synced(it->join(), Round, it);

        for(auto& r : refs) {
            b->act_synced(it->iterate(), GolCell<>::Trigger, r.second, it);
        }

        it->next();

        join();
    }

    for(auto& e : expect) {        
        auto r = s->get<GolCell<>>(e.first);
        if(r) {
            cc::opener o(r);
            
            //std::cout << "{" << e.first.x << "," << e.first.y << "}\t: " << e.second << "; " << r->state << std::endl;
            EXPECT_EQ(r->state, e.second);
        } else
            EXPECT_FALSE(e.second);
    }
}

#define X true
#define o false

std::set<gol_id> grid_to_set(std::vector<std::vector<bool>> grid) {
    std::set<gol_id> set;
    const int x_len = grid[0].size();
    const int y_len = grid.size();
    for(int y = 0; y < y_len; y++)
        for(int x = 0; x < x_len; x++)
            if(grid[y][x])
                set.insert({x,y});
    return set;
}

std::map<gol_id, bool> grid_to_expect(std::vector<std::vector<bool>> grid) {
    std::map<gol_id, bool> exp;
    const int x_len = grid[0].size();
    const int y_len = grid.size();
    for(int y = -1; y < y_len+1; y++)
        for(int x = -1; x < x_len+1; x++)
            exp[{x,y}] = y == -1 || x == -1 || y == y_len || x == x_len ? false : grid[y][x];
    return exp;
}

#define THREADED_PERIODS 10

std::vector<std::vector<bool>> still_block_fig = {
    {X,X},
    {X,X}
};
const size_t still_block_period = 1;
TEST(causal_system_flipping, still_block_simple) {
#ifdef WRITE_FIG
    write_fig("still_block", grid_to_set(still_block_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(still_block_fig), grid_to_expect(still_block_fig), still_block_period);
    EXPECT_EQ(Cnt, 88u);
}
TEST(causal_system_flipping, still_block_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_block_fig), grid_to_expect(still_block_fig), still_block_period*THREADED_PERIODS);
}
TEST(causal_system_flipping, still_block_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_block_fig), grid_to_expect(still_block_fig), still_block_period*THREADED_PERIODS);
}

std::vector<std::vector<bool>> still_beehive_fig = {
    {o,X,X,o},
    {X,o,o,X},
    {o,X,X,o}
};
const size_t still_beehive_period = 1;
TEST(causal_system_flipping, still_beehive_simple) {
#ifdef WRITE_FIG
    write_fig("still_beehive", grid_to_set(still_beehive_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(still_beehive_fig), grid_to_expect(still_beehive_fig), still_beehive_period);
    EXPECT_EQ(Cnt, 130u);
}
TEST(causal_system_flipping, still_beehive_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_beehive_fig), grid_to_expect(still_beehive_fig), still_beehive_period*THREADED_PERIODS);
}
TEST(causal_system_flipping, still_beehive_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_beehive_fig), grid_to_expect(still_beehive_fig), still_beehive_period*THREADED_PERIODS);
}
    
std::vector<std::vector<bool>> still_loaf_fig = {
    {o,X,X,o},
    {X,o,o,X},
    {o,X,o,X},
    {o,o,X,o}
};
const size_t still_loaf_period = 1;
TEST(causal_system_flipping, still_loaf_simple) {
#ifdef WRITE_FIG
    write_fig("still_loaf", grid_to_set(still_loaf_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(still_loaf_fig), grid_to_expect(still_loaf_fig), still_loaf_period);
    EXPECT_EQ(Cnt, 149u);
}
TEST(causal_system_flipping, still_loaf_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_loaf_fig), grid_to_expect(still_loaf_fig), still_loaf_period*THREADED_PERIODS);
}
TEST(causal_system_flipping, still_loaf_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_loaf_fig), grid_to_expect(still_loaf_fig), still_loaf_period*THREADED_PERIODS);
}

std::vector<std::vector<bool>> still_boat_fig = {
    {X,X,o},
    {X,o,X},
    {o,X,o}
};
const size_t still_boat_period = 1;
TEST(causal_system_flipping, still_boat_simple) {
#ifdef WRITE_FIG
    write_fig("still_boat", grid_to_set(still_boat_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(still_boat_fig), grid_to_expect(still_boat_fig), still_boat_period);
    EXPECT_EQ(Cnt, 111u);
}
TEST(causal_system_flipping, still_boat_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_boat_fig), grid_to_expect(still_boat_fig), still_boat_period*THREADED_PERIODS);
}
TEST(causal_system_flipping, still_boat_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_boat_fig), grid_to_expect(still_boat_fig), still_boat_period*THREADED_PERIODS);
}

std::vector<std::vector<bool>> still_tub_fig = {
    {o,X,o},
    {X,o,X},
    {o,X,o}
};
const size_t still_tub_period = 1;
TEST(causal_system_flipping, still_tub_simple) {
#ifdef WRITE_FIG
    write_fig("still_tub", grid_to_set(still_tub_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(still_tub_fig), grid_to_expect(still_tub_fig), still_tub_period);
    EXPECT_EQ(Cnt, 98u);
}
TEST(causal_system_flipping, still_tub_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_tub_fig), grid_to_expect(still_tub_fig), still_tub_period*THREADED_PERIODS);
}
TEST(causal_system_flipping, still_tub_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(still_tub_fig), grid_to_expect(still_tub_fig), still_tub_period*THREADED_PERIODS);
}

std::vector<std::vector<bool>> osc_blinker_fig = {
    {o,o,o},
    {X,X,X},
    {o,o,o}
};
std::vector<std::vector<bool>> osc_blinker_fig_expect = {
    {o,X,o},
    {o,X,o},
    {o,X,o}
};
const size_t osc_blinker_period = 2;
TEST(causal_system_flipping, osc_blinker_simple) {
#ifdef WRITE_FIG
    write_fig("osc_blinker", grid_to_set(osc_blinker_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(osc_blinker_fig), grid_to_expect(osc_blinker_fig_expect), osc_blinker_period-1);
    EXPECT_EQ(Cnt, 111u);
}
TEST(causal_system_flipping, osc_blinker_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_blinker_fig), grid_to_expect(osc_blinker_fig_expect), osc_blinker_period*(THREADED_PERIODS-1)+(osc_blinker_period-1));
}
TEST(causal_system_flipping, osc_blinker_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_blinker_fig), grid_to_expect(osc_blinker_fig_expect), osc_blinker_period*(THREADED_PERIODS-1)+(osc_blinker_period-1));
}

std::vector<std::vector<bool>> osc_toad_fig = {
    {o,o,o,o},
    {o,X,X,X},
    {X,X,X,o},
    {o,o,o,o}
};
std::vector<std::vector<bool>> osc_toad_fig_expect = {
    {o,o,X,o},
    {X,o,o,X},
    {X,o,o,X},
    {o,X,o,o}
};
const size_t osc_toad_period = 2;
TEST(causal_system_flipping, osc_toad_simple) {
#ifdef WRITE_FIG
    write_fig("osc_toad", grid_to_set(osc_toad_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(osc_toad_fig), grid_to_expect(osc_toad_fig_expect), osc_toad_period-1);
    EXPECT_EQ(Cnt, 194u);
}
TEST(causal_system_flipping, osc_toad_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_toad_fig), grid_to_expect(osc_toad_fig_expect), osc_toad_period*(THREADED_PERIODS-1)+(osc_toad_period-1));
}
TEST(causal_system_flipping, osc_toad_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_toad_fig), grid_to_expect(osc_toad_fig_expect), osc_toad_period*(THREADED_PERIODS-1)+(osc_toad_period-1));
}

std::vector<std::vector<bool>> osc_beacon_fig = {
    {X,X,o,o},
    {X,X,o,o},
    {o,o,X,X},
    {o,o,X,X}
};
std::vector<std::vector<bool>> osc_beacon_fig_expect = {
    {X,X,o,o},
    {X,o,o,o},
    {o,o,o,X},
    {o,o,X,X}
};
const size_t osc_beacon_period = 2;
TEST(causal_system_flipping, osc_beacon_simple) {
#ifdef WRITE_FIG
    write_fig("osc_beacon", grid_to_set(osc_beacon_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(osc_beacon_fig), grid_to_expect(osc_beacon_fig_expect), osc_beacon_period-1);
    EXPECT_EQ(Cnt, 174u);
}
TEST(causal_system_flipping, osc_beacon_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_beacon_fig), grid_to_expect(osc_beacon_fig_expect), osc_beacon_period*(THREADED_PERIODS-1)+(osc_beacon_period-1));
}
TEST(causal_system_flipping, osc_beacon_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_beacon_fig), grid_to_expect(osc_beacon_fig_expect), osc_beacon_period*(THREADED_PERIODS-1)+(osc_beacon_period-1));
}

std::vector<std::vector<bool>> osc_pulsar_fig = {
    {o,o,o,o,X,o,o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,X,o,o,o,X,X,o,o,o,o},
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
    {X,X,X,o,o,X,X,o,X,X,o,o,X,X,X},
    {o,o,X,o,X,o,X,o,X,o,X,o,X,o,o},
    {o,o,o,o,X,X,o,o,o,X,X,o,o,o,o},
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
    {o,o,o,o,X,X,o,o,o,X,X,o,o,o,o},
    {o,o,X,o,X,o,X,o,X,o,X,o,X,o,o},
    {X,X,X,o,o,X,X,o,X,X,o,o,X,X,X},
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
    {o,o,o,o,X,X,o,o,o,X,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o,o,X,o,o,o,o}
};
std::vector<std::vector<bool>> osc_pulsar_fig_expect = {
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
    {o,X,o,o,o,o,X,o,X,o,o,o,o,X,o},
    {o,X,o,o,o,o,X,o,X,o,o,o,o,X,o},
    {o,X,o,o,o,o,X,o,X,o,o,o,o,X,o},
    {o,o,o,X,X,X,o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o,X,X,X,o,o,o},
    {o,X,o,o,o,o,X,o,X,o,o,o,o,X,o},
    {o,X,o,o,o,o,X,o,X,o,o,o,o,X,o},
    {o,X,o,o,o,o,X,o,X,o,o,o,o,X,o},
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o}
};
const size_t osc_pulsar_period = 3;
TEST(causal_system_flipping, osc_pulsar_simple) {
#ifdef WRITE_FIG
    write_fig("osc_pulsar", grid_to_set(osc_pulsar_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(osc_pulsar_fig), grid_to_expect(osc_pulsar_fig_expect), osc_pulsar_period-1);
    EXPECT_EQ(Cnt, 2034u);
}
TEST(causal_system_flipping, osc_pulsar_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_pulsar_fig), grid_to_expect(osc_pulsar_fig_expect), osc_pulsar_period*(THREADED_PERIODS-1)+(osc_pulsar_period-1));
}
TEST(causal_system_flipping, osc_pulsar_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_pulsar_fig), grid_to_expect(osc_pulsar_fig_expect), osc_pulsar_period*(THREADED_PERIODS-1)+(osc_pulsar_period-1));
}

std::vector<std::vector<bool>> osc_pentadecathlon_fig = {
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,o,o,o,o,o,o}
};
std::vector<std::vector<bool>> osc_pentadecathlon_fig_expect = {
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,o,o,o,o,o,o},
    {o,o,o,X,X,X,o,o,o},
    {o,o,o,o,X,o,o,o,o},
    {o,o,o,o,X,o,o,o,o}
};
const size_t osc_pentadecathlon_period = 15;
TEST(causal_system_flipping, osc_pentadecathlon_simple) {
#ifdef WRITE_FIG
    write_fig("osc_pentadecathlon", grid_to_set(osc_pentadecathlon_fig));
#endif

    auto d = cp::simple_dispatcher<>::make(); auto b = cc::branch::make(d);
    size_t Cnt = 0;
    gol_test(b, [&](){
        while(d->do_tick())
            Cnt++;
    }, grid_to_set(osc_pentadecathlon_fig), grid_to_expect(osc_pentadecathlon_fig_expect), osc_pentadecathlon_period-1);
    EXPECT_EQ(Cnt, 5250u);
}
TEST(causal_system_flipping, osc_pentadecathlon_manged) {
    auto d = cp::managed_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_pentadecathlon_fig), grid_to_expect(osc_pentadecathlon_fig_expect), osc_pentadecathlon_period*(THREADED_PERIODS-1)+(osc_pentadecathlon_period-1));
}
TEST(causal_system_flipping, osc_pentadecathlon_threaded) {
    auto d = cp::concurrent_dispatcher<>::make(); auto b = cc::branch::make(d);
    gol_test(b, [&](){
        d->join();
    }, grid_to_set(osc_pentadecathlon_fig), grid_to_expect(osc_pentadecathlon_fig_expect), osc_pentadecathlon_period*(THREADED_PERIODS-1)+(osc_pentadecathlon_period-1));
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
