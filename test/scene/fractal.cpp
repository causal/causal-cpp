// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <vector>
#include <memory>

#include <gtest/gtest.h>

#include "causal/core/wirks.hpp"
#include "causal/process/simple.hpp"
#include "causal/process/thread.hpp"

namespace cc = causal::core;
namespace cp = causal::process;

struct Node {
    std::vector<cc::aspect_ptr<Node>> childs;
    size_t value = 0;

    std::vector<cc::aspect_ptr<Node>> recurse() {
        cc::key key;
        std::vector<cc::aspect_ptr<Node>> all;
        for(cc::aspect_ptr<Node>& r : this->childs) {
            all.push_back(r);
            r.focus(&key);
            EXPECT_TRUE(r.borrow(&key));
            auto cc = (*r).recurse();
            all.reserve(all.size() + cc.size());
            all.insert(all.end(), cc.begin(), cc.end());
            r.unborrow(&key);
            r.release(&key);
        }

        return all;
    }
    
    /** | it | next | active | total childs | total ticks |
     *  |  0 |    1 |      1 |            1 |           1 |
     *  |  1 |    2 |      2 |            3 |           2 |
     *  |  2 |    3 |      6 |            9 |           4 |
     *  |  3 |    4 |     24 |           33 |          10 |
     *  |  4 |    5 |    120 |          153 |          34 |
     *  |  5 |    6 |    720 |          873 |         154 |
     *  |  6 |    7 |   5040 |         5913 |         874 |
    */
    static void fork1(Node& me, size_t it) {
        me.value = it;
        size_t next = it+1;

        if(it < 7) for(size_t i = 0; i < next; i++) {
            auto c = cc::spawn<Node>();
            me.childs.push_back(c);
            cc::act_fwd(Node::fork1, c, next);
        }
    }
};

TEST(causal_core_scene_fractal, entangle_measure_fork1) {
    u_long Cnt = 0;
    cc::key key;
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);

    cc::aspect_ptr<Node> root(cc::spawn<Node>());
    
    b->act_fwd(Node::fork1, root, 0);
    while(auto cnt = d->do_tick()) {
        Cnt += cnt;
        if(Cnt >= 874)
            break;
    }
    EXPECT_EQ(Cnt, 874u);
    root.focus(&key);
    EXPECT_TRUE(root.borrow(&key));
    EXPECT_EQ((*root).recurse().size(), 5913u);
    root.unborrow(&key);
    root.release(&key);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
