// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <memory>
#include <cstdlib>
#include <ctime>
#include <math.h>

#include <gtest/gtest.h>

#include "causal/core/wirks.hpp"
#include "causal/process/simple.hpp"

namespace cc = causal::core;
namespace cp = causal::process;

double fRand(double fMin, double fMax) {
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

struct Photon {
    cc::aspect_ptr<double> polarization;
    cc::aspect_ptr<bool> entangled;
    double factor;
    
    static void dice(cc::aspect_ptr<double>& pol, cc::aspect_ptr<const bool>& e) {
        if(*e) {
            auto num = fRand(-M_PI, M_PI);
            *pol = num;

            cc::act_fwd(Photon::dice, pol, e);
        }
    }

    static void entangle(Photon& ph1, Photon& ph2) {
        ph1.entangled = ph2.entangled = cc::forge<bool>(true);
        ph2.polarization = ph1.polarization = cc::spawn<double>();
        ph1.factor = 1.0;
        ph2.factor = -1.0;

        cc::act_fwd(Photon::dice, ph1.polarization, ph1.entangled);
    }

    static void _measure(Photon& ph, double& meas, const double& pol, bool& e) {        
        meas = pol*ph.factor;

        // breaking entanglement
        if(e) {
            e = false;
            ph.entangled = cc::forge<bool>(false);
            ph.polarization = cc::forge<double>(pol);
        }
    }

    static void measure(cc::aspect_ptr<const Photon>& ph, cc::aspect_ptr<const double>& meas) {
        cc::act_fwd(Photon::_measure, ph, meas, ph->polarization, ph->entangled);
    }

    static void _set(double& value, double& pol) {
        pol = value;
    }

    static void set(const Photon& ph, cc::aspect_ptr<const double>& value) {
        cc::act_fwd(Photon::_set, value, ph.polarization);
    }
};

TEST(causal_core_scene_entanglement, entangle_measure) {
    auto d = cp::simple_dispatcher<>::make();
    auto b = cc::branch::make(d);
    
    cc::aspect_ptr<Photon> p1(cc::spawn<Photon>()),
                    p2((cc::spawn<Photon>()));
    cc::aspect_ptr<double> m1(cc::spawn<double>()),
                    m2(cc::spawn<double>()),
                    m3(cc::spawn<double>());
    
    b->act_fwd(Photon::entangle, p1, p2);

    clock_t start = clock();
    while(start + CLOCKS_PER_SEC/10 > clock())
        EXPECT_TRUE(d->do_tick());
    b->act_fwd(Photon::measure, p1, m1);
    while(d->do_tick()); // ensure measurement beeing done
    b->act_fwd(Photon::set, p1, cc::aspect_ptr<double>(cc::forge<double>(4.0)));
    b->act_fwd(Photon::measure, p1, m3);
    while(d->do_tick()); // ensure setting and measuring new value is done
    b->act_fwd(Photon::measure, p2, m2);
    while(d->do_tick()); // tick scenario to the end
    
    cc::key key;
    m1.focus(&key);
    EXPECT_TRUE(m1.borrow(&key));
    m2.focus(&key);
    EXPECT_TRUE(m2.borrow(&key));
    m3.focus(&key);
    EXPECT_TRUE(m3.borrow(&key));
    // check if measured the correct values when entanglement got broken and when second phton was measured
    // this might fail if entanglement would persist and consequtive set operation would inflict second photon
    EXPECT_EQ(*m1, -*m2);
    EXPECT_EQ(*m3, double(4.0));
    m1.unborrow(&key);
    m1.release(&key);
    m2.unborrow(&key);
    m2.release(&key);
    m3.unborrow(&key);
    m3.release(&key);
}

int main(int argc, char** argv){testing::InitGoogleTest(&argc, argv); return RUN_ALL_TESTS();}
