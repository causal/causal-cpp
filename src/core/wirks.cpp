// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/core/wirks.hpp"
#include "causal/trace.hpp"

#include <cassert>
#include <cstddef>

#include <mutex>
#include <memory>
#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <functional>
#include <algorithm>

namespace causal {
    namespace core {
        synchron::synchron(synchron_state init_state) : state(init_state) {}

        void synchron::change(synchron_state new_state) noexcept {
            if(this->state.exchange(new_state) < new_state) {
                std::set<const void*> to_deregister;
                {   std::shared_lock l(this->callbacks_mtx);

                    for(auto it : this->callbacks)
                        if(it.second(*this))
                            to_deregister.insert(it.first);
                }

                if(!to_deregister.empty()) {
                    std::unique_lock l(this->callbacks_mtx);
                    for(auto ptr : to_deregister)
                        this->callbacks.erase(ptr);
                } 
            }
        }

        void synchron::register_callback(const void* const ptr, std::function<bool(synchron&)>&& fn) noexcept {
            ASSERT_MSG(scope_core, ptr, "registering a synchron callback with adress 0 certainly is a bug")
            std::unique_lock l(this->callbacks_mtx);
            this->callbacks[ptr] = fn;
        }
        
        void synchron::deregister_callback(const void* const ptr) noexcept {
            std::unique_lock l(this->callbacks_mtx);
            this->callbacks.erase(ptr);
        }

        bool synchron::is_possible() const noexcept {return this->state < synchron_state::impossible;}
        bool synchron::is_feasible() const noexcept {return this->state == synchron_state::feasible;}

        std::shared_ptr<synchron> synchron::make() {
            return std::shared_ptr<synchron>(new synchron());
        }

        std::vector<access_*> wirks::get_accessor_clones() const {
            std::vector<access_*> as(this->accessors.size());
            for(size_t i = 0; i < this->accessors.size(); i++)
                as[i] = this->accessors[i]->clone();
            return as;
        }

        wirks::wirks(const wirks& o)
        : tick(o.tick), accessors(o.get_accessor_clones()), spark(o.spark) {
            ASSERT_MSG(scope_core, !this->focused, "wirks was already focused")
        }

        wirks::wirks(wirks&& o)
        : tick(std::move(o.tick)), accessors(std::move(o.accessors)), spark(std::move(o.spark)) {}

        wirks::~wirks() {
            if(this->tick && this->focused)
                for(auto& a : this->accessors) {
                    a->release(this);
                    delete a;
                }
        }
        
        bool wirks::focus() noexcept {
            ASSERT_MSG(scope_core, !this->focused, "wirks was already focused")
            for(auto it = this->accessors.begin(); it != this->accessors.end(); ++it)
                (*it)->focus(this);
            this->focused = true;
            return true;
        }

        bool wirks::is_focused() const noexcept {
            return this->focused;
        }

        bool wirks::borrow() const noexcept {
            ASSERT_MSG(scope_core, this->focused, "wirks was not focused")
            for(auto it = this->accessors.begin(); it != this->accessors.end(); ++it) {
                access_* a1 = *it;
                if(!a1->borrow(this)) {
                    if(it != this->accessors.begin())
                        for (auto bit = it; bit != this->accessors.begin();) {
                            access_* a2 = *(--bit);
                            a2->unborrow(this, true);
                        }
                    return false;
                }
            }
            return true;
        }

        std::exception_ptr wirks::trigger() const noexcept {
            ASSERT_MSG(scope_core, this->focused, "wirks was not focused")
            // do tick
            std::exception_ptr exception;
            try {
                this->spark(this);
            } catch(trace::exception_warning& ex) {
                TRACE_WARNING(scope_action, ex.stacktrace) << ex.what();
                exception = std::current_exception();
            } catch(trace::exception_error& ex) {
                TRACE_ERROR(scope_action, ex.stacktrace) << ex.what();
                exception = std::current_exception();
            } catch(trace::exception_fatal& ex) {
                TRACE_FATAL(scope_action, ex.stacktrace) << ex.what();
                exception = std::current_exception();
                std::abort();
            } catch(std::exception& ex) {
                TRACE_ERROR(scope_action, "no stacktrace available") << "unexpected exception: " << ex.what();
                exception = std::current_exception();
            } catch(...) {
                TRACE_ERROR(scope_action, "no stacktrace available") << "unexpected unknown exception";
                exception = std::current_exception();
            }

            for(auto& a : this->accessors)
                a->unborrow(this, static_cast<bool>(exception));

            return exception;
        }
        
        std::vector<std::unique_ptr<wirks>> wirks_set::clone() const {
            std::vector<std::unique_ptr<wirks>> ws(this->size());
            for(size_t i = 0; i < this->size(); i++)
                ws[i] = std::make_unique<wirks>(*(*this)[i]);
            return ws;
        }

        wirks_set::wirks_set(const wirks_set& o)
        : std::vector<std::unique_ptr<wirks>>(o.clone()) {}

        void wirks_set::add(std::unique_ptr<wirks>&& w) noexcept {
            this->push_back(std::forward<std::unique_ptr<wirks>>(w));
        }

        void wirks_set::apply(std::function<void(wirks&)> f) noexcept {
            for(auto& wp : *this)
                f(*wp);
        }

        void wirks_set::apply(std::function<void(const wirks&)> f) const noexcept {
            for(auto& wp : *this)
                f(*wp);
        }

        synchrons::synchrons(const synchrons& o)
        : synchron(synchron_state::possible), syncs(o.syncs) {
            this->init();
        }

        synchrons::synchrons(synchrons&& o)
        : synchron(o.get_state()), syncs(o.syncs) {
            this->init();
        }

        synchrons::synchrons(std::list<std::shared_ptr<synchron>> s)
        : synchron(synchron_state::possible), syncs(s) {
            this->init();
        }

        void synchrons::init() {
            if(!this->syncs.empty()) {
                for(auto& s : this->syncs) {
                    this->eval(s->get_state());

                    s->register_callback(this, [this, s](synchron&) {
                        this->eval(s->get_state());
                        return false;
                    });
                }
            } else
                this->change(synchron_state::feasible);
        }

        synchrons::~synchrons() {
            for(auto& s : this->syncs)
                s->deregister_callback(this);
        }

        synchrons synchrons::operator*() const {
            std::list<std::shared_ptr<synchron>> s;
            for(auto c : this->syncs)
                if(dynamic_cast<const fwd_synchron*>(&*c) != nullptr || this->is_possible())
                    s.push_back(c);

            return synchrons(s);
        }

        synchrons synchrons::operator+(const synchrons& o) const {
            auto s = this->syncs;
            for(auto os : o.syncs)
                if(std::find(s.begin(), s.end(), os) == s.end())
                    s.push_back(os);
            
            return synchrons(s);
        }

        synchrons synchrons::operator|(const synchrons& o) const {
            std::list<std::shared_ptr<synchron>> s;
            for(auto c : this->syncs)
                if(dynamic_cast<const fwd_synchron*>(&*c) != nullptr || this->is_possible())
                    s.push_back(c);

            for(auto os : o.syncs)
                if(dynamic_cast<const fwd_synchron*>(&*os) != nullptr || os->is_possible())
                    if(std::find(s.begin(), s.end(), os) == s.end())
                        s.push_back(os);

            return synchrons(s);
        }

        void synchrons::loaded(const action *a) {
            for(auto c : this->syncs)
                c->loaded(a);
        }

        void synchrons::finished(const action *a) {
            for(auto c : this->syncs)
                c->finished(a);
        }

        void synchrons::eval(synchron_state s) {
            switch(s) {
                case synchron_state::feasible:
                    if(this->syncs.size() == ++this->feasible_cnt)
                        this->change(s);
                    break;
                case synchron_state::impossible:
                    this->change(s);
                    break;
                default:
                    break;
            }
        }

        std::shared_ptr<synchrons> synchrons::make(std::list<std::shared_ptr<synchron>> s) {
            return std::shared_ptr<synchrons>(new synchrons(std::move(s)));
        }

        thread_local const action* action::executing = nullptr;

        action::action(const std::shared_ptr<branch> c, synchrons&& s, std::unique_ptr<wirks>&& w)
        : act(std::forward<std::unique_ptr<wirks>>(w)),
          context(c),
          sync(new synchrons(std::forward<synchrons>(s)))
        {
            this->act->focus();
        }

        action::~action() {
            const auto cnt = --this->context->cnt_actions;
            std::shared_lock l(this->context->callbacks_mtx);
            for(auto& cb : this->context->callbacks)
                cb.second(*this->context, cnt);
        }

        bool action::has_current()
        {return executing != nullptr;} 

        const action& action::current() {
            if(executing != nullptr)
                return *executing;
            else
                INTERN_THROW_FATAL(scope_core, attepmt_to_imply_branch_outside_act());
        }

        synchrons action::current_syncs()
        {return has_current() ? *executing->sync : synchrons{};}

        bool action::is_loaded() const noexcept
        {return this->loaded;}

        bool action::load() noexcept {
            bool exp = false;
            if(this->loaded.compare_exchange_strong(exp, true)) {
                // there might be benefit in borrowing before asking for feasability, but it would be expensive
                if(this->sync->is_feasible() && this->act->borrow()) {
                    this->sync->loaded(this);
                    return true;
                } else {
                    this->loaded = false;
                    return false;
                }
            } else {
                return false;
            }
        }

        bool action::run() noexcept {
            if(this->context) {
                action::executing = this;
                this->exception = this->act->trigger();
                
                action::executing = nullptr;

                this->sync->finished(this);

                if(this->exception) {
                    std::unique_lock l(this->context->exceptions_mtx);
                    this->context->exceptions.push_back(this->exception);
                    return false;
                } else
                    return true;
            } else
                return false;
        }

        action_set::action_set(const std::shared_ptr<branch> c, synchrons&& s, const wirks_set& ws) {
            for(auto& wp : ws)
                this->push_back(std::make_unique<action>(std::shared_ptr<branch>(c), synchrons(s), std::make_unique<wirks>(*wp)));
        }

        action_set::action_set(const std::shared_ptr<branch> c, synchrons&& s, wirks_set&& ws) {
            for(auto& wp : ws)
                this->push_back(std::make_unique<action>(std::shared_ptr<branch>(c), synchrons(s), std::move(wp)));
        }

        branch::branch(const std::shared_ptr<dispatcher> d) : dispatch(d) {
            ASSERT_MSG(scope_core, this->dispatch, "trying to create branch without valid dispatcher")
        }

        branch::~branch() {
            std::shared_lock l(this->callbacks_mtx);
            for(auto& cb : this->callbacks)
                cb.second(*this, 0);
        }
        
        std::shared_ptr<branch> branch::make(const std::shared_ptr<dispatcher> d) {
            return std::shared_ptr<branch>(new branch(d));
        }

        void branch::reg(const void* id, std::function<void(branch&, const size_t)> cb) {
            std::unique_lock l(this->callbacks_mtx);
            this->callbacks[id] = cb;
        }

        void branch::dereg(const void* id) {
            std::unique_lock l(this->callbacks_mtx);
            this->callbacks.erase(id);
        }

        bool branch::alive() const noexcept {
            return this->cnt_actions > 0;
        }

        const std::list<std::exception_ptr> branch::flush_exceptions() {
            std::unique_lock l(this->exceptions_mtx);
            auto exceptions = this->exceptions;
            this->exceptions.clear();
            return exceptions;
        }

        const std::list<std::exception_ptr>& get_exceptions() noexcept;

        bool branch::enqueue(std::unique_ptr<action>&& a) {
            if(a->sync->is_possible()) {
                const auto cnt = ++this->cnt_actions;
                {   std::shared_lock l(this->callbacks_mtx);
                    for(auto& cb : this->callbacks)
                        cb.second(*this, cnt);
                }
                this->dispatch->enqueue(std::forward<std::unique_ptr<action>>(a));
                return true;
            }
            return false;
        }

        bool branch::enqueue_set(action_set&& as) {
            for(auto& ap : as)
                if(!ap->sync->is_possible())
                    return false;

            for(auto& ap : as) {
                const auto cnt = ++this->cnt_actions;
                {   std::shared_lock l(this->callbacks_mtx);
                    for(auto& cb : this->callbacks)
                        cb.second(*this, cnt);
                }
                this->dispatch->enqueue(std::move(ap));
            }

            return true;
        }

        void branch::act(std::unique_ptr<wirks>&& w) {
            this->enqueue(std::make_unique<action>(
                this->shared_from_this(), synchrons{},
                std::forward<std::unique_ptr<wirks>>(w)));
        }

        void branch::act(wirks_set&& ws) {
            this->enqueue_set(action_set(
                this->shared_from_this(), {}, 
                std::forward<wirks_set>(ws)));
        }

        bool branch::act_synced(synchrons&& s, std::unique_ptr<wirks>&& w) {
            return this->enqueue(std::make_unique<action>(
                this->shared_from_this(),
                std::forward<synchrons>(s),
                std::forward<std::unique_ptr<wirks>>(w)));
        }

        bool branch::act_synced(synchrons&& s, wirks_set&& ws) {
            return this->enqueue_set(action_set(
                this->shared_from_this(),
                std::forward<synchrons>(s),
                std::forward<wirks_set>(ws)));
        }

        bool branch::act_fwd(std::unique_ptr<wirks>&& w) {
            return this->enqueue(std::make_unique<action>(
                this->shared_from_this(),
                *action::current_syncs(),
                std::forward<std::unique_ptr<wirks>>(w)));
        }

        bool branch::act_fwd(wirks_set&& ws) {
            return this->enqueue_set(action_set(
                this->shared_from_this(),
                *action::current_syncs(),
                std::forward<wirks_set>(ws)));
        }

        bool branch::act_fwd_synced(synchrons&& s, std::unique_ptr<wirks>&& w) {
            return this->enqueue(std::make_unique<action>(
                this->shared_from_this(),
                action::current_syncs() | s,
                std::forward<std::unique_ptr<wirks>>(w)));
        }

        bool branch::act_fwd_synced(synchrons&& s, wirks_set&& ws) {            
            return this->enqueue_set(action_set(
                this->shared_from_this(),
                action::current_syncs() | s,
                std::forward<wirks_set>(ws)));
        }
    }
}