// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/sync/iterate.hpp"
#include <memory>

namespace causal {
    namespace sync {
        iterator::iterator() : share(boost::make_shared<common>()) {}

        iterator::~iterator() {
            this->invalidate();
        }

        std::shared_ptr<iterator> iterator::make() {
            return std::shared_ptr<iterator>(new iterator());
        }

        std::shared_ptr<cc::synchrons> iterator::iterate() const {
            auto s = this->share.load();
            std::shared_lock l(s->mtx);
            // if modifying mind destruction order since dependency needs to be destructed before iterated
            return cc::synchrons::make(s->next_iterated, s->dep.on_branch());
        }

        std::shared_ptr<cc::synchrons> iterator::join() const {
            auto s = this->share.load();
            std::shared_lock l(s->mtx);
            return cc::synchrons::make(s->dep.depend(), s->next_joined);
        }

        void iterator::lock() {
            auto s = this->share.load();
            std::unique_lock l(s->mtx);
            s->locked = true;
        }

        void iterator::unlock() {
            auto s = this->share.load();
            std::unique_lock l(s->mtx);
            s->locked = false;
        }

        bool iterator::is_locked() const {
            auto s = this->share.load();
            std::shared_lock l(s->mtx);
            return s->locked;
        }

        bool iterator::next() const {
            auto s = this->share.load();
            std::unique_lock l(s->mtx);
            if(!s->locked && s->act_iterated.expired()) {
                auto n_iterated = s->next_iterated;
                auto n_joined = s->next_joined;
                s->dep = depender();
                s->act_iterated = n_iterated;
                s->act_joined = n_joined;
                s->next_iterated = std::shared_ptr<iterated>(new iterated());
                s->next_joined = std::shared_ptr<joined>(new joined());
                s->round++;

                n_iterated->change(cc::synchron_state::feasible);
                n_joined->change(cc::synchron_state::feasible);

                return true;
            } else
                return false;
        }

        size_t iterator::get_round() const {
            auto s = this->share.load();
            std::shared_lock l(s->mtx);
            return s->round - 1;
        }

        void iterator::invalidate() {
            auto s = this->share.load();
            std::unique_lock l(s->mtx);
            s->round = 0;

            if(auto a = s->act_iterated.lock())
                a->change(cc::synchron_state::impossible);

            if(auto a = s->act_joined.lock())
                a->change(cc::synchron_state::impossible);
            
            s->next_iterated->change(cc::synchron_state::impossible);

            s->next_joined->change(cc::synchron_state::impossible);
        }

        void iterator::reset() {
            this->invalidate();
            this->share = boost::make_shared<common>();
        }

        joined::joined()
        : synchron(cc::synchron_state::possible)
        {}

        iterated::iterated()
        : synchron(cc::synchron_state::possible)
        {}
    }
}