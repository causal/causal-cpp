// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/sync/unite.hpp"

namespace causal {
    namespace sync {
        unifier_front::unifier_front(const unifier_front& o) : active(o.active) {}
        
        unifier_front::unifier_front()
        : active(std::make_shared<std::atomic<bool>>(false))
        {}

        unifier_front& unifier_front::operator=(const unifier_front& o) {
            this->active = o.active;
            return *this;
        }

        std::shared_ptr<united<unifier_front>> unifier_front::make() const {
            return std::shared_ptr<united<unifier_front>>(new united<unifier_front>(*this));
        }
        
        unifier_back::unifier_back(const unifier_back& o) : share(o.share) {}

        unifier_back::unifier_back()
        : share(std::make_shared<common>())
        {}

        unifier_back& unifier_back::operator=(const unifier_back& o) {
            this->share = o.share;
            return *this;
        }

        std::shared_ptr<united<unifier_back>> unifier_back::make() const {
            return std::shared_ptr<united<unifier_back>>(new united<unifier_back>(*this));
        }
    }
}