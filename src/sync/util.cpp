// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/sync/util.hpp"
#include "causal/core/wirks.hpp"
#include "causal/trace.hpp"
#include <exception>
#include <mutex>
#include <string>

namespace causal {
    namespace sync {
        depender::depender(depender&& o) : dep(o.dep) {}

        depender::depender(const depender& o) : dep(o.dep) {}

        depender::depender() : dep(std::shared_ptr<dependant>(new dependant())) {}

        depender& depender::operator=(depender&& o) {
            this->dep = o.dep;
            return *this;
        }

        depender& depender::operator=(const depender& o) {
            this->dep = o.dep;
            return *this;
        }

        std::shared_ptr<dependant> depender::depend() const {
            return this->dep;
        }

        std::shared_ptr<dependency> depender::on() const {
            if(this->dep->get_state() < cc::synchron_state::feasible)
                return std::shared_ptr<dependency>(new dependency(*this));
            else
                INTERN_THROW_ERROR(scope_sync, dependant_already_feasible())
        }

        std::shared_ptr<branch_dependency> depender::on_branch() const {
            if(this->dep->get_state() < cc::synchron_state::feasible)
                return std::shared_ptr<branch_dependency>(new branch_dependency(*this));
            else
                INTERN_THROW_ERROR(scope_sync, dependant_already_feasible())
        }

        dependant::dependant()
        : synchron(cc::synchron_state::possible)
        {}

        void dependant::loaded(const cc::action *a) {
            this->change(cc::synchron_state::impossible);
        }

        void anchor::dispose() {
            this->change(cc::synchron_state::impossible);
        }

        ground::~ground() {
            for(const auto& a_wptr : this->anchors) {
                if(auto ptr = a_wptr.lock())
                    ptr->dispose();
            }
        }

        std::shared_ptr<anchor> ground::drop() {
            auto ptr = std::shared_ptr<anchor>(new anchor());
            this->anchors.push_back(ptr);
            return ptr;
        }

        sync_ground::~sync_ground() {
            std::unique_lock lk(this->mtx);
            for(const auto& a_wptr : this->anchors) {
                if(auto ptr = a_wptr.lock())
                    ptr->dispose();
            }
        }

        std::shared_ptr<anchor> sync_ground::drop() {
            auto ptr = std::shared_ptr<anchor>(new anchor());
            std::unique_lock lk(this->mtx);
            this->anchors.push_back(ptr);
            return ptr;
        }

        await::await(std::shared_ptr<cc::branch> b) : synchron(b->alive() ? cc::synchron_state::possible : cc::synchron_state::feasible), branch(b) {
            b->reg(this, [this](cc::branch& b, const size_t cnt) {
                if(!cnt)
                    this->change(cc::synchron_state::feasible);
            });
        }
        
        await::~await() {
            bool exp = false;
            if(this->done.compare_exchange_strong(exp, true))
                if(auto b = this->branch.lock())
                    b->dereg(this);
        }

        void await::loaded(const cc::action *const a) {
            bool exp = false;
            if(this->done.compare_exchange_strong(exp, true))
                if(auto b = this->branch.lock())
                    b->dereg(this);
            this->change(cc::synchron_state::impossible);
        }

        std::shared_ptr<await> await::make(std::shared_ptr<cc::branch> b) {
            return std::shared_ptr<await>(new await(b));
        }

        catcher::catcher(std::shared_ptr<cc::branch> b, std::unique_ptr<cc::wirks> wp)
        : synchron(cc::synchron_state::feasible), branch(b), act(std::move(wp)) {}

        void catcher::finished(const cc::action *const a) {
            const std::exception_ptr exc = a->get_exception();
            const auto b = this->branch.lock();
            
            if(b && exc) {
                auto nw = std::unique_ptr<cc::wirks>(new cc::wirks(*this->act));
                auto a = new cc::access<const std::exception_ptr&, const std::exception_ptr>(exc);
                if(a) {
                    nw->template set_accessor<0>(a);
                }
                b->act(std::move(nw));
            }
        }
    }
}