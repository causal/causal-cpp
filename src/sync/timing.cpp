// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/sync/timing.hpp"

namespace causal {
    namespace sync {
        delay_ms::delay_ms(u_long ms)
        : synchron(cc::synchron_state::possible),
          until(std::chrono::high_resolution_clock::now()+std::chrono::milliseconds(ms)) {
            if(std::chrono::high_resolution_clock::now() < this->until)
                this->thr = std::thread(&delay_ms::waiter, this);
            else
                this->done = true;
        }

        delay_ms::~delay_ms() {
            if(!this->done) {
                this->destroy = true;
                std::unique_lock lcv(this->mtx_cv);
                this->cv_wait.notify_one();
                this->cv_destroy.wait(lcv, [&](){
                    return this->done;
                });
            }
        }
        
        void delay_ms::waiter() {
            std::unique_lock lcv(this->mtx_cv);
            this->cv_wait.wait_until(lcv, this->until, [&](){
                return this->destroy || this->until <= std::chrono::high_resolution_clock::now();
            });

            if(!this->destroy)
                this->change(cc::synchron_state::feasible);

            this->thr.detach();

            this->done = true;
            this->cv_destroy.notify_one();
        }

        void delay_ms::loaded(const cc::action *a) {
            this->change(cc::synchron_state::impossible);
        }

        std::shared_ptr<delay_ms> delay_ms::make(u_long ms) {
            return std::shared_ptr<delay_ms>(new delay_ms(ms));
        }

        expire_ms::expire_ms(u_long ms)
        : synchron(cc::synchron_state::feasible),
          until(std::chrono::high_resolution_clock::now()+std::chrono::milliseconds(ms)) {
            if(std::chrono::high_resolution_clock::now() < this->until)
                this->thr = std::thread(&expire_ms::waiter, this);
            else
                this->done = true;
        }

        expire_ms::~expire_ms() {
            if(!this->done) {
                this->destroy = true;
                std::unique_lock lcv(this->mtx_cv);
                this->cv_wait.notify_one();
                this->cv_destroy.wait(lcv, [&](){
                    return this->done;
                });
            }
        }
        
        void expire_ms::waiter() {
            std::unique_lock lcv(this->mtx_cv);
            this->cv_wait.wait_until(lcv, this->until, [&](){
                return this->destroy || this->until <= std::chrono::high_resolution_clock::now();
            });

            if(!this->destroy)
                this->change(cc::synchron_state::impossible);

            this->thr.detach();

            this->done = true;
            this->cv_destroy.notify_one();
        }

        void expire_ms::loaded(const cc::action *a) {
            this->change(cc::synchron_state::impossible);
        }

        std::shared_ptr<expire_ms> expire_ms::make(u_long ms) {
            return std::shared_ptr<expire_ms>(new expire_ms(ms));
        }
    }
}