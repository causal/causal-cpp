// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <memory>
#include <sstream>

#include "causal/data/crypto.hpp"
#include "causal/data/opendht.hpp"

namespace causal {
    namespace data {
        INIT_TRACE_SCOPE(scope_opendht, "opendht")

        std::string opendht_sha_hasher::hash(const std::string& data, u_short hash_length) const {
            dht::Blob d(data.begin(), data.end());
            auto h = dht::crypto::hash(d, hash_length);
            return std::string(h.begin(), h.end());
        }

        opendht_crypto_provider::opendht_crypto_provider(const opendht_crypto_provider& o) {
            std::shared_lock l(o.mtx);
            this->identities = o.identities;
            this->certificates = o.certificates;
        }

        opendht_crypto_provider::opendht_crypto_provider(opendht_crypto_provider&& o) {
            std::unique_lock l(o.mtx);
            this->identities = o.identities;
            this->certificates = o.certificates;
        }
        
        const hasher& opendht_crypto_provider::get_hasher() const {
            static const opendht_sha_hasher h;
            return h;
            //return this->hasher_;
        }

        dht::crypto::Identity opendht_crypto_provider::add_internal(const Identity& id) {
            dht::Blob priv_blob(id.data.begin(), id.data.end());
            dht::Blob crt_blob(id.certificate.data.begin(), id.certificate.data.end());
            return {
                std::make_shared<dht::crypto::PrivateKey>(priv_blob),
                std::make_shared<dht::crypto::Certificate>(crt_blob)
            };
        }

        dht::crypto::PublicKey opendht_crypto_provider::add_internal(const PublicIdentity& id) {
            dht::Blob pk_blob(id.pkey.begin(), id.pkey.end());
            return pk_blob;
        }

        dht::crypto::Certificate opendht_crypto_provider::add_internal(const Certificate& crt) {
            dht::Blob crt_blob(crt.data.begin(), crt.data.end());
            return crt_blob;
        }

        IdentityId opendht_crypto_provider::add(const Identity id) {
            auto dht_id = this->add_internal(id);
            std::unique_lock l(this->mtx);
            if(this->identities.find(id) == this->identities.end())
                this->identities.insert({id, dht_id});
            
            return id;
        }

        IdentityId opendht_crypto_provider::add(const PublicIdentity id) {
            auto dht_pk = this->add_internal(id);
            std::unique_lock l(this->mtx);
            if(this->publics.find(id) == this->publics.end())
                this->publics.insert({id, std::make_shared<dht::crypto::PublicKey>(std::move(dht_pk))});
            
            return id;
        }

        CertificateId opendht_crypto_provider::add(const Certificate crt) {
            auto dht_crt = this->add_internal(crt);
            std::unique_lock l(this->mtx);
            if(this->certificates.find(crt) == this->certificates.end())
                this->certificates.insert({crt, std::make_shared<dht::crypto::Certificate>(std::move(dht_crt))});
            
            return crt;
        }

        size_t opendht_crypto_provider::add(const std::map<std::string, Identity> ids) {
            size_t count = 0;
            for(auto& id : ids) {
                auto dht_id = this->add_internal(id.second);
                std::unique_lock l(this->mtx);
                if(this->identities.find(id.second) == this->identities.end()) {
                    count++;
                    this->identities.insert({id.second, dht_id});
                }
            }
            return count;
        }

        size_t opendht_crypto_provider::add(const std::map<std::string, PublicIdentity> pks) {
            size_t count = 0;
            for(auto& pk : pks) {
                auto dht_pk = this->add_internal(pk.second);
                std::unique_lock l(this->mtx);
                if(this->publics.find(pk.second) == this->publics.end()) {
                    count++;
                    this->publics.insert({pk.second, std::make_shared<dht::crypto::PublicKey>(std::move(dht_pk))});
                }
            }
            return count;
        }

        size_t opendht_crypto_provider::add(const std::map<std::string, Certificate> crts) {
            size_t count = 0;
            for(auto& crt : crts) {
                auto dht_crt = this->add_internal(crt.second);
                std::unique_lock l(this->mtx);
                if(this->certificates.find(crt.second) == this->certificates.end()) {
                    count++;
                    this->certificates.insert({crt.second, std::make_shared<dht::crypto::Certificate>(std::move(dht_crt))});
                }
            }
            return count;
        }

        void opendht_crypto_provider::remove(const IdentityId id) {
            std::unique_lock l(this->mtx);
            this->identities.erase(id);
            this->publics.erase(id);
        }

        void opendht_crypto_provider::remove(const CertificateId id) {
            std::unique_lock l(this->mtx);
            this->certificates.erase(id);
        }
        
        bool opendht_crypto_provider::has(const IdentityId id) const {
            std::shared_lock l(this->mtx);
            return this->identities.find(id) != this->identities.end();
        }
        bool opendht_crypto_provider::has_public(const IdentityId id) const {
            std::shared_lock l(this->mtx);
            return this->publics.find(id) != this->publics.end();
        }
        bool opendht_crypto_provider::has(const CertificateId id) const {
            std::shared_lock l(this->mtx);
            return this->certificates.find(id) != this->certificates.end();
        }

        Identity opendht_crypto_provider::get_internal(const dht::crypto::Identity& dht_id) const {
            auto pub_id = dht_id.second->getPublicKey().getId();
            dht::Blob priv_blob = dht_id.first->serialize();
            dht::Blob crt_blob = dht_id.second->getPacked();
            Identity full_id = {
                {
                    {
                        IdentityType::x509,
                        std::string(pub_id.begin(), pub_id.end())
                    },
                    dht_id.second->getPublicKey().toString()
                },
                std::string(priv_blob.begin(), priv_blob.end()),
                {
                    {
                        {
                            IdentityType::x509,
                            std::string(pub_id.begin(), pub_id.end())
                        },
                        dht_id.second->getName()
                    },
                    std::string(crt_blob.begin(), crt_blob.end()),
                    dht_id.second->getIssuerName()
                }
            };

            return full_id;
        }

        PublicIdentity opendht_crypto_provider::get_internal(const dht::crypto::PublicKey& dht_pk) const {
            auto pub_id = dht_pk.getId();
            PublicIdentity public_id = {
                {
                    IdentityType::x509,
                    std::string(pub_id.begin(), pub_id.end())
                },
                dht_pk.toString()
            };

            return public_id;
        }

        Certificate opendht_crypto_provider::get_internal(const dht::crypto::Certificate& dht_crt) const {
            auto crt_id = dht_crt.getId();
            dht::Blob crt_blob = dht_crt.getPacked();
            return {
                {
                    {
                        IdentityType::x509,
                        std::string(crt_id.begin(), crt_id.end())
                    },
                    dht_crt.getName()
                },
                std::string(crt_blob.begin(), crt_blob.end()),
                dht_crt.getIssuerName()
            };
        }

        Identity opendht_crypto_provider::get(const IdentityId id) const {
            dht::crypto::Identity dht_id;
            {   std::shared_lock l(this->mtx);
                dht_id = this->identities.at(id);
            }

            return this->get_internal(dht_id);
        }

        PublicIdentity opendht_crypto_provider::get_public(const IdentityId id) const {
            std::shared_lock l(this->mtx);
            if(this->has(id))
                return this->get(id);
            else {
                auto& dht_pk = this->publics.at(id);
                return this->get_internal(*dht_pk);
            }
        }

        Certificate opendht_crypto_provider::get(const CertificateId id) const {
            std::shared_lock l(this->mtx);
            if(this->has(static_cast<IdentityId>(id)))
                return this->get(static_cast<IdentityId>(id)).certificate;
            else {
                auto& dht_crt = this->certificates.at(id);
                return this->get_internal(*dht_crt);
            }
        }

        std::map<std::string, Identity> opendht_crypto_provider::get_identities() const {
            std::shared_lock l(this->mtx);
            std::map<std::string, Identity> ids;
            for(auto& id : this->identities)
                ids.insert({id.first.id, this->get_internal(id.second)});
            return ids;
        }

        std::map<std::string, PublicIdentity> opendht_crypto_provider::get_publics() const {
            std::shared_lock l(this->mtx);
            std::map<std::string, PublicIdentity> pks;
            for(auto& pk : this->publics)
                pks.insert({pk.first.id, this->get_internal(*pk.second)});
            return pks;
        }

        std::map<std::string, Certificate> opendht_crypto_provider::get_certificates() const {
            std::shared_lock l(this->mtx);
            std::map<std::string, Certificate> crts;
            for(auto& crt : this->certificates)
                crts.insert({crt.first.id, this->get_internal(*crt.second)});
            return crts;
        }

        IdentityId opendht_crypto_provider::create_rsa(const std::string name, const IdentityId ca_id, const u_short key_length, const bool is_ca) {
            dht::crypto::Identity ca;

            if(ca_id) {
                std::shared_lock l(this->mtx);
                ca = this->identities.at(ca_id);
            }

            auto dht_id = dht::crypto::generateIdentity(name, ca, key_length, is_ca);
            auto key_blob = dht_id.first->serialize();
            auto pub_id = dht_id.second->getPublicKey().getId();

            IdentityId id = {
                IdentityType::x509,
                std::string(pub_id.begin(), pub_id.end())
            };

            {   std::unique_lock l(this->mtx);
                this->identities.insert({id, dht_id});
            }

            return id;
        }

        std::string opendht_crypto_provider::sign(const IdentityId id, const std::string& data) const {
            std::shared_lock l(this->mtx);
            auto& dht_id = this->identities.at(id);
            auto s = dht_id.first->sign(dht::Blob(data.begin(), data.end()));
            return std::string(s.begin(), s.end());
        }

        bool opendht_crypto_provider::check(const IdentityId id, const std::string& data, const std::string& signature) const {
            std::shared_lock l(this->mtx);
            if(!this->has_public(id))
                return this->identities.at(id).second->getPublicKey().checkSignature(dht::Blob(data.begin(), data.end()), dht::Blob(signature.begin(), signature.end()));
            else
                return this->publics.at(id)->checkSignature(dht::Blob(data.begin(), data.end()), dht::Blob(signature.begin(), signature.end()));
        }

        bool opendht_crypto_provider::check(const CertificateId id, const std::string& data, const std::string& signature) const {
            std::shared_lock l(this->mtx);
            if(!this->has(id))
                return this->identities.at(id).second->getPublicKey().checkSignature(dht::Blob(data.begin(), data.end()), dht::Blob(signature.begin(), signature.end()));
            else
                return this->certificates.at(id)->getPublicKey().checkSignature(dht::Blob(data.begin(), data.end()), dht::Blob(signature.begin(), signature.end()));
        }

        std::string opendht_crypto_provider::encrypt(const IdentityId id, const std::string& data) const {
            std::shared_lock l(this->mtx);
            dht::Blob crypt;
            if(!this->has_public(id))
                crypt = this->identities.at(id).second->getPublicKey().encrypt(dht::Blob(data.begin(), data.end()));
            else
                crypt = this->publics.at(id)->encrypt(dht::Blob(data.begin(), data.end()));
            return std::string(crypt.begin(), crypt.end());
        }

        std::string opendht_crypto_provider::encrypt(const CertificateId id, const std::string& data) const {
            std::shared_lock l(this->mtx);
            dht::Blob crypt;
            if(!this->has(id))
                crypt = this->identities.at(id).second->getPublicKey().encrypt(dht::Blob(data.begin(), data.end()));
            else
                crypt = this->certificates.at(id)->getPublicKey().encrypt(dht::Blob(data.begin(), data.end()));
            return std::string(crypt.begin(), crypt.end());
        }

        std::string opendht_crypto_provider::decrypt(const IdentityId id, const std::string& encrypted_data) const {
            std::shared_lock l(this->mtx);
            auto& dht_id = this->identities.at(id);
            auto data = dht_id.first->decrypt(dht::Blob(encrypted_data.begin(), encrypted_data.end()));
            return std::string(data.begin(), data.end());
        }

        opendht_aes_cipher::opendht_aes_cipher(const std::string& pw, const u_short key_length)
        : key(dht::crypto::hash(dht::Blob(pw.begin(), pw.end()), key_length / 8)), key_length(key_length) {}
        
        const hasher& opendht_aes_cipher::get_hasher() const {
            static const opendht_sha_hasher h;
            return h;
        }

        const std::string opendht_aes_cipher::get_token() const {
            auto blob = dht::crypto::hash(this->key, this->key_length / 8);
            return std::string(blob.begin(), blob.end());
        }

        std::string opendht_aes_cipher::encrypt(const std::string& data) const {
            auto e = dht::crypto::aesEncrypt(dht::Blob(data.begin(), data.end()), this->key);
            return std::string(e.begin(), e.end());
        }

        std::string opendht_aes_cipher::decrypt(const std::string& encrypted_data) const {
            auto d = dht::crypto::aesDecrypt(dht::Blob(encrypted_data.begin(), encrypted_data.end()), this->key);
            return std::string(d.begin(), d.end());
        }

        opendht_node::opendht_node(const dht::crypto::Identity id,
                                   const std::string bootstrap, const std::string socket,
                                   const in_port_t port,
                                   const std::string service_name,
                                   const in_port_t dport,
                                   const std::chrono::milliseconds timeout)
        : id(id), timeout(timeout), service_name(service_name) {
            this->run(port, id, true);
            
            // advertise service node
            this->discovery.startPublish(service_name, service{this->getNodeId(), dport});

            // start dicovery of locally available service nodes
            this->discovery.startDiscovery<service>(service_name, [this](service&& s, dht::SockAddr&& addr) {
                addr.setPort(s.port);
                this->bootstrap(s.id, addr);
            });

            if(!bootstrap.empty())
                this->bootstrap(bootstrap, std::to_string(port));
        }

        std::shared_ptr<opendht_node> opendht_node::make(const dht::crypto::Identity id,
                                   const std::string bootstrap, const std::string socket,
                                   const in_port_t port,
                                   const std::string service_name,
                                   const in_port_t dport,
                                   const std::chrono::milliseconds timeout) {
            return std::shared_ptr<opendht_node>(new opendht_node(id, bootstrap, socket, port, service_name, dport, timeout));
        }

        opendht_node::opendht_node(const dht::crypto::Identity id,
                                   const dht::SockAddr addr,
                                   const in_port_t port,
                                   const std::string service_name,
                                   const in_port_t dport,
                                   const std::chrono::milliseconds timeout)
        : id(id), timeout(timeout), service_name(service_name) {
            this->run(port, id, true);
            
            // advertise service node
            this->discovery.startPublish(service_name, service{this->getNodeId(), dport});

            // start dicovery of locally available service nodes
            this->discovery.startDiscovery<service>(service_name, [this](service&& s, dht::SockAddr&& addr) {
                addr.setPort(s.port);
                this->bootstrap(s.id, addr);
            });

            if(addr)
                this->bootstrap(addr);
        }

        std::shared_ptr<opendht_node> opendht_node::make(const dht::crypto::Identity id,
                                   const dht::SockAddr addr,
                                   const in_port_t port,
                                   const std::string service_name,
                                   const in_port_t dport,
                                   const std::chrono::milliseconds timeout) {
            return std::shared_ptr<opendht_node>(new opendht_node(id, addr, port, service_name, dport, timeout));
        }

        opendht_node::~opendht_node() {
            std::unique_lock l(this->mtx);
            ASSERT_MSG(scope_opendht, this->sources.empty(), "destroying opendht_node while there are still opendht_sources attached.")
            
            this->discovery.stopPublish(this->service_name);
            this->discovery.stopDiscovery(this->service_name);
            this->shutdown();
            this->join();
        }

        void opendht_node::add_source(const opendht_source& s) {
            std::unique_lock l(this->mtx);                    
            this->sources.insert(&s);
        }

        void opendht_node::remove_source(const opendht_source& s) {
            std::unique_lock l(this->mtx);
            this->sources.erase(&s);
        }
        
        void opendht_source::listen() {
            this->listen_token = this->node->listen(this->key,
                [this](const std::vector<std::shared_ptr<dht::Value>>& values, bool expired) {
                    if(!expired)
                        for (const auto& v : values) {
                            auto pub_id = v->getOwner()->getId();
                            causal::data::IdentityId ii = {
                                IdentityType::x509,
                                std::string(pub_id.begin(), pub_id.end())
                            };
                            this->pull(v->unpack<std::string>(), {
                                {
                                    v->id,
                                    v->seq
                                },
                                {
                                    ii,
                                    v->getOwner()->toString()
                                }
                            });
                        }
                    return true; // keep listening
                }
            );   
            
            if(this->listen_token.wait_for(this->node->timeout) != std::future_status::ready)
                INTERN_THROW_ERROR(scope_opendht, opendht_source_listen_timeout(this->key))
        }

        std::shared_ptr<opendht_source> opendht_source::make(const std::shared_ptr<opendht_node> node, const dht::InfoHash k, const dht::InfoHash to) {
            return std::shared_ptr<opendht_source>(new opendht_source(node, k, to));
        }

        opendht_source::opendht_source(const std::shared_ptr<opendht_node> node,
                                       const dht::InfoHash k,
                                       const dht::InfoHash to)
        : to_id(to), key(k), node(std::move(node)) {
            this->node->add_source(*this);
            this->listen();
        }

        opendht_source::~opendht_source() {
            this->node->cancelListen(this->key, std::move(this->listen_token));
            this->node->remove_source(*this);
        }

        void opendht_source::push(const std::string& msg) const {
            if(this->to_id) {
                this->node->putEncrypted(this->key, this->to_id, msg, [this](bool success){
                    if(!success) {
                        std::stringstream ss;
                        msgpack::pack(ss, typeid(opendht_error_msg).name());
                        msgpack::pack(ss, opendht_error_msg{this->to_id, this->key, "could not push message"});
                        this->pull(ss.str(), {});
                    }
                });
            } else {
                this->node->putSigned(this->key, msg, [this](bool success){
                    if(!success) {
                        std::stringstream ss;
                        msgpack::pack(ss, typeid(opendht_error_msg).name());
                        msgpack::pack(ss, opendht_error_msg{{}, this->key, "could not push message"});
                        this->pull(ss.str(), {});
                    }
                });
            }
        }

        void opendht_source::push(std::string&& msg) const {
            if(this->to_id) {
                this->node->putEncrypted(this->key, this->to_id, std::forward<std::string>(msg), [this](bool success){
                    if(!success) {
                        std::stringstream ss;
                        msgpack::pack(ss, typeid(opendht_error_msg).name());
                        msgpack::pack(ss, opendht_error_msg{this->to_id, this->key, "could not push message"});
                        this->pull(ss.str(), {});
                    }
                });
            } else {
                this->node->putSigned(this->key, std::forward<std::string>(msg), [this](bool success){
                    if(!success) {
                        std::stringstream ss;
                        msgpack::pack(ss, typeid(opendht_error_msg).name());
                        msgpack::pack(ss, opendht_error_msg{{}, this->key, "could not push message"});
                        this->pull(ss.str(), {});
                    }
                });
            }
        }
    }
}