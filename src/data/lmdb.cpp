// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <memory>
#include <vector>

#include "causal/data/crypto.hpp"
#include "causal/trace.hpp"

#include "causal/data/lmdb.hpp"
#include "causal/data/msgpack.hpp"

namespace causal {
    namespace data {
        INIT_TRACE_SCOPE(scope_lmdb, "lmdb")

        lmdb_junction::lmdb_junction(const std::string path, const std::string name, const mode_t create_mode, const size_t map_size) {
            this->initialize(path, name, create_mode);
            this->open(name, map_size);
            this->check_crypto_token();
        }

        std::shared_ptr<lmdb_junction> lmdb_junction::make(const std::string path, const std::string name, const mode_t create_mode, const size_t map_size) {
            return std::shared_ptr<lmdb_junction>(new lmdb_junction(path, name, create_mode, map_size));
        }

        lmdb_junction::~lmdb_junction() {
            std::unique_lock l(this->mtx);
            if(this->env)
                mdb_env_close(this->env);
        }

        void lmdb_junction::initialize(const std::string path, const std::string name, const mode_t create_mode) {
            mdb_env_create(&this->env);

            if(!name.empty()) {
                int r = mdb_env_set_maxdbs(this->env, 1);
                switch(r) {
                case 0: break;
                case EINVAL:
                    INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
                default:
                    INTERN_THROW_ERROR(scope_lmdb, lmdb_env_creation_failed())
                }
            }

            int r = mdb_env_open(this->env, path.c_str(), MDB_NOSUBDIR, create_mode);

            switch(r) {
            case 0: break;
            case MDB_VERSION_MISMATCH:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_version_mismatch())
            case MDB_INVALID:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_db_corrupt())
            case ENOENT:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_db_dir_not_found())
            case EACCES:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_db_access_failed())
            case EAGAIN:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_db_locked())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_env_creation_failed())
            }
        }

        void lmdb_junction::open(const std::string name, const size_t map_size) {
            std::unique_lock l(this->mtx);
            auto* txn = this->begin_txn();
            auto r = mdb_dbi_open(txn, name.empty() ? nullptr : name.c_str(), MDB_CREATE, &this->db);
            switch(r) {
            case 0:
                this->commit_txn(txn);
                return;
            case MDB_NOTFOUND:
                this->abort_txn(txn);
                mdb_env_close(this->env);
                this->env = nullptr;
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            case MDB_DBS_FULL:
                this->abort_txn(txn);
                mdb_env_close(this->env);
                this->env = nullptr;
                INTERN_THROW_ERROR(scope_lmdb, lmdb_overcrowded())
            default:
                mdb_env_close(this->env);
                this->abort_txn(txn);
                INTERN_THROW_ERROR(scope_lmdb, lmdb_transaction_failed())
            }

            r = mdb_env_set_mapsize(this->env, map_size);

            if(!r) {
                mdb_env_close(this->env);
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            }
        }

        void lmdb_junction::check_crypto_token() {
            const std::string key("crypto"),
                              expected = this->ciph ? this->ciph->get_token() : std::string("plain");
            std::string given;
            auto* txn = this->begin_txn();
            MDB_val idVal = {.mv_size = key.size(), .mv_data = (void*)key.data()}, data;
            auto r = mdb_get(txn, this->db, &idVal, &data);

            switch(r) {
            case 0:
                given = std::string((char*)data.mv_data, data.mv_size);
            case MDB_NOTFOUND:
                break;
            case EINVAL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_get_failed())
            }

            if(given.empty()) {
                data = {.mv_size = expected.size(), .mv_data = (void*)expected.data()};

                auto r = mdb_put(txn, this->db, &idVal, &data, 0);

                switch(r) {
                case 0:
                    break;
                case MDB_MAP_FULL:
                    this->abort_txn(txn);
                    INTERN_THROW_ERROR(scope_lmdb, lmdb_db_full())
                case MDB_TXN_FULL:
                    this->abort_txn(txn);
                    INTERN_THROW_ERROR(scope_lmdb, lmdb_transaction_full())
                case EACCES:
                case EINVAL:
                case MDB_KEYEXIST:
                    this->abort_txn(txn);
                    INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
                default:
                    this->abort_txn(txn);
                    INTERN_THROW_ERROR(scope_lmdb, lmdb_put_failed())
                }
                
                this->commit_txn(txn);
            } else if(given != expected) {
                this->abort_txn(txn);
                throw lmdb_crypto_token_mismatch();
            } else
                this->abort_txn(txn);
        }

        MDB_txn* lmdb_junction::begin_txn(bool ro) const {
            MDB_txn* txn = nullptr;
            auto r = mdb_txn_begin(this->env, nullptr, ro ? MDB_RDONLY : 0, &txn);

            switch(r) {
            case 0:
                break;
            case MDB_PANIC:
                mdb_env_close(this->env);
                INTERN_THROW_ERROR(scope_lmdb, lmdb_env_panic())
            case MDB_MAP_RESIZED:
                mdb_env_set_mapsize(this->env, 0);
                return this->begin_txn();
            case MDB_READERS_FULL:
                unsigned int amount;
                mdb_env_get_maxreaders(this->env, &amount);
                mdb_env_set_maxreaders(this->env, amount+126);
                return this->begin_txn();
            case ENOMEM:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_out_of_memory())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_transaction_failed())
            }

            return txn;
        }

        void lmdb_junction::commit_txn(MDB_txn* txn) const {
            auto r = mdb_txn_commit(txn);

            switch(r) {
            case 0:
                break;
            case EINVAL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            case ENOSPC:
                TRACE_FATAL(scope_lmdb) << "no more disk set";
            case EIO:
                TRACE_FATAL(scope_lmdb) << "a low-level I/O error occurred while writing";
            case ENOMEM:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_out_of_memory())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_transaction_failed())
            }
        }

        void lmdb_junction::abort_txn(MDB_txn* txn) const {
            mdb_txn_abort(txn);
        }

        void lmdb_junction::reset_txn(MDB_txn* txn) const {
            mdb_txn_reset(txn);
        }

        void lmdb_junction::attract(junction_space& s, const std::string id) {
            s.attracted(id);
        }

        void lmdb_junction::repel(const junction_space& s, const std::string id) {}

        MDB_val lmdb_junction::get_hashed_id(const std::string& id, std::string& buf) const {
            if(this->ciph) {
                buf = this->ciph->get_hasher().hash(id);
                return {.mv_size = buf.size(), .mv_data = (void*)buf.data()};
            } else
                return {.mv_size = id.size(), .mv_data = (void*)id.data()};
        }

        MDB_val lmdb_junction::get_crypt_data(const std::string& data, std::string& buf) const {
            if(this->ciph) {
                buf = this->ciph->encrypt(data);
                return {.mv_size = buf.size(), .mv_data = (void*)buf.data()};
            } else
                return {.mv_size = data.size(), .mv_data = (void*)data.data()};
        }

        std::string lmdb_junction::get_decrypt_data(const MDB_val val) const {
            if(this->ciph) {
                std::string encrypted_data((char*)val.mv_data, val.mv_size);
                return this->ciph->decrypt(encrypted_data);
            } else
                return std::string((char*)val.mv_data, val.mv_size);
        }

        const std::string lmdb_junction::get_val(const std::string& id, MDB_txn* txn) const {
            std::string id_buf;
            MDB_val idVal = this->get_hashed_id(id, id_buf),
                    data;
            auto r = mdb_get(txn, this->db, &idVal, &data);

            switch(r) {
            case 0:
                return this->get_decrypt_data(data);
            case MDB_NOTFOUND:
                return std::string();
            case EINVAL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_get_failed())
            }
        }

        const std::string lmdb_junction::get(const std::string id) const {
            std::shared_lock l(this->mtx);
            std::string r;
            auto* txn = this->begin_txn(true);
            try {
                r = this->get_val(id, txn);
            } catch(...) {
                this->abort_txn(txn);
                throw;
            }
            this->abort_txn(txn);
            return r;
        }

        bool lmdb_junction::set_val(const std::string& id, const std::string& data, MDB_txn* txn, bool override) {
            std::string id_buf, data_buf;
            MDB_val idVal = this->get_hashed_id(id, id_buf),
                    d = this->get_crypt_data(data, data_buf);
            auto r = mdb_put(txn, this->db, &idVal, &d, !override ? MDB_NOOVERWRITE : 0);

            switch(r) {
            case 0:
                return true;
            case MDB_KEYEXIST:
                return false;
            case MDB_MAP_FULL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_db_full())
            case MDB_TXN_FULL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_transaction_full())
            case EACCES:
            case EINVAL:
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_put_failed())
            }
        }

        void lmdb_junction::set(const std::string id, const std::string& data) {
            std::shared_lock l(this->mtx);
            auto* txn = this->begin_txn();
            try {
                this->set_val(id, data, txn);
            } catch(...) {
                this->abort_txn(txn);
                throw;
            }
            this->commit_txn(txn);
        }

        void lmdb_junction::set(const std::string id, std::string&& data) {
            std::shared_lock l(this->mtx);
            auto* txn = this->begin_txn();
            try {
                this->set_val(id, data, txn);
            } catch(...) {
                this->abort_txn(txn);
                throw;
            }
            this->commit_txn(txn);
        }

        void lmdb_junction::erase_val(const std::string& id, MDB_txn* txn) {
            std::string id_buf;
            MDB_val idVal = this->get_hashed_id(id, id_buf);
            auto r = mdb_del(txn, this->db, &idVal, nullptr);

            switch(r) {
            case 0:
            case MDB_NOTFOUND:
                break;
            case EACCES:
            case EINVAL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_del_failed())
            }
        }

        void lmdb_junction::erase(const std::string id) {
            std::shared_lock l(this->mtx);
            auto* txn = this->begin_txn();
            try {
                this->erase_val(id, txn);
            } catch(...) {
                this->abort_txn(txn);
                throw;
            }
            this->commit_txn(txn);
        }

        bool lmdb_junction::merge(const junction& o, bool override) {
            ASSERT_MSG(scope_lmdb, this != &o, "junction cannot merge itself")
            std::shared_lock l(this->mtx);
            auto* txn = this->begin_txn();
            if(o.iterate([&](const std::string& id, const std::string& data) {
                this->set_val(id, data, txn, override);
                return true;
            })) {
                this->commit_txn(txn);
                return true;
            } else {
                this->abort_txn(txn);
                return false;
            }
        }

        bool lmdb_junction::iterate(std::function<bool(const std::string&, const std::string&)> f) const {
            // id is hashed why restoring it is impossible, however it would pose a security risk so do not even try
            ASSERT_MSG(scope_lmdb, !this->ciph, "cannot iterate encrypted database")
            std::shared_lock l(this->mtx);
            auto* txn = this->begin_txn(true);
            MDB_cursor* cur;
            auto r = mdb_cursor_open(txn, this->db, &cur);
            switch(r) {
            case 0:
                break;
            case EINVAL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_get_failed())
            }

            MDB_val idVal, dataVal;
            r = mdb_cursor_get(cur, &idVal, &dataVal, MDB_FIRST);
            while(!r) {
                std::string id((char*)idVal.mv_data, idVal.mv_size),
                            data((char*)dataVal.mv_data, dataVal.mv_size);
                if(!f(id, data))
                    break;
                r = mdb_cursor_get(cur, &idVal, &dataVal, MDB_NEXT);
            }

            mdb_cursor_close(cur);

            switch(r) {
            case 0:
                return false;
            case MDB_NOTFOUND:
                return true;
            case EINVAL:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_driver_bug_detected())
            default:
                INTERN_THROW_ERROR(scope_lmdb, lmdb_get_failed())
            }
        }      
    }
}