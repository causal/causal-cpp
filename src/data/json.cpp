// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <boost/algorithm/string/trim.hpp>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>

#include "causal/data/json.hpp"

namespace causal {
    namespace data {
        INIT_TRACE_SCOPE(scope_json, "json")

        std::string json_junction::clean(const std::string s) {
            std::stringstream ss_out;
            auto ss_in = std::stringstream(s);
            for (std::string line; std::getline(ss_in, line, '\n');) {
                auto trimmed = boost::trim_left_copy(line);
                if(!trimmed.empty() && !trimmed.starts_with('#'))
                    ss_out << line << '\n';
            }

            return ss_out.str();
        }

        json_junction::json_junction(const std::string& j) : store(nlohmann::json::parse(clean(j))) {}

        std::shared_ptr<json_junction> json_junction::make() {
            return std::shared_ptr<json_junction>(new json_junction());
        }

        std::shared_ptr<json_junction> json_junction::make(const std::string& j) {
            return std::shared_ptr<json_junction>(new json_junction(j));
        }

        std::string json_junction::dump(bool pretty) const {
            std::shared_lock l(this->mtx);
            return pretty ? this->store.dump(4) : this->store.dump();
        }

        bool json_junction::add_space(const junction_space& s) {
            // due to inability to cast space at this point, assert cannot work here
            //ASSERT_MSG(scope_json, dynamic_cast<const msgpack_space*>(&s), "json junction can only handle msgpack_space")
            return this->junction::add_space(s);
        }

        void json_junction::attract(junction_space& s, const std::string id) {
            s.attracted(id);
        }

        void json_junction::repel(const junction_space& s, const std::string id) {}

        const std::string json_junction::get(const std::string id) const {
            std::shared_lock l(this->mtx);
            if(this->store.contains(id)) {
                std::vector<std::uint8_t> v = nlohmann::json::to_msgpack(this->store[id]);
                return std::string(v.begin(), v.end());
            } else
                return std::string();
        }

        void json_junction::set(const std::string id, const std::string& data) {
            std::unique_lock l(this->mtx);
            this->store[id] = nlohmann::json::from_msgpack(data);
        }

        void json_junction::set(const std::string id, std::string&& data) {
            std::unique_lock l(this->mtx);
            this->store[id] = nlohmann::json::from_msgpack(data);
        }

        void json_junction::erase(const std::string id) {
            std::unique_lock l(this->mtx);
            if(!this->store.erase(id))
                throw std::out_of_range("no eraseable data found");
        }

        bool json_junction::merge(const junction& o, bool override) {
            ASSERT_MSG(scope_json, this != &o, "junction cannot merge itself")
            std::unique_lock l(this->mtx);
            nlohmann::json tmp = this->store;
            if(o.iterate([&](const std::string& id, const std::string& data) {
                if(!tmp.contains(id) || override)
                    tmp[id] = nlohmann::json::from_msgpack(data);
                return true;
            })) {
                this->store = tmp;
                return true;
            } else
                return false;
        }

        bool json_junction::iterate(std::function<bool(const std::string&, const std::string&)> f) const {
            std::shared_lock l(this->mtx);
            for (auto it=this->store.begin(); it!=this->store.end(); it++) {
                auto v = nlohmann::json::to_msgpack(it.value());
                std::string data(v.begin(), v.end());
                if(!f(it.key(), data))
                    return false;
            }
            return true;
        }
    }
}