// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <string>

#include "causal/data/memory.hpp"

namespace causal {
    namespace data {
        std::shared_ptr<memory_junction> memory_junction::make() {
            return std::shared_ptr<memory_junction>(new memory_junction());
        }

        bool memory_junction::add_space(const junction_space& s) {
            return this->junction::add_space(s);
        }

        void memory_junction::attract(junction_space& s, const std::string id) {
            s.attracted(id);
        }

        void memory_junction::repel(const junction_space& s, const std::string id) {}

        const std::string memory_junction::get(const std::string id) const {
            std::shared_lock l(this->mtx);
            if(this->contains(id)) {
                return this->at(id);
            } else
                return std::string();
        }

        void memory_junction::set(const std::string id, const std::string& data) {
            std::unordered_map<std::string, std::string> &map = *this;
            std::unique_lock l(this->mtx);
            map[id] = data;
        }

        void memory_junction::set(const std::string id, std::string&& data) {
            std::unordered_map<std::string, std::string> &map = *this;
            std::unique_lock l(this->mtx);
            map[id] = std::forward<std::string>(data);
        }

        void memory_junction::erase(const std::string id) {
            std::unordered_map<std::string, std::string> &map = *this;
            std::unique_lock l(this->mtx);
            if(this->contains(id)) {
                map.erase(id);
            }
        }

        bool memory_junction::merge(const junction& o, bool override) {
            ASSERT_MSG(scope_data, this != &o, "junction cannot merge itself")
            std::unordered_map<std::string, std::string> &map = *this;
            std::unique_lock l(this->mtx);
            if(o.iterate([&](const std::string& id, const std::string& data) {
                if(!map.contains(id) || override) {
                    map[id] = data;
                }
                return true;
            })) {
                return true;
            } else
                return false;
        }

        bool memory_junction::iterate(std::function<bool(const std::string&, const std::string&)> f) const {
            const std::unordered_map<std::string, std::string> &map = *this;
            std::shared_lock l(this->mtx);
            for (auto it=map.cbegin(); it!=map.cend(); it++) {                
                if(!f(it->first, map.at(it->first)))
                    return false;
            }
            return true;
        }
    }
}