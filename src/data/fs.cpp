// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <iostream>
#include <fstream>
#include <filesystem>
#include <memory>
#include <string>

#include "causal/data/fs.hpp"
#include "causal/trace.hpp"

namespace causal {
    namespace data {
        INIT_TRACE_SCOPE(scope_fs, "fs")

        namespace fs = std::filesystem;

        fs_junction::fs_junction(const std::string root) : root(root) {
            const auto path = fs::path(this->root);

            if(!fs::exists(path))
                fs::create_directories(path);

            if(fs::status(path).type() != fs::file_type::directory)
                throw fs_root_not_valid(path);
        }

        std::shared_ptr<fs_junction> fs_junction::make(const std::string root) {
            return std::shared_ptr<fs_junction>(new fs_junction(root));
        }

        bool fs_junction::add_space(const junction_space& s) {
            return this->junction::add_space(s);
        }

        void fs_junction::attract(junction_space& s, const std::string id) {
            s.attracted(id);
        }

        void fs_junction::repel(const junction_space& s, const std::string id) {}

        const std::string fs_junction::get(const std::string id) const {
            const auto path = fs::path(root)/fs::path(id);
            std::shared_lock l(this->mtx);
            if(fs::exists(path) && fs::status(path).type() == fs::file_type::regular) {
                std::ifstream datafile(path);
                return {std::istreambuf_iterator<char>{datafile}, std::istreambuf_iterator<char>{}};
            } else if(fs::exists(path))
                throw fs_id_not_valid(path);
            else
                return std::string();
        }

        void fs_junction::set(const std::string id, const std::string& data) {
            const auto path = fs::path(this->root)/fs::path(id);
            std::unique_lock l(this->mtx);
            if(!fs::exists(path) || (fs::exists(path) && fs::status(path).type() == fs::file_type::regular)) {
                const auto base = path.root_path();
                if(!fs::exists(base))
                    fs::create_directories(base);
                else if(fs::status(base).type() != fs::file_type::directory)
                    throw(fs_id_not_valid());

                std::ofstream datafile(path, std::ofstream::out);
                datafile << data;
            } else if(fs::exists(path))
                throw fs_id_not_valid(path);
        }

        void fs_junction::set(const std::string id, std::string&& data) {
            const auto path = fs::path(this->root)/fs::path(id);
            std::unique_lock l(this->mtx);
            if(!fs::exists(path) || (fs::exists(path) && fs::status(path).type() == fs::file_type::regular)) {
                const auto base = path.root_path();
                if(!fs::exists(base))
                    fs::create_directories(base);
                else if(fs::status(base).type() != fs::file_type::directory)
                    throw(fs_id_not_valid());

                std::ofstream datafile(path, std::ofstream::out);
                datafile << std::forward<std::string>(data);
            } else if(fs::exists(path))
                throw fs_id_not_valid(path);
        }

        void fs_junction::erase(const std::string id) {
            const auto path = fs::path(this->root)/fs::path(id);
            std::unique_lock l(this->mtx);
            if(fs::exists(path) && fs::status(path).type() == fs::file_type::regular) {
                fs::remove(path);
            } else if(fs::exists(path))
                throw fs_id_not_valid(path);
        }

        bool fs_junction::merge(const junction& o, bool override) {
            ASSERT_MSG(scope_fs, this != &o, "junction cannot merge itself")
            std::unique_lock l(this->mtx);
            if(o.iterate([&](const std::string& id, const std::string& data) {
                const auto path = fs::path(this->root)/fs::path(id);
                if(!fs::exists(path) || override) {
                    std::ofstream datafile(path, std::ofstream::out);
                    datafile << data;
                }
                return true;
            })) {
                return true;
            } else
                return false;
        }

        bool fs_junction::iterate(std::function<bool(const std::string&, const std::string&)> f) const {
            std::shared_lock l(this->mtx);
            const auto iter = fs::recursive_directory_iterator(this->root);
            for (auto it=fs::begin(iter); it!=fs::end(iter); it++) {
                if(fs::status(it->path()).type() == fs::file_type::regular) {
                    const auto id = fs::relative(it->path(), this->root).generic_string();
                    std::ifstream datafile(it->path());
                    std::string data;
                    datafile >> data;
                    
                    if(!f(id, data))
                        return false;
                }
            }
            return true;
        }
    }
}