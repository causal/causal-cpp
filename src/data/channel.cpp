// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/data/channel.hpp"
#include "causal/trace.hpp"

#include <memory>
#include <random>

namespace causal {
    namespace data {
        std::random_device rng;

        wrap::wrap(std::shared_ptr<causal::core::branch> b)
        : gate(b) {}

        std::shared_ptr<wrap> wrap::make(std::shared_ptr<cc::branch> b) {
            return std::shared_ptr<wrap>(new wrap(b));
        }
        
        std::shared_ptr<wrap::session> wrap::get_session(const std::string &uri) {
            TRACE(cc::scope_core) << "get session '" << uri << "'";
            if(uri.length() == 2) {
                return this->create();
            } else {
                const std::string id = uri.substr(2);

                {   std::shared_lock l(this->serve_mtx);
                    const auto it = this->sessions.find(id);
                    if(it != this->sessions.cend())
                        return it->second;
                }

                return this->create();
            }
        }

        std::shared_ptr<wrap::session> wrap::create() {
            TRACE(cc::scope_core) << "create session";
            const std::string chars(
                "abcdefghijklmnopqrstuvwxyz"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "1234567890"
            );
            std::uniform_int_distribution<> index_dist(0, chars.size() - 1);
            std::stringstream ss;
            for(int i = 0; i < _DATA_WRAP_SESSION_SIZE; ++i) {
                ss << chars[index_dist(rng)];
            }
            const std::string id = ss.str();

            const auto source = loopback_source<metaT>::make(SessionMeta{.session = id});
            const auto s = std::make_shared<session>(id, source, source->meta);

            {   std::unique_lock l(this->serve_mtx);
                this->sessions[id] = s;

                if(!this->can_dispose) {
                    this->subscribe<DisposeSession>(wrap::Dispose, this->shared_from_this());
                    this->can_dispose = true;
                }
            }

            this->creating(s);

            return s;
        }

        void wrap::dispose(const std::string &id) {
            TRACE(cc::scope_core) << "close session '" << id << "'";
            std::shared_ptr<session> s;
            {   std::unique_lock l(this->serve_mtx);
                const auto it = this->sessions.find(id);
                if(it != this->sessions.cend())
                    s = it->second;
            }

            if(s)
                this->disposing(s);
            else
                TRACE_ERROR(cc::scope_core) << "trying to erase unknown session";
        }

        void wrap::creating(const std::shared_ptr<session>& s) {
            TRACE(cc::scope_core) << "creating session '" << s->id << "'";

            std::unique_lock l(s->mtx);
            s->meta.state = SessionState::Initilized;
            this->trigger(SessionCreated{.session=s->meta.session, .peer=s->meta.peer, .src=std::dynamic_pointer_cast<source<SessionMeta>>(s->source)});
        }

        void wrap::disposing(const std::shared_ptr<session>& s) {
            TRACE(cc::scope_core) << "closing session";

            std::unique_lock l(s->mtx);
            s->meta.state = SessionState::Closed;
            const auto it = this->sessions.find(s->meta.session);
            if(it != this->sessions.cend()) {
                this->sessions.erase(it);
            } else
                TRACE_WARNING(cc::scope_core) << "closing non existant session '" << s->meta.session << "'";
            this->trigger(SessionDisposed{.session=s->meta.session, .peer=s->meta.peer});
        }

        void wrap::Dispose(DisposeSession e, const std::shared_ptr<wrap> g) {
            g->dispose(e.session);
        }

        const std::pair<std::string, std::shared_ptr<source<SessionMeta>>> wrap::forge() {
            const auto s = this->create();
            return {s->meta.session, std::dynamic_pointer_cast<source<SessionMeta>>(s->source)};
        }
    }
}