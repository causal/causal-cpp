// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/data/junction.hpp"

namespace causal {
    namespace data {
        junction_space::junction_space(const std::shared_ptr<junction> j) : junc(std::move(j)) {
            ASSERT_MSG(scope_data, this->junc, "trying to create junction_space without valid junction")
            this->junc->add_space(*this);
        }

        junction_space::~junction_space() {
            {   std::unique_lock l(this->mtx);
                this->junc->remove_space(*this);
            }
        }

        void junction_space::create(const std::any id, const cc::essence_ptr<> a) noexcept {
            if(id.type() == typeid(std::string)) {
                std::unique_lock l(this->mtx);
                this->aspects.insert({std::any_cast<std::string>(id), {a}});
            }
        }

        void junction_space::destroy(const std::any id) noexcept {
            if(id.type() == typeid(std::string)) {
                std::unique_lock l(this->mtx);
                auto it = this->aspects.find(std::any_cast<std::string>(id));
                if(it->second.ptr.expired())
                    this->aspects.erase(it);
            }
        }

        junction::~junction() {
            std::unique_lock l(this->mtx);
            ASSERT_MSG(scope_data, this->spaces.empty(), "destroying junction while spaces are connected to it")
        }

        bool junction::add_space(const junction_space& s) {
            std::unique_lock l(this->mtx);
            this->spaces.insert(&s);
            return true;
        }

        bool junction::remove_space(const junction_space& s) {
            std::unique_lock l(this->mtx);
            return this->spaces.erase(&s);
        }

        std::shared_ptr<map_junction> map_junction::make() {
            return std::shared_ptr<map_junction>(new map_junction());
        }

        void map_junction::attract(junction_space& s, const std::string id) {
            s.attracted(id);
        }

        void map_junction::repel(const junction_space& s, const std::string id) {}

        const std::string map_junction::get(const std::string id) const {
            std::shared_lock l(this->mtx);
            if(this->store.contains(id))
                return this->store.at(id);
            else
                return std::string();
        }

        void map_junction::set(const std::string id, const std::string& data) {
            std::unique_lock l(this->mtx);
            this->store.insert_or_assign(id, data);
        }

        void map_junction::set(const std::string id, std::string&& data) {
            std::unique_lock l(this->mtx);
            this->store.insert_or_assign(id, std::forward<std::string>(data));
        }

        void map_junction::erase(const std::string id) {
            std::unique_lock l(this->mtx);
            this->store.erase(id);
        }

        bool map_junction::merge(const junction& o, bool override) {
            ASSERT_MSG(scope_data, this != &o, "junction cannot merge itself")
            std::unique_lock l(this->mtx);
            std::unordered_map<std::string, std::string> tmp;
            if(o.iterate([&](const std::string& id, const std::string& data) {
                tmp.insert({id, data});
                return true;
            })) {
                for(auto& e : tmp) {
                    auto it = this->store.find(e.first);
                    if(it == this->store.end())
                        this->store.insert({e.first, e.second});
                    else if(override)
                        it->second = e.second;
                }
                return true;
            } else
                return false;
        }

        bool map_junction::iterate(std::function<bool(const std::string&, const std::string&)> f) const {
            std::shared_lock l(this->mtx);
            for(auto& e : this->store)
                if(!f(e.first, e.second))
                    return false;
            return true;
        }
    }
}