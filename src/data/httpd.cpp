// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/data/httpd.hpp"
#include "causal/data/channel.hpp"
#include "causal/trace.hpp"

#include <cstddef>
#include <memory>
#include <filesystem>
#include <microhttpd.h>
#include <mutex>
#include <shared_mutex>
#include <sstream>
#include <random>
#include <string>
#include <vector>
#include <arpa/inet.h>

namespace causal {
    namespace data {
        namespace fs = std::filesystem;

        INIT_TRACE_SCOPE(scope_httpd, "httpd")

        std::random_device rng;

        httpd_config::httpd_config(std::string addr, u_short port, const std::optional<std::pair<std::string, std::string>> cert)
        : certificate(cert) {
            if(!addr.empty())
                this->address = addr;
            
            if(port)
                this->port = port;
            else
                this->port = this->certificate ? 443 : 80;
        }

        httpd_config::httpd_config(std::pair<std::string, std::string> cert)
        : httpd_config("", 0, cert) {}

        httpd_config::httpd_config()
        : httpd_config("", 0) {}

        httpd_gate::httpd_gate(std::shared_ptr<causal::core::branch> b,
                               httpd_config cfg, std::shared_ptr<const junction> j)
        : gate(b), config(cfg), exposed(j) {
            this->listen();
        }

        std::shared_ptr<httpd_gate> httpd_gate::make(std::shared_ptr<cc::branch> b, httpd_config cfg, std::shared_ptr<const junction> j) {
            return std::shared_ptr<httpd_gate>(new httpd_gate(b, cfg, j));
        }

        httpd_gate::~httpd_gate() {
            if(this->daemon)
                MHD_stop_daemon(reinterpret_cast<MHD_Daemon *>(this->daemon));
        }

        static std::string connection_ip(MHD_Connection *connection) {
            // Get the IP address of the client
            std::string ip;

            auto const* ci = MHD_get_connection_info(connection, MHD_CONNECTION_INFO_CLIENT_ADDRESS);
            if (nullptr == ci || nullptr == ci->client_addr) {
                return ip;
            }
            std::array<char, INET6_ADDRSTRLEN>client_ip {0};

            if (ci->client_addr->sa_family == AF_INET) { // IPv4
                auto *addr = (struct sockaddr_in const*) ci->client_addr;
                inet_ntop(AF_INET, &addr->sin_addr, client_ip.data(), client_ip.size());
            }
            else if (ci->client_addr->sa_family == AF_INET6) { // IPv6
                auto *addr = (struct sockaddr_in6 const*) ci->client_addr;
                inet_ntop(AF_INET6, &addr->sin6_addr, client_ip.data(), client_ip.size());
            }
            else {
                return ip;
            }

            ip.assign(client_ip.begin(), std::find(client_ip.begin(), client_ip.end(), '\0'));
            return ip;
        }

        static bool is_ip_allowed(const httpd_gate *const n, struct MHD_Connection *connection) {
            auto ip = connection_ip(connection);
            auto blacklist = n->get_blacklist();
            return std::find(blacklist.begin(), blacklist.end(), ip) == blacklist.end();
        }

        std::mutex uploads_mtx;
        std::unordered_map<size_t, std::stringstream> uploads;
        static int request_handler(void * cls, struct MHD_Connection * connection,
                            const char * uri, const char * method, const char * version,
                            const char * upload_data, size_t * upload_data_size, size_t ** ptr) {
            httpd_gate *const n = static_cast<httpd_gate*>(cls);
            
            if(!is_ip_allowed(n, connection)) {
                auto response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
                return MHD_queue_response(connection, MHD_HTTP_FORBIDDEN, response);
            }

            std::string upload;
            if(std::string(method) == "POST") {
                std::unique_lock l(uploads_mtx);
                if(*ptr == nullptr) {
                    const auto &upload_id = uploads.size();
                    const auto &it = uploads.insert({upload_id, std::stringstream()});
                    *ptr = const_cast<size_t *>(&it.first->first);
                    
                    return MHD_YES;
                } else if(*upload_data_size) {
                    const auto &upload_id = **ptr;
                    const auto &it = uploads.find(upload_id);
                    auto &ss = it->second;

                    // if exceeding max upload size, cancel upload
                    if(ss.view().length() + *upload_data_size > _DATA_HTTPD_MAX_UPLOAD) {
                        uploads.erase(it);
                        auto *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
                        return MHD_queue_response (connection, MHD_HTTP_CONTENT_TOO_LARGE, response);
                    }

                    ss << std::string(upload_data, *upload_data_size);

                    *upload_data_size = 0;
                    return MHD_YES;
                } else {
                    const auto &upload_id = **ptr;
                    const auto &it = uploads.find(upload_id);
                    auto &ss = it->second;
                    ss << std::string(upload_data, *upload_data_size);
                    upload = ss.str();
                    uploads.erase(it);
                }
            }

            const auto result = n->handle(std::string(uri),
                   std::string(method),
                   upload);
            if(result.code == 404) {
                auto *response = MHD_create_response_from_buffer(0, nullptr, MHD_RESPMEM_PERSISTENT);
                return MHD_queue_response (connection, MHD_HTTP_NOT_FOUND, response);
            }

            auto *response = MHD_create_response_from_buffer(
                result.data.size(),
                (void *) result.data.c_str(),
                MHD_RESPMEM_MUST_COPY);
            
            for (auto const& [key, val] : result.headers)
                MHD_add_response_header(response, key.c_str(), val.c_str());

            return MHD_queue_response(connection, MHD_HTTP_OK, response);
        }

        void httpd_gate::listen() {
            if(this->config.certificate.has_value()) {
                    auto key_src = this->config.certificate->first;
                    auto cert_src = this->config.certificate->second;

                    // TODO limit interface by given address
                    // TODO tweak
                    this->daemon = MHD_start_daemon(MHD_USE_EPOLL_INTERNALLY | MHD_USE_SSL,
                                               this->config.port, nullptr, nullptr,
                                               reinterpret_cast<MHD_AccessHandlerCallback>(&request_handler),
                                               this, MHD_OPTION_END);

                } else {
                    // TODO limit interface by given address
                    // TODO tweak
                    this->daemon = MHD_start_daemon(MHD_USE_EPOLL_INTERNALLY,
                                               this->config.port, nullptr, nullptr,
                                               reinterpret_cast<MHD_AccessHandlerCallback>(&request_handler),
                                               this, MHD_OPTION_END);
                }
        }

        void httpd_gate::update_blacklist(std::vector<std::string> list) {
            std::unique_lock l(this->blacklist_mtx);
            this->blacklist = std::move(list);
        }

        const std::vector<std::string> httpd_gate::get_blacklist() const {
            std::shared_lock l(this->blacklist_mtx);
            return this->blacklist;
        }

        const std::string get_content_type(const fs::path path) {
            const auto ext = path.extension();
            // https://developer.mozilla.org/en-US/docs/Web/HTTP/MIME_types/Common_types
            if(ext == ".html" || ext == ".htm")
                return "text/html";
            else if(ext == ".xhtml")
                return "application/xhtml+xml";
            else if(ext == ".xml")
                return "application/xml";
            else if(ext == ".js" || ext == ".mjs")
                return "application/javascript";
            else if(ext == ".css")
                return "text/css";
            else if(ext == ".json")
                return "application/json";
            else if(ext == ".csv")
                return "text/csv";
            else if(ext == ".txt")
                return "text/plain";
            else if(ext == ".jpg" || ext == ".jpeg")
                return "image/jpeg";
            else if(ext == ".png")
                return "image/png";
            else if(ext == ".gif")
                return "image/gif";
            else if(ext == ".svg")
                return "image/svg+xml";
            else if(ext == ".webp")
                return "image/webp";
            else if(ext == ".ico")
                return "image/vnd.microsoft.icon";
            else if(ext == ".mpeg")
                return "video/mpeg";
            else if(ext == ".mp3")
                return "audio/mpeg";
            else if(ext == ".mp4")
                return "video/mp4";
            else if(ext == ".oga")
                return "audio/ogg";
            else if(ext == ".ogv")
                return "video/ogg";
            else if(ext == ".weba")
                return "audio/webm";
            else if(ext == ".webm")
                return "video/webm";
            else if(ext == ".pdf")
                return "application/pdf";
            else
                return "application/octet-stream";
        }
        
        std::shared_ptr<httpd_gate::session> httpd_gate::get_session(const std::string &uri) {
            TRACE(scope_httpd) << "get session '" << uri << "'";
            if(uri.length() == 2) {
                return this->create();
            } else {
                const std::string id = uri.substr(2);

                {   std::shared_lock l(this->serve_mtx);
                    const auto it = this->sessions.find(id);
                    if(it != this->sessions.cend())
                        return it->second;
                }

                return this->create();
            }
        }

        std::shared_ptr<httpd_gate::session> httpd_gate::create() {
            TRACE(scope_httpd) << "create session";
            const std::string chars(
                "abcdefghijklmnopqrstuvwxyz"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "1234567890"
            );
            std::uniform_int_distribution<> index_dist(0, chars.size() - 1);
            std::stringstream ss;
            for(int i = 0; i < _DATA_HTTPD_SESSION_SIZE; ++i) {
                ss << chars[index_dist(rng)];
            }
            const std::string id = ss.str();

            const auto source = std::shared_ptr<httpd_source>(new httpd_source(id, *this));
            const auto s = std::make_shared<session>(id, source, SessionMeta{.session = id});

            {   std::unique_lock l(this->serve_mtx);
                this->sessions[id] = s;

                if(!this->can_dispose) {
                    this->subscribe<DisposeSession>(httpd_gate::Dispose, this->shared_from_this());
                    this->can_dispose = true;
                }
            }

            this->creating(s);

            return s;
        }

        void httpd_gate::push(const std::string &id, const std::string &data) const {
            TRACE(scope_httpd) << "push session '" << id << "' " << data;
            std::shared_ptr<session> s;
            {   std::shared_lock l(this->serve_mtx);
                const auto it = this->sessions.find(id);
                if(it != this->sessions.cend())
                    s = it->second;
            }

            if(s) {
                s->queue.push(std::move(data));
            } else {
                TRACE_INFO(scope_httpd) << "session '" << id << "'" << " is gone";
            }
        }

        void httpd_gate::dispose(const std::string &id) {
            TRACE(scope_httpd) << "close session '" << id << "'";
            std::shared_ptr<session> s;
            {   std::unique_lock l(this->serve_mtx);
                const auto it = this->sessions.find(id);
                if(it != this->sessions.cend())
                    s = it->second;
            }

            if(s)
                this->disposing(s);
            else
                TRACE_ERROR(scope_httpd) << "trying to erase unknown session";
        }

        void httpd_gate::creating(const std::shared_ptr<session>& s) {
            TRACE(scope_httpd) << "creating session '" << s->id << "'";

            std::unique_lock l(s->mtx);
            s->meta.state = SessionState::Initilized;
            this->trigger(SessionCreated{.session=s->meta.session, .peer=s->meta.peer, .src=std::dynamic_pointer_cast<source<SessionMeta>>(s->source)});
        }

        void httpd_gate::disposing(const std::shared_ptr<session>& s) {
            TRACE(scope_httpd) << "closing session";

            std::unique_lock l(s->mtx);
            s->meta.state = SessionState::Closed;
            const auto it = this->sessions.find(s->meta.session);
            if(it != this->sessions.cend()) {
                this->sessions.erase(it);
            } else
                TRACE_WARNING(scope_httpd) << "closing non existant session '" << s->meta.session << "'";
            this->trigger(SessionDisposed{.session=s->meta.session, .peer=s->meta.peer});
        }

        void httpd_gate::Dispose(DisposeSession e, const std::shared_ptr<httpd_gate> g) {
            g->dispose(e.session);
        }

        const httpd_result httpd_gate::handle(const std::string uri, const std::string method, const std::string upload) {
            TRACE(scope_httpd) << "handle " << method << " " << uri;
            if(this->exposed && method == "GET" && !uri.starts_with("/_")) {
                std::string path = uri.empty() ? uri : uri.substr(1);

                if(path.empty())
                    path = "index.html";

                const auto data = this->exposed->get(path);
                if(data.empty())
                    return {.code=404};
                else
                    return {.data=data, .headers={{"Content-Type", get_content_type(path)}}};
            } else if(method == "GET" && uri.starts_with("/_")) {
                auto r = nlohmann::json::object();
                auto q = nlohmann::json::array();

                auto s = this->get_session(uri);
                
                {   std::unique_lock l(s->mtx);
                    r["session"] = s->id;

                    while(!s->queue.empty()) {
                        const auto msg = nlohmann::json::parse(s->queue.front());
                        q.push_back(msg);
                        s->queue.pop();
                    }
                }

                r["queue"] = q;

                return {r.dump()};
            } else if(method == "POST" && uri.starts_with("/_")) {
                TRACE(scope_httpd) << "POST BODY "<< upload;
                auto s = this->get_session(uri);
                auto res = nlohmann::json::parse(upload);
                if(res["session"] == s->id) {
                    for(const auto &msg : res["queue"])
                        s->source->pull(msg.dump(), s->meta);

                    return {.code=200};
                } else {
                    return {.code=406};
                }
            } else if(method == "DELETE" && uri.starts_with("/_")) {
                if(uri.length() > 2) {
                    auto s = this->get_session(uri);
                    this->dispose(s->id);
                    return {.code=200};
                } else
                    return {.code=404};
            } else
                return {.code=404};
        }

        httpd_source::httpd_source(const std::string session, httpd_gate &gate)
        : session(session), gate(gate) {}

        void httpd_source::push(const std::string& msg) const {
            this->gate.push(this->session, msg);
        }

        void httpd_source::push(std::string&& msg) const {
            this->gate.push(this->session, std::forward<std::string>(msg));
        }

        void httpd_source::dispose() {
            this->gate.dispose(this->session);
        }
    }
}