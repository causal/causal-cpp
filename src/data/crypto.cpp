// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/data/crypto.hpp"
#include "causal/trace.hpp"

#include <sstream>

#include <gnutls/crypto.h>
#include <gnutls/gnutls.h>
#include <string>

namespace causal {
    namespace data {
        gnutls_init* gnutls_init::instance = nullptr;
        std::once_flag gnutls_init::initFlag;
        gnutls_init::~gnutls_init() {
            gnutls_global_deinit();
        }

        void gnutls_init::init() {
            std::call_once(initFlag, &gnutls_init::_init);
        }
        
        gnutls_init::gnutls_init() {
            gnutls_global_init();
            TRACE(scope_data) << "GNU TLS library successfully initialized.";
        }
        
        void gnutls_init::_init() {
            gnutls_init::instance = new gnutls_init();
        }

        gnutls_sha_hasher::gnutls_sha_hasher() {
            gnutls_init::init();
        }

        std::string gnutls_sha_hasher::hash(const std::string& data, u_short hash_length) const {
            gnutls_digest_algorithm_t algorithm;

            if (hash_length >= 512) {
                algorithm = GNUTLS_DIG_SHA512;
            } else if (hash_length >= 384) {
                algorithm = GNUTLS_DIG_SHA384;
            } else if (hash_length >= 256) {
                algorithm = GNUTLS_DIG_SHA256;
            } else {
                throw sha_hash_lenght_invalid();
            }
            size_t digest_size = gnutls_hash_get_len(algorithm);

            unsigned char *digest = new unsigned char[digest_size];

            // Hash initialization
            gnutls_hash_hd_t handle;
            if (gnutls_hash_init(&handle, algorithm) < 0) {
                delete[] digest;
                throw gnutls_error("gnutls_hash_init failed");
            }

            // Input the data to be hashed
            if (gnutls_hash(handle, data.data(), data.length()) < 0) {
                gnutls_hash_deinit(handle, NULL);
                delete[] digest;
                throw gnutls_error("gnutls_hash failed");
            }

            // Output hash value
            gnutls_hash_output(handle, digest);

            // Free and deinitialize handle
            gnutls_hash_deinit(handle , NULL);

            // Convert binary hash value to hexadecimal string
            std::ostringstream oss;
            for (size_t i = 0; i < digest_size; i++)
                oss << static_cast<char>(digest[i]);

            delete[] digest;
            return oss.str();
        }

        // Generate a random Initialization Vector of length 16 bytes
        std::string generate_iv() {
            unsigned char iv[16]; // AES requires a 128-bit IV
            
            int ret = gnutls_rnd(GNUTLS_RND_NONCE, iv, sizeof(iv));
            if (ret < 0) {
                throw gnutls_error("Generating random nonce failed.");
            }

            return std::string((const char *)iv, sizeof(iv));
        }

        std::string gnutls_derrive_key_sha(const std::string& password, u_short key_length) {
            gnutls_mac_algorithm_t algorithm;

            if (key_length >= 512) {
                algorithm = GNUTLS_MAC_SHA512;
            } else if (key_length >= 384) {
                algorithm = GNUTLS_MAC_SHA384;
            } else if (key_length >= 256) {
                algorithm = GNUTLS_MAC_SHA256;
            } else {
                throw sha_hash_lenght_invalid();
            }
            
            // Choose suitable this->algo based on key length
            u_char derived_key[key_length / 8];
            const char *key = (const char *)(&derived_key[0]);
            void *digest = derived_key;

            gnutls_hmac_hd_t hmac_handle;
            
            int ret = gnutls_hmac_init(&hmac_handle, algorithm, (const u_char *)password.data(), password.size());
            if (ret < 0) {
                throw gnutls_error("Init HMAC failed.");
            }

            ret = gnutls_hmac(hmac_handle, nullptr, 0); // Use empty input as we just need the key derivation
            if (ret < 0) {
                throw gnutls_error("Computing HMAC failed.");
            }

            gnutls_hmac_output(hmac_handle, digest);
            
            gnutls_hmac_deinit(hmac_handle, digest);

            return std::string(key, key_length / 8);
        }

        gnutls_aes_cipher::gnutls_aes_cipher(const std::string& pw, const u_short key_length)
        : key(gnutls_derrive_key_sha(pw, key_length)), iv(generate_iv()),
          algo((key_length == 128) ? GNUTLS_CIPHER_AES_128_CBC :
               (key_length == 192) ? GNUTLS_CIPHER_AES_192_CBC :
               (key_length == 256) ? GNUTLS_CIPHER_AES_256_CBC : GNUTLS_CIPHER_UNKNOWN),
          key_length(key_length) {
            if (this->algo == GNUTLS_CIPHER_UNKNOWN) {
                throw gnutls_error("Unsupported key length for AES encryption.");
            }
            gnutls_init::init();
        }
        
        const hasher& gnutls_aes_cipher::get_hasher() const {
            static const gnutls_sha_hasher h;
            return h;
        }

        const std::string gnutls_aes_cipher::get_token() const {
            return this->get_hasher().hash(this->key, this->key_length);
        }

        // Padding function for PKCS#7
        std::string pkcs7_pad(const std::string &data, const unsigned int block_size = 16) {
            // Determine the number of padding bytes needed
            size_t padding_length = (block_size - (data.size() % block_size));
            
            // Create a new string that is a copy of data and appends the required padding bytes
            std::string padded_data = data;
            for(unsigned int i=0; i<padding_length; ++i){
                padded_data.push_back(static_cast<char>(padding_length));
            }
            return padded_data;
        }
        
        // Unpadding function for PKCS#7
        std::string pkcs7_unpad(const std::string &data, const unsigned int block_size = 16) {
            // Check that the data is a multiple of the block size
            if(data.size() % block_size != 0){
                throw gnutls_error("Data length is not a multiple of the block size.");
            }
            
            // Get the padding byte from the end of the string
            unsigned char padding_length = static_cast<unsigned char>(data.back());
            
            // Validate the padding length (must be within range)
            if(padding_length > data.size() || padding_length == 0){
                throw gnutls_error("Invalid PKCS#7 padding.");
            }
            
            // Remove the padding bytes
            return data.substr(0, data.size() - padding_length);
        }

        std::string gnutls_aes_cipher::encrypt(const std::string& data) const {
            // Create a cipher handle using the desired key length for AES encryption
            gnutls_cipher_hd_t cipher_handle;

            gnutls_datum_t _key{.data = (u_char *)this->key.data(), .size = static_cast<u_int>(this->key.size())};
            gnutls_datum_t _iv{.data = (u_char *)this->iv.data(), .size = static_cast<u_int>(this->iv.size())};
            
            try {
                //TRACE(scope_data) << "encrypt->data: " << std::to_string(data.size()) << " | " << data;
                int ret = gnutls_cipher_init(&cipher_handle, this->algo, &_key, &_iv);
                if (ret < 0) {
                    throw gnutls_error("Error initializing cipher handle.");
                }

                // Pad the data to a multiple of the block size (16 bytes for AES)
                const std::string padded_data = pkcs7_pad(data);
                const size_t padded_data_length = padded_data.size();

                //TRACE(scope_data) << "encrypt->padded_data: " << std::to_string(padded_data.size()) << " | " << padded_data;

                // Allocate memory for the ciphertext
                // Insert the IV at the beginning of the ciphertext
                std::string ciphertext;
                ciphertext.resize(padded_data_length);

                // Encrypt the data
                ret = gnutls_cipher_encrypt2(cipher_handle, (const u_char *)padded_data.data(),
                                            padded_data_length, (u_char *)ciphertext.data(), padded_data_length);
                if (ret < 0) {
                    throw gnutls_error("Error during encryption: "+std::to_string(ret));
                }

                //TRACE(scope_data) << "encrypt->ciphertext: " << std::to_string(ciphertext.size()) << " | " << ciphertext;
                
                const auto encrypted_data = this->iv + ciphertext;

                //TRACE(scope_data) << "encrypt->encrypted_data: " << std::to_string(encrypted_data.size()) << " | " << encrypted_data;

                // Clean up resources
                gnutls_cipher_deinit(cipher_handle);

                return encrypted_data;
            } catch (...) {
                gnutls_cipher_deinit(cipher_handle);
                throw;  // Re-throw with original exception message.
            }
        }

        std::string gnutls_aes_cipher::decrypt(const std::string& encrypted_data) const {
            gnutls_cipher_hd_t cipher_handle;

            try {
                //TRACE(scope_data) << "decrypt->encrypted_data: " << std::to_string(encrypted_data.size()) << " | " << encrypted_data;
                // 1. Extract IV from prefix of ciphertext
                if(encrypted_data.size() < this->iv.size()) { throw gnutls_error("Ciphertext must include an IV."); }

                gnutls_datum_t _key{.data = (u_char *)this->key.data(), .size = static_cast<u_int>(this->key.size())};
                const std::string iv_str = encrypted_data.substr(0, this->iv.size());
                const gnutls_datum_t _iv = {.data = (u_char *)iv_str.data(), .size = static_cast<u_int>(iv_str.size())};
                
                // 3. Set up the Cipher handle for AES decryption
                int ret = gnutls_cipher_init(&cipher_handle, this->algo, &_key, &_iv);
                if (ret < 0) {
                    throw gnutls_error("Error initializing cipher handle.");
                }

                const auto ciphertext = encrypted_data.substr(_iv.size);

                //TRACE(scope_data) << "decrypt->ciphertext: " << std::to_string(ciphertext.size()) << " | " << ciphertext;

                const size_t padded_data_length = ciphertext.size();
                std::string padded_data;
                padded_data.resize(padded_data_length);

                ret = gnutls_cipher_decrypt2(cipher_handle,
                                        (const u_char *)ciphertext.data(), 
                                        padded_data_length,
                                        (u_char *)padded_data.data(),
                                        padded_data_length);
                if (ret < 0) {
                    throw gnutls_error("AES decryption failed.");
                }

                //TRACE(scope_data) << "decrypt->padded_data: " << std::to_string(padded_data.size()) << " | " << padded_data;

                const auto data = pkcs7_unpad(padded_data);

                //TRACE(scope_data) << "decrypt->data: " << std::to_string(data.size()) << " | " << data;

                // Clean up handle after use
                gnutls_cipher_deinit(cipher_handle);

                return data;
            } catch (const std::exception& e) {
                gnutls_cipher_deinit(cipher_handle);
                throw;  // Re-throw with original exception message.
            }
        }
    }
}