// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include <functional>

#include "causal/data/msgpack.hpp"

namespace causal {
    namespace data {
        INIT_TRACE_SCOPE(scope_msgpack, "msgpack")

        thread_local msgpack_space* msgpack_space::active = nullptr;

        msgpack_space::msgpack_space(const std::shared_ptr<junction> j) : junction_space(std::move(j)) {}


        std::shared_ptr<msgpack_space> msgpack_space::make(const std::shared_ptr<junction> j) {
            return std::shared_ptr<msgpack_space>(new msgpack_space(j));
        }

        void msgpack_space::focus(const std::any id, const void* const key) noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                this->junc->attract(*this, sid);
                std::unique_lock l(this->mtx);
                auto& m = this->aspects.at(sid);
                m.focus_keys.insert(key);
            }
        }

        void msgpack_space::release(const std::any id, const void* const key) noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                std::unique_lock l(this->mtx);
                auto& m = this->aspects.at(sid);
                auto e = m.ptr.lock();
                if(!m.focus_keys.empty() &&
                !m.borrow_key && m.const_borrow_keys.empty()) {
                    m.focus_keys.erase(key);
                    if(m.focus_keys.empty()) {
                        e->push(this);
                        this->junc->repel(*this, sid);
                        m.attracted = false;
                    }
                }
            }
        }

        bool msgpack_space::is_focused(const std::any id) const noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                std::unique_lock l(this->mtx);
                return !this->aspects.at(sid).focus_keys.empty();
            } else
                return false;
        }

        bool msgpack_space::borrow(const std::any id, const void* const key) noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                std::unique_lock l(this->mtx);
                auto& m = this->aspects.at(sid);
                if(m.attracted) {
                    ASSERT_MSG(scope_msgpack, !m.borrow_key, "aspect already borrowed mutably")
                    m.borrow_key = key;
                    return true;
                } else
                    return false;
            } else
                return false;
        }

        bool msgpack_space::const_borrow(const std::any id, const void* const key) noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                std::unique_lock l(this->mtx);
                auto& m = this->aspects.at(sid);
                if(m.attracted) {
                    m.const_borrow_keys.insert(key);
                    return true;
                } else
                    return false;
            } else
                return false;
        }

        void msgpack_space::unborrow(const std::any id, const void* const key, const bool abort) noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                std::unique_lock l(this->mtx);
                auto& m = this->aspects.at(sid);
                if(m.borrow_key == key)
                    m.borrow_key = nullptr;
                else if(!m.const_borrow_keys.empty())
                    m.const_borrow_keys.erase(key);
            }
        }

        bool msgpack_space::is_borrowed(const std::any id) const noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                std::unique_lock l(this->mtx);
                return this->aspects.at(sid).borrow_key;
            } else
                return false;
        }

        bool msgpack_space::is_const_borrowed(const std::any id) const noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                std::unique_lock l(this->mtx);
                return !this->aspects.at(sid).const_borrow_keys.empty();
            } else
                return false;
        }

        void msgpack_space::dispose(const std::any id) noexcept {
            if(id.type() == typeid(std::string)) {
                auto sid = std::any_cast<std::string>(id);
                this->junc->erase(sid);
                this->junc->erase(sid+TYPE_POSTFIX);
            }
        }

        void msgpack_space::push(std::string type, std::any id, void* ptr) noexcept {
            if(id.type() == typeid(std::string)) {
                auto it = this->type_registry.find(type);
                if(it != this->type_registry.end()) {
                    auto sid = std::any_cast<std::string>(id);
                    it->second.serialize(sid, ptr);
                }
            }
        }

        void* const msgpack_space::pull(std::string type, std::any id) noexcept {
            if(id.type() == typeid(std::string)) {
                auto it = this->type_registry.find(type);
                if(it != this->type_registry.end()) {
                    auto sid = std::any_cast<std::string>(id);
                    return it->second.deserialize(sid);
                }
            }
            
            return nullptr;
        }

        void msgpack_space::attracted(const std::string id) noexcept {
            std::unique_lock l(this->mtx);
            auto& m = this->aspects.at(id);
            auto e = m.ptr.lock();
            m.attracted = true;
            e->pull(this);
        }

        cc::essence_ptr<> msgpack_space::clone(const void* const key, cc::essence_ptr<> ptr, std::any id) {
            if(id.has_value()) {
                if(id.type() == typeid(std::string))
                    return ptr->clone(key, id, this->shared_from_this());
                else
                    INTERN_THROW_ERROR(scope_data, cc::incompatible_id())
                
            } else
                return ptr->clone(key, boost::uuids::to_string(boost::uuids::random_generator()()), this->shared_from_this());
        }
    }
}