// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/ui/gtk.hpp"

#include <chrono>
#include <thread>

using namespace Glib;
using namespace Gtk;

namespace causal {
    namespace ui {
        gtk::gtk(std::string domain) {
            this->app = Application::create(domain);
        }

        std::shared_ptr<gtk> gtk::make(std::string domain) {
            return std::shared_ptr<gtk>(new gtk(domain));
        }

        bool gtk::on_tick() {
            while(this->do_tick());
            return true;
        }

        bool gtk::is_ui_thread() {
            return this->ui_thread == std::this_thread::get_id();
        }

        void gtk::quit() {
            this->app->quit();
        }

        int gtk::run(Window& w, int& argc, char** argv) {
            Glib::signal_timeout().connect(sigc::mem_fun(*this, &gtk::on_tick), 10);
            this->ui_thread = std::this_thread::get_id();
            return this->app->run(w, argc, argv);
        }
    }
}