// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/ui/qtquick.hpp"

#include <chrono>
#include <memory>

namespace causal {
    namespace ui {
        qtquick::qtquick(int& argc, char** argv) : app(argc, argv) {
            this->app.connect(&this->tickTimer, &QTimer::timeout, [this] {while(this->do_tick());});
        }

        std::shared_ptr<qtquick> qtquick::make(int& argc, char** argv) {
            return std::shared_ptr<qtquick>(new qtquick(argc, argv));
        }

        void qtquick::load(std::string uri) {
            this->engine.load(QUrl(QString::fromStdString(uri)));
        }

        bool qtquick::is_ui_thread() {
            return this->ui_thread == std::this_thread::get_id();
        }

        void qtquick::quit() {
            this->app.quit();
        }

        int qtquick::run() {
            this->ui_thread = std::this_thread::get_id();

            this->tickTimer.start(10);

            auto ret = this->app.exec();
            this->tickTimer.stop();
            while(this->do_tick());
            return ret;
        }
    }
}