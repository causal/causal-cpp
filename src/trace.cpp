// Licensed under the terms of the AGPL-3.0-only (GNU AFFERO GENERAL PUBLIC LICENSE version 3)
// Copyright (C) 2018-2024 by Ralph Alexander Bariz

#include "causal/trace.hpp"

#ifdef WITH_STACKTRACE
#include <boost/stacktrace.hpp>
#endif

namespace trace {
#ifdef WITH_STACKTRACE
    std::string stacktrace(size_t skip) {
        return boost::stacktrace::to_string(boost::stacktrace::stacktrace(skip+1, 50));
    }
#endif

    exception::exception(std::string msg) : msg(msg), stacktrace(trace::stacktrace(2)) {}

    const char* exception::what() const noexcept { return this->msg.c_str(); }

    void log(trace::level l, std::string msg) {
        switch(l) {
        case trace::level::TRACE:
            BOOST_LOG_TRIVIAL(trace) << msg;
            break;
        case trace::level::INFO:
            BOOST_LOG_TRIVIAL(info) << msg;
            break;
        case trace::level::WARNING:
            BOOST_LOG_TRIVIAL(warning) << msg;
            break;
        case trace::level::ERROR:
            BOOST_LOG_TRIVIAL(error) << msg;
            break;
        case trace::level::FATAL:
            BOOST_LOG_TRIVIAL(fatal) << msg;
            break;
        }
    }
}

namespace causal {
    namespace core {
        INIT_TRACE_SCOPE(scope_core, "core")
        INIT_TRACE_SCOPE(scope_action, "action")
    }
    namespace sync {INIT_TRACE_SCOPE(scope_sync, "sync")}
    namespace process {INIT_TRACE_SCOPE(scope_process, "process")}
    namespace data {INIT_TRACE_SCOPE(scope_data, "data")}
    namespace facet {INIT_TRACE_SCOPE(scope_facet, "facet")}
    namespace ui {INIT_TRACE_SCOPE(scope_ui, "ui")}
}
// main, json, lmdb, opendht, msgpack
INIT_TRACE_SCOPE(scope_main, "main")