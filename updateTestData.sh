#!/bin/bash

ROOTDIR=`pwd`

mkdir -p ${ROOTDIR}/test/_data

rm -fr ${ROOTDIR}/test/_data/archive.ics.uci.edu/ml/machine-learning-databases/faces-mld
cd ${ROOTDIR}/test/_data
wget -r -np http://archive.ics.uci.edu/ml/machine-learning-databases/faces-mld/
rm -fr ${ROOTDIR}/test/_data/archive.ics.uci.edu/ml/machine-learning-databases/faces-mld/index.html\?*

rm -fr ${ROOTDIR}/test/_data/yann.lecun.com/exdb/mnist
cd ${ROOTDIR}/test/_data
wget -r -np http://yann.lecun.com/exdb/mnist/

cd ${ROOTDIR}