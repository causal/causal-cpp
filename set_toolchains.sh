BIN_GCC=/usr/bin/gcc-10
BIN_GPP=/usr/bin/g++-10
BIN_CLANG=/usr/bin/clang-11
BIN_CLANGPP=/usr/bin/clang++-11

BIN_GCC_GENERIC=/usr/bin/gcc
BIN_GPP_GENERIC=/usr/bin/g++
BIN_CLANG_GENERIC=/usr/bin/clang
BIN_CLANGPP_GENERIC=/usr/bin/clang++

if ! command -v ${BIN_GCC} &> /dev/null
then
BIN_GCC=$BIN_GCC_GENERIC
fi

if ! command -v ${BIN_GPP} &> /dev/null
then
BIN_GPP=$BIN_GPP_GENERIC
fi

if ! command -v ${BIN_CLANG} &> /dev/null
then
BIN_CLANG=$BIN_CLANG_GENERIC
fi

if ! command -v ${BIN_CLANGPP} &> /dev/null
then
BIN_CLANGPP=$BIN_CLANGPP_GENERIC
fi