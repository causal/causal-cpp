#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

set -e

if [ -z "$ANDROID_NDK" -o -z "$ANDROID_SDK" ]; then
   echo "You must define ANDROID_NDK, ANDROID_SDK and ANDROID_ABI before starting."
   echo "They must point to your NDK and SDK directories."
   exit 1
fi
if [ -z "$ANDROID_ABI" ]; then
   echo "Please set ANDROID_ABI to your architecture: armeabi-v7a, x86."
   exit 1
fi

platform=$(echo "`uname`" | tr '[:upper:]' '[:lower:]')
arch=`uname -m`

ANDROID_TOPLEVEL_DIR="`pwd`"
ANDROID_APP_DIR="${ANDROID_TOPLEVEL_DIR}/ring-android"

# Set up ABI variables
if [ ${ANDROID_ABI} = "x86" ] ; then
    TARGET="i686-linux-android"
    PJ_TARGET="i686-pc-linux-android"
    PLATFORM_SHORT_ARCH="x86"
elif [ ${ANDROID_ABI} = "x86_64" ] ; then
    TARGET="x86_64-linux-android"
    PJ_TARGET="x86_64-pc-linux-android"
    PLATFORM_SHORT_ARCH="x86_64"
elif [ ${ANDROID_ABI} = "arm64-v8a" ] ; then
    TARGET="aarch64-linux-android"
    PJ_TARGET="aarch64-unknown-linux-android"
    PLATFORM_SHORT_ARCH="arm64"
else
    TARGET_CC="armv7a-linux-androideabi"
    TARGET="arm-linux-androideabi"
    PJ_TARGET="arm-unknown-linux-androideabi"
    PLATFORM_SHORT_ARCH="arm"
fi
TARGET_CC=${TARGET_CC:-$TARGET}

export API=23
export ANDROID_API=android-$API
export TOOLCHAIN=$ANDROID_NDK/toolchains/llvm/prebuilt/$platform-$arch
export TARGET

# Make in //
MAKEFLAGS=
if which nproc >/dev/null; then
MAKEFLAGS=-j`nproc`
elif [ "$platform" == "darwin" ] && which sysctl >/dev/null; then
MAKEFLAGS=-j`sysctl -n machdep.cpu.thread_count`
fi
# Setup cross-compilation build environemnt
export AR=$TOOLCHAIN/bin/llvm-ar
export AS=$TOOLCHAIN/bin/$TARGET-as
export CC=$TOOLCHAIN/bin/$TARGET_CC$API-clang
export CXX=$TOOLCHAIN/bin/$TARGET_CC$API-clang++
export LD=$TOOLCHAIN/bin/ld
export RANLIB=$TOOLCHAIN/bin/llvm-ranlib
export STRIP=$TOOLCHAIN/bin/llvm-strip

FLAGS_COMMON="-fPIC -g"
EXTRA_CFLAGS="${EXTRA_CFLAGS} ${FLAGS_COMMON}"
EXTRA_CXXFLAGS="${EXTRA_CXXFLAGS} ${FLAGS_COMMON}"
EXTRA_LDFLAGS="${EXTRA_LDFLAGS} -g"

############
# Contribs #
############
echo "Building the contribs"
CONTRIB_DIR=${DIR}/contrib/build_android-${TARGET}
CONTRIB_SYSROOT=${DIR}/contrib/${TARGET}
mkdir -p ${CONTRIB_DIR}
mkdir -p ${CONTRIB_SYSROOT}/lib/pkgconfig

cd ${CONTRIB_DIR}
../bootstrap --host=${TARGET} #--enable-lmdb

make list
make fetch
export PATH="$PATH:$CONTRIB_SYSROOT/bin"
make $MAKEFLAGS