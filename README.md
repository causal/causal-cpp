# causal runtime

## LICENSE
[GNU AFFERO GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/agpl-3.0.en.html)

Causal is a runtime written in C++ dedicated to make aspects of information interact in frame of a selfsynchronizing non linear system.

[causal-rt.org](http://causal-rt.org)

## Get
```bash
git clone https://git.ralph.or.at/causal/causal-cpp.git
cd causal-cpp
```

## For building examples please see [CI/CD](.gitlab-ci.yml)

## Compiler Notes
GCC >= 12 and Clang >= 14 are supported.
